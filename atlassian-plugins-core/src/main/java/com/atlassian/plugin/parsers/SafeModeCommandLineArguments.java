package com.atlassian.plugin.parsers;

import com.atlassian.annotations.nonnull.ReturnValuesAreNonnullByDefault;
import io.atlassian.util.concurrent.LazyReference;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.Arrays.asList;


/**
 * This is used by the application this library is bundled with, it shouldn't be changed without also changing all the products it is bundled with.
 */
@ParametersAreNonnullByDefault
@ReturnValuesAreNonnullByDefault
public class SafeModeCommandLineArguments {
    public static final String PLUGIN_LIST_SEPARATOR = ":";
    private static final String DISABLE_NON_SYSTEM_LINUX = "--disable-all-addons";
    private static final String DISABLE_NON_SYSTEM_WINDOWS = "/disablealladdons";
    private static final String DISABLE_LAST_ENABLED_LINUX = "--disable-last-enabled";
    private static final String DISABLE_LAST_ENABLED_WINDOWS = "/disablelastenabled";

    //needs to handle case when people put it in quotes
    //matches --disable-addons="my.cool.plugin"
    private static final Pattern DISABLE_ADDONS_ARGUMENT_PATTERN = Pattern.compile(".*disable-?addons=([^\\s]+).*");

    //matches --disable-addons="my cool plugin"
    private static final Pattern QUOTED_DISABLE_ADDONS_ARGUMENT_PATTERN = Pattern.compile(".*disable-?addons=\"([^\"].+)\".*$");

    //matches --disable-addons (exists for analytics)
    private static final Pattern DISABLE_ADDONS_PRESENT_PATTERN = Pattern.compile(".*disable-?addons.*");

    private final String commandLineArguments;
    private final LazyReference<Boolean> safeMode = new LazyReference<Boolean>() {
        @Override
        protected Boolean create() {
            return commandLineArguments.contains(getDisableNonSystemWindows()) ||
                    commandLineArguments.contains(getDisableNonSystemLinux());
        }
    };

    private final LazyReference<Boolean> lastEnabledDisabled = new LazyReference<Boolean>() {
        @Override
        protected Boolean create() {
            return commandLineArguments.contains(getDisableLastEnabledLinux()) ||
                    commandLineArguments.contains(getDisableLastEnabledWindows());
        }
    };

    /**
     * The reason we're returning an optional of a list, instead of returning an empty list, is that we want to
     * distinguish the case where the user supplied no addons to the disable-addons parameter from the case where
     * they didn't supply the parameter at all
     *
     */
    private final LazyReference<Optional<List<String>>> disabledPlugins = new LazyReference<Optional<List<String>>>() {
        @Override
        protected Optional<List<String>> create() {

            Matcher disableAddonsArgumentMatcher = DISABLE_ADDONS_ARGUMENT_PATTERN.matcher(commandLineArguments);
            Matcher quotedDisableAddonsArgumentMatcher = QUOTED_DISABLE_ADDONS_ARGUMENT_PATTERN.matcher(commandLineArguments);
            Matcher disableAddonsPresentMatcher = DISABLE_ADDONS_PRESENT_PATTERN.matcher(commandLineArguments);

            if (quotedDisableAddonsArgumentMatcher.matches()) {
                return Optional.of(Collections.unmodifiableList(asList(quotedDisableAddonsArgumentMatcher.group(1)
                        .split(getPluginListSeparator()))));
            } else if (disableAddonsArgumentMatcher.matches()) {
                return Optional.of(Collections.unmodifiableList(asList(disableAddonsArgumentMatcher.group(1).split(getPluginListSeparator()))));
            } else if (disableAddonsPresentMatcher.matches()){
               return Optional.of(Collections.emptyList());
            } else {
                return Optional.empty();
            }
        }
    };

    public SafeModeCommandLineArguments(final String commandLineArguments) {
        this.commandLineArguments = commandLineArguments;
    }

    public static String getDisableLastEnabledLinux() {
        return DISABLE_LAST_ENABLED_LINUX;
    }

    public static String getDisableLastEnabledWindows() {
        return DISABLE_LAST_ENABLED_WINDOWS;
    }

    static String getPluginListSeparator() {
        return PLUGIN_LIST_SEPARATOR;
    }

    static String getDisableNonSystemLinux() {
        return DISABLE_NON_SYSTEM_LINUX;
    }

    static String getDisableNonSystemWindows() {
        return DISABLE_NON_SYSTEM_WINDOWS;
    }

    public String getSafeModeArguments() {
        return commandLineArguments;
    }

    /**
     * Used for analytics
     *
     * @return all plugins that have been specified using the parameter --disable-addons
     */
    public Optional<List<String>> getDisabledPlugins() {
        return disabledPlugins.get();
    }

    @SuppressWarnings("ConstantConditions")
    public boolean isSafeMode() {
        return safeMode.get();
    }

    @SuppressWarnings("ConstantConditions")
    public boolean shouldLastEnabledBeDisabled() {
        return lastEnabledDisabled.get();
    }

    @SuppressWarnings("ConstantConditions")
    public boolean isDisabledByParam(String pluginKey) {
        return getDisabledPlugins().map(plugins -> plugins.contains(pluginKey)).orElse(false);
    }
}
