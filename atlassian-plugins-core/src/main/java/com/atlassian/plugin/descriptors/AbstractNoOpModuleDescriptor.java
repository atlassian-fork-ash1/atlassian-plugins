package com.atlassian.plugin.descriptors;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.module.ModuleFactory;

abstract class AbstractNoOpModuleDescriptor<T> extends AbstractModuleDescriptor<T> {
    private String errorText;

    protected AbstractNoOpModuleDescriptor() {
        super(new ModuleFactory() {
            @Override
            public <T> T createModule(String name, ModuleDescriptor<T> moduleDescriptor) throws PluginParseException {
                throw new IllegalStateException("The module is either unloadable or unrecognised, in any case this shouldn't be called!");
            }
        });
    }

    public final String getErrorText() {
        return errorText;
    }

    public final void setErrorText(final String errorText) {
        this.errorText = errorText;
    }

    @Override
    public final T getModule() {
        return null;
    }

    @Override
    public final boolean isEnabledByDefault() {
        return false;
    }

    /**
     * Sets the key of the ModuleDescriptor
     *
     * This is theoretically bad, as the superclass and the interface doesn't define this method,
     * but it's required to construct an module descriptor when we don't have the XML Element.
     *
     * @param key the key of the ModuleDescriptor
     */
    public final void setKey(final String key) {
        this.key = key;
    }

    /**
     * Sets the name of the ModuleDescriptor
     *
     * This is theoretically bad, as the superclass and the interface doesn't define this method,
     * but it's required to construct an module descriptor when we don't have the XML Element.
     *
     * @param name the name of the ModuleDescriptor
     */
    public final void setName(final String name) {
        this.name = name;
    }
}
