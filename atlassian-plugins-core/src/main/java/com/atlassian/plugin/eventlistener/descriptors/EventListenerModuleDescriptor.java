package com.atlassian.plugin.eventlistener.descriptors;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.Permissions;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.RequirePermission;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;
import io.atlassian.util.concurrent.LazyReference;
import org.dom4j.Element;

import javax.annotation.Nonnull;

@RequirePermission(Permissions.EXECUTE_JAVA)
public class EventListenerModuleDescriptor extends AbstractModuleDescriptor<Object> {

    /**
     * This is fallback property in case if registration by module descriptor somehow failed to work.
     * <p>
     * It will restore old behaviour when registration happening by module.
     */
    static final String FALLBACK_MODE = "EventListenerModuleDescriptor.Fallback.Mode";

    private final EventPublisher eventPublisher;

    private LazyReference<Object> moduleObj = new LazyReference<Object>() {
        @Override
        protected Object create() throws Exception {
            return moduleFactory.createModule(moduleClassName, EventListenerModuleDescriptor.this);
        }
    };

    public EventListenerModuleDescriptor(ModuleFactory moduleFactory, EventPublisher eventPublisher) {
        super(moduleFactory);
        this.eventPublisher = eventPublisher;
    }

    @Override
    public void init(@Nonnull Plugin plugin, @Nonnull Element element) throws PluginParseException {
        super.init(plugin, element);
        checkPermissions();
    }

    @Override
    public Object getModule() {
        return moduleObj.get();
    }

    @Override
    public void enabled() {
        super.enabled();

        if (Boolean.getBoolean(FALLBACK_MODE)) {
            eventPublisher.register(getModule());
        } else {
            eventPublisher.register(this);
        }
    }

    public void disabled() {
        if (Boolean.getBoolean(FALLBACK_MODE)) {
            eventPublisher.unregister(getModule());
        } else {
            eventPublisher.unregister(this);
        }

        super.disabled();
    }
}