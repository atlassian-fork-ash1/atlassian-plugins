package com.atlassian.plugin.jmx;

import com.google.common.annotations.VisibleForTesting;

import javax.management.ObjectName;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * An base class for managing registration and unregistration of a JMX MXBean.
 * <p>
 * This implementation keeps a weak reference to the underlying implementation so that failing to unregister from the platform MBean
 * server does not leak implementation instances. In the event of usage after the implementation is garbage collected, an {@link
 * IllegalStateException} is thrown.
 * <p>
 * The idiomatic usage of this class is to subclass, pass the MXBean interface type as parameter, directly implement the MXBean
 * using a strong reference to the required system components, and override {@link AbstractJmxBridge#getMXBean} to return this. The
 * {@link AbstractJmxBridge#register()} method will construct a proxy which weakly references this implementation.
 *
 * @since v3.0.24
 */
public abstract class AbstractJmxBridge<MXBean> {
    private final ObjectName objectName;
    private final Class<MXBean> mxBeanClass;

    /**
     * Construct given JMX ObjectName and interface class.
     *
     * @param objectName  the ObjectName to register under.
     * @param mxBeanClass the interface class for the MXBean.
     */
    public AbstractJmxBridge(final ObjectName objectName, final Class<MXBean> mxBeanClass) {
        this.objectName = objectName;
        this.mxBeanClass = mxBeanClass;
    }

    /**
     * Obtain the actual MXBean implementation.
     * <p>
     * The return value is weakly held.
     *
     * @return The implementation of the MXBean to expose via JMX.
     */
    protected abstract MXBean getMXBean();

    /**
     * Register the MXBean with the platform MBean server.
     */
    public void register() {
        registerInternal();
    }


    public ObjectName getObjectName() {
        return objectName;
    }

    @VisibleForTesting
    WeakMXBeanInvocationHandler<MXBean> registerInternal() {
        final WeakMXBeanInvocationHandler<MXBean> handler = new WeakMXBeanInvocationHandler<MXBean>(objectName, getMXBean());
        final Object proxy = Proxy.newProxyInstance(mxBeanClass.getClassLoader(), new Class[]{mxBeanClass}, handler);
        JmxUtil.register(proxy, objectName);
        return handler;
    }

    /**
     * Unregister the MXBean from the platform MBean server.
     */
    public void unregister() {
        JmxUtil.unregister(objectName);
    }

    /**
     * An InvocationHandler which weakly holds an MXBean and forwards requests to it, defaulting if it is discarded.
     * <p>
     * This inner class is static, and must not retain references to the outer class, either explicit or implicit. Instances of
     * {@link Proxy} which hold this InvocationHandler are registered with JMX, and the weak reference logic only works with the
     * simple idiomatic usage of {@link AbstractJmxBridge} if we do not hold outer references.
     */
    @VisibleForTesting
    static class WeakMXBeanInvocationHandler<MXBean> implements InvocationHandler {
        private final ObjectName objectName;
        private final WeakReference<MXBean> implementationReference;

        /**
         * Construct given implementation.
         * <p>
         * The implementation is weakly held so that registration of this as an MXBean does not stop garbage collection.
         *
         * @param objectName     the object name of the MXBean which is used in exception messages.
         * @param implementation the MXBean implementation to forward calls to, weakly held.
         */
        public WeakMXBeanInvocationHandler(final ObjectName objectName, final MXBean implementation) {
            this.objectName = objectName;
            this.implementationReference = new WeakReference<>(implementation);
        }

        @VisibleForTesting
        WeakReference<MXBean> getImplementationReference() {
            return implementationReference;
        }

        @Override
        public Object invoke(final Object proxy, final Method method, final Object[] args) throws Throwable {
            final MXBean implementation = implementationReference.get();
            if (null == implementation) {
                // We want to unregister in case our interface classes come from classloaders that need collection.
                JmxUtil.unregister(objectName);
                throw new IllegalStateException("Cannot use stale MXBean '" + objectName + "'");
            } else {
                return method.invoke(implementation, args);
            }
        }
    }
}
