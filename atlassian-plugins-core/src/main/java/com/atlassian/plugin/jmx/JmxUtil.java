package com.atlassian.plugin.jmx;

import com.google.common.collect.ImmutableMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanRegistrationException;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectInstance;
import javax.management.ObjectName;
import java.lang.management.ManagementFactory;
import java.util.Hashtable;
import java.util.concurrent.atomic.AtomicInteger;

public class JmxUtil {
    private static final Logger log = LoggerFactory.getLogger(JmxUtil.class);

    private static final String DOMAIN = "com.atlassian.plugin";

    /**
     * Obtain a JMX ObjectName for a new instance of a given type.
     *
     * @param counter a counter used to ensure a unique instance id amongst instances with given type.
     * @param type    the friendly type name for the instance.
     * @return a JMX ObjectName for the instance, or null if creation fails.
     */
    public static ObjectName objectName(final AtomicInteger counter, final String type) {
        try {
            final String instance = Integer.toString(counter.getAndIncrement());
            return new ObjectName(DOMAIN, new Hashtable<String, String>(ImmutableMap.of("instance", instance, "type", type)));
        } catch (final MalformedObjectNameException emon) {
            log.warn("Failed to create ObjectName: ", emon.getMessage());
        }
        return null;
    }

    /**
     * Register a JMX MBean against a given ObjectName.
     * <p>
     * This is simply an error logging wrapper around {@link javax.management.MBeanServer#registerMBean}.
     *
     * @param object     the MBean implementation to register.
     * @param objectName the ObjectName to register against.
     * @return the ObjectInstance encapsulating the registration, or null if registration failed.
     */
    public static ObjectInstance register(final Object object, final ObjectName objectName) {
        try {
            if (null != objectName) {
                return ManagementFactory.getPlatformMBeanServer().registerMBean(object, objectName);
            } else {
                log.warn("Failed to register, objectName null");
            }
        } catch (final InstanceAlreadyExistsException eiae) {
            log.warn("Failed to register, instance already exists: ", eiae.getMessage());
        } catch (final MBeanRegistrationException emr) {
            log.warn("Failed to register, registration exception: ", emr.getMessage());
        } catch (final NotCompliantMBeanException encm) {
            log.warn("Failed to register, not compliant: ", encm.getMessage());
        }
        return null;
    }

    /**
     * Unregister the JMX MBean with a given ObjectName.
     * <p>
     * This is simply an error logging wrapper around {@link javax.management.MBeanServer#unregisterMBean}.
     *
     * @param objectName the ObjectName of the MBean to unregister.
     * @return true if unregister succeeded, or false if unregister failed.
     */
    public static boolean unregister(final ObjectName objectName) {
        try {
            ManagementFactory.getPlatformMBeanServer().unregisterMBean(objectName);
            return true;
        } catch (final InstanceNotFoundException einf) {
            log.warn("Failed to unregister, instance not found: ", einf.getMessage());
        } catch (final MBeanRegistrationException emr) {
            log.warn("Failed to unregister, registration exception: ", emr.getMessage());
        }
        return false;
    }
}
