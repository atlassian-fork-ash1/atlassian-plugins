package com.atlassian.plugin.loaders;

import com.atlassian.plugin.DefaultPluginArtifactFactory;
import com.atlassian.plugin.PluginArtifactFactory;
import com.atlassian.plugin.ReferenceMode;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.factories.PluginFactory;
import com.atlassian.plugin.util.EnumUtils;

import java.io.File;
import java.util.List;

/**
 * A PluginLoader which manages plugins specified by a roster file.
 *
 * @see RosterFileScanner
 * @since v3.0.24
 */
public class RosterFilePluginLoader extends ScanningPluginLoader {
    public static String getReferenceModePropertyName() {
        return RosterFilePluginLoader.class.getName() + ".referenceMode";
    }

    private static ReferenceMode referenceModeFromProperty() {
        final String propertyName = getReferenceModePropertyName();
        return EnumUtils.enumValueFromProperty(propertyName, ReferenceMode.values(), ReferenceMode.FORBID_REFERENCE);
    }

    /**
     * Create a RosterFilePluginLoader which loads a given roster file to locate plugins.
     *
     * @param rosterFile         the file to load the plugin roster from, as per {@link RosterFileScanner}.
     * @param pluginFactories    the factories used to convert PluginArtifacts to Plugins.
     * @param pluginEventManager the event manager for the plugin system.
     */
    public RosterFilePluginLoader(final File rosterFile,
                                  final List<PluginFactory> pluginFactories,
                                  final PluginEventManager pluginEventManager) {
        this(rosterFile, pluginFactories, referenceModeFromProperty(), pluginEventManager);
    }

    /**
     * Create a RosterFilePluginLoader which loads a given roster file to locate plugins.
     *
     * @param rosterFile         the file to load the plugin roster from, as per {@link RosterFileScanner}.
     * @param pluginFactories    the factories used to convert PluginArtifacts to Plugins.
     * @param referenceMode      the ReferenceMode to use for the implicit {@link DefaultPluginArtifactFactory}.
     * @param pluginEventManager the event manager for the plugin system.
     */
    public RosterFilePluginLoader(final File rosterFile,
                                  final List<PluginFactory> pluginFactories,
                                  final ReferenceMode referenceMode,
                                  final PluginEventManager pluginEventManager) {
        this(rosterFile, pluginFactories, new DefaultPluginArtifactFactory(referenceMode), pluginEventManager);
    }

    /**
     * Create a RosterFilePluginLoader which loads a given roster file to locate plugins.
     *
     * @param rosterFile            the file to load the plugin roster from, as per {@link RosterFileScanner}.
     * @param pluginFactories       the factories used to convert PluginArtifacts to Plugins.
     * @param pluginArtifactFactory the factory used to convert URIs to PluginArtifacts.
     * @param pluginEventManager    the event manager for the plugin system.
     */
    public RosterFilePluginLoader(final File rosterFile,
                                  final List<PluginFactory> pluginFactories,
                                  final PluginArtifactFactory pluginArtifactFactory,
                                  final PluginEventManager pluginEventManager) {
        super(new RosterFileScanner(rosterFile), pluginFactories, pluginArtifactFactory, pluginEventManager);
    }
}
