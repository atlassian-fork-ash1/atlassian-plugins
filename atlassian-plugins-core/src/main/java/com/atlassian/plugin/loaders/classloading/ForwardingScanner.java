package com.atlassian.plugin.loaders.classloading;

import com.atlassian.plugin.PluginException;

import java.util.Collection;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Forwarding delegate for a {@link Scanner}.
 *
 * @since 3.0.8
 */
public class ForwardingScanner implements Scanner {
    /**
     * The delegate to forward calls to.
     */
    private final Scanner delegate;

    public ForwardingScanner(Scanner delegate) {
        this.delegate = checkNotNull(delegate);
    }

    public Collection<DeploymentUnit> scan() {
        return delegate.scan();
    }

    public Collection<DeploymentUnit> getDeploymentUnits() {
        return delegate.getDeploymentUnits();
    }

    public void reset() {
        delegate.reset();
    }

    public void remove(DeploymentUnit unit) throws PluginException {
        delegate.remove(unit);
    }
}
