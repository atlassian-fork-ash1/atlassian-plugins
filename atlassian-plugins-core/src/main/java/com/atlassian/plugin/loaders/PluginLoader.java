package com.atlassian.plugin.loaders;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginException;
import com.atlassian.plugin.PluginParseException;
import org.dom4j.Element;

/**
 * Handles loading and unloading plugin artifacts from a location
 */
public interface PluginLoader {
    /**
     * Loads all plugins that can be installed in the plugin system.
     *
     * @param moduleDescriptorFactory the factory for module descriptors
     * @return the list of found plugins, may be empty
     * @throws PluginParseException if any error occurred loading plugins
     */
    Iterable<Plugin> loadAllPlugins(ModuleDescriptorFactory moduleDescriptorFactory) throws PluginParseException;

    /**
     * Load all newly found plugins that can be installed in the plugin system. Only plugins not previously loaded will
     * be added.
     *
     * @param moduleDescriptorFactory the factory for module descriptors
     * @return a list of newly discovered plugins since the last time plugins were loaded
     * @throws PluginParseException if any error occurred loading plugins
     */
    Iterable<Plugin> loadFoundPlugins(ModuleDescriptorFactory moduleDescriptorFactory) throws PluginParseException;

    /**
     * @return true if this PluginLoader tracks whether or not plugins are added to it.
     */
    boolean supportsAddition();

    /**
     * @return true if this PluginLoader tracks whether or not plugins are removed from it.
     */
    boolean supportsRemoval();

    /**
     * Remove a specific plugin
     */
    void removePlugin(Plugin plugin) throws PluginException;

    /**
     * @return {@code true} if this plugin loader can load plugins dynamically
     * @since 3.0
     */
    boolean isDynamicPluginLoader();

    /**
     * If this loader is capable of loading a plugin of the type passed, attempt to create a module descriptor. Add that
     * module descriptor to the plugin.
     * <p>
     * If capable, always return a ModuleDescriptor, even if it indicates a failure case. Caller is responsible for
     * handling exceptional {@link com.atlassian.plugin.ModuleDescriptor}.
     *
     * @param plugin                  that the module will be added to
     * @param module                  to create
     * @param moduleDescriptorFactory basic factory, may be overridden
     * @return null if incapable of creating
     * @since 4.0.0
     */
    ModuleDescriptor<?> createModule(Plugin plugin, Element module, ModuleDescriptorFactory moduleDescriptorFactory);
}
