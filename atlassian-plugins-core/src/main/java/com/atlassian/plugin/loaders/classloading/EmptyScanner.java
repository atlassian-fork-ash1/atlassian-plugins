package com.atlassian.plugin.loaders.classloading;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Collections;

/**
 * A {@link Scanner} which never finds anything.
 *
 * @since 3.0.16
 */
public class EmptyScanner implements Scanner {
    private static final Logger log = LoggerFactory.getLogger(EmptyScanner.class);

    public Collection<DeploymentUnit> scan() {
        return Collections.emptyList();
    }

    public Collection<DeploymentUnit> getDeploymentUnits() {
        return Collections.emptyList();
    }

    public void reset() {
        // Nothing to reset
    }

    public void remove(final DeploymentUnit unit) {
        // Nothing to remove, but the caller shouldn't be trying really, so WARN - it's
        // recoverable, but probably indicates a bug
        log.warn("EmptyScanner.remove called for {}", unit);
    }
}
