package com.atlassian.plugin.loaders;

import com.atlassian.plugin.Plugin;

/**
 * A PluginLoader which requires plugins to be discarded after load if they are not removed.
 *
 * PluginLoader implementations which also implement this interface are requesting that
 * either discardPlugin or removePlugin be called for every plugin which is returned by
 * loadAllPlugins or loadFoundPlugins.
 */
public interface DiscardablePluginLoader extends PluginLoader {
    /**
     * Notify the PluginLoader that the system will not be loading the given plugin.
     *
     * This instructs the PluginLoader to discard any resources associated with the Plugin.
     */
    void discardPlugin(Plugin plugin);
}
