package com.atlassian.plugin.loaders;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginException;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.util.ClassLoaderUtils;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URL;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.google.common.collect.ImmutableList.copyOf;

/**
 * Loads plugins from the classpath
 */
public class ClassPathPluginLoader implements PluginLoader {
    private static Logger log = LoggerFactory.getLogger(ClassPathPluginLoader.class);

    private final String fileNameToLoad;

    private Map<Plugin, SinglePluginLoader> pluginLoaderMap;

    public ClassPathPluginLoader() {
        this(PluginAccessor.Descriptor.FILENAME);
    }

    public ClassPathPluginLoader(final String fileNameToLoad) {
        this.fileNameToLoad = fileNameToLoad;
    }

    private void loadClassPathPlugins(final ModuleDescriptorFactory moduleDescriptorFactory) throws PluginParseException {
        final Enumeration<URL> pluginDescriptorFiles;
        try {
            pluginDescriptorFiles = ClassLoaderUtils.getResources(fileNameToLoad, this.getClass());
        } catch (final IOException e) {
            log.error("Could not load classpath plugins: " + e, e);
            return;
        }

        pluginLoaderMap = new LinkedHashMap<>();
        while (pluginDescriptorFiles.hasMoreElements()) {
            final URL url = pluginDescriptorFiles.nextElement();
            final SinglePluginLoader singlePluginLoader = new SinglePluginLoader(url);
            for (final Plugin plugin : singlePluginLoader.loadAllPlugins(moduleDescriptorFactory)) {
                pluginLoaderMap.put(plugin, singlePluginLoader);
            }
        }
    }

    public Iterable<Plugin> loadAllPlugins(final ModuleDescriptorFactory moduleDescriptorFactory) throws PluginParseException {
        if (pluginLoaderMap == null) {
            loadClassPathPlugins(moduleDescriptorFactory);
        }
        return copyOf(pluginLoaderMap.keySet());
    }

    public boolean supportsRemoval() {
        return false;
    }

    public boolean supportsAddition() {
        return false;
    }

    @Override
    public boolean isDynamicPluginLoader() {
        return false;
    }

    @Override
    public ModuleDescriptor<?> createModule(final Plugin plugin, final Element module, final ModuleDescriptorFactory moduleDescriptorFactory) {
        if (pluginLoaderMap.containsKey(plugin)) {
            return pluginLoaderMap.get(plugin).createModule(plugin, module, moduleDescriptorFactory);
        } else {
            return null;
        }
    }

    public Iterable<Plugin> loadFoundPlugins(final ModuleDescriptorFactory moduleDescriptorFactory) {
        throw new UnsupportedOperationException("This PluginLoader does not support addition.");
    }

    public void removePlugin(final Plugin plugin) throws PluginException {
        throw new PluginException("This PluginLoader does not support removal.");
    }
}
