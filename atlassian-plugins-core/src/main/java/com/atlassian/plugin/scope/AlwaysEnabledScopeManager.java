package com.atlassian.plugin.scope;

/**
 * Allows scope checks to be introduced ahead of real Vertigo implementation
 * so scope checks could be safely baked in platform modules and products
 *
 * @since 4.1
 * @deprecated since 4.1 for removal in 6.0 when {@link ScopeManager} is removed completely
 *
 */
@Deprecated
public class AlwaysEnabledScopeManager implements ScopeManager {

    @Override
    public boolean isScopeActive(String scopeKey) {
        return true;
    }
}
