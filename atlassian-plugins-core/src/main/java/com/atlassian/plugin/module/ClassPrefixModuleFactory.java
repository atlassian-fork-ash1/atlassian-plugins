package com.atlassian.plugin.module;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.hostcontainer.HostContainer;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * The ClassModuleFactory creates a java bean for the given module class by using either the plugins container or the hostcontainer, depending
 * if the plugin implements {@link com.atlassian.plugin.module.ContainerManagedPlugin}.
 * The returned bean class should have all constructor dependencies injected. However it is the containers responsibility to inject the dependencies.
 * <p>
 * The ClassModuleFactory expects the fully qualified name of the java class.
 *
 * @since 2.5.0
 */
public class ClassPrefixModuleFactory implements PrefixModuleFactory {
    protected final HostContainer hostContainer;

    public ClassPrefixModuleFactory(final HostContainer hostContainer) {
        this.hostContainer = hostContainer;
    }

    public <T> T createModule(final String name, final ModuleDescriptor<T> moduleDescriptor) throws PluginParseException {
        final Class<T> cls = getModuleClass(name, moduleDescriptor);

        final Plugin plugin = moduleDescriptor.getPlugin();
        if (plugin instanceof ContainerManagedPlugin) {
            final ContainerManagedPlugin cmPlugin = (ContainerManagedPlugin) plugin;
            final ContainerAccessor containerAccessor = checkNotNull(cmPlugin.getContainerAccessor(),
                    "Plugin container accessor is null. Plugin: %s. Module name: %s.", cmPlugin, name);

            return containerAccessor.createBean(cls);
        } else if (cls != null) {
            return hostContainer.create(cls);
        }
        return null;
    }

    <T> Class<T> getModuleClass(final String name, final ModuleDescriptor moduleDescriptor) throws ModuleClassNotFoundException {
        try {
            return moduleDescriptor.getPlugin().loadClass(name, null);
        } catch (ClassNotFoundException e) {
            throw new ModuleClassNotFoundException(name, moduleDescriptor.getPluginKey(), moduleDescriptor.getKey(), e, createErrorMsg(name));
        }
    }

    private String createErrorMsg(final String className) {
        // TinyURL resolves to --> https://developer.atlassian.com/display/DOCS/BundleException
        return "Couldn't load the class '" + className + "'. "
                + "This could mean that you misspelled the name of the class (double check) or that "
                + "you're using a class in your plugin that you haven't provided bundle instructions for. "
                + "See https://developer.atlassian.com/x/mQAN for more details on how to fix this.";
    }

    public String getPrefix() {
        return "class";
    }
}
