package com.atlassian.plugin.util;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * InvocationHandler for a dynamic proxy that ensures all methods are executed with the
 * object class's class loader as the context class loader.
 *
 * @since 3.1.0
 */
public class ContextClassLoaderSettingInvocationHandler implements InvocationHandler {
    private final Object service;

    private final ClassLoader serviceClassLoader;

    public ContextClassLoaderSettingInvocationHandler(final Object service) {
        this.service = service;
        this.serviceClassLoader = service.getClass().getClassLoader();
    }

    public Object invoke(final Object o, final Method method, final Object[] objects) throws Throwable {
        ClassLoaderStack.push(serviceClassLoader);
        try {
            return method.invoke(service, objects);
        } catch (final InvocationTargetException e) {
            throw e.getTargetException();
        } finally {
            ClassLoaderStack.pop();
        }
    }
}
