package com.atlassian.plugin.util.resource;

import com.atlassian.plugin.Plugin;

import java.io.InputStream;
import java.net.URL;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Loads resources from the plugin.
 *
 * @since 3.0.0
 */
public final class AlternativePluginResourceLoader implements AlternativeResourceLoader {
    private final Plugin plugin;

    public AlternativePluginResourceLoader(final Plugin plugin) {
        this.plugin = checkNotNull(plugin);
    }

    @Override
    public URL getResource(final String path) {
        return plugin.getResource(path);
    }

    @Override
    public InputStream getResourceAsStream(final String name) {
        return plugin.getResourceAsStream(name);
    }
}
