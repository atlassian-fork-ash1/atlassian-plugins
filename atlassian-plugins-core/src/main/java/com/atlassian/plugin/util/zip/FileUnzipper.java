/*
 * Copyright (c) 2003 by Atlassian Software Systems Pty. Ltd.
 * All rights reserved.
 */
package com.atlassian.plugin.util.zip;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.zip.ZipEntry;

public class FileUnzipper extends AbstractUnzipper {
    private static final Logger log = LoggerFactory.getLogger(FileUnzipper.class);

    private File zipFile;
    private File destDir;

    public FileUnzipper(File zipFile, File destDir) {
        this.zipFile = zipFile;
        this.destDir = destDir;
    }

    /**
     * Unzips all files in the archive
     *
     * @throws IOException
     */
    public void unzip() throws IOException {
        if ((zipFile == null) || !zipFile.isFile())
            return;

        getStreamUnzipper().unzip();
    }

    public ZipEntry[] entries() throws IOException {
        return getStreamUnzipper().entries();
    }

    /**
     * Specify a specific file inside the archive to extract
     *
     * @param fileName
     */
    public File unzipFileInArchive(String fileName) throws IOException {
        if ((zipFile == null) || !zipFile.isFile() || StringUtils.isEmpty(fileName)) {
            return null;
        }

        File result = getStreamUnzipper().unzipFileInArchive(fileName);
        if (result == null) {
            log.error("The file: {} could not be found in the archive: {}", fileName, zipFile.getAbsolutePath());
        }

        return result;
    }

    private StreamUnzipper getStreamUnzipper() throws FileNotFoundException {
        return new StreamUnzipper(new BufferedInputStream(new FileInputStream(zipFile)), destDir);
    }

}
