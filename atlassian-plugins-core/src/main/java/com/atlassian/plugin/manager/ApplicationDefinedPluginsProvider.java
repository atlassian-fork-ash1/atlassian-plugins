package com.atlassian.plugin.manager;

import com.atlassian.annotations.PublicSpi;
import com.atlassian.plugin.ModuleDescriptor;

import java.util.Set;

import static java.util.Collections.emptySet;

/**
 * Allows to extract the plugin keys defined by 'applications' built on top of a 'product'
 * This component should be overridden by the host product (if it supports the concept of 'applications')
 * For example: For the 'JIRA' product, we have applications such as 'JIRA Service Desk', 'JIRA Software', etc.
 * {@code com.atlassian.application.api.ApplicationManager})
 */
@PublicSpi
public interface ApplicationDefinedPluginsProvider {
    /**
     * Default implementation of application defined plugins provider
     * Allows to omit all the checks for application defined descriptors
     */
    ApplicationDefinedPluginsProvider NO_APPLICATION_PLUGINS  = descriptors -> emptySet();
    /**
     * This method extracts the set of plugin keys from the list of module descriptors
     *
     * @param descriptors list of module descriptors available in the system
     * @return set of the plugin keys that are related to the applications
     */
    Set<String> getPluginKeys(Iterable<ModuleDescriptor> descriptors);
}
