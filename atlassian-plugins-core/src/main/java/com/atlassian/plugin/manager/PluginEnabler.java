package com.atlassian.plugin.manager;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginController;
import com.atlassian.plugin.PluginDependencies;
import com.atlassian.plugin.PluginState;
import com.atlassian.plugin.exception.PluginExceptionInterception;
import com.atlassian.plugin.util.PluginUtils;
import com.atlassian.plugin.util.WaitUntil;
import com.google.common.collect.ImmutableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.TimeUnit;

import static com.atlassian.plugin.util.PluginUtils.isAtlassianDevMode;

/**
 * Helper class that handles the problem of enabling a set of plugins at once. This functionality is used for both the
 * initial plugin loading and manual plugin enabling. The system waits 60 seconds for all dependencies to be resolved,
 * then resets the timer to 5 seconds if only one remains.
 *
 * @since 2.2.0
 */
public class PluginEnabler {
    private static final Logger log = LoggerFactory.getLogger(PluginEnabler.class);
    private static final long LAST_PLUGIN_TIMEOUT = 30 * 1000;
    private static final long LAST_PLUGIN_WARN_TIMEOUT = 5 * 1000;

    private final PluginAccessor pluginAccessor;
    private final PluginController pluginController;
    private final PluginExceptionInterception pluginExceptionInterception;
    private final Set<Plugin> pluginsBeingEnabled = new CopyOnWriteArraySet<>();

    public PluginEnabler(
            final PluginAccessor pluginAccessor,
            final PluginController pluginController,
            final PluginExceptionInterception pluginExceptionInterception) {
        this.pluginAccessor = pluginAccessor;
        this.pluginController = pluginController;
        this.pluginExceptionInterception = pluginExceptionInterception;
    }

    /**
     * Determines, recursively, which disabled plugins this plugin depends upon, and enables all of them at once.
     * Returns the plugins that were successfully enabled, including dependencies that weren't explicitly specified.
     *
     * @param plugins The set of plugins to enable
     * @return a collection of plugins that were actually enabled
     */
    Collection<Plugin> enableAllRecursively(final Collection<Plugin> plugins) {
        final Collection<Plugin> pluginsToEnable = new ArrayList<>();
        final Set<String> requiredKeys = new HashSet<>();

        for (final Plugin plugin : plugins) {
            scanDependencies(plugin, requiredKeys);
        }

        for (final String key : requiredKeys) {
            pluginsToEnable.add(pluginAccessor.getPlugin(key));
        }
        enable(pluginsToEnable);

        final ImmutableList.Builder<Plugin> enabledPlugins = new ImmutableList.Builder<>();
        for (final Plugin plugin : pluginsToEnable) {
            if (plugin.getPluginState().equals(PluginState.ENABLED)) {
                enabledPlugins.add(plugin);
            }
        }
        return enabledPlugins.build();
    }

    /**
     * @param plugin The plugin to test
     * @return If the plugin is currently part of a set that is being enabled
     */
    public boolean isPluginBeingEnabled(final Plugin plugin) {
        return pluginsBeingEnabled.contains(plugin);
    }

    /**
     * Enables a collection of plugins at once, waiting for 60 seconds. If any plugins are still in the enabling state,
     * the plugins are explicitly disabled.
     *
     * @param plugins The plugins to enable
     */
    void enable(final Collection<Plugin> plugins) {
        pluginsBeingEnabled.addAll(plugins);
        try {
            actualEnable(plugins);
        } finally {
            pluginsBeingEnabled.removeAll(plugins);
        }
    }

    private void actualEnable(final Collection<Plugin> plugins) {
        log.info("Resolving {} plugins", plugins.size());
        final Set<Plugin> pluginsInEnablingState = new HashSet<>();
        // As an optimization, perform a resolve first. This moves work that can be done up front out of the period
        // covered by the DependencyWaiterApplicationContextExecutor timeouts.
        for (final Plugin plugin : plugins) {
            try {
                plugin.resolve();
            } catch (final RuntimeException runtime) {
                // Just log - if the situation is not transient (it's probably an OsgiPlugin missing a needed bundle),
                // the enable below will hit it and pass it over to interception.
                log.error("Cannot resolve plugin '" + plugin.getKey() + "'", runtime);
            }
        }
        log.info("Enabling {} plugins: {}", plugins.size(), plugins);
        for (final Plugin plugin : plugins) {
            try {
                plugin.enable();
                final PluginState pluginState = plugin.getPluginState();
                if (pluginState == PluginState.ENABLING) {
                    pluginsInEnablingState.add(plugin);
                } else {
                    log.info("Plugin '{}' immediately {}", plugin.getKey(), pluginState);
                }
            } catch (final RuntimeException ex) {
                final boolean logMsg = pluginExceptionInterception.onEnableException(plugin, ex);
                if (logMsg) {
                    log.error("Unable to enable plugin " + plugin.getKey(), ex);
                }
            }
        }

        if (!pluginsInEnablingState.isEmpty()) {
            log.info("Waiting for {} plugins to finish ENABLING: {}", pluginsInEnablingState.size(), pluginsInEnablingState);

            // Now try to enable plugins that weren't enabled before, probably due to dependency ordering issues
            WaitUntil.invoke(new WaitUntil.WaitCondition() {
                private long singlePluginTimeout;
                private long singlePluginWarn;

                public boolean isFinished() {
                    if (singlePluginTimeout > 0 && singlePluginTimeout < System.currentTimeMillis()) {
                        return true;
                    }
                    for (final Iterator<Plugin> i = pluginsInEnablingState.iterator(); i.hasNext(); ) {
                        final Plugin plugin = i.next();
                        final PluginState pluginState = plugin.getPluginState();
                        if (pluginState != PluginState.ENABLING) {
                            log.info("Plugin '{}' is now {}", plugin.getKey(), pluginState);
                            i.remove();
                        }
                    }
                    if (isAtlassianDevMode() && pluginsInEnablingState.size() == 1) {
                        final long currentTime = System.currentTimeMillis();
                        if (singlePluginTimeout == 0) {
                            log.info("Only one plugin left not enabled. Resetting the timeout to " + (LAST_PLUGIN_TIMEOUT / 1000) + " seconds.");

                            singlePluginWarn = currentTime + LAST_PLUGIN_WARN_TIMEOUT;
                            singlePluginTimeout = currentTime + LAST_PLUGIN_TIMEOUT;
                        } else if (singlePluginWarn <= currentTime) {
                            //PLUG-617: Warn people when it takes a long time to enable a plugin when in dev mode. We bumped
                            //this timeout from 5 to 30 seconds because the gadget publisher in JIRA can take this long to
                            //load when running java in DEBUG mode. We are also now going to log a message about slow startup
                            //since 30 seconds is a long time to wait for your plugin to fail.
                            final Plugin plugin = pluginsInEnablingState.iterator().next();
                            final long remainingWait = Math.max(0, Math.round((singlePluginTimeout - currentTime) / 1000.0));

                            log.warn("Plugin '" + plugin + "' did not enable within " + (LAST_PLUGIN_WARN_TIMEOUT / 1000) + " seconds." + "The plugin should not take this long to enable. Will only attempt to load plugin for another '" + remainingWait + "' seconds.");
                            singlePluginWarn = Long.MAX_VALUE;
                        }
                    }
                    return pluginsInEnablingState.isEmpty();
                }

                public String getWaitMessage() {
                    return "Plugins that have yet to be enabled: (" + pluginsInEnablingState.size() + "): " +
                            pluginsInEnablingState;
                }

            }, PluginUtils.getDefaultEnablingWaitPeriod(), TimeUnit.SECONDS, 1);

            // Disable any plugins that aren't enabled by now
            if (!pluginsInEnablingState.isEmpty()) {
                final StringBuilder sb = new StringBuilder();
                for (final Plugin plugin : pluginsInEnablingState) {
                    sb.append(plugin.getKey()).append(',');
                    pluginController.disablePluginWithoutPersisting(plugin.getKey());
                }
                sb.deleteCharAt(sb.length() - 1);
                log.error("Unable to start the following plugins due to timeout while waiting for plugin to enable: " + sb.toString());
            }
        }
    }

    /**
     * Scans, recursively, to build a set of plugin dependencies for the target plugin
     *
     * @param plugin       The plugin to scan
     * @param requiredKeys The set of keys collected so far
     */
    private void scanDependencies(final Plugin plugin, final Set<String> requiredKeys) {
        requiredKeys.add(plugin.getKey());

        final PluginDependencies dependencies = plugin.getDependencies();
        Set<String> dependencyKeys = new HashSet<>(dependencies.getMandatory());

        // Ensure dependent plugins are enabled first
        for (String dependencyKey : dependencyKeys) {
            if (!requiredKeys.contains(dependencyKey) &&
                    (pluginAccessor.getPlugin(dependencyKey) != null) &&
                    !pluginAccessor.isPluginEnabled(dependencyKey)) {
                scanDependencies(pluginAccessor.getPlugin(dependencyKey), requiredKeys);
            }
        }
    }

    public Set<Plugin> getPluginsBeingEnabled() {
        return pluginsBeingEnabled;
    }
}
