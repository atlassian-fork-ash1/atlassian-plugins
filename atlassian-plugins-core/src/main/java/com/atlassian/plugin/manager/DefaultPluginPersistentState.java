package com.atlassian.plugin.manager;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginRestartState;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import static com.atlassian.plugin.manager.PluginEnabledState.UNKNOWN_ENABLED_TIME;
import static com.atlassian.plugin.manager.PluginPersistentState.Util.buildStateKey;
import static java.util.Collections.unmodifiableMap;
import static java.util.stream.Collectors.toMap;

/**
 * Immutable implementation of the {@link PluginPersistentState} interface.
 * <p>
 * The state stored in this object represents changes done by a user to the plugin's state.
 * If "getPluginState()" or "getPluginModuleState()" return null, then the manager
 *  should assume that the default state applies instead.
 */
public final class DefaultPluginPersistentState implements Serializable, PluginPersistentState {
    private final Map<String, PluginEnabledState> map;

    /* for use from within this package, the second parameter is ignored */
    DefaultPluginPersistentState(final Map<String, PluginEnabledState> map, final boolean ignore) {
        this.map = unmodifiableMap(new HashMap<>(map));
    }

    @Override
    public Map<String, PluginEnabledState> getStatesMap() {
        return unmodifiableMap(map);
    }

    /* (non-Javadoc)
     * @see com.atlassian.plugin.PluginPersistentState#isEnabled(com.atlassian.plugin.Plugin)
     */
    public boolean isEnabled(final Plugin plugin) {
        final PluginEnabledState state = map.get(plugin.getKey());
        return (state == null) ? plugin.isEnabledByDefault() : state.isEnabled();
    }

    /* (non-Javadoc)
     * @see com.atlassian.plugin.PluginPersistentState#isEnabled(com.atlassian.plugin.ModuleDescriptor)
     */
    public boolean isEnabled(final ModuleDescriptor<?> pluginModule) {
        if (pluginModule == null) {
            return false;
        }

        final PluginEnabledState state = map.get(pluginModule.getCompleteKey());
        return (state == null) ? pluginModule.isEnabledByDefault() : state.isEnabled();
    }

    @Override
    public Map<String, PluginEnabledState> getPluginEnabledStateMap(final Plugin plugin) {
        return getStatesMap().entrySet().stream()
                .filter(e -> e.getKey().startsWith(plugin.getKey()))
                .collect(toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    public PluginRestartState getPluginRestartState(final String pluginKey) {
        for (final PluginRestartState state : PluginRestartState.values()) {
            if (map.containsKey(buildStateKey(pluginKey, state))) {
                return state;
            }
        }
        return PluginRestartState.NONE;
    }

    public static Map<String, PluginEnabledState> getPluginEnabledStateMap(final Map<String, Boolean> map) {
        return unmodifiableMap(new HashMap<>(map.entrySet().stream()
                .collect(toMap(Map.Entry::getKey, e -> new PluginEnabledState(e.getValue(), UNKNOWN_ENABLED_TIME)))));
    }
}
