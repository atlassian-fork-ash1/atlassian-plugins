package com.atlassian.plugin.manager.store;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.plugin.manager.PluginPersistentState;
import com.atlassian.plugin.manager.PluginPersistentStateStore;

/**
 * A read-only implementation of {@link PluginPersistentStateStore} which returns a state provided on construction.
 * <p>
 * The held {@link PluginPersistentState} is not read-only - external modification to it is not prevented, and will be reflected
 * in the state returned from {@link #load()}.
 *
 * @since 3.2.0
 */
@ExperimentalApi
public class LoadOnlyPluginPersistentStateStore implements PluginPersistentStateStore {
    private final PluginPersistentState pluginPersistentState;

    public LoadOnlyPluginPersistentStateStore() {
        this(PluginPersistentState.Builder.create().toState());
    }

    public LoadOnlyPluginPersistentStateStore(final PluginPersistentState pluginPersistentState) {
        this.pluginPersistentState = pluginPersistentState;
    }

    /**
     * This implementation ignores the provided state and throws.
     *
     * @param state ignored
     * @throws IllegalStateException always
     */
    @Override
    public void save(final PluginPersistentState state) {
        throw new IllegalStateException("Cannot save state to " + LoadOnlyPluginPersistentStateStore.class);
    }

    @Override
    public PluginPersistentState load() {
        return pluginPersistentState;
    }
}
