package com.atlassian.plugin.manager;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginRegistry;
import io.atlassian.util.concurrent.CopyOnWriteMap;

import java.util.Collection;
import java.util.Map;

public final class PluginRegistryImpl implements PluginRegistry.ReadWrite {
    private final Map<String, Plugin> plugins;

    public PluginRegistryImpl() {
        this.plugins = CopyOnWriteMap.<String, Plugin>builder().stableViews().newHashMap();
    }

    @Override
    public Collection<Plugin> getAll() {
        return plugins.values();
    }

    public Plugin get(final String pluginKey) {
        return plugins.get(pluginKey);
    }

    @Override
    public void clear() {
        plugins.clear();
    }

    @Override
    public void put(final Plugin plugin) {
        plugins.put(plugin.getKey(), plugin);
    }

    @Override
    public Plugin remove(final String key) {
        return plugins.remove(key);
    }
}
