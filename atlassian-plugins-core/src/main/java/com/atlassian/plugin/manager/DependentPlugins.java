package com.atlassian.plugin.manager;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginDependencies;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;

import javax.annotation.Nonnull;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static com.atlassian.plugin.PluginDependencies.Type.DYNAMIC;
import static com.atlassian.plugin.PluginDependencies.Type.MANDATORY;
import static com.atlassian.plugin.PluginDependencies.Type.OPTIONAL;
import static com.google.common.base.Preconditions.checkArgument;


/**
 * A collection of dependent plugins, in the dependency order. This means that plugins should be disabled
 * according to the order of {@link #plugins}.
 *
 * @since v4.0
 * @since v5.2 builds the dependency order and includes root plugins
 */
final class DependentPlugins {

    private static final Set<PluginDependencies.Type> ALL_TYPES = ImmutableSet.of(MANDATORY, OPTIONAL, DYNAMIC);

    private final List<DependentPlugin> plugins;

    /**
     * Obtain the plugins which require, possibly indirectly, given plugins specified by {@code rootPluginKeys}.
     * <p>
     * The plugins with keys in {@code rootPluginKeys} are also placed in {@link #plugins}.
     * <p>
     * Plugins are paired with their most significant dependency type, with the following order of significance:
     * {@link PluginDependencies.Type#MANDATORY},{@link PluginDependencies.Type#OPTIONAL}, {@link
     * PluginDependencies.Type#DYNAMIC}.
     * @param rootPluginKeys  plugin keys to list dependents of.
     * @param allEnabledPlugins  a List of plugins
     * @param dependencyTypes only include plugins with these dependency types
     */
    public DependentPlugins(final Collection<String> rootPluginKeys,
                            final Iterable<Plugin> allEnabledPlugins,
                            final Set<PluginDependencies.Type> dependencyTypes) {
        if (dependencyTypes.isEmpty()) {
            throw new IllegalArgumentException("Dependency types must be provided");
        }
        final PluginDependencies.Type leastSignificantType = getLeastSignificantType(dependencyTypes);

        final Multimap<String, DependentPlugin> pluginsToAllDependents = buildPluginToItsDependants(
                allEnabledPlugins,
                leastSignificantType);

        final Map<String, Plugin> pluginKeyToPlugin = Maps.uniqueIndex(allEnabledPlugins, Plugin::getKey);

        final Map<String, DependentPlugin> transitiveDependentsByName = calculateTransitivePluginDependencies(
                rootPluginKeys,
                dependencyTypes,
                pluginsToAllDependents,
                pluginKeyToPlugin
        );

        final Map<String, Set<String>> workMap = constructWorkMap(
                pluginsToAllDependents,
                transitiveDependentsByName.values());

        plugins = getInDependencyOrder(workMap, transitiveDependentsByName);
    }

    /**
     * Takes workMap (see {@link #constructWorkMap} and empties it, returning all its entries in dependency order.
     * <p/>
     * The algorithm loops over workMap, searching for all plugins that have no more dependants. These can be added
     * to the returned list and removed from the workMap.
     * When workMap is empty, we're done.
     * When workMap is not empty, but there is no plugin that no one depends on, then we must have a dependency cycle.
     * We cut the cycle by removing the plugin returned from {@link #findPluginToCutTheCycle} from the workMap.
     * @param workMap map of dependencies which we will drain
     * @param transitiveDependenciesByName
     * @return List of plugins in the order they should be disabled
     */
    private List<DependentPlugin> getInDependencyOrder(final Map<String, Set<String>> workMap,
                                                       final Map<String, DependentPlugin> transitiveDependenciesByName) {
        final List<DependentPlugin> pluginsInDependencyOrder = new ArrayList<>();

        while (!workMap.isEmpty()) {
            final List<String> pluginsWithNoMoreDependencies = workMap.keySet().stream()
                    .filter(k -> workMap.get(k).isEmpty())
                    .collect(Collectors.toList());
            if (pluginsWithNoMoreDependencies.isEmpty()) {
                // we have a dependency cycle
                String current = findPluginToCutTheCycle(workMap);
                removeFromWorkmap(workMap, current);
                DependentPlugin currentPlugin = transitiveDependenciesByName.get(current);
                if (currentPlugin != null) {
                    pluginsInDependencyOrder.add(currentPlugin);
                }
            } else {
                for (String pluginWithNoMoreDependencies : pluginsWithNoMoreDependencies) {
                    removeFromWorkmap(workMap, pluginWithNoMoreDependencies);
                    DependentPlugin currentPlugin = transitiveDependenciesByName.get(pluginWithNoMoreDependencies);
                    if (currentPlugin != null) {
                        pluginsInDependencyOrder.add(currentPlugin);
                    }
                }
            }
        }
        return pluginsInDependencyOrder;
    }

    /**
     * Returns the key of the plugin that first sees the dependency cycle (i.e. whose dependency has been already seen).
     */
    private String findPluginToCutTheCycle(final Map<String, Set<String>> workMap) {
        final Set<String> alreadySeen = new HashSet();
        String next = workMap.keySet().iterator().next();
        String current = next;

        // This method is only called once we check that each plugin has a non-empty set of those that depend on it.
        // This means we can move from each plugin to one (randomly selected) that depends on it.
        // Because there is a finite number of plugins, we know that eventually we'll come back to a plugin we've
        // already visited. Then we have a proof there is a cycle and that we can cut it by removing the plugin
        // that first noticed the already seen plugin.
        while (alreadySeen.add(next)) {
            // get any plugin that depends on us
            current = next;
            next = workMap.get(current).stream()
                    .findFirst()
                    .orElseThrow(() -> new IllegalStateException("Each plugin has a dependency. " +
                            "This suggests there is a cyclic dependency, yet we could not find a cycle. " +
                            "The internal data structure is corrupted: " + workMap));
        }
        return current;
    }

    private void removeFromWorkmap(final Map<String, Set<String>> workMap,
                                   final String pluginKey) {
        // remove key (it either leads to an empty set, or causes a dependency cycle)
        workMap.remove(pluginKey);
        // remove this plugin from other plugins dependency lists
        for (Set<String> plugins : workMap.values()) {
            plugins.remove(pluginKey);
        }
    }

    /**
     * Build a map of plugin to all plugins that depend on it. We'll later on drain this map in {@link #getInDependencyOrder}.
     */
    private Map<String, Set<String>> constructWorkMap(final Multimap<String, DependentPlugin> pluginsToAllDependents,
                                                      final Collection<DependentPlugin> mostSignificantTransitiveDependencies) {
        final Map<String, Set<String>> workMap = new HashMap<>();

        for (DependentPlugin plugin : mostSignificantTransitiveDependencies) {
            final Set<String> dependentPlugins = pluginsToAllDependents
                    .get(plugin.getPlugin().getKey())
                    .stream()
                    .map(DependentPlugin::getPlugin)
                    .map(Plugin::getKey)
                    .collect(Collectors.toSet());
            workMap.put(plugin.getPlugin().getKey(), dependentPlugins);
            // Add plugins with no dependencies. If later on we find them to have a dependency, we'll override this empty set
            for (String dependentPlugin : dependentPlugins) {
                workMap.putIfAbsent(dependentPlugin, Collections.emptySet());
            }
        }

        return workMap;
    }

    /**
     * Builds a collection of all plugins that transitively depend on {@code rootPluginKeys}. Each dependency is described
     * by the plugin and its most significant dependency type.
     * The root plugins will also be included.
     * <p/>
     * The map is really a list of {@link DependentPlugin}s, indexed by each plugin's key, e.g.
     *
     * "A" -> ("A", MANDATORY, root)
     * "B" -> ("B", OPTIONAL, not root)
     *
     * The list is not ordered. It will be used by {@link #constructWorkMap} and {@link #getInDependencyOrder} to build an ordered list.
     */
    private Map<String, DependentPlugin> calculateTransitivePluginDependencies(final Collection<String> rootPluginKeys,
                                                                               final Set<PluginDependencies.Type> dependencyTypes,
                                                                               final Multimap<String, DependentPlugin> allPluginDependencies,
                                                                               final Map<String, Plugin> pluginKeyToPlugin) {
        final Map<String, DependentPlugin> transitivePluginDependencies = new HashMap<>();

        // A queue of the plugin keys with capped dependency type we have yet to explore dependencies of
        final DependencyQueue dependenciesToExplore = new DependencyQueue();

        final Set<CappedDep> visited = Sets.newHashSet();
        for (String rootPluginKey : rootPluginKeys) {
            dependenciesToExplore.addLast(new CappedDep(rootPluginKey, MANDATORY));

            for (PluginDependencies.Type type : PluginDependencies.Type.values()) {
                visited.add(new CappedDep(rootPluginKey, type));
            }

            Plugin rootPlugin = pluginKeyToPlugin.get(rootPluginKey);
            if (rootPlugin != null) {
                transitivePluginDependencies.put(rootPluginKey, new DependentPlugin(rootPlugin, MANDATORY, true));
            } // else this root plugin is already disabled
        }

        while (!dependenciesToExplore.isEmpty()) {
            final CappedDep currentPlugin = dependenciesToExplore.removeFirst();
            for (final DependentPlugin pluginWithDependencyType : allPluginDependencies.get(currentPlugin.key)) {
                final PluginDependencies.Type dependencyType = currentPlugin.cap(pluginWithDependencyType.getDependencyType());

                if (dependencyTypes.contains(dependencyType)) {
                    final Plugin dependentPlugin = pluginWithDependencyType.getPlugin();
                    final String dependentPluginKey = dependentPlugin.getKey();

                    final CappedDep newDep = new CappedDep(dependentPluginKey, dependencyType);
                    if (visited.add(newDep)) {

                        final DependentPlugin existingDependencyType = transitivePluginDependencies.get(dependentPluginKey);
                        if (existingDependencyType == null || existingDependencyType.getDependencyType().lessSignificant(dependencyType)) {
                            transitivePluginDependencies.put(dependentPlugin.getKey(), new DependentPlugin(dependentPlugin, dependencyType, false));
                        }

                        dependenciesToExplore.addLast(newDep);
                    }
                }
            }
        }

        return transitivePluginDependencies;
    }

    /**
     * Compute dependencies map from a key to each plugin that requires it,
     * cutting out dependencies less significant than the leastSignificantType
     */
    private Multimap<String, DependentPlugin> buildPluginToItsDependants(final Iterable<Plugin> allEnabledPlugins,
                                                                         final PluginDependencies.Type leastSignificantType) {
        final Multimap<String, DependentPlugin> dependencies = ArrayListMultimap.create();
        for (final Plugin p : allEnabledPlugins) {
            for (final Map.Entry<String, PluginDependencies.Type> keyType : p.getDependencies().getByPluginKey().entries()) {
                // Cut out dependencies that less significant that we want
                if (!keyType.getValue().lessSignificant(leastSignificantType)) {
                    dependencies.put(keyType.getKey(), new DependentPlugin(p, keyType.getValue(), false));
                }
            }
        }
        return dependencies;
    }

    private PluginDependencies.Type getLeastSignificantType(final Set<PluginDependencies.Type> dependencyTypes) {
        PluginDependencies.Type leastSignificantType = MANDATORY;
        for (final PluginDependencies.Type type : dependencyTypes) {
            if (type.lessSignificant(leastSignificantType)) {
                leastSignificantType = type;
            }
        }
        return leastSignificantType;
    }

    /**
     * See {@link #toStringList(Set)}.
     */
    public List<String> toStringList() {
        return toStringList(ALL_TYPES);
    }

    /**
     * Returns a list of strings representing all non-root dependent plugins, together with their dependency types,
     * for all the dependency types provided.
     * @param dependencyTypes A set of dependency types for which strings should be generated.
     */
    public List<String> toStringList(final Set<PluginDependencies.Type> dependencyTypes) {
        return plugins.stream()
                .filter(dp -> !dp.isRoot())
                .filter(dp -> dependencyTypes.contains(dp.getDependencyType()))
                .map(dp -> dp.getPlugin().getKey() + "(" + dp.getDependencyType() + ")")
                .collect(Collectors.toList());
    }

    /**
     * Get all plugins in dependency order.
     * @param includeRoots if the root plugins should be included (see {@link DependentPlugin#isRoot()})
     */
    public List<Plugin> getPlugins(boolean includeRoots) {
        return getPluginsByTypes(ALL_TYPES, includeRoots);
    }

    /**
     * Get plugins in dependency order, limited to those whose dependency type is in the set provided.
     * @param dependencyTypes return only plugins with dependencies of these types
     * @param includeRoots    if the root plugins should be included (see {@link DependentPlugin#isRoot()}).
     *                        If {@code includeRoots=true}, then {@code dependencyTypes} must contain {@link PluginDependencies.Type#MANDATORY}.
     */
    public List<Plugin> getPluginsByTypes(final Set<PluginDependencies.Type> dependencyTypes, boolean includeRoots) {
        checkArgument(!includeRoots || dependencyTypes.contains(MANDATORY),
                "Roots always have dependency type " + MANDATORY + ". Cannot ask for includeRoots=true and not have "
                        + MANDATORY + " in dependencyTypes (" + dependencyTypes + ").");
        return plugins.stream()
                .filter(p -> includeRoots || ! p.isRoot())
                .filter(p -> dependencyTypes.contains(p.getDependencyType()))
                .map(DependentPlugin::getPlugin)
                .collect(Collectors.toList());
    }

    private static class DependentPlugin {
        private final Plugin plugin;
        private final PluginDependencies.Type dependencyType;
        private final boolean isRoot;

        DependentPlugin(Plugin plugin, PluginDependencies.Type dependencyType, boolean isRoot) {
            this.plugin = plugin;
            this.dependencyType = dependencyType;
            this.isRoot = isRoot;
        }

        Plugin getPlugin() {
            return plugin;
        }

        PluginDependencies.Type getDependencyType() {
            return dependencyType;
        }

        /**
         * Returns true if this plugin was in the {@code rootPluginKeys} list when {@link DependentPlugins} constructor was called.
         */
        boolean isRoot() {
            return isRoot;
        }

        @Override
        public String toString() {
            return "DependentPlugin{" +
                    "plugin=" + plugin +
                    ", dependencyType=" + dependencyType +
                    ", isRoot=" + isRoot +
                    '}';
        }
    }

    /**
     * A dependency with a cap on transient dependency type.
     *
     * This is to propagate the least significant dependency type when calculating transient dependencies.
     * E.g. for dependencies
     * A <---OPTIONAL--- B <---MANDATORY--- C
     *                       `-DYNAMIC--- D
     * when we calculate A's dependants, once we get to B, this dependency is capped at OPTIONAL.
     * This means that even though C has a MANDATORY dependency on B, it can have at most OPTIONAL dependency on A.
     * However, as D has a DYNAMIC dependency on B, it has a DYNAMIC dependency on A (because DYNAMIC is less significant than OPTIONAL).
     */
    private static class CappedDep {
        @Nonnull
        final String key;
        @Nonnull
        final PluginDependencies.Type cap;

        CappedDep(final String key, final PluginDependencies.Type cap) {
            this.key = key;
            this.cap = cap;
        }

        // Cap the depType by significance
        PluginDependencies.Type cap(final PluginDependencies.Type depType) {
            return depType.lessSignificant(cap) ? depType : cap;
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            final CappedDep cappedDep = (CappedDep) o;

            if (!key.equals(cappedDep.key)) {
                return false;
            }
            return cap == cappedDep.cap;
        }

        @Override
        public int hashCode() {
            int result = key.hashCode();
            result = 31 * result + cap.hashCode();
            return result;
        }
    }

    /**
     * A queue of dependencies
     */
    private static class DependencyQueue {
        private final Deque<CappedDep> queue = new ArrayDeque<>();

        CappedDep removeFirst() {
            return queue.removeFirst();
        }

        boolean isEmpty() {
            return queue.isEmpty();
        }

        /**
         * Add, replace or do nothing depending on the new dependency significance.
         *
         * <h4>add</h4>
         * If there is no dependency on this plugin in the {@link #queue} yet, add it at the end.
         *
         * <h4>replace</h4>
         * If there already is a dependency on this plugin, but it is less significant than the new one,
         * replace the old one with the new higher significance. This should not be confused with {@link CappedDep#cap},
         * which is used when calculating <b>transitive</b> dependencies. In here we replace a dependency <b>on the same plugin</b>.
         *
         * <h4>do nothing</h4>
         * If this queue already contains a dependency on this plugin, with the same or higher dependency type, then this one can be ignored.
         *
         */
        void addLast(final CappedDep newDep) {
            boolean addToQueue = true;
            for (final Iterator<CappedDep> iter = queue.iterator(); iter.hasNext(); ) {
                final CappedDep current = iter.next();

                if (current.key.equals(newDep.key)) {
                    addToQueue = current.cap.lessSignificant(newDep.cap);
                    if (addToQueue) {
                        // remove if we have the same key lined up with less significant dependency
                        iter.remove();
                    }
                    // only one should exist in the queue
                    break;
                }
            }
            if (addToQueue) {
                queue.addLast(newDep);
            }
        }
    }
}
