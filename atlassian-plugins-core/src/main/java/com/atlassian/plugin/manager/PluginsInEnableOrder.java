package com.atlassian.plugin.manager;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginRegistry;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * A class to sort plugins in enable order
 *
 * @since v4.0
 */
final class PluginsInEnableOrder {
    // the list of plugins sorted by requirement order.
    final List<Plugin> sortedList;

    public PluginsInEnableOrder(final Collection<Plugin> pluginsToEnable, PluginRegistry.ReadOnly pluginRegistry) {
        this.sortedList = new ArrayList<>();
        final Set<Plugin> visited = new HashSet<>();

        for (final Plugin plugin : pluginsToEnable) {
            sortPluginForEnable(plugin, visited, pluginsToEnable, pluginRegistry);
        }
    }

    /**
     * If currentPlugin has not been visited, recurse on to each recognized plugin that it requires, and
     * then add it to the sorted list if it is allowed.
     *
     * @param currentPlugin  the plugin we are inspecting requirements of.
     * @param visited        the list of plugins we've already recursed into.
     * @param allowedPlugins the plugins which we can accumulate into sortedList.
     */
    private void sortPluginForEnable(
            final Plugin currentPlugin,
            final Set<Plugin> visited,
            final Collection<Plugin> allowedPlugins,
            final PluginRegistry.ReadOnly pluginRegistry) {
        if (!visited.add(currentPlugin)) {
            return;
        }

        for (final String key : currentPlugin.getDependencies().getAll()) {
            final Plugin requiredPlugin = pluginRegistry.get(key);

            if (null != requiredPlugin) {
                sortPluginForEnable(requiredPlugin, visited, allowedPlugins, pluginRegistry);
            }
        }

        if (allowedPlugins.contains(currentPlugin)) {
            sortedList.add(currentPlugin);
        }
    }

    public List<Plugin> get() {
        return Collections.unmodifiableList(sortedList);
    }
}
