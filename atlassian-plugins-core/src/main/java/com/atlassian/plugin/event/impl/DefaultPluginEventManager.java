package com.atlassian.plugin.event.impl;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.event.config.ListenerHandlersConfiguration;
import com.atlassian.event.internal.AsynchronousAbleEventDispatcher;
import com.atlassian.event.internal.DirectEventExecutorFactory;
import com.atlassian.event.internal.EventPublisherImpl;
import com.atlassian.event.internal.EventThreadPoolConfigurationImpl;
import com.atlassian.event.spi.EventDispatcher;
import com.atlassian.event.spi.EventExecutorFactory;
import com.atlassian.event.spi.ListenerHandler;
import com.atlassian.plugin.event.NotificationException;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.scope.ScopeManager;

import java.util.ArrayList;
import java.util.List;

import static com.atlassian.plugin.util.Assertions.notNull;

/**
 * Simple, synchronous event manager that uses one or more method selectors to determine event listeners.
 * <p>
 * The default method selectors are {@link MethodNameListenerMethodSelector} and
 * {@link AnnotationListenerMethodSelector}.
 */
public class DefaultPluginEventManager implements PluginEventManager {
    private final EventPublisher eventPublisher;

    public DefaultPluginEventManager() {
        this(defaultMethodSelectors());
    }

    /**
     * Uses the supplied selectors to determine listener methods.
     *
     * @param selectors used to determine which are listener methods
     */
    public DefaultPluginEventManager(final ListenerMethodSelector... selectors) {
        final ListenerHandlersConfiguration configuration = new ListenerHandlersConfiguration() {
            public List<ListenerHandler> getListenerHandlers() {
                final List<ListenerHandler> handlers = new ArrayList<>(selectors.length);
                for (final ListenerMethodSelector selector : selectors) {
                    handlers.add(new MethodSelectorListenerHandler(selector));
                }
                return handlers;
            }
        };

        final EventExecutorFactory executorFactory = new DirectEventExecutorFactory(new EventThreadPoolConfigurationImpl());
        final EventDispatcher eventDispatcher = new AsynchronousAbleEventDispatcher(executorFactory);
        eventPublisher = new EventPublisherImpl(eventDispatcher, configuration);
    }

    /**
     * @deprecated in 5.0 for removal in 6.0 when {@link ScopeManager} will be removed.
     *             Use {@link #DefaultPluginEventManager()} instead.
     */
    @Deprecated
    public DefaultPluginEventManager(ScopeManager scopeManager) {
        this(defaultMethodSelectors());
    }

    /**
     * @deprecated in 5.0 for removal in 6.0 when {@link ScopeManager} will be removed.
     *             Use {@link #DefaultPluginEventManager(ListenerMethodSelector[])} instead.
     */
    @Deprecated
    public DefaultPluginEventManager(ScopeManager scopeManager, final ListenerMethodSelector... selectors) {
        this(selectors);
    }

    /**
     * Delegate all event publication to the supplied {@code EventPublisher}.
     */
    public DefaultPluginEventManager(final EventPublisher eventPublisher) {
        this.eventPublisher = notNull("eventPublisher", eventPublisher);
    }

    public void register(final Object listener) {
        eventPublisher.register(notNull("listener", listener));
    }

    public void unregister(final Object listener) {
        eventPublisher.unregister(notNull("listener", listener));
    }

    public void broadcast(final Object event) throws NotificationException {
        notNull("event", event);
        try {
            eventPublisher.publish(event);
        } catch (final RuntimeException e) {
            throw new NotificationException(e);
        }
    }

    public EventPublisher getEventPublisher() {
        return eventPublisher;
    }

    static ListenerMethodSelector[] defaultMethodSelectors() {
        final ListenerMethodSelector methodNames = new MethodNameListenerMethodSelector();
        final ListenerMethodSelector pluginEvent = new AnnotationListenerMethodSelector();
        final ListenerMethodSelector eventListener = new AnnotationListenerMethodSelector(EventListener.class);
        return new ListenerMethodSelector[]{methodNames, pluginEvent, eventListener};
    }
}
