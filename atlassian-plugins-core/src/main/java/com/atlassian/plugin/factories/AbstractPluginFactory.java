package com.atlassian.plugin.factories;

import com.atlassian.plugin.Application;
import com.atlassian.plugin.PluginArtifact;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.parsers.DescriptorParser;
import com.atlassian.plugin.parsers.DescriptorParserFactory;

import java.io.InputStream;
import java.util.Set;
import java.util.function.Predicate;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.apache.commons.io.IOUtils.closeQuietly;

/**
 * @since 3.0
 */
public abstract class AbstractPluginFactory implements PluginFactory {

    protected final DescriptorParserFactory descriptorParserFactory;
    protected final Set<Application> applications;

    protected AbstractPluginFactory(DescriptorParserFactory descriptorParserFactory, Set<Application> applications) {
        this.descriptorParserFactory = checkNotNull(descriptorParserFactory);
        this.applications = checkNotNull(applications);
    }

    /**
     * Determines if this deployer can handle this artifact by looking for the plugin descriptor
     *
     * @param pluginArtifact The artifact to test
     * @return The plugin key, null if it cannot load the plugin
     * @throws com.atlassian.plugin.PluginParseException If there are exceptions parsing the plugin configuration
     */
    public String canCreate(PluginArtifact pluginArtifact) throws PluginParseException {
        return getPluginKeyFromDescriptor(pluginArtifact);
    }

    protected final boolean hasDescriptor(PluginArtifact pluginArtifact) {
        InputStream descriptorStream = null;
        try {
            descriptorStream = getDescriptorInputStream(pluginArtifact);
            return descriptorStream != null;
        } finally {
            closeQuietly(descriptorStream);
        }
    }

    protected final String getPluginKeyFromDescriptor(PluginArtifact pluginArtifact) {
        String pluginKey = null;
        InputStream descriptorStream = null;
        try {
            descriptorStream = getDescriptorInputStream(pluginArtifact);
            if (descriptorStream != null) {
                final DescriptorParser descriptorParser = descriptorParserFactory.getInstance(descriptorStream, applications);
                if (isValidPluginsVersion().test(descriptorParser.getPluginsVersion())) {
                    pluginKey = descriptorParser.getKey();
                }
            }
        } finally {
            closeQuietly(descriptorStream);
        }
        return pluginKey;
    }

    protected abstract InputStream getDescriptorInputStream(PluginArtifact pluginArtifact);

    protected abstract Predicate<Integer> isValidPluginsVersion();
}
