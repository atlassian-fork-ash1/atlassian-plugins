package com.atlassian.plugin.loaders;

import com.atlassian.plugin.loaders.classloading.DeploymentUnit;
import org.hamcrest.Matcher;
import org.junit.Test;

import java.io.File;
import java.util.Arrays;
import java.util.Collection;

import static com.atlassian.plugin.loaders.classloading.TestDeploymentUnit.deploymentUnitWithPath;
import static com.atlassian.plugin.test.Matchers.fileNamed;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;

public class TestFileListScanner {
    @Test
    public void testNormalOperation() throws Exception {
        final FileListScanner scanner = new FileListScanner(Arrays.asList(new File("foo.txt"), new File("bar.txt")));
        assertContainsFooAndBar(scanner);

        // Second scan should have no new units.
        assertThat(scanner.scan(), empty());

        scanner.reset();

        assertContainsFooAndBar(scanner);
    }

    private void assertContainsFooAndBar(final FileListScanner scanner) {
        final Collection<DeploymentUnit> scan = scanner.scan();
        final Matcher[] expected = {deploymentUnitWithPath(fileNamed("foo.txt")), deploymentUnitWithPath(fileNamed("bar.txt"))};
        assertThat(scan, contains(expected));
    }
}
