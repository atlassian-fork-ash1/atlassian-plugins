package com.atlassian.plugin.loaders;

import com.google.common.collect.ImmutableMap;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class LoaderUtilsTest {
    @Test
    public void noParamsMeansAnEmptyMap() {
        Element element = DocumentHelper.createElement("parent");
        assertEquals(ImmutableMap.of(), LoaderUtils.getParams(element));
    }

    @Test
    public void getParamsWorksWhenNameAndValueAreSpecified() {
        Element element = DocumentHelper.createElement("parent");

        Element param = DocumentHelper.createElement("param");
        param.addAttribute("name", "param-name");
        param.addAttribute("value", "param-value");
        element.add(param);

        assertEquals(ImmutableMap.of("param-name", "param-value"),
                LoaderUtils.getParams(element));
    }

    /**
     * Allowing maps with null keys and values into the system is not a good idea.
     * This test is to document current behaviour.
     */
    @Test
    public void getParamsReturnsNullMapWhenNameAndValueAreOmitted() {
        Element element = DocumentHelper.createElement("parent");

        Element param = DocumentHelper.createElement("param");
        element.add(param);

        Map<String, String> expected = new HashMap<>();
        expected.put(null, null);

        assertEquals(expected,
                LoaderUtils.getParams(element));
    }

    @Test
    public void valueIsTakenFromElementContentIfNoValueSet() {
        Element element = DocumentHelper.createElement("parent");

        Element param = DocumentHelper.createElement("param");
        param.addAttribute("name", "param-name");
        param.addText("param-content");
        element.add(param);

        assertEquals(ImmutableMap.of("param-name", "param-content"),
                LoaderUtils.getParams(element));
    }
}
