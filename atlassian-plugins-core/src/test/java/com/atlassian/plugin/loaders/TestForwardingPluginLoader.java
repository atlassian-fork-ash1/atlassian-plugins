package com.atlassian.plugin.loaders;

import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginArtifact;
import com.google.common.collect.ImmutableList;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestForwardingPluginLoader {
    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    Plugin plugin;

    @Mock
    PluginArtifact pluginArtifact;

    @Test
    public void forwardsPluginLoaderMethodsToDelegate() {
        PluginLoader delegate = mock(PluginLoader.class);
        final ForwardingPluginLoader loader = new ForwardingPluginLoader(delegate);
        final ModuleDescriptorFactory moduleDescriptorFactory = mock(ModuleDescriptorFactory.class);
        final Plugin pluginB = mock(Plugin.class);
        when(delegate.loadAllPlugins(moduleDescriptorFactory)).thenReturn(ImmutableList.of(plugin, pluginB));
        when(delegate.loadFoundPlugins(moduleDescriptorFactory)).thenReturn(ImmutableList.of(plugin));
        when(delegate.supportsAddition()).thenReturn(true);
        when(delegate.supportsRemoval()).thenReturn(true);
        when(delegate.isDynamicPluginLoader()).thenReturn(false);

        assertThat(loader.loadAllPlugins(moduleDescriptorFactory), containsInAnyOrder(plugin, pluginB));
        verify(delegate).loadAllPlugins(moduleDescriptorFactory);
        assertThat(loader.loadFoundPlugins(moduleDescriptorFactory), containsInAnyOrder(plugin));
        verify(delegate).loadFoundPlugins(moduleDescriptorFactory);
        assertTrue(loader.supportsAddition());
        verify(delegate).supportsAddition();
        assertTrue(loader.supportsRemoval());
        verify(delegate).supportsRemoval();
        loader.removePlugin(pluginB);
        verify(delegate).removePlugin(pluginB);
        assertFalse(loader.isDynamicPluginLoader());
        verify(delegate).isDynamicPluginLoader();
        try {
            loader.canLoad(pluginArtifact);
            fail();
        } catch (IllegalStateException ise) {
            // Expected
        }
        // Should be ignored - there is no state we can check
        loader.discardPlugin(plugin);
    }

    @Test
    public void forwardsDynamicPluginLoaderMethodsToDelegate() {
        DynamicPluginLoader delegate = mock(DynamicPluginLoader.class);
        final ForwardingPluginLoader loader = new ForwardingPluginLoader(delegate);
        final String pluginKey = "plugin-key-A";
        final PluginArtifact pluginArtifactB = mock(PluginArtifact.class);
        when(delegate.isDynamicPluginLoader()).thenReturn(true);
        when(delegate.canLoad(pluginArtifact)).thenReturn(pluginKey);
        when(delegate.canLoad(pluginArtifactB)).thenReturn(null);

        assertEquals(loader.canLoad(pluginArtifact), pluginKey);
        verify(delegate).canLoad(pluginArtifact);
        assertNull(loader.canLoad(pluginArtifactB));
        verify(delegate).canLoad(pluginArtifactB);
        verify(delegate, atLeastOnce()).isDynamicPluginLoader();
    }

    @Test
    public void forwardsDiscardablePluginLoaderMethodsToDelegate() {
        DiscardablePluginLoader delegate = mock(DiscardablePluginLoader.class);
        final ForwardingPluginLoader loader = new ForwardingPluginLoader(delegate);
        loader.discardPlugin(plugin);
        verify(delegate).discardPlugin(plugin);
    }
}
