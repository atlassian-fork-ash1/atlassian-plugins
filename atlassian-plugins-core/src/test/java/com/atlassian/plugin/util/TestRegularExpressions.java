package com.atlassian.plugin.util;

import org.junit.Test;

import java.util.Arrays;
import java.util.regex.Pattern;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class TestRegularExpressions {
    @Test
    public void anyOfEmptyDoesntMatchAnything() {
        final String compound = RegularExpressions.anyOf(Arrays.<String>asList());
        final Pattern pattern = Pattern.compile(compound);
        assertThat(pattern.matcher("").matches(), is(false));
        assertThat(pattern.matcher("a").matches(), is(false));
        // Yes, the next few are a little whitebox, but they're designed to pick up likely implementation faults
        assertThat(pattern.matcher("a/A").matches(), is(false));
        assertThat(pattern.matcher("a\\A").matches(), is(false));
        assertThat(pattern.matcher("a\\\\A").matches(), is(false));
    }

    @Test
    public void anyOfOneMatchesIt() {
        final String compound = RegularExpressions.anyOf(Arrays.asList("a+"));
        final Pattern pattern = Pattern.compile(compound);
        assertThat(pattern.matcher("").matches(), is(false));
        assertThat(pattern.matcher("a").matches(), is(true));
        assertThat(pattern.matcher("aa").matches(), is(true));
        assertThat(pattern.matcher("b").matches(), is(false));
        assertThat(pattern.matcher("bb").matches(), is(false));
        assertThat(pattern.matcher("ab").matches(), is(false));
    }

    @Test
    public void anyOfTwoMatchesEither() {
        final String compound = RegularExpressions.anyOf(Arrays.asList("a+", "b+"));
        final Pattern pattern = Pattern.compile(compound);
        assertThat(pattern.matcher("").matches(), is(false));
        assertThat(pattern.matcher("a").matches(), is(true));
        assertThat(pattern.matcher("aa").matches(), is(true));
        assertThat(pattern.matcher("b").matches(), is(true));
        assertThat(pattern.matcher("bb").matches(), is(true));
        assertThat(pattern.matcher("ab").matches(), is(false));
        assertThat(pattern.matcher("c").matches(), is(false));
        assertThat(pattern.matcher("cc").matches(), is(false));
    }

    @Test
    public void anyOfWithOverlap() {
        final String compound = RegularExpressions.anyOf(Arrays.asList("a(a|b)+b", "a(a|b)+b"));
        final Pattern pattern = Pattern.compile(compound);
        assertThat(pattern.matcher("").matches(), is(false));
        assertThat(pattern.matcher("ab").matches(), is(false));
        assertThat(pattern.matcher("aab").matches(), is(true));
        assertThat(pattern.matcher("abb").matches(), is(true));
        assertThat(pattern.matcher("acb").matches(), is(false));
    }


    @Test
    public void naiveComplexExample() {
        final String compound = RegularExpressions.anyOf(Arrays.asList("ab", "c(d|e)f", "gh*g+"));
        final Pattern pattern = Pattern.compile(compound);
        assertThat(pattern.matcher("").matches(), is(false));
        assertThat(pattern.matcher("a").matches(), is(false));
        assertThat(pattern.matcher("ab").matches(), is(true));
        assertThat(pattern.matcher("cf").matches(), is(false));
        assertThat(pattern.matcher("cdf").matches(), is(true));
        assertThat(pattern.matcher("cef").matches(), is(true));
        assertThat(pattern.matcher("cdef").matches(), is(false));
        assertThat(pattern.matcher("g").matches(), is(false));
        assertThat(pattern.matcher("gg").matches(), is(true));
        assertThat(pattern.matcher("ghg").matches(), is(true));
        assertThat(pattern.matcher("ghgh").matches(), is(false));
    }
}
