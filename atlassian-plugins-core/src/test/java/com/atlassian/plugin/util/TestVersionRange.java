package com.atlassian.plugin.util;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public final class TestVersionRange {
    @Test
    public void testSingleVersion() {
        final VersionRange range = VersionRange.single("2.0");
        assertFalse(range.isInRange("1.0"));
        assertTrue(range.isInRange("2.0"));
        assertFalse(range.isInRange("3.0"));
    }

    @Test
    public void testActualRangeWithIncludes() {
        final VersionRange range = VersionRange.include("2.0").include("4.0");
        assertFalse(range.isInRange("1.0"));
        assertTrue(range.isInRange("2.0"));
        assertTrue(range.isInRange("3.0"));
        assertTrue(range.isInRange("4.0"));
        assertFalse(range.isInRange("5.0"));
    }

    @Test
    public void testActualRangeWithIncludeExclude() {
        final VersionRange range = VersionRange.include("2.0").exclude("4.0");
        assertFalse(range.isInRange("1.0"));
        assertTrue(range.isInRange("2.0"));
        assertTrue(range.isInRange("3.0"));
        assertFalse(range.isInRange("4.0"));
        assertFalse(range.isInRange("5.0"));
    }

    @Test
    public void testActualRangeWithExcludeInclude() {
        final VersionRange range = VersionRange.exclude("2.0").include("4.0");
        assertFalse(range.isInRange("1.0"));
        assertFalse(range.isInRange("2.0"));
        assertTrue(range.isInRange("3.0"));
        assertTrue(range.isInRange("4.0"));
        assertFalse(range.isInRange("5.0"));
    }

    @Test
    public void testActualRangeWithExcludes() {
        final VersionRange range = VersionRange.exclude("2.0").exclude("4.0");
        assertFalse(range.isInRange("1.0"));
        assertFalse(range.isInRange("2.0"));
        assertTrue(range.isInRange("3.0"));
        assertFalse(range.isInRange("4.0"));
        assertFalse(range.isInRange("5.0"));
    }

    @Test
    public void testLeftUnboundedRangeWithIncludes() {
        final VersionRange range = VersionRange.unbounded().include("4.0");

        assertTrue(range.isInRange("1.0"));
        assertTrue(range.isInRange("2.0"));
        assertTrue(range.isInRange("3.0"));
        assertTrue(range.isInRange("4.0"));
        assertFalse(range.isInRange("5.0"));
    }

    @Test
    public void testLeftUnboundedRangeWithExcludes() {
        final VersionRange range = VersionRange.unbounded().exclude("4.0");

        assertTrue(range.isInRange("1.0"));
        assertTrue(range.isInRange("2.0"));
        assertTrue(range.isInRange("3.0"));
        assertFalse(range.isInRange("4.0"));
        assertFalse(range.isInRange("5.0"));
    }

    @Test
    public void testRightUnboundedRangeWithIncludes() {
        final VersionRange range = VersionRange.include("2.0").unbounded();

        assertFalse(range.isInRange("1.0"));
        assertTrue(range.isInRange("2.0"));
        assertTrue(range.isInRange("3.0"));
        assertTrue(range.isInRange("4.0"));
        assertTrue(range.isInRange("5.0"));
    }

    @Test
    public void testRightUnboundedRangeWithExcludes() {
        final VersionRange range = VersionRange.exclude("2.0").unbounded();

        assertFalse(range.isInRange("1.0"));
        assertFalse(range.isInRange("2.0"));
        assertTrue(range.isInRange("3.0"));
        assertTrue(range.isInRange("4.0"));
        assertTrue(range.isInRange("5.0"));
    }

    @Test
    public void testOrRange() {
        final VersionRange range = VersionRange.single("2.0").or(VersionRange.single("4.0"));

        assertFalse(range.isInRange("1.0"));
        assertTrue(range.isInRange("2.0"));
        assertFalse(range.isInRange("3.0"));
        assertTrue(range.isInRange("4.0"));
        assertFalse(range.isInRange("5.0"));
    }

    @Test(expected = IllegalStateException.class)
    public void testAllUnboundedRangeWithExcludes() {
        VersionRange.unbounded().unbounded();
    }

    @Test
    public void testParseSimpleVersion() {
        assertEquals(VersionRange.include("1.0").unbounded(), VersionRange.parse("1.0"));
    }

    @Test
    public void testParseSingleVersion() {
        assertEquals(VersionRange.single("1.0"), VersionRange.parse("[1.0]"));
    }

    @Test
    public void testParseUnboundedLeftVersionRightIncluded() {
        assertEquals(VersionRange.unbounded().include("1.0"), VersionRange.parse("(,1.0]"));
    }

    @Test
    public void testParseUnboundedLeftVersionRightExcluded() {
        assertEquals(VersionRange.unbounded().exclude("1.0"), VersionRange.parse("(,1.0)"));
    }

    @Test
    public void testParseUnboundedRightVersionLeftIncluded() {
        assertEquals(VersionRange.include("1.0").unbounded(), VersionRange.parse("[1.0,)"));
    }

    @Test
    public void testParseUnboundedRightVersionLeftExcluded() {
        assertEquals(VersionRange.exclude("1.0").unbounded(), VersionRange.parse("(1.0,)"));
    }

    @Test
    public void testParseRightIncludedLeftIncluded() {
        assertEquals(VersionRange.include("1.0").include("2.0"), VersionRange.parse("[1.0,2.0]"));
    }

    @Test
    public void testParseRightExcludedLeftIncluded() {
        assertEquals(VersionRange.exclude("1.0").include("2.0"), VersionRange.parse("(1.0,2.0]"));
    }

    @Test
    public void testParseRightIncludedLeftExcluded() {
        assertEquals(VersionRange.include("1.0").exclude("2.0"), VersionRange.parse("[1.0,2.0)"));
    }

    @Test
    public void testParseRightExcludedLeftExcluded() {
        assertEquals(VersionRange.exclude("1.0").exclude("2.0"), VersionRange.parse("(1.0,2.0)"));
    }
}
