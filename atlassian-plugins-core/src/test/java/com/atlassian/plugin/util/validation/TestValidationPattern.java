package com.atlassian.plugin.util.validation;

import com.atlassian.plugin.PluginParseException;
import org.dom4j.DocumentFactory;
import org.dom4j.Element;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static com.atlassian.plugin.util.validation.ValidationPattern.createPattern;
import static com.atlassian.plugin.util.validation.ValidationPattern.test;

public class TestValidationPattern {

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    private Element root;

    @Before
    public void setUp() {
        DocumentFactory factory = DocumentFactory.getInstance();
        root = factory.createElement("root");
        root.addAttribute("foo", "bar");
        Element child = factory.createElement("child");
        child.addAttribute("some", "thing");
        child.setText("mybody");
        Element emptyChild = factory.createElement("child");
        root.add(child);
        root.add(emptyChild);
    }

    @Test
    public void testSuccess() {
        createPattern().
                rule(".",
                        test("child").withError("Child is required"),
                        test("not(baz)").withError("Baz should not exist")).
                rule("child[1]",
                        test(".[@some = 'thing']").withError("Need some attribute")).
                evaluate(root);

    }

    /*
    @Test
    public void testSuccessPerfTest()
    {
        ValidationPattern ptn = createPattern().
                rule(".",
                        test("child").withError("Child is required"),
                        test("not(baz)").withError("Baz should not exist")).
                rule("child[1]",
                        test(".[@some = 'thing']").withError("Need some attribute"));

        for (int x=0; x<1000; x++)
        {
            ptn.evaluate(root);
        }

        long start = System.currentTimeMillis();
        for (int x=0; x<5000; x++)
        {
            ptn.evaluate(root);
        }
        long end = System.currentTimeMillis();
        System.out.println("Time elapsed: "+(end-start)+" ms");

    }*/

    @Test
    public void testErrorMessageWithEmptyList() {
        expectedException.expect(PluginParseException.class);
        expectedException.expectMessage(Matchers.startsWith("Baz should exist"));

        createPattern().
                rule(".",
                        test("baz").withError("Baz should exist")).
                evaluate(root);
    }

    @Test
    public void testErrorMessageWithNull() {
        expectedException.expect(PluginParseException.class);
        expectedException.expectMessage(Matchers.startsWith("Baz should exist"));

        createPattern().
                rule(".",
                        test("baz[1]").withError("Baz should exist")).
                evaluate(root);
    }

    @Test
    public void testErrorMessageWithBoolean() {
        expectedException.expect(PluginParseException.class);
        expectedException.expectMessage(Matchers.startsWith("Baz should exist"));

        createPattern().
                rule(".",
                        test("not(not(baz))").withError("Baz should exist")).
                evaluate(root);
    }
}
