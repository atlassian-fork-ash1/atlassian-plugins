package com.atlassian.plugin.predicate;

import com.atlassian.plugin.ModuleDescriptor;
import com.mockobjects.dynamic.Mock;
import org.junit.Before;
import org.junit.Test;

import java.util.function.Predicate;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Testing {@link ModuleOfClassPredicate}
 */
public class TestModuleOfClassPredicate {
    private Predicate<ModuleDescriptor<StubModule>> moduleDescriptorPredicate;

    private Mock mockModuleDescriptor;
    private ModuleDescriptor<StubModule> moduleDescriptor;

    @Before
    public void setUp() throws Exception {
        moduleDescriptorPredicate = new ModuleOfClassPredicate<>(StubModule.class);

        mockModuleDescriptor = new Mock(ModuleDescriptor.class);
        moduleDescriptor = (ModuleDescriptor<StubModule>) mockModuleDescriptor.proxy();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCannotCreateWithNullClass() {
        new ModuleOfClassPredicate<>(null);
    }

    @Test
    public void testMatchesModuleExtendingClass() {
        mockModuleDescriptor.matchAndReturn("getModuleClass", StubSubclassModule.class);
        assertTrue(moduleDescriptorPredicate.test(moduleDescriptor));
    }

    @Test
    public void testDoesNotMatchModuleNotExtendingClass() {
        mockModuleDescriptor.matchAndReturn("getModuleClass", Object.class);
        assertFalse(moduleDescriptorPredicate.test(moduleDescriptor));
    }

    private static class StubModule {
    }

    private static class StubSubclassModule extends StubModule {
    }
}
