package com.atlassian.plugin;

import com.atlassian.plugin.test.PluginJarBuilder;
import com.google.common.collect.ImmutableMap;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Collections;

import static com.atlassian.plugin.ReferenceMode.FORBID_REFERENCE;
import static com.atlassian.plugin.ReferenceMode.PERMIT_REFERENCE;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public final class TestJarPluginArtifact {
    @Test
    public void testContainsSpringContextWithXml() throws IOException {
        final JarPluginArtifact artifact = new JarPluginArtifact(new PluginJarBuilder()
                .addResource("META-INF/spring/components.xml", "data")
                .build());

        assertThat(artifact.containsSpringContext(), equalTo(true));
    }

    @Test
    public void testContainsSpringContextWithManifest() throws IOException {
        final JarPluginArtifact artifact = new JarPluginArtifact(new PluginJarBuilder()
                .manifest(Collections.singletonMap("Spring-Context", "*"))
                .build());

        assertThat(artifact.containsSpringContext(), equalTo(true));
    }

    @Test
    public void testContainsSpringContextNoContext() throws IOException {
        final JarPluginArtifact artifact = new JarPluginArtifact(new PluginJarBuilder()
                .build());

        assertThat(artifact.containsSpringContext(), equalTo(false));
    }

    @Test
    public void testGetResourceAsStream() throws IOException {
        final JarPluginArtifact artifact = new JarPluginArtifact(new PluginJarBuilder()
                .addResource("foo", "bar")
                .build());

        assertNotNull(artifact.getResourceAsStream("foo"));
        assertNull(artifact.getResourceAsStream("bar"));
    }

    @Test
    public void testContainsJavaExecutableCodeWithNoJava() throws Exception {
        final JarPluginArtifact artifact = new JarPluginArtifact(new PluginJarBuilder()
                .addResource("foo", "bar")
                .build());

        assertFalse(artifact.containsJavaExecutableCode());
    }

    @Test
    public void testContainsJavaExecutableCodeWithNoJavaAndNoManifest() throws Exception {
        final JarPluginArtifact artifact = new JarPluginArtifact(new PluginJarBuilder()
                .addResource("foo", "bar")
                .buildWithNoManifest());

        assertFalse(artifact.containsJavaExecutableCode());
    }

    @Test
    public void testContainsJavaExecutableCodeAsJavaClass() throws Exception {
        final JarPluginArtifact artifact = new JarPluginArtifact(new PluginJarBuilder()
                .addFormattedJava("foo.Bar",
                        "package foo;",
                        "public final class Bar {}")
                .build());

        assertTrue(artifact.containsJavaExecutableCode());
    }

    @Test
    public void testContainsJavaExecutableCodeAsJavaLibrary() throws Exception {
        File innerJar = new PluginJarBuilder().build();

        final JarPluginArtifact artifact = new JarPluginArtifact(new PluginJarBuilder()
                .addFile("innerJar.jar", innerJar)
                .build());

        assertTrue(artifact.containsJavaExecutableCode());
    }

    @Test
    public void testContainsJavaExecutableCodeAsSpringContextXmlFiles() throws Exception {
        final JarPluginArtifact artifact = new JarPluginArtifact(new PluginJarBuilder()
                .addFormattedResource("META-INF/spring/file.xml", "bla")
                .build());

        assertTrue(artifact.containsJavaExecutableCode());
    }

    @Test
    public void testContainsJavaExecutableCodeAsSpringContextManifestEntry() throws Exception {
        final JarPluginArtifact artifact = new JarPluginArtifact(new PluginJarBuilder()
                .manifest(ImmutableMap.of("Spring-Context", "bla"))
                .build());

        assertTrue(artifact.containsJavaExecutableCode());
    }

    @Test
    public void testContainsJavaExecutableCodeAsBundleActivator() throws Exception {
        final JarPluginArtifact artifact = new JarPluginArtifact(new PluginJarBuilder()
                .manifest(ImmutableMap.of("Bundle-Activator", "bla"))
                .build());

        assertTrue(artifact.containsJavaExecutableCode());
    }

    @Test
    public void referenceModeDefaultsToForbid() {
        final JarPluginArtifact jarPluginArtifact = new JarPluginArtifact(new File("some.jar"));
        assertThat(jarPluginArtifact.getReferenceMode(), is(FORBID_REFERENCE));
    }

    @Test
    public void referenceModeForbidReferenceDoesNotAllowReference() {
        final JarPluginArtifact jarPluginArtifact = new JarPluginArtifact(new File("some.jar"), FORBID_REFERENCE);
        assertThat(jarPluginArtifact.getReferenceMode(), is(FORBID_REFERENCE));
    }

    @Test
    public void referenceModePermitReferenceAllowsReference() {
        final JarPluginArtifact jarPluginArtifact = new JarPluginArtifact(new File("some.jar"), PERMIT_REFERENCE);
        assertThat(jarPluginArtifact.getReferenceMode(), is(PERMIT_REFERENCE));
    }

    @Test
    public void testExtraModuleDescriptorFiles() throws Exception {
        final JarPluginArtifact jarPluginArtifact = new JarPluginArtifact(new PluginJarBuilder("plugin")
                .addResource("META-INF/atlassian/foo.xml", "<atlassian-plugin/>")
                .build());
        assertThat(jarPluginArtifact.extraModuleDescriptorFiles("META-INF/atlassian"), hasItem("META-INF/atlassian/foo.xml"));
    }

    @Test
    public void testExtraModuleDescriptorFilesIgnoresSubfolders() throws Exception {
        final JarPluginArtifact jarPluginArtifact = new JarPluginArtifact(new PluginJarBuilder("plugin")
                .addResource("META-INF/atlassian/bar/foo.xml", "<atlassian-plugin/>")
                .build());
        assertThat(jarPluginArtifact.extraModuleDescriptorFiles("META-INF/atlassian"), empty());
    }

    @Test
    public void testExtraModuleDescriptorFilesIgnoresNonXmlFiles() throws Exception {
        final JarPluginArtifact jarPluginArtifact = new JarPluginArtifact(new PluginJarBuilder("plugin")
                .addResource("META-INF/atlassian/foo.txt", "<atlassian-plugin/>")
                .build());
        assertThat(jarPluginArtifact.extraModuleDescriptorFiles("META-INF/atlassian"), empty());
    }
}
