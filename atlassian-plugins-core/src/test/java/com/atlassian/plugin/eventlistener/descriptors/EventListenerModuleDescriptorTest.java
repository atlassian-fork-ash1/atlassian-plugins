package com.atlassian.plugin.eventlistener.descriptors;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.module.ModuleFactory;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.RestoreSystemProperties;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static com.atlassian.plugin.eventlistener.descriptors.EventListenerModuleDescriptor.FALLBACK_MODE;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class EventListenerModuleDescriptorTest {
    @Rule
    public final RestoreSystemProperties restoreSystemProperties = new RestoreSystemProperties();

    @Mock
    private ModuleFactory factory;
    @Mock
    private Object module;
    @Mock
    private EventPublisher publisher;


    @Test
    public void enabled() {
        final EventListenerModuleDescriptor descriptor = new EventListenerModuleDescriptor(factory, publisher);
        descriptor.enabled();

        verify(publisher).register(descriptor);
    }

    @Test
    public void disabled() {
        final EventListenerModuleDescriptor descriptor = new EventListenerModuleDescriptor(factory, publisher);
        descriptor.disabled();

        verify(publisher).unregister(descriptor);
    }

    @Test
    public void enabledInFallbackMode() {
        System.setProperty(FALLBACK_MODE, "true");
        when(factory.createModule(any(), any())).thenReturn(module);

        final EventListenerModuleDescriptor descriptor = new EventListenerModuleDescriptor(factory, publisher);
        descriptor.enabled();

        verify(publisher).register(module);
    }

    @Test
    public void disabledInFallbackMode()  {
        System.setProperty(FALLBACK_MODE, "true");

        when(factory.createModule(any(), any())).thenReturn(module);

        final EventListenerModuleDescriptor descriptor = new EventListenerModuleDescriptor(factory, publisher);
        descriptor.disabled();

        verify(publisher).unregister(module);
    }
}