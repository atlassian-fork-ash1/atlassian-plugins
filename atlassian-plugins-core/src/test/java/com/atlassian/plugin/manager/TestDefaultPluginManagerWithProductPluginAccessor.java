package com.atlassian.plugin.manager;

import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginRegistry;
import com.atlassian.plugin.event.PluginEventManager;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class TestDefaultPluginManagerWithProductPluginAccessor extends TestDefaultPluginManager {

    private interface PluginAccessorSupplier {
        PluginAccessor supply(final PluginRegistry.ReadOnly pluginRegistry,
                              final PluginPersistentStateStore store,
                              final ModuleDescriptorFactory moduleDescriptorFactory,
                              final PluginEventManager pluginEventManager);
    }

    @Parameterized.Parameters(name = "{0}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"ProductPluginAccessor", new PluginAccessorSupplier() {
                    @Override
                    public PluginAccessor supply(final PluginRegistry.ReadOnly pluginRegistry,
                                                 final PluginPersistentStateStore store,
                                                 final ModuleDescriptorFactory moduleDescriptorFactory,
                                                 final PluginEventManager pluginEventManager) {
                        return new ProductPluginAccessor(pluginRegistry, store, moduleDescriptorFactory, pluginEventManager);
                    }
                }},
                {"ProductPluginAccessorBase", new PluginAccessorSupplier() {
                    @Override
                    public PluginAccessor supply(final PluginRegistry.ReadOnly pluginRegistry,
                                                 final PluginPersistentStateStore store,
                                                 final ModuleDescriptorFactory moduleDescriptorFactory,
                                                 final PluginEventManager pluginEventManager) {
                        return new ProductPluginAccessorBase(pluginRegistry, store, moduleDescriptorFactory, pluginEventManager);
                    }
                }}
        });
    }

    @Parameterized.Parameter
    public String testName;

    @Parameterized.Parameter(1)
    public PluginAccessorSupplier pluginAccessorSupplier;

    private PluginAccessor pluginAccessor;

    /**
     * Whenever the plugin manager is recreated, set up the plugin accessor used by the base class tests to use the
     * product accessor
     */
    @Override
    protected DefaultPluginManager newDefaultPluginManager(DefaultPluginManager.Builder builder) {
        final PluginRegistry.ReadWrite pluginRegistry = new PluginRegistryImpl();

        pluginAccessor = pluginAccessorSupplier.supply(pluginRegistry, pluginStateStore, moduleDescriptorFactory, pluginEventManager);

        builder
                .withPluginRegistry(pluginRegistry)
                .withPluginAccessor(pluginAccessor);
        manager = super.newDefaultPluginManager(builder);

        return manager;
    }

    /**
     * Make base class tests use our product plugin accessor
     */
    @Override
    protected PluginAccessor getPluginAccessor() {
        return pluginAccessor;
    }
}
