package com.atlassian.plugin.manager;

import com.atlassian.plugin.ModuleCompleteKey;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginDependencies;
import com.atlassian.plugin.PluginInformation;
import com.atlassian.plugin.PluginRestartState;
import com.atlassian.plugin.PluginState;
import com.atlassian.plugin.StateAware;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.PluginEvent;
import com.atlassian.plugin.event.events.PluginModuleEvent;
import com.atlassian.plugin.impl.StaticPlugin;
import com.atlassian.plugin.loaders.PluginLoader;
import com.atlassian.plugin.module.ModuleFactory;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeMatcher;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.AnyOf.anyOf;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.withSettings;

class DefaultPluginManagerMocks {
    public enum FailureMode {
        FAIL_TO_ENABLE,
        FAIL_TO_DISABLE
    }

    public static ModuleDescriptor<Object> mockFailingModuleDescriptor(final String completeKey, final FailureMode... failureModes) {
        return new AbstractModuleDescriptor<Object>(ModuleFactory.LEGACY_MODULE_FACTORY) {
            @Override
            public String getKey() {
                return completeKey.substring(completeKey.lastIndexOf(":") + 1, completeKey.length());
            }

            @Override
            public String getCompleteKey() {
                return completeKey;
            }

            @Override
            public void enabled() {
                if (Lists.newArrayList(failureModes).contains(FailureMode.FAIL_TO_ENABLE))
                    throw new IllegalArgumentException("Cannot enable");
            }

            @Override
            public void disabled() {
                if (Lists.newArrayList(failureModes).contains(FailureMode.FAIL_TO_DISABLE))
                    throw new IllegalArgumentException("Cannot disable");
            }

            @Override
            public Object getModule() {
                return null;
            }
        };
    }

    public static PluginLoader mockPluginLoaderForPlugins(final Plugin... plugins) {
        final PluginLoader pluginLoader = mock(PluginLoader.class);
        when(pluginLoader.loadAllPlugins(isA(ModuleDescriptorFactory.class))).thenReturn(Arrays.asList(plugins));
        when(pluginLoader.supportsRemoval()).thenReturn(true);
        return pluginLoader;
    }

    public static void mockPluginsSortOrder(final Plugin... mockPlugins) {
        for (int i = 0; i < mockPlugins.length; i++) {
            for (int j = 0; j < mockPlugins.length; j++) {
                when(mockPlugins[i].compareTo(mockPlugins[j])).thenReturn(i - j);
            }
        }
    }

    public static PluginPersistentStateStore mockPluginPersistentStateStore() {
        final PluginPersistentStateStore pluginPersistentStateStore = mock(PluginPersistentStateStore.class, RETURNS_DEEP_STUBS);
        when(pluginPersistentStateStore.load().getPluginRestartState(anyString())).thenReturn(PluginRestartState.NONE);
        when(pluginPersistentStateStore.load().isEnabled(any(Plugin.class))).thenReturn(true);
        when(pluginPersistentStateStore.load().isEnabled(any(ModuleDescriptor.class))).thenReturn(true);
        return pluginPersistentStateStore;
    }

    public static Plugin mockStateChangePlugin(final String pluginKey, final PluginEventManager pluginEventManager) {
        final Plugin plugin = mockPlugin(pluginKey);

        doAnswerPluginStateChangeWhen(plugin, PluginState.INSTALLED, pluginEventManager).install();
        doAnswerPluginStateChangeWhen(plugin, PluginState.ENABLED, pluginEventManager).enable();
        doAnswerPluginStateChangeWhen(plugin, PluginState.DISABLED, pluginEventManager).disable();
        return plugin;
    }

    public static Plugin mockPluginWithVersion(final String pluginKey, final String version) {
        final Plugin plugin = mockPlugin(pluginKey);
        when(plugin.getPluginInformation().getVersion()).thenReturn(version);
        return plugin;
    }

    public static Plugin mockPlugin(final String pluginKey) {
        final Plugin plugin = mock(Plugin.class, RETURNS_DEEP_STUBS);
        when(plugin.getKey()).thenReturn(pluginKey);
        when(plugin.getPluginState()).thenReturn(PluginState.UNINSTALLED);
        when(plugin.getModuleDescriptors()).thenReturn(ImmutableList.of());
        when(plugin.getPluginInformation().satisfiesMinJavaVersion()).thenReturn(true);
        when(plugin.isUninstallable()).thenReturn(true);
        when(plugin.toString()).thenAnswer(new Answer<String>() {
            @Override
            public String answer(InvocationOnMock invocation) throws Throwable {
                return plugin.getKey() + ":" + plugin.getPluginInformation().getVersion();
            }
        });
        return plugin;
    }

    public static Plugin mockTestPlugin(Collection moduleDescriptors) {
        final Plugin mockPlugin = mock(Plugin.class);
        when(mockPlugin.getKey()).thenReturn("some-plugin-key");
        when(mockPlugin.isEnabledByDefault()).thenReturn(true);
        when(mockPlugin.getPluginState()).thenReturn(PluginState.ENABLED);
        when(mockPlugin.getModuleDescriptors()).thenReturn(moduleDescriptors);
        when(mockPlugin.getDependencies()).thenReturn(new PluginDependencies());
        return mockPlugin;
    }

    public static Plugin mockStaticPlugin(final String pluginKey, final ModuleDescriptor<?>... descriptors) {
        return new StaticPlugin() {
            {
                setPluginInformation(new PluginInformation());
                setEnabledByDefault(true);
                setKey(pluginKey);
            }

            @Override
            public Collection<ModuleDescriptor<?>> getModuleDescriptors() {
                return Arrays.<ModuleDescriptor<?>>asList(descriptors);
            }

            @Override
            public ModuleDescriptor<Object> getModuleDescriptor(final String moduleKey) {
                for (ModuleDescriptor desc : descriptors) {
                    if (desc.getKey().equals(moduleKey))
                        return desc;
                }
                return null;
            }
        };
    }

    /**
     * Obtain an answer which updates {@link Plugin#getPluginState()} and sends a marked event when called.
     *
     * @param plugin             the Plugin to stub.
     * @param pluginState        the plugin state to report
     * @param pluginEventManager the {@link PluginEventManager} to broadcast a {@link PluginStateMarkerEvent} via.
     * @return an answer which updates {@code plugin.getPluginState()} and fires a {@code PluginStateMarkerEvent}.
     */
    public static Plugin doAnswerPluginStateChangeWhen(
            final Plugin plugin, final PluginState pluginState, final PluginEventManager pluginEventManager) {
        final Answer answer = new Answer() {
            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable {
                when(plugin.getPluginState()).thenReturn(pluginState);
                pluginEventManager.broadcast(new PluginStateMarkerEvent(plugin, pluginState));
                return null;
            }
        };
        return doAnswer(answer).when(plugin);
    }

    public static ModuleDescriptor<?> mockStateChangePluginModule(
            final String pluginKey, final String moduleKey, final PluginEventManager pluginEventManager) {
        final ModuleDescriptor<?> module = mock(ModuleDescriptor.class, withSettings().extraInterfaces(StateAware.class));
        when(module.getPluginKey()).thenReturn(pluginKey);
        when(module.getCompleteKey()).thenReturn(new ModuleCompleteKey(pluginKey, moduleKey).getCompleteKey());
        doAnswerModuleStateChangeWhen(module, true, pluginEventManager).enabled();
        doAnswerModuleStateChangeWhen(module, false, pluginEventManager).disabled();
        return module;
    }

    public static Matcher<PluginModuleEvent> pluginModuleStateChange(final ModuleDescriptor module, final boolean enabled) {
        final Matcher<PluginModuleEvent> pluginModuleEventMatcher = pluginModuleEvent(PluginModuleStateMarkerEvent.class, module);
        final Matcher<Boolean> pluginModuleStateMatcher = is(enabled);

        return new TypeSafeMatcher<PluginModuleEvent>() {
            @Override
            protected boolean matchesSafely(final PluginModuleEvent event) {
                return pluginModuleEventMatcher.matches(event)
                        // pluginModuleEventMatcher matches, so event instanceof PluginModuleStateMarkerEvent, so this cast is safe
                        && pluginModuleStateMatcher.matches(((PluginModuleStateMarkerEvent) event).isEnabled());
            }

            @Override
            public void describeTo(final Description description) {
                pluginModuleEventMatcher.describeTo(description);
                description.appendText(" and .isEnabled() ");
                pluginModuleStateMatcher.describeTo(description);
            }
        };
    }

    /**
     * Obtain an answer which updates {@link ModuleDescriptor#isEnabled} and sends a marked event when called.
     *
     * @param module             the moduleDescriptor to stub, which must also extend {@link StateAware}.
     * @param enabled            the enabled state to report
     * @param pluginEventManager the {@link PluginEventManager} to broadcast a {@link PluginModuleStateMarkerEvent} via.
     * @return an answer which stubs {@code moduleDescriptor.isEnabled} and fires a {@code PluginModuleStateMarkerEvent}.
     */
    public static StateAware doAnswerModuleStateChangeWhen(
            final ModuleDescriptor<?> module, final boolean enabled, final PluginEventManager pluginEventManager) {
        final Answer answer = new Answer() {
            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable {
                when(module.isEnabled()).thenReturn(enabled);
                pluginEventManager.broadcast(new PluginModuleStateMarkerEvent(module, enabled));
                return null;
            }
        };
        return doAnswer(answer).when((StateAware) module);
    }

    /**
     * This is a trick to conveniently check sequencing of plugin module enabled change functions wrt other events.
     */
    public static class PluginModuleStateMarkerEvent extends PluginModuleEvent {
        private final boolean enabled;

        PluginModuleStateMarkerEvent(final ModuleDescriptor module, final boolean enabled) {
            super(module);
            this.enabled = enabled;
        }

        public boolean isEnabled() {
            return enabled;
        }

        @Override
        public String toString() {
            return super.toString() + " with enabled " + Boolean.toString(enabled);
        }
    }

    public static Matcher<PluginModuleEvent> pluginModuleEvent(final Class clazz, final ModuleDescriptor module) {
        final Matcher<Class> classMatcher = instanceOf(clazz);
        final Matcher<ModuleDescriptor> pluginModuleMatcher = is(module);
        return new TypeSafeMatcher<PluginModuleEvent>() {
            @Override
            protected boolean matchesSafely(final PluginModuleEvent pluginModuleEvent) {
                return classMatcher.matches(pluginModuleEvent) && pluginModuleMatcher.matches(pluginModuleEvent.getModule());
            }

            @Override
            public void describeTo(final Description description) {
                classMatcher.describeTo(description);
                description.appendText(" for which .getModule() ");
                pluginModuleMatcher.describeTo(description);
            }
        };
    }

    /**
     * This is a trick to conveniently check sequencing of plugin state change functions wrt other events.
     */
    public static class PluginStateMarkerEvent extends PluginEvent {
        private final PluginState pluginState;

        PluginStateMarkerEvent(final Plugin plugin, final PluginState pluginState) {
            super(plugin);
            this.pluginState = pluginState;
        }

        public PluginState getPluginState() {
            return pluginState;
        }

        @Override
        public String toString() {
            return super.toString() + " with pluginState " + pluginState;
        }
    }

    public static Matcher<PluginEvent> anyPluginStateChange(final PluginState pluginState, final Plugin... plugin) {
        final Matcher<PluginEvent> pluginEventMatcher = anyPluginEvent(PluginStateMarkerEvent.class, plugin);
        final Matcher<PluginState> pluginStateMatcher = is(pluginState);

        return new TypeSafeMatcher<PluginEvent>() {
            @Override
            protected boolean matchesSafely(final PluginEvent event) {
                return pluginEventMatcher.matches(event)
                        // pluginEventMatcher matches, so event instanceof PluginStateMarkerEvent, so this cast is safe
                        && pluginStateMatcher.matches(((PluginStateMarkerEvent) event).getPluginState());
            }

            @Override
            public void describeTo(final Description description) {
                pluginEventMatcher.describeTo(description);
                description.appendText(" and .getPluginState() ");
                pluginStateMatcher.describeTo(description);
            }
        };
    }

    public static Matcher<PluginEvent> pluginStateChange(final PluginState pluginState, final Plugin plugin) {
        return anyPluginStateChange(pluginState, plugin);
    }

    public static Matcher<PluginEvent> anyPluginEvent(final Class clazz, final Plugin... plugins) {
        final Matcher<Class> classMatcher = instanceOf(clazz);
        final Matcher<Plugin> pluginMatcher = anyOf(stream(plugins).map(Matchers::is).collect(toList()));

        return new TypeSafeMatcher<PluginEvent>() {
            @Override
            protected boolean matchesSafely(final PluginEvent pluginEvent) {
                return classMatcher.matches(pluginEvent) && pluginMatcher.matches(pluginEvent.getPlugin());
            }

            @Override
            public void describeTo(final Description description) {
                classMatcher.describeTo(description);
                description.appendText(" for which .getPlugin() ");
                pluginMatcher.describeTo(description);
            }
        };
    }

    public static Matcher<PluginEvent> pluginEvent(final Class clazz, final Plugin plugin) {
        return anyPluginEvent(clazz, plugin);
    }
}
