package com.atlassian.plugin.manager;

import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginDependencies;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.loaders.PluginLoader;
import com.google.common.base.Predicates;
import com.google.common.collect.Collections2;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import static com.atlassian.plugin.PluginDependencies.Type.DYNAMIC;
import static com.atlassian.plugin.PluginDependencies.Type.MANDATORY;
import static com.atlassian.plugin.PluginDependencies.Type.OPTIONAL;
import static com.atlassian.plugin.manager.PluginWithDeps.GET_KEY;

public class TestDefaultPluginManagerDependentPlugins {
    private Collection<Plugin> enabledPlugins;
    private ImmutableMap<String, Plugin> pluginsByKey;
    private DefaultPluginManager pluginManager;

    @Before
    public void setUp() throws Exception {
        enabledPlugins = ImmutableList.of(
                // Draw it on paper first if you want to modify it, the more letters in the key the lower row
                new PluginWithDeps("root"),

                new PluginWithDeps("a", "root"),
                new PluginWithDeps("b", "root", "a"),
                new PluginWithDeps("c", "root"),

                new PluginWithDeps("aa", "a"),
                new PluginWithDeps("ba", "b"),
                new PluginWithDeps("bb", "b"),
                new PluginWithDeps("ca", "c"),
                new PluginWithDeps("cb", "c", "ca"),

                new PluginWithDeps("aaa", "aa"),
                new PluginWithDeps("caa", "c", "ca", "cb"),

                new PluginWithDeps("disconnected.a"),
                new PluginWithDeps("disconnected.b", "disconnected.a"),
                new PluginWithDeps("disconnected.c", "disconnected.b")
        );

        pluginsByKey = Maps.uniqueIndex(enabledPlugins, GET_KEY);

        pluginManager = new DefaultPluginManager(Mockito.mock(PluginPersistentStateStore.class), Collections.<PluginLoader>emptyList(),
                Mockito.mock(ModuleDescriptorFactory.class), Mockito.mock(PluginEventManager.class));
    }

    private Set<String> getDependentPluginKeys(String parentKey) {
        return getDependentPluginKeys(pluginsByKey.get(parentKey), enabledPlugins);
    }

    private Set<String> getDependentPluginKeys(Plugin root, Collection<Plugin> enabledPlugins) {
        final DependentPlugins dependentPlugins = new DependentPlugins(ImmutableList.of(root.getKey()), enabledPlugins, ImmutableSet.of(MANDATORY, OPTIONAL, DYNAMIC));
        return ImmutableSet.copyOf(Collections2.transform(dependentPlugins.getPlugins(false), GET_KEY));
    }

    @Test
    public void testNoDependencies() throws Exception {
        Assert.assertEquals(ImmutableSet.of(), getDependentPluginKeys("aaa"));
    }

    @Test
    public void testSingleDependency() throws Exception {
        Assert.assertEquals(ImmutableSet.of("aaa"), getDependentPluginKeys("aa"));
    }

    @Test
    public void testSimpleTransitiveDependency() throws Exception {
        Assert.assertEquals(ImmutableSet.of("disconnected.b", "disconnected.c"), getDependentPluginKeys("disconnected.a"));
    }

    @Test
    public void testTransitiveDependency() throws Exception {
        Assert.assertEquals(ImmutableSet.of("aa", "aaa", "b", "ba", "bb"), getDependentPluginKeys("a"));
    }

    @Test
    public void testCrossLayerDependency() throws Exception {
        Assert.assertEquals(ImmutableSet.of("ca", "cb", "caa"), getDependentPluginKeys("c"));
    }

    @Test
    public void testFullTreeDependency() throws Exception {
        final Set<String> notDisconnected = Sets.filter(pluginsByKey.keySet(), Predicates.not(Predicates.contains(
                Pattern.compile("disconnected|root"))));
        Assert.assertEquals(notDisconnected, getDependentPluginKeys("root"));
    }

    @Test(timeout = 1000)
    public void testCircularDependency() throws Exception {
        final ImmutableList<Plugin> plugins = ImmutableList.of(
                new PluginWithDeps("a", "c"),
                new PluginWithDeps("b", "a"),
                new PluginWithDeps("c", "b")
        );
        Assert.assertEquals(ImmutableSet.of("b", "c"), getDependentPluginKeys(plugins.get(0), plugins));
    }

    @Test(timeout = 1000)
    public void testVeryDeepDependencyTreePerformance() throws Exception {
        final Map<String, Plugin> plugins = Maps.newHashMap();
        plugins.put("root", new PluginWithDeps("root"));

        for (int lvl = 0; lvl < 10; lvl++) {
            final Set<String> existingKeys = ImmutableSet.copyOf(plugins.keySet());
            for (int i = 0; i < 10; i++) {
                final String key = lvl + "." + i;
                plugins.put(key, new PluginWithDeps(key, new PluginDependencies(existingKeys, null, null)));
            }
        }

        final Set<String> allButRoot = Sets.difference(plugins.keySet(), ImmutableSet.of("root"));
        Assert.assertEquals(allButRoot, getDependentPluginKeys(plugins.get("root"), plugins.values()));
    }
}
