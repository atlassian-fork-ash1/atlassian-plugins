package com.atlassian.plugin.manager;

import com.atlassian.plugin.DefaultModuleDescriptorFactory;
import com.atlassian.plugin.JarPluginArtifact;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.PluginDisabledEvent;
import com.atlassian.plugin.event.events.PluginEnabledEvent;
import com.atlassian.plugin.event.events.PluginModuleDisabledEvent;
import com.atlassian.plugin.event.events.PluginModuleEnabledEvent;
import com.atlassian.plugin.event.events.PluginTransactionStartEvent;
import com.atlassian.plugin.event.events.PluginTransactionEndEvent;
import com.atlassian.plugin.event.impl.DefaultPluginEventManager;
import com.atlassian.plugin.event.listeners.RecordingListener;
import com.atlassian.plugin.factories.LegacyDynamicPluginFactory;
import com.atlassian.plugin.hostcontainer.DefaultHostContainer;
import com.atlassian.plugin.loaders.DirectoryPluginLoader;
import com.atlassian.plugin.loaders.PluginLoader;
import com.atlassian.plugin.loaders.SinglePluginLoader;
import com.atlassian.plugin.loaders.classloading.DirectoryPluginLoaderUtils;
import com.atlassian.plugin.manager.store.MemoryPluginPersistentStateStore;
import com.atlassian.plugin.mock.MockAnimalModuleDescriptor;
import com.atlassian.plugin.mock.MockMineralModuleDescriptor;
import com.atlassian.plugin.mock.MockVegetableModuleDescriptor;
import com.atlassian.plugin.repositories.FilePluginInstaller;
import com.google.common.collect.ImmutableList;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestDefaultPluginManagerEvents {
    private DefaultPluginManager manager;
    private RecordingListener listener;

    @Before
    public void setUp() throws Exception {
        listener = new RecordingListener(
                PluginTransactionStartEvent.class,
                PluginTransactionEndEvent.class,
                PluginEnabledEvent.class,
                PluginDisabledEvent.class,
                PluginModuleEnabledEvent.class,
                PluginModuleDisabledEvent.class);

        manager = buildPluginManager(listener);
        manager.init();
        listener.reset();
    }

    @After
    public void tearDown() {
        // prevent resources being used until end of all tests
        manager = null;
        listener = null;
    }

    @Test
    public void testInitialisationEvents() throws Exception {
        DefaultPluginManager manager = buildPluginManager(listener);
        manager.init();

        assertListEquals(listener.getEventClasses(),
                PluginTransactionStartEvent.class,
                PluginModuleEnabledEvent.class,
                PluginModuleEnabledEvent.class,
                PluginModuleEnabledEvent.class,
                PluginEnabledEvent.class,
                PluginModuleEnabledEvent.class,
                PluginEnabledEvent.class,
                PluginModuleEnabledEvent.class,
                PluginEnabledEvent.class,
                PluginTransactionEndEvent.class);
        assertListEquals(listener.getEventPluginOrModuleKeys(),
                asList(null,
                        "test.atlassian.plugin:bear",
                        "test.atlassian.plugin:gold",
                        "test.atlassian.plugin:veg",
                        "test.atlassian.plugin",
                        "test.atlassian.plugin.classloaded:paddington",
                        "test.atlassian.plugin.classloaded",
                        "test.atlassian.plugin.classloaded2:pooh",
                        "test.atlassian.plugin.classloaded2",
                        null),
                asList(null,
                        "test.atlassian.plugin:bear",
                        "test.atlassian.plugin:gold",
                        "test.atlassian.plugin:veg",
                        "test.atlassian.plugin",
                        "test.atlassian.plugin.classloaded2:pooh",
                        "test.atlassian.plugin.classloaded2",
                        "test.atlassian.plugin.classloaded:paddington",
                        "test.atlassian.plugin.classloaded",
                        null));
    }

    @Test
    public void testDisablePlugin() {
        manager.disablePlugin("test.atlassian.plugin");

        assertListEquals(listener.getEventClasses(),
                PluginTransactionStartEvent.class,
                PluginModuleDisabledEvent.class,
                PluginModuleDisabledEvent.class,
                PluginModuleDisabledEvent.class,
                PluginDisabledEvent.class,
                PluginTransactionEndEvent.class);
        assertListEquals(listener.getEventPluginOrModuleKeys(),
                null,
                "test.atlassian.plugin:veg",  // a  module that can't be individually disabled can still be disabled with the plugin
                "test.atlassian.plugin:gold", // modules in reverse order to enable
                "test.atlassian.plugin:bear",
                "test.atlassian.plugin",
                null);
    }

    @Test
    public void testEnablePlugin() {
        manager.disablePlugin("test.atlassian.plugin");
        listener.reset();
        manager.enablePlugins("test.atlassian.plugin");

        assertListEquals(listener.getEventClasses(),
                PluginTransactionStartEvent.class,
                PluginModuleEnabledEvent.class,
                PluginModuleEnabledEvent.class,
                PluginModuleEnabledEvent.class,
                PluginEnabledEvent.class,
                PluginTransactionEndEvent.class);
        assertListEquals(listener.getEventPluginOrModuleKeys(),
                null,
                "test.atlassian.plugin:bear",
                "test.atlassian.plugin:gold",
                "test.atlassian.plugin:veg",
                "test.atlassian.plugin",
                null);
    }

    @Test
    public void testEnableDisabledByDefaultPlugin() {
        manager.enablePlugins("test.disabled.plugin");

        assertListEquals(listener.getEventClasses(),
                PluginTransactionStartEvent.class,
                PluginEnabledEvent.class,
                PluginTransactionEndEvent.class);
        assertListEquals(listener.getEventPluginOrModuleKeys(),
                null,
                "test.disabled.plugin",
                null);

        listener.reset();
        manager.enablePluginModule("test.disabled.plugin:gold");

        assertListEquals(listener.getEventClasses(),
                PluginTransactionStartEvent.class,
                PluginModuleEnabledEvent.class,
                PluginTransactionEndEvent.class);
        assertListEquals(listener.getEventPluginOrModuleKeys(),
                null,
                "test.disabled.plugin:gold",
                null);
    }

    @Test
    public void testDisableModule() {
        manager.disablePluginModule("test.atlassian.plugin:bear");

        assertListEquals(listener.getEventClasses(),
                PluginTransactionStartEvent.class,
                PluginModuleDisabledEvent.class,
                PluginTransactionEndEvent.class);
        assertListEquals(listener.getEventPluginOrModuleKeys(),
                null,
                "test.atlassian.plugin:bear",
                null);
    }

    @Test
    public void testDisableModuleWithCannotDisableDoesNotFireEvent() {
        manager.disablePluginModule("test.atlassian.plugin:veg");
        assertListEquals(listener.getEventClasses(),
                PluginTransactionStartEvent.class,
                PluginTransactionEndEvent.class);
    }

    @Test
    public void testEnableModule() {
        manager.disablePluginModule("test.atlassian.plugin:bear");
        listener.reset();
        manager.enablePluginModule("test.atlassian.plugin:bear");

        assertListEquals(listener.getEventClasses(),
                PluginTransactionStartEvent.class,
                PluginModuleEnabledEvent.class,
                PluginTransactionEndEvent.class);
        assertListEquals(listener.getEventPluginOrModuleKeys(),
                null,
                "test.atlassian.plugin:bear",
                null);
    }

    @Test
    public void testInstallPlugin() {
        // have to uninstall one of the directory plugins
        manager.uninstall(manager.getPlugin("test.atlassian.plugin.classloaded2"));
        listener.reset();
        File pluginJar = new File(DirectoryPluginLoaderUtils.getTestPluginsDirectory(),
                "pooh-test-plugin.jar");
        manager.installPlugins(new JarPluginArtifact(pluginJar));

        assertListEquals(listener.getEventClasses(),
                PluginTransactionStartEvent.class,
                PluginModuleEnabledEvent.class,
                PluginEnabledEvent.class,
                PluginTransactionEndEvent.class);
    }

    @Test
    public void testUninstallPlugin() {
        // have to uninstall one of the directory plugins
        manager.uninstall(manager.getPlugin("test.atlassian.plugin.classloaded2"));

        assertListEquals(listener.getEventClasses(),
                PluginTransactionStartEvent.class,
                PluginModuleDisabledEvent.class,
                PluginDisabledEvent.class,
                PluginTransactionEndEvent.class);
    }

    // yeah, the expected values should come first in jUnit, but varargs are so convenient...
    private static void assertListEquals(List<String> actual, List<String>... oneOfExpected) {
        boolean found = false;
        for (List<String> expected : oneOfExpected) {
            if (actual.equals(expected)) {
                found = true;
                break;
            }
        }
        assertTrue("Unexpected list: " + actual, found);
    }

    private static void assertListEquals(List actual, Object... expected) {
        String message = "Expected list was: " + Arrays.toString(expected) + ", " +
                "but actual was: " + actual;
        assertEquals(message, expected.length, actual.size());
        for (int i = 0; i < actual.size(); i++) {
            assertEquals(message, expected[i], actual.get(i));
        }
    }

    private DefaultPluginManager buildPluginManager(RecordingListener listener) throws Exception {
        PluginEventManager pluginEventManager = new DefaultPluginEventManager();
        pluginEventManager.register(listener);

        DefaultModuleDescriptorFactory moduleDescriptorFactory = new DefaultModuleDescriptorFactory(new DefaultHostContainer());
        moduleDescriptorFactory.addModuleDescriptor("animal", MockAnimalModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("mineral", MockMineralModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("vegetable", MockVegetableModuleDescriptor.class);

        File pluginTempDirectory = DirectoryPluginLoaderUtils.copyTestPluginsToTempDirectory();
        List<PluginLoader> pluginLoaders = buildPluginLoaders(pluginEventManager, pluginTempDirectory);

        DefaultPluginManager manager = new DefaultPluginManager(new MemoryPluginPersistentStateStore(), pluginLoaders,
                moduleDescriptorFactory, pluginEventManager);
        manager.setPluginInstaller(new FilePluginInstaller(pluginTempDirectory));

        return manager;
    }

    private List<PluginLoader> buildPluginLoaders(PluginEventManager pluginEventManager, File pluginTempDirectory) {
        List<PluginLoader> pluginLoaders = new ArrayList<>();
        pluginLoaders.add(new SinglePluginLoader("test-atlassian-plugin.xml"));
        pluginLoaders.add(new SinglePluginLoader("test-disabled-plugin.xml"));
        DirectoryPluginLoader directoryPluginLoader = new DirectoryPluginLoader(
                pluginTempDirectory,
                ImmutableList.of(new LegacyDynamicPluginFactory(PluginAccessor.Descriptor.FILENAME)),
                pluginEventManager);
        pluginLoaders.add(directoryPluginLoader);
        return pluginLoaders;
    }
}
