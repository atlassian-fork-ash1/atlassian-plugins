package com.atlassian.plugin.manager;

import com.atlassian.plugin.PluginAccessor;

/**
 * Tests that {@link DefaultPluginManager}'s plugin accessor methods work correctly when wrapped with
 * {@link EnabledModuleCachingPluginAccessor}.
 *
 * @see TestEnabledModuleCachingPluginAccessor for mock-based unit tests of {@link EnabledModuleCachingPluginAccessor}
 */
public class TestDefaultPluginManagerWithCachingPluginAccessor extends TestDefaultPluginManager {

    private PluginAccessor cachingPluginAccessor;

    /**
     * Whenever the plugin manager is recreated, set up the plugin accessor used by the base class tests
     * to use the caching accessor
     */
    @Override
    protected DefaultPluginManager newDefaultPluginManager(DefaultPluginManager.Builder builder) {
        manager = super.newDefaultPluginManager(builder);

        cachingPluginAccessor = new EnabledModuleCachingPluginAccessor(manager, pluginEventManager, manager);

        return manager;
    }

    /**
     * Make base class tests use our caching plugin accessor
     */
    @Override
    protected PluginAccessor getPluginAccessor() {
        return cachingPluginAccessor;
    }
}
