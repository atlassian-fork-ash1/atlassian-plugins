
package com.atlassian.plugin.manager;

import com.atlassian.plugin.DefaultModuleDescriptorFactory;
import com.atlassian.plugin.JarPluginArtifact;
import com.atlassian.plugin.MockModuleDescriptor;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginArtifact;
import com.atlassian.plugin.PluginDependencies;
import com.atlassian.plugin.PluginException;
import com.atlassian.plugin.PluginInformation;
import com.atlassian.plugin.PluginInternal;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.PluginRestartState;
import com.atlassian.plugin.PluginState;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.descriptors.MockUnusedModuleDescriptor;
import com.atlassian.plugin.descriptors.RequiresRestart;
import com.atlassian.plugin.event.PluginEventListener;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.PluginContainerUnavailableEvent;
import com.atlassian.plugin.event.events.PluginDisabledEvent;
import com.atlassian.plugin.event.events.PluginEnabledEvent;
import com.atlassian.plugin.event.events.PluginModuleAvailableEvent;
import com.atlassian.plugin.event.events.PluginModuleDisabledEvent;
import com.atlassian.plugin.event.events.PluginModuleEnabledEvent;
import com.atlassian.plugin.event.events.PluginModuleUnavailableEvent;
import com.atlassian.plugin.event.impl.DefaultPluginEventManager;
import com.atlassian.plugin.event.listeners.FailListener;
import com.atlassian.plugin.event.listeners.PassListener;
import com.atlassian.plugin.factories.LegacyDynamicPluginFactory;
import com.atlassian.plugin.hostcontainer.DefaultHostContainer;
import com.atlassian.plugin.impl.StaticPlugin;
import com.atlassian.plugin.impl.UnloadablePlugin;
import com.atlassian.plugin.loaders.DirectoryPluginLoader;
import com.atlassian.plugin.loaders.DynamicPluginLoader;
import com.atlassian.plugin.loaders.PluginLoader;
import com.atlassian.plugin.loaders.SinglePluginLoader;
import com.atlassian.plugin.loaders.classloading.DirectoryPluginLoaderUtils;
import com.atlassian.plugin.manager.store.MemoryPluginPersistentStateStore;
import com.atlassian.plugin.metadata.PluginMetadataManager;
import com.atlassian.plugin.mock.MockAnimal;
import com.atlassian.plugin.mock.MockAnimalModuleDescriptor;
import com.atlassian.plugin.mock.MockBear;
import com.atlassian.plugin.mock.MockMineral;
import com.atlassian.plugin.mock.MockMineralModuleDescriptor;
import com.atlassian.plugin.mock.MockThing;
import com.atlassian.plugin.mock.MockVegetableModuleDescriptor;
import com.atlassian.plugin.mock.MockVegetableSubclassModuleDescriptor;
import com.atlassian.plugin.parsers.SafeModeCommandLineArguments;
import com.atlassian.plugin.parsers.SafeModeCommandLineArgumentsFactory;
import com.atlassian.plugin.repositories.FilePluginInstaller;
import com.atlassian.plugin.test.CapturedLogging;
import com.atlassian.plugin.test.PluginJarBuilder;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.dom4j.Element;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.RestoreSystemProperties;
import org.junit.rules.ExpectedException;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;

import static com.atlassian.plugin.loaders.classloading.DirectoryPluginLoaderUtils.PADDINGTON_JAR;
import static com.atlassian.plugin.manager.DefaultPluginManagerMocks.FailureMode.FAIL_TO_DISABLE;
import static com.atlassian.plugin.manager.DefaultPluginManagerMocks.FailureMode.FAIL_TO_ENABLE;
import static com.atlassian.plugin.manager.DefaultPluginManagerMocks.mockFailingModuleDescriptor;
import static com.atlassian.plugin.manager.DefaultPluginManagerMocks.mockPluginLoaderForPlugins;
import static com.atlassian.plugin.manager.DefaultPluginManagerMocks.mockStaticPlugin;
import static com.atlassian.plugin.manager.DefaultPluginManagerMocks.mockTestPlugin;
import static com.atlassian.plugin.test.PluginTestUtils.createTempDirectory;
import static com.google.common.collect.ImmutableList.copyOf;
import static java.util.Collections.emptyMap;
import static java.util.Collections.emptySet;
import static org.apache.commons.io.FileUtils.deleteQuietly;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.hamcrest.MockitoHamcrest.argThat;

/**
 * Unit and low level integration tests for DefaultPluginManager, and base for tests of wrapping/delegating variants.
 * <p>
 * If you're tempted to try to push mocks further through this, be aware that there are subclasses
 * which need to wrap components and which react to events sent during processing. That is, these
 * tests are also testing to some extent integration between DefaultPluginManager and
 * DefaultPluginEventManager and other related classes.
 */
public class TestDefaultPluginManager {
    @Rule
    public final CapturedLogging capturedLogging = new CapturedLogging(DefaultPluginManager.class);
    @Rule
    public final ExpectedException expectedException = ExpectedException.none();
    @Rule
    public final RestoreSystemProperties restoreSystemProperties = new RestoreSystemProperties();

    /**
     * the object being tested
     */
    protected DefaultPluginManager manager;

    protected PluginPersistentStateStore pluginStateStore;
    protected DefaultModuleDescriptorFactory moduleDescriptorFactory; // we should be able to use the interface here?

    private DirectoryPluginLoader directoryPluginLoader;
    protected PluginEventManager pluginEventManager;
    private SafeModeManager safeModeManager;
    private ClusterEnvironmentProvider clusterEnvironmentProvider;
    private SafeModeCommandLineArguments safeModeCommandLineArgs;
    private SafeModeCommandLineArgumentsFactory safeModeCommandLineArgsFactory;
    private PluginMetadataManager pluginMetadataManager;
    private ApplicationDefinedPluginsProvider appDefinedPluginsProvider;
    @Mock
    private PluginPersistentStateStore mockPluginPersistentStateStore;
    private File pluginsDirectory;
    private File pluginsTestDir;
    private File temporaryDirectory;

    private PluginPersistentState mockPluginPersistentState;

    private void createFillAndCleanTempPluginDirectory() throws IOException {
        final DirectoryPluginLoaderUtils.ScannerDirectories directories = DirectoryPluginLoaderUtils.createFillAndCleanTempPluginDirectory();
        pluginsDirectory = directories.pluginsDirectory;
        pluginsTestDir = directories.pluginsTestDir;
    }

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        pluginEventManager = new DefaultPluginEventManager();
        safeModeCommandLineArgsFactory = mock(SafeModeCommandLineArgumentsFactory.class);
        pluginStateStore = new MemoryPluginPersistentStateStore();
        moduleDescriptorFactory = new DefaultModuleDescriptorFactory(new DefaultHostContainer());
        temporaryDirectory = createTempDirectory(TestDefaultPluginManager.class);
        pluginMetadataManager = mock(PluginMetadataManager.class);
        appDefinedPluginsProvider = mock(ApplicationDefinedPluginsProvider.class);
        clusterEnvironmentProvider = mock(ClusterEnvironmentProvider.class);
        when(pluginMetadataManager.isSystemProvided(any())).thenReturn(false);
        when(pluginMetadataManager.isOptional(any(Plugin.class))).thenReturn(true);
        safeModeCommandLineArgs = mock(SafeModeCommandLineArguments.class);
        when(safeModeCommandLineArgsFactory.get()).thenReturn(safeModeCommandLineArgs);
        when(safeModeCommandLineArgs.isSafeMode()).thenReturn(false);
        safeModeManager = SafeModeManager.START_ALL_PLUGINS;
    }

    @After
    public void tearDown() throws Exception {
        manager = null;
        moduleDescriptorFactory = null;
        pluginStateStore = null;

        if (directoryPluginLoader != null) {
            directoryPluginLoader = null;
        }
        deleteQuietly(temporaryDirectory);
    }

    /**
     * Overridden in test subclasses which wrap manager or plugin accessor
     */
    protected DefaultPluginManager newDefaultPluginManager(DefaultPluginManager.Builder builder) {
        return builder
                .withModuleDescriptorFactory(moduleDescriptorFactory)
                .withPluginEventManager(pluginEventManager)
                .withStore(pluginStateStore)
                .withSafeModeManager(safeModeManager)
                .withVerifyRequiredPlugins(true)
                .build();
    }

    private SafeModeManager newDefaultSafeModeManager(){

        mockPluginPersistentStateStore = mock(PluginPersistentStateStore.class);
        mockPluginPersistentState = mock(PluginPersistentState.class);
        when(mockPluginPersistentState.getStatesMap()).thenReturn(emptyMap());
        when(mockPluginPersistentStateStore.load()).thenReturn(mockPluginPersistentState);
        return spy(new DefaultSafeModeManager(pluginMetadataManager,appDefinedPluginsProvider, clusterEnvironmentProvider, safeModeCommandLineArgsFactory, mockPluginPersistentStateStore));
    }

    private DefaultPluginManager newDefaultPluginManager(PluginLoader... pluginLoaders) {
        return newDefaultPluginManager(DefaultPluginManager.newBuilder().withPluginLoaders(copyOf(pluginLoaders)));
    }

    /**
     * Overridden in test subclasses which wrap plugin accessor
     */
    protected PluginAccessor getPluginAccessor() {
        return manager;
    }

    @Test
    public void testRetrievePlugins() throws PluginParseException {
        manager = newDefaultPluginManager(
                new SinglePluginLoader("test-atlassian-plugin.xml"),
                new SinglePluginLoader("test-disabled-plugin.xml"));

        moduleDescriptorFactory.addModuleDescriptor("animal", MockAnimalModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("mineral", MockMineralModuleDescriptor.class);
        manager.init();

        assertThat(getPluginAccessor().getPlugins(), hasSize(2));
        assertThat(getPluginAccessor().getEnabledPlugins(), hasSize(1));
        manager.enablePlugins("test.disabled.plugin");
        assertThat(getPluginAccessor().getEnabledPlugins(), hasSize(2));
    }

    @Test
    public void testEnableModuleFailed() throws PluginParseException {
        final PluginLoader mockPluginLoader = mock(PluginLoader.class);
        final ModuleDescriptor<Object> badModuleDescriptor = mockFailingModuleDescriptor("foo:bar", FAIL_TO_ENABLE);

        final AbstractModuleDescriptor goodModuleDescriptor = mock(AbstractModuleDescriptor.class);
        when(goodModuleDescriptor.getKey()).thenReturn("baz");
        when(goodModuleDescriptor.getPluginKey()).thenReturn("foo");
        when(goodModuleDescriptor.isEnabledByDefault()).thenReturn(true);

        Plugin plugin = mockStaticPlugin("foo", goodModuleDescriptor, badModuleDescriptor);

        when(mockPluginLoader.loadAllPlugins(isA(ModuleDescriptorFactory.class))).thenReturn(Collections.singletonList(plugin));

        pluginEventManager.register(new FailListener(PluginEnabledEvent.class));

        MyModuleDisabledListener listener = new MyModuleDisabledListener(goodModuleDescriptor);
        pluginEventManager.register(listener);

        manager = newDefaultPluginManager(mockPluginLoader);
        manager.init();

        assertThat(getPluginAccessor().getPlugins(), hasSize(1));
        assertThat(getPluginAccessor().getEnabledPlugins(), hasSize(0));
        plugin = getPluginAccessor().getPlugin("foo");
        assertFalse(plugin.getPluginState() == PluginState.ENABLED);
        assertTrue(plugin instanceof UnloadablePlugin);
        assertTrue(listener.isCalled());
    }

    public static class MyModuleDisabledListener {
        private final ModuleDescriptor goodModuleDescriptor;
        private volatile boolean disableCalled = false;

        public MyModuleDisabledListener(ModuleDescriptor goodModuleDescriptor) {
            this.goodModuleDescriptor = goodModuleDescriptor;
        }

        @PluginEventListener
        public void onDisable(PluginModuleDisabledEvent evt) {
            if (evt.getModule().equals(goodModuleDescriptor)) {
                disableCalled = true;
            }
        }

        public boolean isCalled() {
            return disableCalled;
        }
    }

    @Test
    public void testEnabledModuleOutOfSyncWithPlugin() throws PluginParseException {
        final PluginLoader mockPluginLoader = mock(PluginLoader.class);
        Plugin plugin = new StaticPlugin();
        plugin.setKey("foo");
        plugin.setEnabledByDefault(true);
        plugin.setPluginInformation(new PluginInformation());

        when(mockPluginLoader.loadAllPlugins(isA(ModuleDescriptorFactory.class))).thenReturn(Collections.singletonList(plugin));

        manager = newDefaultPluginManager(mockPluginLoader);
        manager.init();

        assertThat(getPluginAccessor().getPlugins(), hasSize(1));
        assertThat(getPluginAccessor().getEnabledPlugins(), hasSize(1));
        plugin = getPluginAccessor().getPlugin("foo");
        assertTrue(plugin.getPluginState() == PluginState.ENABLED);
        assertTrue(getPluginAccessor().isPluginEnabled("foo"));
        plugin.disable();
        assertFalse(plugin.getPluginState() == PluginState.ENABLED);
        assertFalse(getPluginAccessor().isPluginEnabled("foo"));
    }

    @Test
    public void disablePluginsBySafeModeShouldNotWorkInClusterEnvironment() throws Exception {
        safeModeManager = newDefaultSafeModeManager();
        when(safeModeCommandLineArgs.isDisabledByParam(anyString())).thenReturn(true);
        when(safeModeCommandLineArgs.isSafeMode()).thenReturn(true);
        when(clusterEnvironmentProvider.isInCluster()).thenReturn(true);
        manager = newDefaultPluginManager(new SinglePluginLoader("test-another-plugin.xml"));
        manager.init();
        assertNotNull(getPluginAccessor().getEnabledPlugin("test.another.plugin"));
    }

    @Test
    public void disablePluginsBySafeModeManagerShouldNotAffectNewPlugins() {
        safeModeManager = newDefaultSafeModeManager();
        when(safeModeCommandLineArgs.isDisabledByParam(any())).thenReturn(true);
        final SinglePluginLoaderWithAddition loader = new SinglePluginLoaderWithAddition("test-atlassian-plugin.xml");
        loader.setAddPluginLoader(new SinglePluginLoader("test-another-plugin.xml"));
        manager = newDefaultPluginManager(loader);
        manager.init();
        manager.scanForNewPlugins();
        assertNull(getPluginAccessor().getEnabledPlugin("test.atlassian.plugin"));
        assertNotNull(getPluginAccessor().getEnabledPlugin("test.another.plugin"));
    }

    @Test
    public void disableParticularPluginBySafeModeManagerShouldDisableAnyPlugin() {
        safeModeManager = newDefaultSafeModeManager();
        final PluginLoader[] loaders = {new SinglePluginLoader("test-another-plugin.xml"), new SinglePluginLoader("test-system-plugin.xml")};
        when(pluginMetadataManager.isSystemProvided(argThat(hasProperty("key", equalTo("test.atlassian.plugin"))))).thenReturn(true);

        // disable system plugin
        when(safeModeCommandLineArgs.isDisabledByParam(any())).thenReturn(false);
        when(safeModeCommandLineArgs.isDisabledByParam(eq("test.atlassian.plugin"))).thenReturn(true);
        manager = newDefaultPluginManager(loaders);
        manager.init();
        assertNull(getPluginAccessor().getEnabledPlugin("test.atlassian.plugin"));
        assertNotNull(getPluginAccessor().getEnabledPlugin("test.another.plugin"));

        // disable both - user installed and system plugins
        when(safeModeCommandLineArgs.isDisabledByParam(eq("test.atlassian.plugin"))).thenReturn(true);
        when(safeModeCommandLineArgs.isDisabledByParam(eq("test.another.plugin"))).thenReturn(true);
        manager = newDefaultPluginManager(loaders);
        manager.init();
        assertNull(getPluginAccessor().getEnabledPlugin("test.another.plugin"));
        assertNull(getPluginAccessor().getEnabledPlugin("test.atlassian.plugin"));
    }

    @Test
    public void disableAllPluginsBySafeModeShouldNotDisableApplicationPlugins() {
        safeModeManager = newDefaultSafeModeManager();
        when(safeModeCommandLineArgs.isDisabledByParam(eq("test.atlassian.plugin.app"))).thenReturn(false);
        when(safeModeCommandLineArgs.isSafeMode()).thenReturn(true);
        when(appDefinedPluginsProvider.getPluginKeys(any(Iterable.class))).thenReturn(new HashSet(Collections.singletonList("test.atlassian.plugin.app")));
        // reinit manager
        manager = newDefaultPluginManager(new SinglePluginLoader("test-plugin-dynamic-modules.xml"), new SinglePluginLoader("test-atlassian-plugin-with-app.xml"));
        manager.init();

        assertNull(getPluginAccessor().getEnabledPlugin("pluginKey"));
        assertNotNull(getPluginAccessor().getEnabledPlugin("test.atlassian.plugin.app"));
    }

    @Test
    public void disableAllPluginsBySafeModeManagerShouldNotDisableRequiredPlugins() {
        safeModeManager = newDefaultSafeModeManager();
        final PluginLoader[] loaders = {
                new SinglePluginLoader("test-another-plugin.xml"), new SinglePluginLoader("test-system-plugin.xml")};
        when(pluginMetadataManager.isOptional(ArgumentMatchers.argThat(
                (Plugin item) -> item.getKey().equals("test.another.plugin")))).thenReturn(false);
        when(safeModeCommandLineArgs.isSafeMode()).thenReturn(true);
        when(safeModeCommandLineArgs.isDisabledByParam(eq("test.another.plugin"))).thenReturn(false);
        // reinit manager
        manager = newDefaultPluginManager(loaders);
        manager.init();
        assertNotNull(getPluginAccessor().getEnabledPlugin("test.another.plugin"));
        assertNull(getPluginAccessor().getEnabledPlugin("test.atlassian.plugin"));
    }

    @Test
    public void disableAllPluginsBySafeModeManagerShouldNotDisableSystemPlugins() {
        safeModeManager = newDefaultSafeModeManager();
        final PluginLoader[] loaders = {new SinglePluginLoader("test-another-plugin.xml"), new SinglePluginLoader("test-system-plugin.xml")};
        when(pluginMetadataManager.isSystemProvided(argThat(hasProperty("key", equalTo("test.atlassian.plugin"))))).thenReturn(true);
        // disable all plugins, but not particular one
        when(safeModeCommandLineArgs.isDisabledByParam(any())).thenReturn(true);
        when(safeModeCommandLineArgs.isDisabledByParam(eq("test.atlassian.plugin"))).thenReturn(false);
        // reinit manager
        manager = newDefaultPluginManager(loaders);
        manager.init();
        assertNull(getPluginAccessor().getEnabledPlugin("test.another.plugin"));
        assertNotNull(getPluginAccessor().getEnabledPlugin("test.atlassian.plugin"));
    }

    @Test
    public void testDisablePluginModuleWithCannotDisableAnnotation() {
        moduleDescriptorFactory.addModuleDescriptor("animal", MockAnimalModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("mineral", MockMineralModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("bullshit", MockUnusedModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("vegetable", MockVegetableModuleDescriptor.class);

        manager = newDefaultPluginManager(new SinglePluginLoader("test-atlassian-plugin.xml"));
        manager.init();

        final String pluginKey = "test.atlassian.plugin";
        final String disablableModuleKey = pluginKey + ":bear";
        final String moduleKey = pluginKey + ":veg";

        // First, make sure we can disable the bear module
        manager.disablePluginModule(disablableModuleKey);
        assertNull(getPluginAccessor().getEnabledPluginModule(disablableModuleKey));

        // Now, make sure we can't disable the veg module
        manager.disablePluginModule(moduleKey);
        assertNotNull(getPluginAccessor().getEnabledPluginModule(moduleKey));
    }

    @Test
    public void testDisablePluginModuleWithCannotDisableAnnotationInSuperclass() {
        moduleDescriptorFactory.addModuleDescriptor("animal", MockAnimalModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("mineral", MockMineralModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("bullshit", MockUnusedModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("vegetable", MockVegetableModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("vegetableSubclass", MockVegetableSubclassModuleDescriptor.class);

        manager = newDefaultPluginManager(new SinglePluginLoader("test-atlassian-plugin.xml"));
        manager.init();

        final String pluginKey = "test.atlassian.plugin";
        final String disablableModuleKey = pluginKey + ":bear";
        final String moduleKey = pluginKey + ":vegSubclass";

        // First, make sure we can disable the bear module
        manager.disablePluginModule(disablableModuleKey);
        assertNull(getPluginAccessor().getEnabledPluginModule(disablableModuleKey));

        // Now, make sure we can't disable the vegSubclass module
        manager.disablePluginModule(moduleKey);
        assertNotNull(getPluginAccessor().getEnabledPluginModule(moduleKey));
    }

    @Test
    public void testEnabledDisabledRetrieval() throws PluginParseException {
        moduleDescriptorFactory.addModuleDescriptor("animal", MockAnimalModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("mineral", MockMineralModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("bullshit", MockUnusedModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("vegetable", MockVegetableModuleDescriptor.class);

        final PassListener enabledListener = new PassListener(PluginEnabledEvent.class);
        final PassListener disabledListener = new PassListener(PluginDisabledEvent.class);
        pluginEventManager.register(enabledListener);
        pluginEventManager.register(disabledListener);

        manager = newDefaultPluginManager(new SinglePluginLoader("test-atlassian-plugin.xml"));
        manager.init();

        // check non existent plugins don't show
        assertNull(getPluginAccessor().getPlugin("bull:shit"));
        assertNull(getPluginAccessor().getEnabledPlugin("bull:shit"));
        assertNull(getPluginAccessor().getPluginModule("bull:shit"));
        assertNull(getPluginAccessor().getEnabledPluginModule("bull:shit"));
        assertTrue(getPluginAccessor().getEnabledModuleDescriptorsByClass(NothingModuleDescriptor.class).isEmpty());

        final String pluginKey = "test.atlassian.plugin";
        final String moduleKey = pluginKey + ":bear";

        // retrieve everything when enabled
        assertNotNull(getPluginAccessor().getPlugin(pluginKey));
        assertNotNull(getPluginAccessor().getEnabledPlugin(pluginKey));
        assertNotNull(getPluginAccessor().getPluginModule(moduleKey));
        assertNotNull(getPluginAccessor().getEnabledPluginModule(moduleKey));
        assertNull(getPluginAccessor().getEnabledPluginModule(pluginKey + ":shit"));
        assertFalse(getPluginAccessor().getEnabledModuleDescriptorsByClass(MockAnimalModuleDescriptor.class).isEmpty());
        assertFalse(getPluginAccessor().getEnabledModulesByClass(MockBear.class).isEmpty());
        assertEquals(new MockBear(), getPluginAccessor().getEnabledModulesByClass(MockBear.class).get(0));
        enabledListener.assertCalled();

        // now only retrieve via always retrieve methods
        manager.disablePlugin(pluginKey);
        assertNotNull(getPluginAccessor().getPlugin(pluginKey));
        assertNull(getPluginAccessor().getEnabledPlugin(pluginKey));
        assertNotNull(getPluginAccessor().getPluginModule(moduleKey));
        assertNull(getPluginAccessor().getEnabledPluginModule(moduleKey));
        assertTrue(getPluginAccessor().getEnabledModulesByClass(com.atlassian.plugin.mock.MockBear.class).isEmpty());
        assertTrue(getPluginAccessor().getEnabledModuleDescriptorsByClass(MockAnimalModuleDescriptor.class).isEmpty());
        disabledListener.assertCalled();

        // now enable again and check back to start
        manager.enablePlugins(pluginKey);
        assertNotNull(getPluginAccessor().getPlugin(pluginKey));
        assertNotNull(getPluginAccessor().getEnabledPlugin(pluginKey));
        assertNotNull(getPluginAccessor().getPluginModule(moduleKey));
        assertNotNull(getPluginAccessor().getEnabledPluginModule(moduleKey));
        assertFalse(getPluginAccessor().getEnabledModulesByClass(com.atlassian.plugin.mock.MockBear.class).isEmpty());
        assertFalse(getPluginAccessor().getEnabledModuleDescriptorsByClass(MockAnimalModuleDescriptor.class).isEmpty());
        enabledListener.assertCalled();

        // now let's disable the module, but not the plugin
        pluginEventManager.register(new FailListener(PluginEnabledEvent.class));
        manager.disablePluginModule(moduleKey);
        assertNotNull(getPluginAccessor().getPlugin(pluginKey));
        assertNotNull(getPluginAccessor().getEnabledPlugin(pluginKey));
        assertNotNull(getPluginAccessor().getPluginModule(moduleKey));
        assertNull(getPluginAccessor().getEnabledPluginModule(moduleKey));
        assertTrue(getPluginAccessor().getEnabledModulesByClass(com.atlassian.plugin.mock.MockBear.class).isEmpty());
        assertTrue(getPluginAccessor().getEnabledModuleDescriptorsByClass(MockAnimalModuleDescriptor.class).isEmpty());

        // now enable the module again
        pluginEventManager.register(new FailListener(PluginDisabledEvent.class));
        manager.enablePluginModule(moduleKey);
        assertNotNull(getPluginAccessor().getPlugin(pluginKey));
        assertNotNull(getPluginAccessor().getEnabledPlugin(pluginKey));
        assertNotNull(getPluginAccessor().getPluginModule(moduleKey));
        assertNotNull(getPluginAccessor().getEnabledPluginModule(moduleKey));
        assertFalse(getPluginAccessor().getEnabledModulesByClass(com.atlassian.plugin.mock.MockBear.class).isEmpty());
        assertFalse(getPluginAccessor().getEnabledModuleDescriptorsByClass(MockAnimalModuleDescriptor.class).isEmpty());
    }

    @Test
    public void testDuplicatePluginKeysAreIgnored() throws PluginParseException {
        moduleDescriptorFactory.addModuleDescriptor("mineral", MockMineralModuleDescriptor.class);

        manager = newDefaultPluginManager(new SinglePluginLoader("test-atlassian-plugin.xml"), new SinglePluginLoader("test-atlassian-plugin.xml"));
        manager.init();
        assertThat(getPluginAccessor().getEnabledPlugins(), hasSize(1));
    }

    @Test
    public void testDuplicateSnapshotVersionsAreNotLoaded() throws PluginParseException {
        moduleDescriptorFactory.addModuleDescriptor("mineral", MockMineralModuleDescriptor.class);

        manager = newDefaultPluginManager(new SinglePluginLoader("test-atlassian-snapshot-plugin.xml"), new SinglePluginLoader("test-atlassian-snapshot-plugin.xml"));
        manager.init();
        assertThat(getPluginAccessor().getEnabledPlugins(), hasSize(1));
    }

    @Test
    public void testChangedSnapshotVersionIsLoaded() throws PluginParseException {
        moduleDescriptorFactory.addModuleDescriptor("mineral", MockMineralModuleDescriptor.class);

        manager = newDefaultPluginManager(
                new SinglePluginLoader("test-atlassian-snapshot-plugin.xml"),
                new DelayedSinglePluginLoader("test-atlassian-snapshot-plugin-changed-same-version.xml")
        );
        manager.init();
        assertThat(getPluginAccessor().getEnabledPlugins(), hasSize(1));
        final Plugin plugin = getPluginAccessor().getPlugin("test.atlassian.plugin");
        assertEquals("1.1-SNAPSHOT", plugin.getPluginInformation().getVersion());
        assertEquals("This plugin descriptor has been changed!", plugin.getPluginInformation().getDescription());
    }

    @Test
    public void testLoadOlderDuplicatePlugin() {
        moduleDescriptorFactory.addModuleDescriptor("mineral", MockMineralModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("animal", MockAnimalModuleDescriptor.class);

        manager = newDefaultPluginManager(new MultiplePluginLoader("test-atlassian-plugin-newer.xml"), new MultiplePluginLoader("test-atlassian-plugin.xml", "test-another-plugin.xml"));
        manager.init();
        assertThat(getPluginAccessor().getEnabledPlugins(), hasSize(2));
    }

    @Test
    public void testLoadOlderDuplicatePluginDoesNotTryToEnableIt() {
        moduleDescriptorFactory.addModuleDescriptor("mineral", MockMineralModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("animal", MockAnimalModuleDescriptor.class);

        final Plugin plugin = new StaticPlugin() {
            @Override
            protected PluginState enableInternal() {
                fail("enable() must never be called on a earlier version of plugin when later version is installed");
                return null;
            }

            @Override
            public void disableInternal() {
                fail("disable() must never be called on a earlier version of plugin when later version is installed");
            }
        };
        plugin.setKey("test.atlassian.plugin");
        plugin.getPluginInformation().setVersion("1.0");

        PluginLoader pluginLoader = new MultiplePluginLoader("test-atlassian-plugin-newer.xml");
        manager = newDefaultPluginManager(pluginLoader);
        manager.init();
        manager.addPlugins(pluginLoader, Collections.singletonList(plugin));
    }

    @Test
    public void testLoadNewerDuplicatePlugin() {
        moduleDescriptorFactory.addModuleDescriptor("mineral", MockMineralModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("animal", MockAnimalModuleDescriptor.class);

        manager = newDefaultPluginManager(
                new SinglePluginLoader("test-atlassian-plugin.xml"),
                new SinglePluginLoader("test-atlassian-plugin-newer.xml"));
        manager.init();
        assertThat(getPluginAccessor().getEnabledPlugins(), hasSize(1));
        final Plugin plugin = getPluginAccessor().getPlugin("test.atlassian.plugin");
        assertEquals("1.1", plugin.getPluginInformation().getVersion());
    }

    @Test
    public void testLoadNewerDuplicateDynamicPluginPreservesPluginState() throws PluginParseException {
        moduleDescriptorFactory.addModuleDescriptor("mineral", MockMineralModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("animal", MockAnimalModuleDescriptor.class);

        manager = newDefaultPluginManager(new SinglePluginLoaderWithRemoval("test-atlassian-plugin.xml"));
        manager.init();

        pluginStateStore.save(PluginPersistentState.Builder.create(pluginStateStore.load()).setEnabled(getPluginAccessor().getPlugin("test.atlassian.plugin"),
                false).toState());

        manager.shutdown();

        manager = newDefaultPluginManager(new SinglePluginLoaderWithRemoval("test-atlassian-plugin-newer.xml"));
        manager.init();

        final Plugin plugin = getPluginAccessor().getPlugin("test.atlassian.plugin");
        assertEquals("1.1", plugin.getPluginInformation().getVersion());
        assertFalse(getPluginAccessor().isPluginEnabled("test.atlassian.plugin"));
    }

    @Test
    public void testLoadNewerDuplicateDynamicPluginPreservesModuleState() throws PluginParseException {
        moduleDescriptorFactory.addModuleDescriptor("mineral", MockMineralModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("animal", MockAnimalModuleDescriptor.class);

        manager = newDefaultPluginManager(new SinglePluginLoaderWithRemoval("test-atlassian-plugin.xml"));
        manager.init();

        pluginStateStore.save(PluginPersistentState.Builder.create(pluginStateStore.load()).setEnabled(
                getPluginAccessor().getPluginModule("test.atlassian.plugin:bear"), false).toState());

        manager.shutdown();

        manager = newDefaultPluginManager(new SinglePluginLoaderWithRemoval("test-atlassian-plugin-newer.xml"));
        manager.init();

        final Plugin plugin = getPluginAccessor().getPlugin("test.atlassian.plugin");
        assertEquals("1.1", plugin.getPluginInformation().getVersion());
        assertFalse(getPluginAccessor().isPluginModuleEnabled("test.atlassian.plugin:bear"));
        assertTrue(getPluginAccessor().isPluginModuleEnabled("test.atlassian.plugin:gold"));
    }

    @Test
    public void testLoadChangedDynamicPluginWithSameVersionNumberDoesNotReplaceExisting() throws PluginParseException {
        moduleDescriptorFactory.addModuleDescriptor("mineral", MockMineralModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("animal", MockAnimalModuleDescriptor.class);

        manager = newDefaultPluginManager(
                new SinglePluginLoaderWithRemoval("test-atlassian-plugin.xml"),
                new SinglePluginLoaderWithRemoval("test-atlassian-plugin-changed-same-version.xml"));
        manager.init();

        final Plugin plugin = getPluginAccessor().getPlugin("test.atlassian.plugin");
        assertEquals("Test Plugin", plugin.getName());
    }

    @Test
    public void testGetPluginsWithPluginMatchingPluginPredicate() throws Exception {
        final Plugin plugin = mockTestPlugin(Collections.emptyList());

        final Predicate<Plugin> mockPluginPredicate = mock(Predicate.class);
        when(mockPluginPredicate.test(plugin)).thenReturn(true);

        manager = newDefaultPluginManager();
        manager.addPlugins(null, Collections.singletonList(plugin));
        final Collection<Plugin> plugins = getPluginAccessor().getPlugins(mockPluginPredicate);

        assertThat(plugins, hasSize(1));
        assertTrue(plugins.contains(plugin));
        verify(mockPluginPredicate).test(any(Plugin.class));
    }

    @Test
    public void testGetPluginsWithPluginNotMatchingPluginPredicate() {
        final Plugin plugin = mockTestPlugin(Collections.emptyList());

        manager = newDefaultPluginManager();
        manager.addPlugins(null, Collections.singletonList(plugin));
        final Collection<Plugin> plugins = getPluginAccessor().getPlugins((Predicate<Plugin>) p -> false);

        assertThat(plugins, hasSize(0));
    }

    @Test
    public void testGetPluginModulesWithModuleMatchingPredicate() throws Exception {
        final MockThing module = new MockThing() {
        };
        @SuppressWarnings("unchecked")
        final ModuleDescriptor<MockThing> moduleDescriptor = mock(ModuleDescriptor.class);
        when(moduleDescriptor.getModule()).thenReturn(module);
        when(moduleDescriptor.getPluginKey()).thenReturn("some-plugin-key");
        when(moduleDescriptor.isEnabledByDefault()).thenReturn(true);

        final Plugin plugin = mockTestPlugin(Collections.singleton(moduleDescriptor));
        when(plugin.getModuleDescriptor("module")).thenReturn((ModuleDescriptor) moduleDescriptor);

        final Predicate<ModuleDescriptor<MockThing>> predicate = mock(Predicate.class);
        when(predicate.test(moduleDescriptor)).thenReturn(true);

        manager = newDefaultPluginManager();
        manager.addPlugins(null, Collections.singletonList(plugin));
        @SuppressWarnings("unchecked")
        final Collection<MockThing> modules = getPluginAccessor().getModules(predicate);

        assertThat(modules, hasSize(1));
        assertTrue(modules.contains(module));

        verify(predicate).test(moduleDescriptor);
    }

    @Test
    public void testGetPluginModulesWithGetModuleThrowingException() throws Exception {
        final Plugin badPlugin = new StaticPlugin();
        badPlugin.setKey("bad");
        final AtomicInteger getModuleCallCount = new AtomicInteger();

        final MockModuleDescriptor<Object> badDescriptor = new MockModuleDescriptor<Object>(badPlugin, "bad", new Object()) {
            @Override
            public Object getModule() {
                getModuleCallCount.incrementAndGet();
                throw new RuntimeException();
            }
        };
        badPlugin.addModuleDescriptor(badDescriptor);

        final Plugin goodPlugin = new StaticPlugin();
        goodPlugin.setKey("good");
        final MockModuleDescriptor<Object> goodDescriptor = new MockModuleDescriptor<Object>(goodPlugin, "good", new Object());
        goodPlugin.addModuleDescriptor(goodDescriptor);

        manager = newDefaultPluginManager();
        manager.addPlugins(null, Arrays.asList(goodPlugin, badPlugin));
        manager.enablePlugins("bad", "good");

        assertTrue(getPluginAccessor().isPluginEnabled("bad"));
        assertTrue(getPluginAccessor().isPluginEnabled("good"));
        final Collection<Object> modules = getPluginAccessor().getEnabledModulesByClass(Object.class);

        assertThat(modules, hasSize(1));

        getPluginAccessor().getEnabledModulesByClass(Object.class);
        assertThat(getModuleCallCount.get(), is(1));
    }

    @Test
    public void testGetPluginModulesWith2GetModulesThrowingExceptionOnlyNotifiesOnce() throws Exception {
        final Plugin badPlugin = new StaticPlugin();
        badPlugin.setKey("bad");
        final MockModuleDescriptor<Object> badDescriptor = new MockModuleDescriptor<Object>(badPlugin, "bad", new Object()) {
            @Override
            public Object getModule() {
                throw new RuntimeException();
            }
        };
        badPlugin.addModuleDescriptor(badDescriptor);
        final MockModuleDescriptor<Object> badDescriptor2 = new MockModuleDescriptor<Object>(badPlugin, "bad2", new Object()) {
            @Override
            public Object getModule() {
                throw new RuntimeException();
            }
        };
        badPlugin.addModuleDescriptor(badDescriptor2);

        final Plugin goodPlugin = new StaticPlugin();
        goodPlugin.setKey("good");
        final MockModuleDescriptor<Object> goodDescriptor = new MockModuleDescriptor<Object>(goodPlugin, "good", new Object());
        goodPlugin.addModuleDescriptor(goodDescriptor);
        DisabledPluginCounter counter = new DisabledPluginCounter();
        pluginEventManager.register(counter);

        manager = newDefaultPluginManager();
        manager.addPlugins(null, Arrays.asList(goodPlugin, badPlugin));
        manager.enablePlugins("bad", "good");

        assertTrue(getPluginAccessor().isPluginEnabled("bad"));
        assertTrue(getPluginAccessor().isPluginEnabled("good"));
        final Collection<Object> modules = getPluginAccessor().getEnabledModulesByClass(Object.class);

        assertThat(modules, hasSize(1));
    }

    public static class DisabledPluginCounter {
        int disableCount = 0;

        @PluginEventListener
        public void consume(PluginDisabledEvent element) {
            disableCount++;
        }
    }

    @Test
    public void testGetPluginModulesWithModuleNotMatchingPredicate() throws Exception {
        @SuppressWarnings("unchecked")
        final ModuleDescriptor<MockThing> moduleDescriptor = mock(ModuleDescriptor.class);
        when(moduleDescriptor.getPluginKey()).thenReturn("some-plugin-key");
        when(moduleDescriptor.isEnabledByDefault()).thenReturn(true);

        final Plugin plugin = mockTestPlugin(Collections.singleton(moduleDescriptor));
        when(plugin.getModuleDescriptor("module")).thenReturn((ModuleDescriptor) moduleDescriptor);

        @SuppressWarnings("unchecked")
        final Predicate<ModuleDescriptor<MockThing>> predicate = mock(Predicate.class);

        manager = newDefaultPluginManager();
        manager.addPlugins(null, Collections.singletonList(plugin));
        final Collection<MockThing> modules = getPluginAccessor().getModules(predicate);

        assertThat(modules, hasSize(0));

        verify(predicate).test(moduleDescriptor);
    }

    @Test
    public void testGetPluginModuleDescriptorWithModuleMatchingPredicate() throws Exception {
        @SuppressWarnings("unchecked")
        final ModuleDescriptor<MockThing> moduleDescriptor = mock(ModuleDescriptor.class);
        when(moduleDescriptor.getPluginKey()).thenReturn("some-plugin-key");
        when(moduleDescriptor.isEnabledByDefault()).thenReturn(true);

        final Plugin plugin = mockTestPlugin(Collections.singleton(moduleDescriptor));
        when(plugin.getModuleDescriptor("module")).thenReturn((ModuleDescriptor) moduleDescriptor);

        @SuppressWarnings("unchecked")
        final Predicate<ModuleDescriptor<MockThing>> predicate = mock(Predicate.class);
        when(predicate.test(moduleDescriptor)).thenReturn(true);

        manager = newDefaultPluginManager();
        manager.addPlugins(null, Collections.singletonList(plugin));
        final Collection<ModuleDescriptor<MockThing>> modules = getPluginAccessor().getModuleDescriptors(predicate);

        assertThat(modules, hasSize(1));
        assertTrue(modules.contains(moduleDescriptor));

        verify(predicate).test(moduleDescriptor);
    }

    @Test
    public void testGetPluginModuleDescriptorsWithModuleNotMatchingPredicate() throws Exception {
        @SuppressWarnings("unchecked")
        final ModuleDescriptor<MockThing> moduleDescriptor = mock(ModuleDescriptor.class);
        when(moduleDescriptor.getPluginKey()).thenReturn("some-plugin-key");
        when(moduleDescriptor.isEnabledByDefault()).thenReturn(true);

        final Plugin plugin = mockTestPlugin(Collections.singleton(moduleDescriptor));
        when(plugin.getModuleDescriptor("module")).thenReturn((ModuleDescriptor) moduleDescriptor);

        @SuppressWarnings("unchecked")
        final Predicate<ModuleDescriptor<MockThing>> predicate = mock(Predicate.class);

        manager = newDefaultPluginManager();
        manager.addPlugins(null, Collections.singletonList(plugin));
        final Collection<MockThing> modules = getPluginAccessor().getModules(predicate);

        assertThat(modules, hasSize(0));

        verify(predicate).test(moduleDescriptor);
    }

    @Test
    public void testGetPluginAndModules() throws PluginParseException {
        moduleDescriptorFactory.addModuleDescriptor("animal", MockAnimalModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("mineral", MockMineralModuleDescriptor.class);

        manager = newDefaultPluginManager(new SinglePluginLoader("test-atlassian-plugin.xml"));
        manager.init();

        final Plugin plugin = getPluginAccessor().getPlugin("test.atlassian.plugin");
        assertNotNull(plugin);
        assertEquals("Test Plugin", plugin.getName());

        final ModuleDescriptor<?> bear = plugin.getModuleDescriptor("bear");
        assertEquals(bear, getPluginAccessor().getPluginModule("test.atlassian.plugin:bear"));
    }

    @Test
    public void testGetModuleByModuleClassOneFound() throws PluginParseException {
        moduleDescriptorFactory.addModuleDescriptor("animal", MockAnimalModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("mineral", MockMineralModuleDescriptor.class);

        manager = newDefaultPluginManager(new SinglePluginLoader("test-atlassian-plugin.xml"));
        manager.init();

        final List<MockAnimalModuleDescriptor> animalDescriptors = getPluginAccessor().getEnabledModuleDescriptorsByClass(MockAnimalModuleDescriptor.class);
        assertNotNull(animalDescriptors);
        assertThat(animalDescriptors, hasSize(1));
        final ModuleDescriptor<MockAnimal> moduleDescriptor = animalDescriptors.iterator().next();
        assertEquals("Bear Animal", moduleDescriptor.getName());

        final List<MockMineralModuleDescriptor> mineralDescriptors = getPluginAccessor().getEnabledModuleDescriptorsByClass(MockMineralModuleDescriptor.class);
        assertNotNull(mineralDescriptors);
        assertThat(mineralDescriptors, hasSize(1));
        final ModuleDescriptor<MockMineral> mineralDescriptor = mineralDescriptors.iterator().next();
        assertEquals("Bar", mineralDescriptor.getName());
    }

    @Test
    public void testGetModuleByModuleClassNoneFound() throws PluginParseException {
        moduleDescriptorFactory.addModuleDescriptor("animal", MockAnimalModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("mineral", MockMineralModuleDescriptor.class);

        manager = newDefaultPluginManager(new SinglePluginLoader("test-atlassian-plugin.xml"));
        manager.init();

        class MockSilver implements MockMineral {
            public int getWeight() {
                return 3;
            }
        }

        final Collection<MockSilver> descriptors = getPluginAccessor().getEnabledModulesByClass(MockSilver.class);
        assertNotNull(descriptors);
        assertTrue(descriptors.isEmpty());
    }

    @Test
    public void testGetEnabledPluginsDoesNotReturnEnablingPlugins() throws Exception {
        final PluginLoader mockPluginLoader = mock(PluginLoader.class);

        final Plugin firstPlugin = new StaticPlugin();
        firstPlugin.setKey("first");
        firstPlugin.setEnabledByDefault(false);
        firstPlugin.setPluginInformation(new PluginInformation());

        manager = newDefaultPluginManager();

        final Plugin secondPlugin = new StaticPlugin() {
            public PluginState enableInternal() {
                try {
                    // Assert here when the first plugin has been started but this plugin still has not been loaded
                    assertThat(getPluginAccessor().getPlugins(), hasSize(2));
                    assertThat("First plugin should not be enabled", manager.getEnabledPlugins(), hasSize(0));
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
                return PluginState.ENABLED;
            }

            public void disableInternal() {
                // do nothing
            }
        };
        PluginPersistentStateModifier stateModifier = new PluginPersistentStateModifier(pluginStateStore);
        stateModifier.enable(firstPlugin);
        secondPlugin.setKey("second");
        secondPlugin.setEnabledByDefault(false);
        secondPlugin.setPluginInformation(new PluginInformation());
        stateModifier.enable(secondPlugin);

        when(mockPluginLoader.loadAllPlugins(any(ModuleDescriptorFactory.class))).thenReturn(Arrays.asList(firstPlugin, secondPlugin));

        manager = newDefaultPluginManager(mockPluginLoader);
        manager.init();
    }

    @Test
    public void testFindingNewPlugins() throws PluginParseException, IOException {
        createFillAndCleanTempPluginDirectory();

        //delete paddington for the timebeing
        final File paddington = new File(pluginsTestDir, PADDINGTON_JAR);
        paddington.delete();

        manager = makeClassLoadingPluginManager();

        assertThat(getPluginAccessor().getPlugins(), hasSize(1));
        assertNotNull(getPluginAccessor().getPlugin("test.atlassian.plugin.classloaded2"));

        //restore paddington to test plugins dir
        FileUtils.copyDirectory(pluginsDirectory, pluginsTestDir);

        final int foundFirstScan = manager.scanForNewPlugins();
        assertThat(foundFirstScan, is(1));
        assertThat(getPluginAccessor().getPlugins(), hasSize(2));
        assertNotNull(getPluginAccessor().getPlugin("test.atlassian.plugin.classloaded2"));
        assertNotNull(getPluginAccessor().getPlugin("test.atlassian.plugin.classloaded"));

        final int foundSecondScan = manager.scanForNewPlugins();
        assertThat(foundSecondScan, is(0));
        assertThat(getPluginAccessor().getPlugins(), hasSize(2));
        assertNotNull(getPluginAccessor().getPlugin("test.atlassian.plugin.classloaded2"));
        assertNotNull(getPluginAccessor().getPlugin("test.atlassian.plugin.classloaded"));
    }

    @Test
    public void testFindingNewPluginsNotLoadingRestartRequiredDescriptors() throws PluginParseException, IOException {
        createFillAndCleanTempPluginDirectory();

        final DynamicSinglePluginLoader dynamicSinglePluginLoader = new DynamicSinglePluginLoader("test.atlassian.plugin", "test-requiresRestart-plugin.xml");
        manager = makeClassLoadingPluginManager(dynamicSinglePluginLoader);

        moduleDescriptorFactory.addModuleDescriptor("requiresRestart", RequiresRestartModuleDescriptor.class);

        assertThat(getPluginAccessor().getPlugins(), hasSize(2));
        assertNotNull(getPluginAccessor().getPlugin("test.atlassian.plugin.classloaded2"));

        // enable the dynamic plugin loader
        dynamicSinglePluginLoader.canLoad.set(true);

        manager.scanForNewPlugins();
        assertThat(getPluginAccessor().getPlugins(), hasSize(3));
        assertNotNull(getPluginAccessor().getPlugin("test.atlassian.plugin.classloaded2"));
        assertNotNull(getPluginAccessor().getPlugin("test.atlassian.plugin"));

        final Plugin plugin = getPluginAccessor().getPlugin("test.atlassian.plugin");
        assertTrue(plugin instanceof UnloadablePlugin);
        assertTrue(((UnloadablePlugin) plugin).getErrorText().contains("foo"));

        assertEquals(PluginRestartState.INSTALL, getPluginAccessor().getPluginRestartState("test.atlassian.plugin"));
    }

    /**
     * Tests upgrade of plugin where the old version didn't have any restart required module descriptors, but the new one does
     */
    @Test
    public void testFindingUpgradePluginsNotLoadingRestartRequiredDescriptors() throws PluginParseException, IOException {
        createFillAndCleanTempPluginDirectory();

        moduleDescriptorFactory.addModuleDescriptor("requiresRestart", RequiresRestartModuleDescriptor.class);

        final DynamicSinglePluginLoader dynamicSinglePluginLoader = new DynamicSinglePluginLoader("test.atlassian.plugin.classloaded2", "test-requiresRestartWithUpgrade-plugin.xml");
        manager = makeClassLoadingPluginManager(dynamicSinglePluginLoader);

        assertThat(getPluginAccessor().getPlugins(), hasSize(2));
        assertNotNull(getPluginAccessor().getPlugin("test.atlassian.plugin.classloaded2"));
        assertThat(getPluginAccessor().getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(0));


        dynamicSinglePluginLoader.canLoad.set(true);

        manager.scanForNewPlugins();
        assertThat(getPluginAccessor().getPlugins(), hasSize(2));
        assertNotNull(getPluginAccessor().getPlugin("test.atlassian.plugin.classloaded2"));
        assertEquals(PluginRestartState.UPGRADE, getPluginAccessor().getPluginRestartState("test.atlassian.plugin.classloaded2"));
        assertThat(getPluginAccessor().getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(0));
    }

    @Test
    public void testInstallPluginThatRequiresRestart() throws PluginParseException, IOException, InterruptedException {
        createFillAndCleanTempPluginDirectory();
        moduleDescriptorFactory.addModuleDescriptor("requiresRestart", RequiresRestartModuleDescriptor.class);
        manager = makeClassLoadingPluginManager();
        assertThat(getPluginAccessor().getPlugins(), hasSize(2));

        new PluginJarBuilder().addFormattedResource("atlassian-plugin.xml",
                "<atlassian-plugin name='Test 2' i18n-name-key='test.name' key='test.restartrequired' pluginsVersion='1'>", "    <plugin-info>", "        <version>1.0</version>",
                "    </plugin-info>", "    <requiresRestart key='foo' />", "</atlassian-plugin>").build(pluginsTestDir);
        manager.scanForNewPlugins();

        assertThat(getPluginAccessor().getPlugins(), hasSize(3));
        Plugin plugin = getPluginAccessor().getPlugin("test.restartrequired");
        assertNotNull(plugin);
        assertEquals("Test 2", plugin.getName());
        assertEquals("test.name", plugin.getI18nNameKey());
        assertEquals(1, plugin.getPluginsVersion());
        assertEquals("1.0", plugin.getPluginInformation().getVersion());
        assertFalse(getPluginAccessor().isPluginEnabled("test.restartrequired"));
        assertThat(getPluginAccessor().getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(0));
        assertEquals(PluginRestartState.INSTALL, getPluginAccessor().getPluginRestartState("test.restartrequired"));

        manager.shutdown();
        manager.init();

        assertThat(getPluginAccessor().getPlugins(), hasSize(3));
        assertNotNull(plugin);
        assertTrue(getPluginAccessor().isPluginEnabled("test.restartrequired"));
        assertThat(getPluginAccessor().getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(1));
        assertEquals(PluginRestartState.NONE, getPluginAccessor().getPluginRestartState("test.restartrequired"));
    }

    @Test
    public void testInstallPluginThatRequiresRestartThenRevert() throws PluginParseException, IOException, InterruptedException {
        createFillAndCleanTempPluginDirectory();
        moduleDescriptorFactory.addModuleDescriptor("requiresRestart", RequiresRestartModuleDescriptor.class);
        manager = makeClassLoadingPluginManager();
        manager.setPluginInstaller(new FilePluginInstaller(pluginsTestDir));
        assertThat(getPluginAccessor().getPlugins(), hasSize(2));

        File pluginJar = new PluginJarBuilder().addFormattedResource("atlassian-plugin.xml",
                "<atlassian-plugin name='Test 2' i18n-name-key='test.name' key='test.restartrequired' pluginsVersion='1'>", "    <plugin-info>", "        <version>1.0</version>",
                "    </plugin-info>", "    <requiresRestart key='foo' />", "</atlassian-plugin>").build();
        manager.installPlugins(new JarPluginArtifact(pluginJar));

        assertThat(getPluginAccessor().getPlugins(), hasSize(3));
        Plugin plugin = getPluginAccessor().getPlugin("test.restartrequired");
        assertNotNull(plugin);
        assertEquals("Test 2", plugin.getName());
        assertEquals("test.name", plugin.getI18nNameKey());
        assertEquals(1, plugin.getPluginsVersion());
        assertEquals("1.0", plugin.getPluginInformation().getVersion());
        assertFalse(getPluginAccessor().isPluginEnabled("test.restartrequired"));
        assertThat(getPluginAccessor().getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(0));
        assertEquals(PluginRestartState.INSTALL, getPluginAccessor().getPluginRestartState("test.restartrequired"));

        manager.revertRestartRequiredChange("test.restartrequired");
        assertEquals(PluginRestartState.NONE, getPluginAccessor().getPluginRestartState("test.restartrequired"));

        manager.shutdown();
        manager.init();

        assertThat(getPluginAccessor().getPlugins(), hasSize(2));
        assertFalse(getPluginAccessor().isPluginEnabled("test.restartrequired"));
        assertEquals(PluginRestartState.NONE, getPluginAccessor().getPluginRestartState("test.restartrequired"));
    }

    @Test
    public void testUpgradePluginThatRequiresRestart() throws PluginParseException, IOException, InterruptedException {
        createFillAndCleanTempPluginDirectory();
        moduleDescriptorFactory.addModuleDescriptor("requiresRestart", RequiresRestartModuleDescriptor.class);

        final File origFile = new PluginJarBuilder().addFormattedResource("atlassian-plugin.xml",
                "<atlassian-plugin name='Test 2' key='test.restartrequired' pluginsVersion='1'>", "    <plugin-info>", "        <version>1.0</version>",
                "    </plugin-info>", "    <requiresRestart key='foo' />", "</atlassian-plugin>").build(pluginsTestDir);

        manager = makeClassLoadingPluginManager();

        assertThat(getPluginAccessor().getPlugins(), hasSize(3));
        assertNotNull(getPluginAccessor().getPlugin("test.restartrequired"));
        assertTrue(getPluginAccessor().isPluginEnabled("test.restartrequired"));
        assertThat(getPluginAccessor().getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(1));
        assertEquals(PluginRestartState.NONE, getPluginAccessor().getPluginRestartState("test.restartrequired"));

        // Some filesystems only record last modified in seconds
        Thread.sleep(1000);
        final File updateFile = new PluginJarBuilder().addFormattedResource("atlassian-plugin.xml",
                "<atlassian-plugin name='Test 2' key='test.restartrequired' pluginsVersion='1'>", "    <plugin-info>", "        <version>2.0</version>",
                "    </plugin-info>", "    <requiresRestart key='foo' />", "    <requiresRestart key='bar' />", "</atlassian-plugin>").build();

        origFile.delete();
        FileUtils.moveFile(updateFile, origFile);

        manager.scanForNewPlugins();
        assertThat(getPluginAccessor().getPlugins(), hasSize(3));
        assertNotNull(getPluginAccessor().getPlugin("test.restartrequired"));
        assertTrue(getPluginAccessor().isPluginEnabled("test.restartrequired"));
        assertEquals(PluginRestartState.UPGRADE, getPluginAccessor().getPluginRestartState("test.restartrequired"));
        assertThat(getPluginAccessor().getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(1));

        manager.shutdown();
        manager.init();

        assertThat(getPluginAccessor().getPlugins(), hasSize(3));
        assertNotNull(getPluginAccessor().getPlugin("test.restartrequired"));
        assertTrue(getPluginAccessor().isPluginEnabled("test.restartrequired"));
        assertThat(getPluginAccessor().getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(2));
        assertEquals(PluginRestartState.NONE, getPluginAccessor().getPluginRestartState("test.restartrequired"));
    }

    @Test
    public void testUpgradePluginThatRequiresRestartThenReverted() throws PluginParseException, IOException, InterruptedException {
        createFillAndCleanTempPluginDirectory();
        moduleDescriptorFactory.addModuleDescriptor("requiresRestart", RequiresRestartModuleDescriptor.class);

        new PluginJarBuilder().addFormattedResource("atlassian-plugin.xml",
                "<atlassian-plugin name='Test 2' key='test.restartrequired' pluginsVersion='1'>",
                "    <plugin-info>",
                "        <version>1.0</version>",
                "    </plugin-info>",
                "    <requiresRestart key='foo' />",
                "</atlassian-plugin>")
                .build(pluginsTestDir);

        manager = makeClassLoadingPluginManager();
        manager.setPluginInstaller(new FilePluginInstaller(pluginsTestDir));

        assertThat(getPluginAccessor().getPlugins(), hasSize(3));
        assertNotNull(getPluginAccessor().getPlugin("test.restartrequired"));
        assertTrue(getPluginAccessor().isPluginEnabled("test.restartrequired"));
        assertThat(getPluginAccessor().getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(1));
        assertEquals(PluginRestartState.NONE, getPluginAccessor().getPluginRestartState("test.restartrequired"));

        // Some filesystems only record last modified in seconds
        Thread.sleep(1000);
        final File updateFile = new PluginJarBuilder().addFormattedResource("atlassian-plugin.xml",
                "<atlassian-plugin name='Test 2' key='test.restartrequired' pluginsVersion='1'>", "    <plugin-info>", "        <version>2.0</version>",
                "    </plugin-info>", "    <requiresRestart key='foo' />", "    <requiresRestart key='bar' />", "</atlassian-plugin>").build();

        manager.installPlugins(new JarPluginArtifact(updateFile));

        assertThat(getPluginAccessor().getPlugins(), hasSize(3));
        assertNotNull(getPluginAccessor().getPlugin("test.restartrequired"));
        assertTrue(getPluginAccessor().isPluginEnabled("test.restartrequired"));
        assertEquals(PluginRestartState.UPGRADE, getPluginAccessor().getPluginRestartState("test.restartrequired"));
        assertThat(getPluginAccessor().getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(1));

        manager.revertRestartRequiredChange("test.restartrequired");
        assertEquals(PluginRestartState.NONE, manager.getPluginRestartState("test.restartrequired"));

        manager.shutdown();
        manager.init();

        assertThat(getPluginAccessor().getPlugins(), hasSize(3));
        assertNotNull(getPluginAccessor().getPlugin("test.restartrequired"));
        assertTrue(getPluginAccessor().isPluginEnabled("test.restartrequired"));
        assertEquals("1.0", getPluginAccessor().getPlugin("test.restartrequired").getPluginInformation().getVersion());
        assertThat(getPluginAccessor().getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(1));
        assertEquals(PluginRestartState.NONE, getPluginAccessor().getPluginRestartState("test.restartrequired"));
    }

    @Test
    public void testUpgradePluginThatRequiresRestartThenRevertedRevertsToOriginalPlugin() throws PluginParseException, IOException, InterruptedException {
        createFillAndCleanTempPluginDirectory();
        moduleDescriptorFactory.addModuleDescriptor("requiresRestart", RequiresRestartModuleDescriptor.class);

        // Add the plugin to the plugins test directory so it is included when we first start up
        new PluginJarBuilder().addFormattedResource("atlassian-plugin.xml",
                "<atlassian-plugin name='Test 2' key='test.restartrequired' pluginsVersion='1'>", "    <plugin-info>", "        <version>1.0</version>",
                "    </plugin-info>", "    <requiresRestart key='foo' />", "</atlassian-plugin>").build(pluginsTestDir);

        manager = makeClassLoadingPluginManager();
        manager.setPluginInstaller(new FilePluginInstaller(pluginsTestDir));

        assertThat(getPluginAccessor().getPlugins(), hasSize(3));
        assertNotNull(getPluginAccessor().getPlugin("test.restartrequired"));
        assertTrue(getPluginAccessor().isPluginEnabled("test.restartrequired"));
        assertThat(getPluginAccessor().getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(1));
        assertEquals(PluginRestartState.NONE, getPluginAccessor().getPluginRestartState("test.restartrequired"));

        // Some filesystems only record last modified in seconds
        Thread.sleep(1000);
        final File updateFile = new PluginJarBuilder().addFormattedResource("atlassian-plugin.xml",
                "<atlassian-plugin name='Test 2' key='test.restartrequired' pluginsVersion='1'>", "    <plugin-info>", "        <version>2.0</version>",
                "    </plugin-info>", "    <requiresRestart key='foo' />", "    <requiresRestart key='bar' />", "</atlassian-plugin>").build();

        // Install version 2 of the plugin
        manager.installPlugins(new JarPluginArtifact(updateFile));

        assertThat(getPluginAccessor().getPlugins(), hasSize(3));
        assertNotNull(getPluginAccessor().getPlugin("test.restartrequired"));
        assertTrue(getPluginAccessor().isPluginEnabled("test.restartrequired"));
        assertEquals(PluginRestartState.UPGRADE, getPluginAccessor().getPluginRestartState("test.restartrequired"));
        assertThat(getPluginAccessor().getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(1));

        Thread.sleep(1000);
        final File updateFile2 = new PluginJarBuilder().addFormattedResource("atlassian-plugin.xml",
                "<atlassian-plugin name='Test 2' key='test.restartrequired' pluginsVersion='1'>", "    <plugin-info>", "        <version>3.0</version>",
                "    </plugin-info>", "    <requiresRestart key='foo' />", "    <requiresRestart key='bar' />", "</atlassian-plugin>").build();

        // Install version 3 of the plugin
        manager.installPlugins(new JarPluginArtifact(updateFile2));

        assertThat(getPluginAccessor().getPlugins(), hasSize(3));
        assertNotNull(getPluginAccessor().getPlugin("test.restartrequired"));
        assertTrue(getPluginAccessor().isPluginEnabled("test.restartrequired"));
        assertEquals(PluginRestartState.UPGRADE, getPluginAccessor().getPluginRestartState("test.restartrequired"));
        assertThat(getPluginAccessor().getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(1));

        // Lets revert the whole upgrade so that the original plugin is restored
        manager.revertRestartRequiredChange("test.restartrequired");
        assertEquals(PluginRestartState.NONE, manager.getPluginRestartState("test.restartrequired"));
        assertEquals("1.0", getPluginAccessor().getPlugin("test.restartrequired").getPluginInformation().getVersion());

        manager.shutdown();
        manager.init();

        assertThat(getPluginAccessor().getPlugins(), hasSize(3));
        assertNotNull(getPluginAccessor().getPlugin("test.restartrequired"));
        assertTrue(getPluginAccessor().isPluginEnabled("test.restartrequired"));
        assertEquals("1.0", getPluginAccessor().getPlugin("test.restartrequired").getPluginInformation().getVersion());
        assertThat(getPluginAccessor().getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(1));
        assertEquals(PluginRestartState.NONE, getPluginAccessor().getPluginRestartState("test.restartrequired"));
    }

    @Test
    public void testUpgradePluginThatRequiresRestartMultipleTimeStaysUpgraded() throws PluginParseException, IOException, InterruptedException {
        createFillAndCleanTempPluginDirectory();
        moduleDescriptorFactory.addModuleDescriptor("requiresRestart", RequiresRestartModuleDescriptor.class);

        final File origFile = new PluginJarBuilder().addFormattedResource("atlassian-plugin.xml",
                "<atlassian-plugin name='Test 2' key='test.restartrequired' pluginsVersion='1'>", "    <plugin-info>", "        <version>1.0</version>",
                "    </plugin-info>", "    <requiresRestart key='foo' />", "</atlassian-plugin>").build(pluginsTestDir);

        manager = makeClassLoadingPluginManager();
        manager.setPluginInstaller(new FilePluginInstaller(pluginsTestDir));

        assertThat(getPluginAccessor().getPlugins(), hasSize(3));
        assertNotNull(getPluginAccessor().getPlugin("test.restartrequired"));
        assertTrue(getPluginAccessor().isPluginEnabled("test.restartrequired"));
        assertThat(getPluginAccessor().getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(1));
        assertEquals(PluginRestartState.NONE, getPluginAccessor().getPluginRestartState("test.restartrequired"));

        // Some filesystems only record last modified in seconds
        Thread.sleep(1000);
        final File updateFile = new PluginJarBuilder().addFormattedResource("atlassian-plugin.xml",
                "<atlassian-plugin name='Test 2' key='test.restartrequired' pluginsVersion='1'>", "    <plugin-info>", "        <version>2.0</version>",
                "    </plugin-info>", "    <requiresRestart key='foo' />", "</atlassian-plugin>").build();

        manager.installPlugins(new JarPluginArtifact(updateFile));

        assertThat(getPluginAccessor().getPlugins(), hasSize(3));
        assertNotNull(getPluginAccessor().getPlugin("test.restartrequired"));
        assertTrue(getPluginAccessor().isPluginEnabled("test.restartrequired"));
        assertEquals(PluginRestartState.UPGRADE, getPluginAccessor().getPluginRestartState("test.restartrequired"));
        assertThat(getPluginAccessor().getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(1));

        Thread.sleep(1000);
        final File updateFile2 = new PluginJarBuilder().addFormattedResource("atlassian-plugin.xml",
                "<atlassian-plugin name='Test 2' key='test.restartrequired' pluginsVersion='1'>", "    <plugin-info>", "        <version>3.0</version>",
                "    </plugin-info>", "    <requiresRestart key='foo' />", "</atlassian-plugin>").build();

        manager.installPlugins(new JarPluginArtifact(updateFile2));

        assertThat(getPluginAccessor().getPlugins(), hasSize(3));
        assertNotNull(getPluginAccessor().getPlugin("test.restartrequired"));
        assertTrue(getPluginAccessor().isPluginEnabled("test.restartrequired"));
        assertEquals(PluginRestartState.UPGRADE, getPluginAccessor().getPluginRestartState("test.restartrequired"));
        assertThat(getPluginAccessor().getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(1));

        manager.shutdown();
        manager.init();

        assertThat(getPluginAccessor().getPlugins(), hasSize(3));
        assertNotNull(getPluginAccessor().getPlugin("test.restartrequired"));
        assertTrue(getPluginAccessor().isPluginEnabled("test.restartrequired"));
        assertThat(getPluginAccessor().getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(1));
        assertEquals(PluginRestartState.NONE, getPluginAccessor().getPluginRestartState("test.restartrequired"));
    }

    @Test
    public void testUpgradePluginThatPreviouslyRequiredRestart() throws PluginParseException, IOException, InterruptedException {
        createFillAndCleanTempPluginDirectory();
        moduleDescriptorFactory.addModuleDescriptor("requiresRestart", RequiresRestartModuleDescriptor.class);

        final File origFile = new PluginJarBuilder().addFormattedResource("atlassian-plugin.xml",
                "<atlassian-plugin name='Test 2' key='test.restartrequired' pluginsVersion='1'>", "    <plugin-info>", "        <version>1.0</version>",
                "    </plugin-info>", "    <requiresRestart key='foo' />", "</atlassian-plugin>").build(pluginsTestDir);

        manager = makeClassLoadingPluginManager();

        assertThat(getPluginAccessor().getPlugins(), hasSize(3));
        assertNotNull(getPluginAccessor().getPlugin("test.restartrequired"));
        assertTrue(getPluginAccessor().isPluginEnabled("test.restartrequired"));
        assertThat(getPluginAccessor().getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(1));
        assertEquals(PluginRestartState.NONE, getPluginAccessor().getPluginRestartState("test.restartrequired"));

        // Some filesystems only record last modified in seconds
        Thread.sleep(1000);
        final File updateFile = new PluginJarBuilder().addFormattedResource("atlassian-plugin.xml",
                "<atlassian-plugin name='Test 2' key='test.restartrequired' pluginsVersion='1'>", "    <plugin-info>", "        <version>2.0</version>",
                "    </plugin-info>", "</atlassian-plugin>").build(pluginsTestDir);

        origFile.delete();
        FileUtils.moveFile(updateFile, origFile);

        manager.scanForNewPlugins();
        assertThat(getPluginAccessor().getPlugins(), hasSize(3));
        assertNotNull(getPluginAccessor().getPlugin("test.restartrequired"));
        assertTrue(getPluginAccessor().isPluginEnabled("test.restartrequired"));
        assertEquals(PluginRestartState.UPGRADE, getPluginAccessor().getPluginRestartState("test.restartrequired"));
        assertThat(getPluginAccessor().getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(1));

        manager.shutdown();
        manager.init();

        assertThat(getPluginAccessor().getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(0));
        assertEquals(PluginRestartState.NONE, getPluginAccessor().getPluginRestartState("test.restartrequired"));
    }

    @Test
    public void testInstallPluginThatPreviouslyRequiredRestart() throws PluginParseException, IOException, InterruptedException {
        createFillAndCleanTempPluginDirectory();
        moduleDescriptorFactory.addModuleDescriptor("requiresRestart", RequiresRestartModuleDescriptor.class);

        manager = makeClassLoadingPluginManager();

        assertThat(getPluginAccessor().getPlugins(), hasSize(2));
        assertNull(getPluginAccessor().getPlugin("test.restartrequired"));

        final File origFile = new PluginJarBuilder().addFormattedResource("atlassian-plugin.xml",
                "<atlassian-plugin name='Test 2' key='test.restartrequired' pluginsVersion='1'>", "    <plugin-info>", "        <version>1.0</version>",
                "    </plugin-info>", "    <requiresRestart key='foo' />", "</atlassian-plugin>").build(pluginsTestDir);

        manager.scanForNewPlugins();
        assertThat(getPluginAccessor().getPlugins(), hasSize(3));
        assertNotNull(getPluginAccessor().getPlugin("test.restartrequired"));
        assertFalse(getPluginAccessor().isPluginEnabled("test.restartrequired"));
        assertEquals(PluginRestartState.INSTALL, getPluginAccessor().getPluginRestartState("test.restartrequired"));

        // Some filesystems only record last modified in seconds
        Thread.sleep(1000);
        final File updateFile = new PluginJarBuilder().addFormattedResource("atlassian-plugin.xml",
                "<atlassian-plugin name='Test 2' key='test.restartrequired' pluginsVersion='1'>", "    <plugin-info>", "        <version>2.0</version>",
                "    </plugin-info>", "</atlassian-plugin>").build(pluginsTestDir);

        origFile.delete();
        FileUtils.moveFile(updateFile, origFile);

        manager.scanForNewPlugins();
        assertThat(getPluginAccessor().getPlugins(), hasSize(3));
        assertNotNull(getPluginAccessor().getPlugin("test.restartrequired"));
        assertTrue(getPluginAccessor().isPluginEnabled("test.restartrequired"));
        assertEquals(PluginRestartState.NONE, getPluginAccessor().getPluginRestartState("test.restartrequired"));
        assertThat(getPluginAccessor().getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(0));

        manager.shutdown();
        manager.init();

        assertThat(getPluginAccessor().getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(0));
        assertEquals(PluginRestartState.NONE, getPluginAccessor().getPluginRestartState("test.restartrequired"));
    }

    @Test
    public void testInstallPluginMoreThanOnceStaysAsInstall() throws PluginParseException, IOException, InterruptedException {
        createFillAndCleanTempPluginDirectory();
        moduleDescriptorFactory.addModuleDescriptor("requiresRestart", RequiresRestartModuleDescriptor.class);

        manager = makeClassLoadingPluginManager();

        assertThat(getPluginAccessor().getPlugins(), hasSize(2));
        assertNull(getPluginAccessor().getPlugin("test.restartrequired"));
        assertThat(getPluginAccessor().getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(0));

        final File origFile = new PluginJarBuilder().addFormattedResource("atlassian-plugin.xml",
                "<atlassian-plugin name='Test 2' key='test.restartrequired' pluginsVersion='1'>", "    <plugin-info>", "        <version>1.0</version>",
                "    </plugin-info>", "    <requiresRestart key='foo' />", "</atlassian-plugin>").build(pluginsTestDir);

        manager.scanForNewPlugins();
        assertThat(getPluginAccessor().getPlugins(), hasSize(3));
        assertNotNull(getPluginAccessor().getPlugin("test.restartrequired"));
        assertFalse(getPluginAccessor().isPluginEnabled("test.restartrequired"));
        assertEquals(PluginRestartState.INSTALL, getPluginAccessor().getPluginRestartState("test.restartrequired"));

        // Some filesystems only record last modified in seconds
        Thread.sleep(1000);
        final File updateFile = new PluginJarBuilder().addFormattedResource("atlassian-plugin.xml",
                "<atlassian-plugin name='Test 2' key='test.restartrequired' pluginsVersion='1'>", "    <plugin-info>", "        <version>2.0</version>",
                "    </plugin-info>", "    <requiresRestart key='foo' />", "</atlassian-plugin>").build(pluginsTestDir);

        origFile.delete();
        FileUtils.moveFile(updateFile, origFile);

        manager.scanForNewPlugins();
        assertThat(getPluginAccessor().getPlugins(), hasSize(3));
        assertNotNull(getPluginAccessor().getPlugin("test.restartrequired"));
        assertFalse(getPluginAccessor().isPluginEnabled("test.restartrequired"));
        assertEquals(PluginRestartState.INSTALL, getPluginAccessor().getPluginRestartState("test.restartrequired"));

        manager.shutdown();
        manager.init();

        assertThat(getPluginAccessor().getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(1));
        assertEquals(PluginRestartState.NONE, getPluginAccessor().getPluginRestartState("test.restartrequired"));
    }

    @Test
    public void testRemovePluginThatRequiresRestart() throws PluginParseException, IOException {
        createFillAndCleanTempPluginDirectory();
        moduleDescriptorFactory.addModuleDescriptor("requiresRestart", RequiresRestartModuleDescriptor.class);

        final File pluginFile = new PluginJarBuilder().addFormattedResource("atlassian-plugin.xml",
                "<atlassian-plugin name='Test 2' key='test.restartrequired' pluginsVersion='1'>", "    <plugin-info>", "        <version>1.0</version>",
                "    </plugin-info>", "    <requiresRestart key='foo' />", "</atlassian-plugin>").build(pluginsTestDir);

        manager = makeClassLoadingPluginManager();

        assertThat(getPluginAccessor().getPlugins(), hasSize(3));
        assertNotNull(getPluginAccessor().getPlugin("test.restartrequired"));
        assertTrue(getPluginAccessor().isPluginEnabled("test.restartrequired"));
        assertThat(getPluginAccessor().getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(1));
        assertEquals(PluginRestartState.NONE, getPluginAccessor().getPluginRestartState("test.restartrequired"));

        manager.uninstall(getPluginAccessor().getPlugin("test.restartrequired"));

        assertThat(getPluginAccessor().getPlugins(), hasSize(3));
        assertNotNull(getPluginAccessor().getPlugin("test.restartrequired"));
        assertTrue(getPluginAccessor().isPluginEnabled("test.restartrequired"));
        assertEquals(PluginRestartState.REMOVE, getPluginAccessor().getPluginRestartState("test.restartrequired"));
        assertThat(getPluginAccessor().getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(1));

        manager.shutdown();
        manager.init();

        assertFalse(pluginFile.exists());
        assertThat(getPluginAccessor().getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(0));
        assertThat(getPluginAccessor().getPlugins(), hasSize(2));
    }

    @Test
    public void testRemovePluginThatRequiresRestartThenReverted() throws PluginParseException, IOException {
        createFillAndCleanTempPluginDirectory();
        moduleDescriptorFactory.addModuleDescriptor("requiresRestart", RequiresRestartModuleDescriptor.class);

        final File pluginFile = new PluginJarBuilder().addFormattedResource("atlassian-plugin.xml",
                "<atlassian-plugin name='Test 2' key='test.restartrequired' pluginsVersion='1'>", "    <plugin-info>", "        <version>1.0</version>",
                "    </plugin-info>", "    <requiresRestart key='foo' />", "</atlassian-plugin>").build(pluginsTestDir);

        manager = makeClassLoadingPluginManager();

        assertThat(getPluginAccessor().getPlugins(), hasSize(3));
        assertNotNull(getPluginAccessor().getPlugin("test.restartrequired"));
        assertTrue(getPluginAccessor().isPluginEnabled("test.restartrequired"));
        assertThat(getPluginAccessor().getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(1));
        assertEquals(PluginRestartState.NONE, getPluginAccessor().getPluginRestartState("test.restartrequired"));

        manager.uninstall(getPluginAccessor().getPlugin("test.restartrequired"));

        assertThat(getPluginAccessor().getPlugins(), hasSize(3));
        assertNotNull(getPluginAccessor().getPlugin("test.restartrequired"));
        assertTrue(getPluginAccessor().isPluginEnabled("test.restartrequired"));
        assertEquals(PluginRestartState.REMOVE, getPluginAccessor().getPluginRestartState("test.restartrequired"));
        assertThat(getPluginAccessor().getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(1));

        manager.revertRestartRequiredChange("test.restartrequired");
        assertEquals(PluginRestartState.NONE, getPluginAccessor().getPluginRestartState("test.restartrequired"));

        manager.shutdown();
        manager.init();

        assertThat(getPluginAccessor().getPlugins(), hasSize(3));
        assertNotNull(getPluginAccessor().getPlugin("test.restartrequired"));
        assertTrue(getPluginAccessor().isPluginEnabled("test.restartrequired"));
        assertThat(getPluginAccessor().getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(1));
        assertEquals(PluginRestartState.NONE, getPluginAccessor().getPluginRestartState("test.restartrequired"));
    }

    @Test
    public void testRemovePluginThatRequiresRestartViaSubclass() throws PluginParseException, IOException {
        createFillAndCleanTempPluginDirectory();
        moduleDescriptorFactory.addModuleDescriptor("requiresRestartSubclass", RequiresRestartSubclassModuleDescriptor.class);

        final File pluginFile = new PluginJarBuilder().addFormattedResource("atlassian-plugin.xml",
                "<atlassian-plugin name='Test 2' key='test.restartrequired' pluginsVersion='1'>", "    <plugin-info>", "        <version>1.0</version>",
                "    </plugin-info>", "    <requiresRestartSubclass key='foo' />", "</atlassian-plugin>").build(pluginsTestDir);

        manager = makeClassLoadingPluginManager();

        assertThat(getPluginAccessor().getPlugins(), hasSize(3));
        assertNotNull(getPluginAccessor().getPlugin("test.restartrequired"));
        assertTrue(getPluginAccessor().isPluginEnabled("test.restartrequired"));
        assertThat(getPluginAccessor().getEnabledModuleDescriptorsByClass(RequiresRestartSubclassModuleDescriptor.class), hasSize(1));
        assertEquals(PluginRestartState.NONE, getPluginAccessor().getPluginRestartState("test.restartrequired"));

        manager.uninstall(getPluginAccessor().getPlugin("test.restartrequired"));

        assertThat(getPluginAccessor().getPlugins(), hasSize(3));
        assertNotNull(getPluginAccessor().getPlugin("test.restartrequired"));
        assertTrue(getPluginAccessor().isPluginEnabled("test.restartrequired"));
        assertEquals(PluginRestartState.REMOVE, getPluginAccessor().getPluginRestartState("test.restartrequired"));
        assertThat(getPluginAccessor().getEnabledModuleDescriptorsByClass(RequiresRestartSubclassModuleDescriptor.class), hasSize(1));

        manager.shutdown();
        manager.init();

        assertFalse(pluginFile.exists());
        assertThat(getPluginAccessor().getEnabledModuleDescriptorsByClass(RequiresRestartSubclassModuleDescriptor.class), hasSize(0));
        assertThat(getPluginAccessor().getPlugins(), hasSize(2));
    }

    @Test
    public void testDisableEnableOfPluginThatRequiresRestart() throws PluginParseException, IOException {
        createFillAndCleanTempPluginDirectory();
        moduleDescriptorFactory.addModuleDescriptor("requiresRestart", RequiresRestartModuleDescriptor.class);

        new PluginJarBuilder().addFormattedResource("atlassian-plugin.xml",
                "<atlassian-plugin name='Test 2' key='test.restartrequired' pluginsVersion='1'>", "    <plugin-info>", "        <version>1.0</version>",
                "    </plugin-info>", "    <requiresRestart key='foo' />", "</atlassian-plugin>").build(pluginsTestDir);

        manager = makeClassLoadingPluginManager();

        assertThat(getPluginAccessor().getPlugins(), hasSize(3));
        assertNotNull(getPluginAccessor().getPlugin("test.restartrequired"));
        assertTrue(getPluginAccessor().isPluginEnabled("test.restartrequired"));
        assertThat(getPluginAccessor().getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(1));
        assertEquals(PluginRestartState.NONE, getPluginAccessor().getPluginRestartState("test.restartrequired"));

        manager.disablePlugin("test.restartrequired");
        assertFalse(getPluginAccessor().isPluginEnabled("test.restartrequired"));
        manager.enablePlugins("test.restartrequired");

        assertThat(getPluginAccessor().getPlugins(), hasSize(3));
        assertNotNull(getPluginAccessor().getPlugin("test.restartrequired"));
        assertTrue(getPluginAccessor().isPluginEnabled("test.restartrequired"));
        assertEquals(PluginRestartState.NONE, getPluginAccessor().getPluginRestartState("test.restartrequired"));
        assertThat(getPluginAccessor().getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(1));
    }

    @Test
    public void testCannotRemovePluginFromStaticLoader() throws PluginParseException, IOException {
        createFillAndCleanTempPluginDirectory();
        moduleDescriptorFactory.addModuleDescriptor("requiresRestart", RequiresRestartModuleDescriptor.class);

        directoryPluginLoader = new DirectoryPluginLoader(
                pluginsTestDir,
                ImmutableList.of(new LegacyDynamicPluginFactory(PluginAccessor.Descriptor.FILENAME)),
                pluginEventManager);

        manager = newDefaultPluginManager(directoryPluginLoader, new SinglePluginLoader("test-requiresRestart-plugin.xml"));
        manager.init();

        assertThat(getPluginAccessor().getPlugins(), hasSize(3));
        assertNotNull(getPluginAccessor().getPlugin("test.atlassian.plugin"));
        assertTrue(getPluginAccessor().isPluginEnabled("test.atlassian.plugin"));
        assertThat(getPluginAccessor().getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(1));
        assertEquals(PluginRestartState.NONE, getPluginAccessor().getPluginRestartState("test.atlassian.plugin"));

        try {
            manager.uninstall(getPluginAccessor().getPlugin("test.atlassian.plugin"));
            fail();
        } catch (final PluginException ex) {
            // test passed
        }

        assertThat(getPluginAccessor().getPlugins(), hasSize(3));
        assertNotNull(getPluginAccessor().getPlugin("test.atlassian.plugin"));
        assertTrue(getPluginAccessor().isPluginEnabled("test.atlassian.plugin"));
        assertEquals(PluginRestartState.NONE, getPluginAccessor().getPluginRestartState("test.atlassian.plugin"));
        assertThat(getPluginAccessor().getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(1));
    }

    private DefaultPluginManager makeClassLoadingPluginManager(PluginLoader... pluginLoaders) throws PluginParseException {
        moduleDescriptorFactory.addModuleDescriptor("animal", MockAnimalModuleDescriptor.class);

        directoryPluginLoader = new DirectoryPluginLoader(pluginsTestDir, ImmutableList.of(
                new LegacyDynamicPluginFactory(PluginAccessor.Descriptor.FILENAME)), pluginEventManager);

        final DefaultPluginManager manager = newDefaultPluginManager(Iterables.toArray(ImmutableList.<PluginLoader>builder().add(directoryPluginLoader).addAll(copyOf(pluginLoaders)).build(), PluginLoader.class));

        manager.init();
        return manager;
    }

    @Test
    public void testRemovingPlugins() throws PluginException, IOException {
        createFillAndCleanTempPluginDirectory();

        manager = makeClassLoadingPluginManager();
        assertThat(getPluginAccessor().getPlugins(), hasSize(2));
        final MockAnimalModuleDescriptor moduleDescriptor = (MockAnimalModuleDescriptor) manager.getPluginModule("test.atlassian.plugin.classloaded:paddington");
        assertTrue(moduleDescriptor.isEnabled());
        final PassListener disabledListener = new PassListener(PluginDisabledEvent.class);
        pluginEventManager.register(disabledListener);
        final Plugin plugin = getPluginAccessor().getPlugin("test.atlassian.plugin.classloaded");
        manager.uninstall(plugin);
        assertTrue("Module must have had disable() called before being removed", !moduleDescriptor.isEnabled());

        // uninstalling a plugin should remove it's state completely from the state store - PLUG-13
        assertTrue(pluginStateStore.load().getPluginEnabledStateMap(plugin).isEmpty());
        // test the deprecated method too
        assertTrue(pluginStateStore.load().getPluginStateMap(plugin).isEmpty());

        assertThat(getPluginAccessor().getPlugins(), hasSize(1));
        // plugin is no longer available though the plugin manager
        assertNull(getPluginAccessor().getPlugin("test.atlassian.plugin.classloaded"));
        assertEquals(1, pluginsTestDir.listFiles().length);
        disabledListener.assertCalled();
    }

    @Test
    public void testPluginModuleAvailableAfterInstallation() {
        Plugin plugin = mock(Plugin.class);
        when(plugin.getKey()).thenReturn("dynPlugin");
        when(plugin.isEnabledByDefault()).thenReturn(true);
        when(plugin.isDeleteable()).thenReturn(true);
        when(plugin.isUninstallable()).thenReturn(true);
        when(plugin.getPluginState()).thenReturn(PluginState.ENABLED);
        when(plugin.compareTo(any(Plugin.class))).thenReturn(-1);
        when(plugin.getDependencies()).thenReturn(new PluginDependencies());

        PluginLoader pluginLoader = mockPluginLoaderForPlugins(plugin);
        when(pluginLoader.supportsRemoval()).thenReturn(true);

        manager = newDefaultPluginManager(pluginLoader);
        manager.init();

        PluginModuleEnabledListener listener = new PluginModuleEnabledListener();
        pluginEventManager.register(listener);
        Collection<ModuleDescriptor<?>> mods = new ArrayList<>();
        MockModuleDescriptor<String> moduleDescriptor = new MockModuleDescriptor<>(plugin, "foo", "foo");
        mods.add(moduleDescriptor);
        when(plugin.getModuleDescriptors()).thenReturn(mods);
        when(plugin.getModuleDescriptor("foo")).thenReturn((ModuleDescriptor) moduleDescriptor);
        pluginEventManager.broadcast(new PluginModuleAvailableEvent(moduleDescriptor));

        assertTrue(getPluginAccessor().isPluginModuleEnabled("dynPlugin:foo"));
        assertTrue(listener.called);
    }

    @Test
    public void testPluginModuleAvailableAfterInstallationButConfiguredToBeDisabled() {
        Plugin plugin = mock(Plugin.class);
        when(plugin.getKey()).thenReturn("dynPlugin");
        when(plugin.isEnabledByDefault()).thenReturn(true);
        when(plugin.isDeleteable()).thenReturn(true);
        when(plugin.isUninstallable()).thenReturn(true);
        when(plugin.getPluginState()).thenReturn(PluginState.ENABLED);
        when(plugin.compareTo(any(Plugin.class))).thenReturn(-1);
        when(plugin.getDependencies()).thenReturn(new PluginDependencies());

        PluginLoader pluginLoader = mockPluginLoaderForPlugins(plugin);
        when(pluginLoader.supportsRemoval()).thenReturn(true);

        manager = newDefaultPluginManager(pluginLoader);
        manager.init();

        MockModuleDescriptor<String> moduleDescriptor = new MockModuleDescriptor<>(plugin, "foo", "foo");

        new PluginPersistentStateModifier(pluginStateStore).disable(moduleDescriptor);

        PluginModuleEnabledListener listener = new PluginModuleEnabledListener();
        pluginEventManager.register(listener);
        Collection<ModuleDescriptor<?>> mods = new ArrayList<>();
        mods.add(moduleDescriptor);

        when(plugin.getModuleDescriptors()).thenReturn(mods);
        when(plugin.getModuleDescriptor("foo")).thenReturn((ModuleDescriptor) moduleDescriptor);
        pluginEventManager.broadcast(new PluginModuleAvailableEvent(moduleDescriptor));

        assertFalse(getPluginAccessor().isPluginModuleEnabled("dynPlugin:foo"));
        assertFalse(listener.called);
    }

    @Test
    public void testPluginModuleUnavailableAfterInstallation() {
        Plugin plugin = mock(Plugin.class);
        when(plugin.getKey()).thenReturn("dynPlugin");
        when(plugin.isEnabledByDefault()).thenReturn(true);
        when(plugin.isDeleteable()).thenReturn(true);
        when(plugin.isUninstallable()).thenReturn(true);
        when(plugin.getPluginState()).thenReturn(PluginState.ENABLED);
        when(plugin.compareTo(any(Plugin.class))).thenReturn(-1);
        when(plugin.getDependencies()).thenReturn(new PluginDependencies());

        PluginLoader pluginLoader = mockPluginLoaderForPlugins(plugin);
        when(pluginLoader.supportsRemoval()).thenReturn(true);
        when(pluginLoader.loadAllPlugins(any(ModuleDescriptorFactory.class))).thenReturn(Arrays.asList(plugin));

        manager = newDefaultPluginManager(pluginLoader);
        manager.init();

        PluginModuleDisabledListener listener = new PluginModuleDisabledListener();
        pluginEventManager.register(listener);
        Collection<ModuleDescriptor<?>> mods = new ArrayList<>();
        MockModuleDescriptor<String> moduleDescriptor = new MockModuleDescriptor<>(plugin, "foo", "foo");
        mods.add(moduleDescriptor);
        when(plugin.getModuleDescriptors()).thenReturn(mods);
        when(plugin.getModuleDescriptor("foo")).thenReturn((ModuleDescriptor) moduleDescriptor);
        pluginEventManager.broadcast(new PluginModuleAvailableEvent(moduleDescriptor));
        assertTrue(getPluginAccessor().isPluginModuleEnabled("dynPlugin:foo"));
        assertFalse(listener.called);
        pluginEventManager.broadcast(new PluginModuleUnavailableEvent(moduleDescriptor));
        assertTrue(listener.called);
    }

    @Test
    public void testPluginModuleDisabledOnStartupUnavailableAfterInstallation() {
        PluginLoader pluginLoader = mock(PluginLoader.class);
        when(pluginLoader.supportsRemoval()).thenReturn(true);
        Plugin plugin = mock(Plugin.class);
        when(plugin.getKey()).thenReturn("dynPlugin");
        when(plugin.isEnabledByDefault()).thenReturn(true);
        when(plugin.isDeleteable()).thenReturn(true);
        when(plugin.isUninstallable()).thenReturn(true);
        when(plugin.getPluginState()).thenReturn(PluginState.ENABLED);
        when(plugin.compareTo(any(Plugin.class))).thenReturn(-1);
        when(plugin.getDependencies()).thenReturn(new PluginDependencies());
        when(pluginLoader.loadAllPlugins(any(ModuleDescriptorFactory.class))).thenReturn(Arrays.asList(plugin));

        manager = newDefaultPluginManager(pluginLoader);
        manager.init();

        PluginModuleDisabledListener listener = new PluginModuleDisabledListener();
        pluginEventManager.register(listener);
        Collection<ModuleDescriptor<?>> mods = new ArrayList<>();
        MockModuleDescriptor<String> moduleDescriptor = new MockModuleDescriptor<>(plugin, "foo", "foo");
        mods.add(moduleDescriptor);
        when(plugin.getModuleDescriptors()).thenReturn(mods);
        when(plugin.getModuleDescriptor("foo")).thenReturn((ModuleDescriptor) moduleDescriptor);

        // ideally, we'd set enabledByDefault to false, but that requires calling init with a valid Element.
        // Luckily, setting the persistent state of the module to disabled also does what we need.
        pluginStateStore.save(PluginPersistentState.Builder.create(pluginStateStore.load()).setEnabled(
                moduleDescriptor, false).toState());

        pluginEventManager.broadcast(new PluginModuleAvailableEvent(moduleDescriptor));
        assertFalse(getPluginAccessor().isPluginModuleEnabled("dynPlugin:foo"));
        assertFalse(listener.called);
        pluginEventManager.broadcast(new PluginModuleUnavailableEvent(moduleDescriptor));
        assertFalse(listener.called);
    }

    @Test
    public void testPluginContainerUnavailable() {
        Plugin plugin = mock(Plugin.class);
        when(plugin.getKey()).thenReturn("dynPlugin");
        when(plugin.isEnabledByDefault()).thenReturn(true);
        when(plugin.isDeleteable()).thenReturn(true);
        when(plugin.isUninstallable()).thenReturn(true);
        when(plugin.getPluginState()).thenReturn(PluginState.ENABLED);
        when(plugin.compareTo(any(Plugin.class))).thenReturn(-1);
        Collection<ModuleDescriptor<?>> mods = new ArrayList<>();
        MockModuleDescriptor<String> moduleDescriptor = new MockModuleDescriptor<>(plugin, "foo", "foo");
        mods.add(moduleDescriptor);
        when(plugin.getModuleDescriptors()).thenReturn(mods);
        when(plugin.getModuleDescriptor("foo")).thenReturn((ModuleDescriptor) moduleDescriptor);
        when(plugin.getDependencies()).thenReturn(new PluginDependencies());

        PluginLoader pluginLoader = mockPluginLoaderForPlugins(plugin);
        when(pluginLoader.supportsRemoval()).thenReturn(true);

        manager = newDefaultPluginManager(pluginLoader);
        manager.init();

        PluginDisabledListener listener = new PluginDisabledListener();
        PluginModuleDisabledListener moduleDisabledListener = new PluginModuleDisabledListener();
        pluginEventManager.register(listener);
        pluginEventManager.register(moduleDisabledListener);
        when(plugin.getPluginState()).thenReturn(PluginState.DISABLED);
        pluginEventManager.broadcast(new PluginContainerUnavailableEvent("dynPlugin"));
        //Fix in behaviour, if the plugin is already disabled and you try to disable it there should be no event called
        assertFalse(getPluginAccessor().isPluginEnabled("dynPlugin"));
        assertFalse(listener.called);
        assertFalse(moduleDisabledListener.called);
    }

    @Test
    public void testNonRemovablePlugins() throws PluginParseException {
        moduleDescriptorFactory.addModuleDescriptor("animal", MockAnimalModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("mineral", MockMineralModuleDescriptor.class);

        manager = newDefaultPluginManager(new SinglePluginLoader("test-atlassian-plugin.xml"));
        manager.init();

        final Plugin plugin = getPluginAccessor().getPlugin("test.atlassian.plugin");
        assertFalse(plugin.isUninstallable());
        assertNotNull(plugin.getResourceAsStream("test-atlassian-plugin.xml"));

        try {
            manager.uninstall(plugin);
            fail("Where was the exception?");
        } catch (final PluginException p) {
        }
    }

    @Test
    public void testNonDeletablePlugins() throws PluginException, IOException {
        createFillAndCleanTempPluginDirectory();

        manager = makeClassLoadingPluginManager();
        assertThat(getPluginAccessor().getPlugins(), hasSize(2));

        // Set plugin file can't be deleted.
        final Plugin pluginToRemove = spy(getPluginAccessor().getPlugin("test.atlassian.plugin.classloaded"));
        when(pluginToRemove.isDeleteable()).thenReturn(false);

        // Disable plugin module before uninstall
        final MockAnimalModuleDescriptor moduleDescriptor = (MockAnimalModuleDescriptor) getPluginAccessor().getPluginModule("test.atlassian.plugin.classloaded:paddington");
        assertTrue(moduleDescriptor.isEnabled());

        manager.uninstall(pluginToRemove);

        assertFalse("Module must have had disable() called before being removed", moduleDescriptor.isEnabled());
        assertThat(getPluginAccessor().getPlugins(), hasSize(1));
        assertNull(getPluginAccessor().getPlugin("test.atlassian.plugin.classloaded"));
        assertEquals(2, pluginsTestDir.listFiles().length);
    }

    @Test
    public void testInvalidationOfDynamicResourceCache() throws IOException, PluginException {
        createFillAndCleanTempPluginDirectory();

        manager = makeClassLoadingPluginManager();

        checkResources(manager, true, true);
        manager.disablePlugin("test.atlassian.plugin.classloaded");
        checkResources(manager, false, false);
        manager.enablePlugins("test.atlassian.plugin.classloaded");
        checkResources(manager, true, true);
        manager.uninstall(getPluginAccessor().getPlugin("test.atlassian.plugin.classloaded"));
        checkResources(manager, false, false);
        //restore paddington to test plugins dir
        FileUtils.copyDirectory(pluginsDirectory, pluginsTestDir);
        manager.scanForNewPlugins();
        checkResources(manager, true, true);
    }

    @Test
    public void testValidatePlugin() throws PluginParseException {
        final DynamicPluginLoader mockLoader = mock(DynamicPluginLoader.class);
        when(mockLoader.isDynamicPluginLoader()).thenReturn(true);

        manager = new DefaultPluginManager(pluginStateStore, ImmutableList.of(mockLoader), moduleDescriptorFactory, new DefaultPluginEventManager());

        final PluginArtifact mockPluginJar = mock(PluginArtifact.class);
        final PluginArtifact pluginArtifact = mockPluginJar;
        when(mockLoader.canLoad(pluginArtifact)).thenReturn("foo");

        final String key = manager.validatePlugin(pluginArtifact);
        assertEquals("foo", key);
        verify(mockLoader).canLoad(pluginArtifact);
    }

    @Test
    public void testValidatePluginWithNoDynamicLoaders() throws PluginParseException {
        final PluginLoader loader = mock(PluginLoader.class);
        final DefaultPluginManager manager = new DefaultPluginManager(pluginStateStore, ImmutableList.of(loader), moduleDescriptorFactory, new DefaultPluginEventManager());

        final PluginArtifact pluginArtifact = mock(PluginArtifact.class);
        try {
            manager.validatePlugin(pluginArtifact);
            fail("Should have thrown exception");
        } catch (final IllegalStateException ex) {
            // test passed
        }
    }

    @Test
    public void testInvalidationOfDynamicClassCache() throws IOException, PluginException {
        createFillAndCleanTempPluginDirectory();

        manager = makeClassLoadingPluginManager();

        checkClasses(manager, true);
        manager.disablePlugin("test.atlassian.plugin.classloaded");
        checkClasses(manager, false);
        manager.enablePlugins("test.atlassian.plugin.classloaded");
        checkClasses(manager, true);
        manager.uninstall(getPluginAccessor().getPlugin("test.atlassian.plugin.classloaded"));
        checkClasses(manager, false);
        //restore paddington to test plugins dir
        FileUtils.copyDirectory(pluginsDirectory, pluginsTestDir);
        manager.scanForNewPlugins();
        checkClasses(manager, true);
    }

    @Test
    public void testInstallTwoPluginsButOneFailsToEnableAModuleAndThenFailsToDisableAModule() {
        final PluginLoader mockPluginLoader = mock(PluginLoader.class);
        final ModuleDescriptor<Object> failEnableModuleDescriptor = mockFailingModuleDescriptor("foo:bar", FAIL_TO_ENABLE);
        final ModuleDescriptor<Object> failDisableModuleDescriptor = mockFailingModuleDescriptor("foo:buzz", FAIL_TO_DISABLE);

        Plugin badPlugin = mockStaticPlugin("foo", failDisableModuleDescriptor, failEnableModuleDescriptor);

        final AbstractModuleDescriptor<?> goodModuleDescriptor = mock(AbstractModuleDescriptor.class);
        when(goodModuleDescriptor.getKey()).thenReturn("baz");
        when(goodModuleDescriptor.getPluginKey()).thenReturn("good");
        when(goodModuleDescriptor.isEnabledByDefault()).thenReturn(true);
        Plugin goodPlugin = mockStaticPlugin("good", goodModuleDescriptor);

        when(mockPluginLoader.loadAllPlugins(isA(ModuleDescriptorFactory.class))).thenReturn(Lists.newArrayList(badPlugin, goodPlugin));

        manager = newDefaultPluginManager(mockPluginLoader);
        manager.init();

        assertThat(getPluginAccessor().getPlugins(), hasSize(2));
        assertThat(getPluginAccessor().getEnabledPlugins(), hasSize(1));
        verify(goodModuleDescriptor).enabled();
    }

    private <T> void checkResources(final PluginAccessor manager, final boolean canGetGlobal, final boolean canGetModule) throws IOException {
        InputStream is = manager.getDynamicResourceAsStream("icon.gif");
        assertEquals(canGetGlobal, is != null);
        IOUtils.closeQuietly(is);
        is = manager.getDynamicResourceAsStream("bear/paddington.vm");
        assertEquals(canGetModule, is != null);
        IOUtils.closeQuietly(is);
    }

    private void checkClasses(final PluginAccessor manager, final boolean canGet) {
        try {
            manager.getClassLoader().loadClass("com.atlassian.plugin.mock.MockPaddington");
            if (!canGet) {
                fail("Class in plugin was successfully loaded");
            }
        } catch (final ClassNotFoundException e) {
            if (canGet) {
                fail(e.getMessage());
            }
        }
    }

    @Test
    public void testUninstallPluginClearsState() throws IOException {
        createFillAndCleanTempPluginDirectory();

        manager = makeClassLoadingPluginManager();

        checkClasses(manager, true);
        final Plugin plugin = getPluginAccessor().getPlugin("test.atlassian.plugin.classloaded");

        final ModuleDescriptor<?> module = plugin.getModuleDescriptor("paddington");
        assertTrue(getPluginAccessor().isPluginModuleEnabled(module.getCompleteKey()));
        manager.disablePluginModule(module.getCompleteKey());
        assertFalse(getPluginAccessor().isPluginModuleEnabled(module.getCompleteKey()));
        manager.uninstall(plugin);
        assertFalse(getPluginAccessor().isPluginModuleEnabled(module.getCompleteKey()));
        assertTrue(pluginStateStore.load().getPluginEnabledStateMap(plugin).isEmpty());
        // test the deprecated method too
        assertTrue(pluginStateStore.load().getPluginStateMap(plugin).isEmpty());
    }

    @Test
    public void testUninstallDisabledPlugin() throws IOException {
        createFillAndCleanTempPluginDirectory();

        manager = makeClassLoadingPluginManager();

        final Plugin plugin = getPluginAccessor().getPlugin("test.atlassian.plugin.classloaded");

        manager.disablePlugin(plugin.getKey());
        assertEquals(PluginState.DISABLED, plugin.getPluginState());
        manager.uninstall(plugin);
        assertEquals(PluginState.UNINSTALLED, plugin.getPluginState());
    }

    /**
     * Also relevant to {@link TestDefaultPluginManagerWithCachingPluginAccessor}
     */
    @Test
    public void testDisabledPluginClearsModulesByClass() throws IOException {
        final Class<MockAnimalModuleDescriptor> descriptorClass = MockAnimalModuleDescriptor.class;
        moduleDescriptorFactory.addModuleDescriptor("animal", descriptorClass);
        moduleDescriptorFactory.addModuleDescriptor("mineral", MockMineralModuleDescriptor.class);

        manager = newDefaultPluginManager(new SinglePluginLoaderWithRemoval("test-atlassian-plugin.xml"));
        manager.init();

        final List<MockAnimalModuleDescriptor> animalDescriptors =
                getPluginAccessor().getEnabledModuleDescriptorsByClass(descriptorClass);
        assertThat(animalDescriptors, hasSize(1));
        final MockAnimalModuleDescriptor descriptor = animalDescriptors.get(0);
        final Class<?> moduleClass = descriptor.getModule().getClass();
        assertThat(getPluginAccessor().getEnabledModulesByClass(moduleClass), hasSize(1));

        manager.disablePlugin(descriptor.getPlugin().getKey());

        assertThat(getPluginAccessor().getEnabledModuleDescriptorsByClass(descriptorClass), empty());
        assertThat(getPluginAccessor().getEnabledModulesByClass(moduleClass), empty());
    }

    /**
     * Also relevant to {@link TestDefaultPluginManagerWithCachingPluginAccessor}: prove the cache doesn't need to listen to
     * pluginUninstalled: pluginDisabled is good enough
     */
    @Test
    public void testUninstallPluginClearsModulesByClass() throws IOException {
        final Class<MockAnimalModuleDescriptor> descriptorClass = MockAnimalModuleDescriptor.class;
        moduleDescriptorFactory.addModuleDescriptor("animal", descriptorClass);
        moduleDescriptorFactory.addModuleDescriptor("mineral", MockMineralModuleDescriptor.class);

        manager = newDefaultPluginManager(new SinglePluginLoaderWithRemoval("test-atlassian-plugin.xml"));
        manager.init();

        final List<MockAnimalModuleDescriptor> animalDescriptors =
                getPluginAccessor().getEnabledModuleDescriptorsByClass(descriptorClass);
        assertThat(animalDescriptors, hasSize(1));
        final MockAnimalModuleDescriptor descriptor = animalDescriptors.get(0);
        final Class<?> moduleClass = descriptor.getModule().getClass();
        assertThat(getPluginAccessor().getEnabledModulesByClass(moduleClass), hasSize(1));

        manager.uninstall(descriptor.getPlugin());

        assertThat(getPluginAccessor().getEnabledModuleDescriptorsByClass(descriptorClass), empty());
        assertThat(getPluginAccessor().getEnabledModulesByClass(moduleClass), empty());
    }

    @Test
    public void testGetPluginWithNullKey() {
        manager = newDefaultPluginManager();
        manager.init();
        try {
            getPluginAccessor().getPlugin(null);
            fail();
        } catch (IllegalArgumentException ex) {
            // test passed
        }
    }

    @Test
    public void checkPluginInternal() {
        final PluginLoader pluginLoader = mock(PluginLoader.class);
        final PluginInternal plugin = mock(PluginInternal.class);

        manager = newDefaultPluginManager(pluginLoader);

        assertThat(manager.checkPluginInternal(plugin), is(plugin));
    }

    @Test
    public void checkPluginInternalNotSo() {
        final PluginLoader pluginLoader = mock(PluginLoader.class);
        final Plugin plugin = mock(Plugin.class);

        manager = newDefaultPluginManager(pluginLoader);

        when(plugin.toString()).thenReturn("shitPlugin");

        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("shitPlugin");

        manager.checkPluginInternal(plugin);
    }

    @Test
    public void getDynamicModules() {
        final PluginLoader pluginLoader = mock(PluginLoader.class);
        final PluginInternal plugin = mock(PluginInternal.class);
        final Iterable<ModuleDescriptor<?>> dynamicModules = mock(Iterable.class);

        manager = newDefaultPluginManager(pluginLoader);

        when(plugin.getDynamicModuleDescriptors()).thenReturn(dynamicModules);

        assertThat(getPluginAccessor().getDynamicModules(plugin), is(dynamicModules));
    }

    /**
     * Dummy plugin loader that reports that removal is supported and returns plugins that report that they can
     * be uninstalled.
     */
    private static class SinglePluginLoaderWithRemoval extends SinglePluginLoader {
        public SinglePluginLoaderWithRemoval(final String resource) {
            super(resource);
        }

        public boolean supportsRemoval() {

            return true;
        }

        public void removePlugin(final Plugin plugin) throws PluginException {
            plugins = Collections.emptyList();
        }

        protected StaticPlugin getNewPlugin() {
            return new StaticPlugin() {
                public boolean isUninstallable() {
                    return true;
                }
            };
        }
    }

    private static class SinglePluginLoaderWithAddition extends SinglePluginLoader {
        PluginLoader addPluginLoader;

        public SinglePluginLoaderWithAddition(String resource) {
            super(resource);
        }

        @Override
        public boolean supportsAddition() {
            return true;
        }

        public void setAddPluginLoader(PluginLoader addPluginLoader) {
            this.addPluginLoader = addPluginLoader;
        }

        @Override
        public Iterable<Plugin> loadFoundPlugins(ModuleDescriptorFactory moduleDescriptorFactory) {
            return addPluginLoader == null ? emptySet() : addPluginLoader.loadAllPlugins(moduleDescriptorFactory);
        }
    }

    class NothingModuleDescriptor extends MockUnusedModuleDescriptor {
    }

    @RequiresRestart
    public static class RequiresRestartModuleDescriptor extends MockUnusedModuleDescriptor {
    }

    // A subclass of a module descriptor that @RequiresRestart; should inherit the annotation
    public static class RequiresRestartSubclassModuleDescriptor extends RequiresRestartModuleDescriptor {
    }

    private class MultiplePluginLoader implements PluginLoader {
        private final String[] descriptorPaths;

        public MultiplePluginLoader(final String... descriptorPaths) {
            this.descriptorPaths = descriptorPaths;
        }

        public Iterable<Plugin> loadAllPlugins(final ModuleDescriptorFactory moduleDescriptorFactory) throws PluginParseException {
            final ImmutableList.Builder<Plugin> result = ImmutableList.builder();
            for (final String path : descriptorPaths) {
                final SinglePluginLoader loader = new SinglePluginLoader(path);
                result.addAll(loader.loadAllPlugins(moduleDescriptorFactory));
            }
            return result.build();
        }

        public boolean supportsAddition() {
            return false;
        }

        public boolean supportsRemoval() {
            return false;
        }

        public Iterable<Plugin> loadFoundPlugins(final ModuleDescriptorFactory moduleDescriptorFactory) throws PluginParseException {
            throw new UnsupportedOperationException("This PluginLoader does not support addition.");
        }

        public void removePlugin(final Plugin plugin) throws PluginException {
            throw new UnsupportedOperationException("This PluginLoader does not support addition.");
        }

        @Override
        public boolean isDynamicPluginLoader() {
            return false;
        }

        @Override
        public ModuleDescriptor<?> createModule(final Plugin plugin, final Element module, final ModuleDescriptorFactory moduleDescriptorFactory) {
            return null;
        }
    }

    private static class DynamicSinglePluginLoader extends SinglePluginLoader implements PluginLoader, DynamicPluginLoader {
        private final AtomicBoolean canLoad = new AtomicBoolean(false);

        private final String key;

        public DynamicSinglePluginLoader(final String key, final String resource) {
            super(resource);
            this.key = key;
        }

        @Override
        public boolean isDynamicPluginLoader() {
            return true;
        }

        public String canLoad(final PluginArtifact pluginArtifact) throws PluginParseException {
            return canLoad.get() ? key : null;
        }

        public boolean supportsAddition() {
            return true;
        }

        @Override
        public Iterable<Plugin> loadAllPlugins(ModuleDescriptorFactory moduleDescriptorFactory) {
            if (canLoad.get()) {
                return super.loadAllPlugins(moduleDescriptorFactory);
            } else {
                return ImmutableList.of();
            }
        }

        @Override
        public Iterable<Plugin> loadFoundPlugins(final ModuleDescriptorFactory moduleDescriptorFactory) {
            if (canLoad.get()) {
                return super.loadAllPlugins(moduleDescriptorFactory);
            } else {
                return ImmutableList.of();
            }
        }
    }

    public static class PluginModuleEnabledListener {
        public volatile boolean called;

        @PluginEventListener
        public void onEnable(PluginModuleEnabledEvent event) {
            called = true;
        }
    }

    public static class PluginModuleDisabledListener {
        public volatile boolean called;

        @PluginEventListener
        public void onDisable(PluginModuleDisabledEvent event) {
            called = true;
        }
    }

    public static class PluginDisabledListener {
        public volatile boolean called;

        @PluginEventListener
        public void onDisable(PluginDisabledEvent event) {
            called = true;
        }
    }


    private static class DelayedSinglePluginLoader extends SinglePluginLoader {

        public DelayedSinglePluginLoader(String resource) {
            super(resource);
        }

        @Override
        public Iterable<Plugin> loadAllPlugins(ModuleDescriptorFactory moduleDescriptorFactory) {
            try {
                Thread.sleep(10);
                return super.loadAllPlugins(moduleDescriptorFactory);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
