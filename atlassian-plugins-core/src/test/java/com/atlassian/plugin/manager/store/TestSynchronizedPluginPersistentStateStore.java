package com.atlassian.plugin.manager.store;

import com.atlassian.plugin.manager.PluginPersistentState;
import com.atlassian.plugin.manager.PluginPersistentStateStore;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.RestoreSystemProperties;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;

import static com.atlassian.plugin.manager.store.SynchronizedPluginPersistentStateStore.LockMode;
import static com.atlassian.plugin.manager.store.SynchronizedPluginPersistentStateStore.getLockModeProperty;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestSynchronizedPluginPersistentStateStore {
    @Mock
    PluginPersistentStateStore delegate;
    @Mock
    ReadWriteLock readWriteLock;
    @Mock
    PluginPersistentState pluginPersistentState;

    ReentrantLock readLock;
    ReentrantLock writeLock;

    SynchronizedPluginPersistentStateStore synchronizedPluginPersistentStateStore;

    @Before
    public void setUp() throws Exception {
        readLock = new ReentrantLock();
        writeLock = new ReentrantLock();
        when(readWriteLock.readLock()).thenReturn(readLock);
        when(readWriteLock.writeLock()).thenReturn(writeLock);
        synchronizedPluginPersistentStateStore = new SynchronizedPluginPersistentStateStore(delegate, readWriteLock);
    }

    @Test
    public void saveGetsWriteLockAndForwardsToDelegate() {
        final Answer<Object> answer = new LockCheckingAnswer(false, true, null);
        doAnswer(answer).when(delegate).save(pluginPersistentState);
        synchronizedPluginPersistentStateStore.save(pluginPersistentState);
        verify(delegate).save(pluginPersistentState);
        assertLockState(false, false);
    }

    @Test
    public void loadGetsReadLockAndForwardsToDelegate() {
        final Answer<Object> answer = new LockCheckingAnswer(true, false, pluginPersistentState);
        when(delegate.load()).thenAnswer(answer);
        final PluginPersistentState actual = synchronizedPluginPersistentStateStore.load();
        assertThat(actual, is(pluginPersistentState));
        verify(delegate).load();
        assertLockState(false, false);
    }

    private class LockCheckingAnswer implements Answer<Object> {
        private final boolean readLocked;
        private final boolean writeLocked;
        private final Object result;

        public LockCheckingAnswer(final boolean readLocked, final boolean writeLocked, final Object result) {
            this.readLocked = readLocked;
            this.writeLocked = writeLocked;
            this.result = result;
        }

        @Override
        public Object answer(final InvocationOnMock invocationOnMock) throws Throwable {
            assertLockState(readLocked, writeLocked);
            return result;
        }
    }

    private void assertLockState(final boolean readLocked, final boolean writeLocked) {
        assertThat(readLock.isLocked(), is(readLocked));
        assertThat(writeLock.isLocked(), is(writeLocked));
    }

    public static class TestLockModeDefault {
        @Test
        public void defaultLockModeIsReadWrite() {
            final LockMode lockMode = LockMode.current();
            assertThat(lockMode, is(LockMode.READWRITE));
        }
    }

    @RunWith(Parameterized.class)
    public static class TestLockMode {
        @Rule
        public final RestoreSystemProperties restoreSystemProperties = new RestoreSystemProperties();

        final String propertyValue;
        final LockMode expectedMode;
        final boolean simple;

        @Parameterized.Parameters(name = "{index}: Property {0} yields {1}")
        public static Iterable<Object[]> data() {
            return ImmutableList.<Object[]>builder()
                    .add(new Object[]{"unlocked", LockMode.UNLOCKED, true})
                    .add(new Object[]{"simple", LockMode.SIMPLE, true})
                    .add(new Object[]{"fairsimple", LockMode.FAIRSIMPLE, true})
                    .add(new Object[]{"readwrite", LockMode.READWRITE, false})
                    .add(new Object[]{"fairreadwrite", LockMode.FAIRREADWRITE, false})
                    .build();
        }

        public TestLockMode(final String propertyValue, final LockMode expectedMode, final boolean simple) {
            this.propertyValue = propertyValue;
            this.expectedMode = expectedMode;
            this.simple = simple;
        }

        @Test
        public void propertyValueYieldsExpectedLockMode() {
            System.setProperty(getLockModeProperty(), propertyValue);

            final LockMode lockMode = LockMode.current();
            assertThat(lockMode, is(expectedMode));
            final ReadWriteLock readWriteLock = lockMode.getReadWriteLock();
            assertThat(readWriteLock.readLock() == readWriteLock.writeLock(), is(simple));
        }
    }
}
