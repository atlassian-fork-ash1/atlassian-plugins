package com.atlassian.plugin.manager;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginController;
import com.atlassian.plugin.PluginException;
import com.atlassian.plugin.PluginState;
import com.atlassian.plugin.exception.PluginExceptionInterception;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.RestoreSystemProperties;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;

import static com.atlassian.plugin.util.PluginUtils.ATLASSIAN_PLUGINS_ENABLE_WAIT;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThan;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class TestPluginEnabler {
    @Rule
    public final RestoreSystemProperties restoreSystemProperties = new RestoreSystemProperties();

    @Mock
    private PluginAccessor pluginAccessor;
    @Mock
    private PluginController pluginController;
    @Mock
    private PluginExceptionInterception pluginExceptionInterception;

    private PluginEnabler pluginEnabler;

    @Before
    public void setUp() {
        pluginEnabler = new PluginEnabler(pluginAccessor, pluginController, pluginExceptionInterception);
    }

    @Test
    public void enablePlugin() {
        final Plugin plugin = new PluginWithDeps("foo");

        pluginEnabler.enable(Arrays.asList(plugin));
        assertThat(plugin.getPluginState(), is(PluginState.ENABLED));
    }

    @Test
    public void enableResolvesPlugins() {
        final Plugin plugin = mock(Plugin.class);
        pluginEnabler.enable(Arrays.asList(plugin));
        verify(plugin).resolve();
        verify(plugin).enable();
    }

    @Test
    public void customTimeoutTimesOut() {
        final Plugin plugin = new PluginWithDeps("foo") {
            @Override
            protected PluginState enableInternal() throws PluginException {
                return PluginState.ENABLING;
            }
        };

        System.setProperty(ATLASSIAN_PLUGINS_ENABLE_WAIT, "1");
        final long start = System.currentTimeMillis();
        pluginEnabler = new PluginEnabler(mock(PluginAccessor.class), mock(PluginController.class), pluginExceptionInterception);
        pluginEnabler.enable(Arrays.asList(plugin));
        final long end = System.currentTimeMillis();
        assertThat(end - start, lessThan(5000L));
        assertThat(plugin.getPluginState(), is(PluginState.ENABLING));
    }

    @Test
    public void enableMultiplePluginsWithDependencies() {
        final Plugin plugin = new PluginWithDeps("foo", "foo2");
        final Plugin plugin2 = new PluginWithDeps("foo2", "foo3");
        final Plugin plugin3 = new PluginWithDeps("foo3");

        pluginEnabler.enable(Arrays.asList(plugin, plugin2, plugin3));
        assertThat(plugin.getPluginState(), is(PluginState.ENABLED));
        assertThat(plugin2.getPluginState(), is(PluginState.ENABLED));
        assertThat(plugin3.getPluginState(), is(PluginState.ENABLED));
    }

    @Test
    public void canEnableCircularlyDependentPlugins() {
        final Plugin plugin = new PluginWithDeps("foo", "foo2");
        final Plugin plugin2 = new PluginWithDeps("foo2", "foo3");
        final Plugin plugin3 = new PluginWithDeps("foo3", "foo");

        pluginEnabler.enable(Arrays.asList(plugin, plugin2, plugin3));
        assertThat(plugin.getPluginState(), is(PluginState.ENABLED));
        assertThat(plugin2.getPluginState(), is(PluginState.ENABLED));
        assertThat(plugin3.getPluginState(), is(PluginState.ENABLED));
    }

    @Test
    public void failedEnableIsPassedToPluginInterception() {
        final RuntimeException runtimeException = new RuntimeException();
        final Plugin plugin = new PluginWithDeps("foo") {
            @Override
            protected PluginState enableInternal() throws PluginException {
                throw runtimeException;
            }
        };

        pluginEnabler.enable(Arrays.asList(plugin));
        verify(pluginExceptionInterception).onEnableException(plugin, runtimeException);
    }
}
