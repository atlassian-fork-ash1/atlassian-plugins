package com.atlassian.plugin.manager;

import com.atlassian.plugin.MockModuleDescriptor;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginController;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.PluginDisabledEvent;
import com.atlassian.plugin.event.events.PluginEnabledEvent;
import com.atlassian.plugin.event.events.PluginModuleDisabledEvent;
import com.atlassian.plugin.event.events.PluginModuleEnabledEvent;
import com.atlassian.plugin.event.impl.DefaultPluginEventManager;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterators;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.Collections.singletonList;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Mock-based unit tests of {@link EnabledModuleCachingPluginAccessor}.
 *
 * @see TestDefaultPluginManagerWithCachingPluginAccessor for integration test with {@link DefaultPluginManager}.
 */
public class TestEnabledModuleCachingPluginAccessor {
    private PluginEventManager pluginEventManager;

    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private PluginAccessor delegate;
    @Mock
    private PluginController pluginController;
    private PluginAccessor cachingPluginAccessor;

    @Before
    public void setUp() throws Exception {
        pluginEventManager = new DefaultPluginEventManager();
        cachingPluginAccessor = new EnabledModuleCachingPluginAccessor(delegate, pluginEventManager, pluginController);
    }

    @Test
    public void testGetEnabledModuleDescriptorsByClassDelegateShouldCalculateAtMostOnce() {
        // call the cached method multiple times.
        cachingPluginAccessor.getEnabledModuleDescriptorsByClass(Descriptor.class);
        cachingPluginAccessor.getEnabledModuleDescriptorsByClass(Descriptor.class);
        // should have been called only once
        verify(delegate, times(1)).getEnabledModuleDescriptorsByClass(Descriptor.class);

        // broadcast new module and then call again multiple times
        pluginEventManager.broadcast(new PluginModuleEnabledEvent(new Descriptor(new Module())));
        cachingPluginAccessor.getEnabledModuleDescriptorsByClass(Descriptor.class);
        cachingPluginAccessor.getEnabledModuleDescriptorsByClass(Descriptor.class);

        // should have been called only once more (because the cache was cleared and re-loaded following enabled event)
        verify(delegate, times(2)).getEnabledModuleDescriptorsByClass(Descriptor.class);
    }

    @Test
    public void testGetEnabledModuleDescriptorsByClassFlushCacheAfterAnyPluginDisable() {
        // call the cached method - populates cache
        cachingPluginAccessor.getEnabledModuleDescriptorsByClass(Descriptor.class);
        verify(delegate, times(1)).getEnabledModuleDescriptorsByClass(Descriptor.class);

        pluginEventManager.broadcast(new PluginDisabledEvent(mock(Plugin.class)));
        cachingPluginAccessor.getEnabledModuleDescriptorsByClass(Descriptor.class);
        cachingPluginAccessor.getEnabledModuleDescriptorsByClass(Descriptor.class);

        // Called again (but only once more) following cache flush
        verify(delegate, times(2)).getEnabledModuleDescriptorsByClass(Descriptor.class);
    }

    @Test
    public void testGetEnabledModuleDescriptorsByClassFlushCacheAfterAnyPluginEnable() {
        // call the cached method - populates cache
        cachingPluginAccessor.getEnabledModuleDescriptorsByClass(Descriptor.class);
        verify(delegate, times(1)).getEnabledModuleDescriptorsByClass(Descriptor.class);

        pluginEventManager.broadcast(new PluginEnabledEvent(mock(Plugin.class)));
        cachingPluginAccessor.getEnabledModuleDescriptorsByClass(Descriptor.class);
        cachingPluginAccessor.getEnabledModuleDescriptorsByClass(Descriptor.class);

        // Called again (but only once more) following cache flush
        verify(delegate, times(2)).getEnabledModuleDescriptorsByClass(Descriptor.class);
    }

    @Test
    public void testGetEnabledModuleDescriptorsByClassChildAndParentClassBeingTrackedSeparately() {
        final ChildDescriptor<ChildModule> childDescriptor = new ChildDescriptor<>(new ChildModule());
        final ParentDescriptor<ParentModule> parentDescriptor = new ParentDescriptor<>(new ParentModule());

        when(delegate.getEnabledModuleDescriptorsByClass(ChildDescriptor.class))
                .thenReturn(Collections.<ChildDescriptor>singletonList(childDescriptor));
        when(delegate.getEnabledModuleDescriptorsByClass(ParentDescriptor.class))
                .thenReturn(Lists.<ParentDescriptor>newArrayList(parentDescriptor, childDescriptor));

        final List<ChildDescriptor> result = cachingPluginAccessor.getEnabledModuleDescriptorsByClass(ChildDescriptor.class);
        assertEquals(1, result.size());
        assertSame(childDescriptor, result.get(0));

        final List<ParentDescriptor> result2 = cachingPluginAccessor.getEnabledModuleDescriptorsByClass(ParentDescriptor.class);
        // (gave up on trying to get generics to work with 'contains/containsInAnyOrder')
        assertEquals(2, result2.size());
        assertEquals(parentDescriptor, result2.get(0));
        assertEquals(childDescriptor, result2.get(1));
    }

    @Test
    public void testGetEnabledModuleDescriptorsByClassFindsLateChild() {
        final ChildDescriptor childDescriptor = new ChildDescriptor<>(new ChildModule());
        final ParentDescriptor parentDescriptor = new ParentDescriptor<>(new ParentModule());

        when(delegate.getEnabledModuleDescriptorsByClass(ChildDescriptor.class)).thenReturn(ImmutableList.of());
        when(delegate.getEnabledModuleDescriptorsByClass(ParentDescriptor.class)).thenReturn(ImmutableList.of(parentDescriptor));

        final List<ChildDescriptor> childBefore = cachingPluginAccessor.getEnabledModuleDescriptorsByClass(ChildDescriptor.class);
        final List<ParentDescriptor> parentBefore = cachingPluginAccessor.getEnabledModuleDescriptorsByClass(ParentDescriptor.class);

        assertThat(childBefore, empty());
        assertThat(parentBefore, contains(parentDescriptor));

        // Now enable child

        when(delegate.getEnabledModuleDescriptorsByClass(ChildDescriptor.class)).thenReturn(ImmutableList.of(childDescriptor));
        when(delegate.getEnabledModuleDescriptorsByClass(ParentDescriptor.class)).
                thenReturn(ImmutableList.of(parentDescriptor, childDescriptor));

        pluginEventManager.broadcast(new PluginModuleEnabledEvent(childDescriptor));

        final List<ChildDescriptor> childAfter = cachingPluginAccessor.getEnabledModuleDescriptorsByClass(ChildDescriptor.class);
        final List<ParentDescriptor> parentAfter = cachingPluginAccessor.getEnabledModuleDescriptorsByClass(ParentDescriptor.class);

        assertThat(childAfter, contains(childDescriptor));
        assertThat(parentAfter, contains(parentDescriptor, childDescriptor));
    }

    @Test
    public void testGetEnabledModuleDescriptorsByClassWhenModuleEnabledBeforeCacheAccessed() {
        final Descriptor descriptor = new Descriptor(new Module());

        // Mock for cache loader as if module already enabled
        when(delegate.getEnabledModuleDescriptorsByClass(Descriptor.class)).thenReturn(singletonList(descriptor));

        // Cache loader should see the module
        List<Descriptor> result = cachingPluginAccessor.getEnabledModuleDescriptorsByClass(Descriptor.class);
        assertThat(result, contains(descriptor));
    }

    @Test
    public void testGetEnabledModuleDescriptorsByClassWhenModuleEnabledAfterCacheAccessed() {
        final Descriptor descriptor = new Descriptor(new Module());

        // Mock for cache loader as if module initially disabled
        when(delegate.getEnabledModuleDescriptorsByClass(Descriptor.class)).thenReturn(Collections.<Descriptor>emptyList());

        // Cache loader
        List<Descriptor> result = cachingPluginAccessor.getEnabledModuleDescriptorsByClass(Descriptor.class);
        assertThat(result, empty());

        // Now gets enabled - should see the event and reload the cache
        when(delegate.getEnabledModuleDescriptorsByClass(Descriptor.class)).thenReturn(singletonList(descriptor));
        pluginEventManager.broadcast(new PluginModuleEnabledEvent(descriptor));

        result = cachingPluginAccessor.getEnabledModuleDescriptorsByClass(Descriptor.class);
        assertThat(result, contains(descriptor));
    }

    @Test
    public void testGetEnabledModulesByClassDelegateShouldCalculateAtMostOnce() {
        // call the cached method multiple times.
        cachingPluginAccessor.getEnabledModulesByClass(Module.class);
        cachingPluginAccessor.getEnabledModulesByClass(Module.class);

        // Cache loading of ByModuleClassCacheLoader works via a predicate across all plugins' modules,
        // not by calling the delegate: see EnabledModuleCachingPluginAccessor.getEnabledModuleDescriptorsByModuleClass()
        verify(delegate, times(1)).getEnabledPlugins();
        verify(delegate, never()).getEnabledModulesByClass(any(Class.class));

        // broadcast new module and then call again.
        pluginEventManager.broadcast(new PluginModuleEnabledEvent(new Descriptor(new Module())));
        cachingPluginAccessor.getEnabledModulesByClass(Module.class);
        cachingPluginAccessor.getEnabledModulesByClass(Module.class);

        // Only one more call through to delegate (due to the enabled event clearing the cache entry)
        verify(delegate, times(2)).getEnabledPlugins();
        verify(delegate, never()).getEnabledModulesByClass(any(Class.class));
    }

    @Test
    public void testGetEnabledModulesByClassFlushCacheAfterAnyPluginEnable() {
        // call the cached method - populates cache
        cachingPluginAccessor.getEnabledModulesByClass(Module.class);

        // Cache loading of ByModuleClassCacheLoader works via a predicate across all plugins' modules,
        // not by calling the delegate: see EnabledModuleCachingPluginAccessor.getEnabledModuleDescriptorsByModuleClass()
        verify(delegate, times(1)).getEnabledPlugins();
        verify(delegate, never()).getEnabledModulesByClass(any(Class.class));

        pluginEventManager.broadcast(new PluginEnabledEvent(mock(Plugin.class)));
        cachingPluginAccessor.getEnabledModulesByClass(Module.class);
        cachingPluginAccessor.getEnabledModulesByClass(Module.class);

        // Cache has been cleared, now gets loaded again
        verify(delegate, times(2)).getEnabledPlugins();
        verify(delegate, never()).getEnabledModulesByClass(any(Class.class));
    }

    @Test
    public void testGetEnabledModulesByClassFlushCacheAfterAnyPluginDisable() {
        // call the cached method - populates cache
        cachingPluginAccessor.getEnabledModulesByClass(Module.class);

        // Cache loading of ByModuleClassCacheLoader works via a predicate across all plugins' modules,
        // not by calling the delegate: see EnabledModuleCachingPluginAccessor.getEnabledModuleDescriptorsByModuleClass()
        verify(delegate, times(1)).getEnabledPlugins();
        verify(delegate, never()).getEnabledModulesByClass(any(Class.class));

        pluginEventManager.broadcast(new PluginDisabledEvent(mock(Plugin.class)));
        cachingPluginAccessor.getEnabledModulesByClass(Module.class);
        cachingPluginAccessor.getEnabledModulesByClass(Module.class);

        // Cache has been cleared, now gets loaded again
        verify(delegate, times(2)).getEnabledPlugins();
        verify(delegate, never()).getEnabledModulesByClass(any(Class.class));
    }

    @Test
    public void testGetEnabledModulesByClassFlushCacheAfterAnyPluginModuleDisable() {
        Descriptor descriptor = new Descriptor(new Module());

        // call the cached method - populates cache
        cachingPluginAccessor.getEnabledModulesByClass(Module.class);

        // Cache loading of ByModuleClassCacheLoader works via a predicate across all plugins' modules,
        // not by calling the delegate: see EnabledModuleCachingPluginAccessor.getEnabledModuleDescriptorsByModuleClass()
        verify(delegate, times(1)).getEnabledPlugins();
        verify(delegate, never()).getEnabledModulesByClass(any(Class.class));

        pluginEventManager.broadcast(new PluginModuleDisabledEvent(descriptor, false));
        cachingPluginAccessor.getEnabledModulesByClass(Module.class);
        cachingPluginAccessor.getEnabledModulesByClass(Module.class);

        // Cache has been cleared, now gets loaded again
        verify(delegate, times(2)).getEnabledPlugins();
        verify(delegate, never()).getEnabledModulesByClass(any(Class.class));
    }

    @Test
    public void testGetEnabledModulesByClassChildAndParentClassBeingTrackedSeparately() {
        final ChildModule childModule = new ChildModule();
        final ChildDescriptor<ChildModule> childDescriptor = new ChildDescriptor<>(childModule);
        childDescriptor.enabled();
        final ParentModule parentModule = new ParentModule();
        final ParentDescriptor<ParentModule> parentDescriptor = new ParentDescriptor<>(parentModule);
        parentDescriptor.enabled();

        // Mock for cache loader
        final Plugin plugin = mock(Plugin.class);
        when(delegate.getEnabledPlugins()).thenReturn(newArrayList(plugin));
        when(plugin.getModuleDescriptors()).thenReturn(Lists.<ModuleDescriptor<?>>newArrayList(parentDescriptor, childDescriptor));

        assertThat(cachingPluginAccessor.getEnabledModulesByClass(ChildModule.class),
                contains(childModule));
        assertThat(cachingPluginAccessor.getEnabledModulesByClass(ParentModule.class),
                containsInAnyOrder(parentModule, childModule));
    }

    @Test
    public void testGetEnabledModulesByClassWhenModuleEnabledBeforeCacheAccessed() {
        final Module module = new Module();
        final Descriptor descriptor = new Descriptor(module);
        descriptor.enabled();

        // Mock for cache loader - module already enabled
        final Plugin plugin = mock(Plugin.class);
        when(delegate.getEnabledPlugins()).thenReturn(newArrayList(plugin));
        when(plugin.getModuleDescriptors()).thenReturn(Collections.<ModuleDescriptor<?>>singletonList(descriptor));

        // Cache loader should see the module
        List<Module> result = cachingPluginAccessor.getEnabledModulesByClass(Module.class);
        assertThat(result, contains(module));
    }

    @Test
    public void testGetEnabledModulesByClassWhenModuleEnabledAfterCacheAccessed() {
        final Module module = new Module();
        final Descriptor descriptor = new Descriptor(module);
        assertFalse(descriptor.isEnabled());

        // Mock for cache loader - module returned, but initially disabled
        final Plugin plugin = mock(Plugin.class);
        when(delegate.getEnabledPlugins()).thenReturn(newArrayList(plugin));
        when(plugin.getModuleDescriptors()).thenReturn(Collections.<ModuleDescriptor<?>>singletonList(descriptor));

        // Cache loader
        List<Module> result = cachingPluginAccessor.getEnabledModulesByClass(Module.class);
        assertThat(result, empty());

        // Now gets enabled - tracker should see the event and return the module
        descriptor.enabled();
        pluginEventManager.broadcast(new PluginModuleEnabledEvent(descriptor));

        result = cachingPluginAccessor.getEnabledModulesByClass(Module.class);
        assertThat(result, contains(module));
    }

    @Test
    public void testGetEnabledModulesByClassDoesNotCacheModulesOnlyDescriptors() {
        // Different module each time 'getModule' is called
        final Module module1 = mock(Module.class, "module1");
        final Module module2 = mock(Module.class, "module2");
        final Module module3 = mock(Module.class, "module3");
        final MockModuleDescriptor<Module> descriptor = new MockModuleDescriptor<Module>(mock(Plugin.class), "descriptor", module1) {
            private Iterator<Module> modules = Iterators.forArray(module1, module2, module3);

            @Override
            public Module getModule() {
                return modules.next();
            }
        };
        descriptor.enabled();

        // Mock for cache loader - module already enabled
        final Plugin plugin = mock(Plugin.class);
        when(delegate.getEnabledPlugins()).thenReturn(newArrayList(plugin));
        when(plugin.getModuleDescriptors()).thenReturn(Collections.<ModuleDescriptor<?>>singletonList(descriptor));

        List<Module> result = cachingPluginAccessor.getEnabledModulesByClass(Module.class);
        assertEquals(1, result.size());
        assertThat(result.get(0), sameInstance(module1));

        // Module is re-retrieved from descriptor every time
        result = cachingPluginAccessor.getEnabledModulesByClass(Module.class);
        assertEquals(1, result.size());
        assertThat(result.get(0), sameInstance(module2));

        result = cachingPluginAccessor.getEnabledModulesByClass(Module.class);
        assertEquals(1, result.size());
        assertThat(result.get(0), sameInstance(module3));
    }

    class Module {
    }

    class ParentModule {
    }

    class ChildModule extends ParentModule {
    }

    class Descriptor extends MockModuleDescriptor<Module> {
        public Descriptor(Module module) {
            super(mock(Plugin.class), "descriptorKey", module);
        }
    }

    class ParentDescriptor<M extends ParentModule> extends MockModuleDescriptor<M> {
        public ParentDescriptor(String key, M module) {
            super(mock(Plugin.class), key, module);
        }

        public ParentDescriptor(M module) {
            this("parentDescriptorKey", module);
        }
    }

    class ChildDescriptor<M extends ChildModule> extends ParentDescriptor<M> {
        public ChildDescriptor(M module) {
            super("childDescriptorKey", module);
        }
    }
}
