package com.atlassian.plugin.manager;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.metadata.PluginMetadataManager;
import com.atlassian.plugin.parsers.SafeModeCommandLineArguments;
import com.atlassian.plugin.parsers.SafeModeCommandLineArgumentsFactory;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static com.atlassian.plugin.manager.ApplicationDefinedPluginsProvider.NO_APPLICATION_PLUGINS;
import static com.atlassian.plugin.manager.ClusterEnvironmentProvider.SINGLE_NODE;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestDefaultSafeModeManager {

    private ApplicationDefinedPluginsProvider appRelatedPlugProvider = NO_APPLICATION_PLUGINS;
    private SafeModeCommandLineArguments safeModeCommandLineArgs;
    private SafeModeCommandLineArgumentsFactory safeModeCommandLineArgsFactory;
    private PluginMetadataManager mockPluginMetadataManager = mock(PluginMetadataManager.class);
    private final String PLUGIN_KEY = "com.atlassian.test";

    private PluginPersistentStateStore mockPluginPersistentStateStore;

    @Before
    public void setUp() throws Exception {

        safeModeCommandLineArgs = mock(SafeModeCommandLineArguments.class);
        safeModeCommandLineArgsFactory = mock(SafeModeCommandLineArgumentsFactory.class);
        when(safeModeCommandLineArgsFactory.get()).thenReturn(safeModeCommandLineArgs);
        mockPluginPersistentStateStore = mock(PluginPersistentStateStore.class);
        PluginPersistentState mockPluginPersistentState = mock(PluginPersistentState.class);
        when(mockPluginPersistentState.getStatesMap()).thenReturn(Collections.singletonMap(PLUGIN_KEY, new PluginEnabledState(true, 1)));
        when(mockPluginPersistentStateStore.load()).thenReturn(mockPluginPersistentState);
    }

    @Test
    public void nonSystemPluginShouldNotBeStartedWhenInSafeMode() {
        when(safeModeCommandLineArgs.isSafeMode()).thenReturn(true);
        Plugin mockPlugin = mock(Plugin.class);
        DefaultSafeModeManager defaultSafeModeManager = new DefaultSafeModeManager(mockPluginMetadataManager, appRelatedPlugProvider, SINGLE_NODE, safeModeCommandLineArgsFactory, mockPluginPersistentStateStore);
        when(mockPluginMetadataManager.isSystemProvided(any(Plugin.class))).thenReturn(false);
        when(mockPluginMetadataManager.isOptional(any(Plugin.class))).thenReturn(true);
        assertFalse(defaultSafeModeManager.pluginShouldBeStarted(mockPlugin, emptyList()));
    }

    @Test
    public void systemPluginShouldBeStartedWhenInSafeMode() {
        when(safeModeCommandLineArgs.isSafeMode()).thenReturn(true);
        Plugin mockPlugin = mock(Plugin.class);
        DefaultSafeModeManager defaultSafeModeManager = new DefaultSafeModeManager(mockPluginMetadataManager, appRelatedPlugProvider, SINGLE_NODE, safeModeCommandLineArgsFactory, mockPluginPersistentStateStore);
        when(mockPluginMetadataManager.isOptional(any(Plugin.class))).thenReturn(true);
        when(mockPluginMetadataManager.isSystemProvided(any(Plugin.class))).thenReturn(true);
        assertTrue(defaultSafeModeManager.pluginShouldBeStarted(mockPlugin, emptyList()));
    }

    @Test
    public void nonSystemPluginShouldBeStartedWhenNotInSafeMode() {
        Plugin mockPlugin = mock(Plugin.class);
        DefaultSafeModeManager defaultSafeModeManager = new DefaultSafeModeManager(mockPluginMetadataManager, appRelatedPlugProvider, SINGLE_NODE, safeModeCommandLineArgsFactory, mockPluginPersistentStateStore);
        when(mockPluginMetadataManager.isSystemProvided(any(Plugin.class))).thenReturn(false);
        when(mockPluginMetadataManager.isOptional(any(Plugin.class))).thenReturn(true);
        assertTrue(defaultSafeModeManager.pluginShouldBeStarted(mockPlugin, emptyList()));
    }

    @Test
    public void systemPluginShouldBeStartedWhenNotInSafeMode() {
        Plugin mockPlugin = mock(Plugin.class);
        DefaultSafeModeManager defaultSafeModeManager = new DefaultSafeModeManager(mockPluginMetadataManager, appRelatedPlugProvider, SINGLE_NODE, safeModeCommandLineArgsFactory, mockPluginPersistentStateStore);
        when(mockPluginMetadataManager.isOptional(any(Plugin.class))).thenReturn(true);
        when(mockPluginMetadataManager.isSystemProvided(any(Plugin.class))).thenReturn(true);
        assertTrue(defaultSafeModeManager.pluginShouldBeStarted(mockPlugin, emptyList()));
        when(mockPluginMetadataManager.isSystemProvided(any(Plugin.class))).thenReturn(false);
        assertTrue(defaultSafeModeManager.pluginShouldBeStarted(mockPlugin, emptyList()));
    }

    @Test
    public void pluginShouldNotBeStartedWhenIsLastEnabledAndDisableLastEnabledParameterSet() {
        when(safeModeCommandLineArgs.shouldLastEnabledBeDisabled()).thenReturn(true);
        Plugin mockPlugin = mock(Plugin.class);
        ModuleDescriptor descriptor = mock(ModuleDescriptor.class);
        when(mockPlugin.getKey()).thenReturn(PLUGIN_KEY);
        DefaultSafeModeManager defaultSafeModeManager = new DefaultSafeModeManager(mockPluginMetadataManager, appRelatedPlugProvider, SINGLE_NODE, safeModeCommandLineArgsFactory, mockPluginPersistentStateStore);
        assertFalse(defaultSafeModeManager.pluginShouldBeStarted(mockPlugin, singletonList(descriptor)));
    }

    @Test
    public void pluginShouldBeStartedWhenIsNotLastEnabledAndDisableLastEnabledParameterSet() {
        when(safeModeCommandLineArgs.shouldLastEnabledBeDisabled()).thenReturn(true);
        Map<String, PluginEnabledState> enabledStateHashMap = new HashMap<>();
        enabledStateHashMap.put("asdf", new PluginEnabledState(true, 2));
        enabledStateHashMap.put(PLUGIN_KEY, new PluginEnabledState(true, 1));
        when(mockPluginPersistentStateStore.load().getStatesMap()).thenReturn(enabledStateHashMap);
        Plugin mockPlugin = mock(Plugin.class);
        ModuleDescriptor descriptor = mock(ModuleDescriptor.class);
        when(mockPlugin.getKey()).thenReturn(PLUGIN_KEY);
        DefaultSafeModeManager defaultSafeModeManager = new DefaultSafeModeManager(mockPluginMetadataManager, appRelatedPlugProvider, SINGLE_NODE, safeModeCommandLineArgsFactory, mockPluginPersistentStateStore);
        assertTrue(defaultSafeModeManager.pluginShouldBeStarted(mockPlugin, singletonList(descriptor)));
    }

    @Test
    public void shouldNotBeInSafeModeWhenDisableAllAddonsParameterNotPresent(){
        when(safeModeCommandLineArgs.isSafeMode()).thenReturn(false);
        DefaultSafeModeManager defaultSafeModeManager = new DefaultSafeModeManager(mockPluginMetadataManager, appRelatedPlugProvider, SINGLE_NODE, safeModeCommandLineArgsFactory, mockPluginPersistentStateStore);
        assertFalse(defaultSafeModeManager.isInSafeMode());
    }

}