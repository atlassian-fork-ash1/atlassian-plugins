package com.atlassian.plugin.manager.store;

import com.atlassian.plugin.manager.PluginPersistentState;
import com.atlassian.plugin.manager.PluginPersistentStateStore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class TestDefaultStoredPluginStateAccessor {

    @Mock
    private PluginPersistentState pluginPersistentState;

    @Mock
    private PluginPersistentStateStore pluginPersistentStateStore;

    @InjectMocks
    private DefaultStoredPluginStateAccessor defaultStoredPluginStateAccessor;

    @Test
    public void when_getIsCalled_itDelegatesToLoad() {
        when(pluginPersistentStateStore.load()).thenReturn(pluginPersistentState);
        defaultStoredPluginStateAccessor.get();
        verify(pluginPersistentStateStore).load();
    }
}