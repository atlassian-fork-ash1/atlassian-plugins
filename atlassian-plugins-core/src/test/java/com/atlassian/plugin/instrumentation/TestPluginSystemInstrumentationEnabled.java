package com.atlassian.plugin.instrumentation;

import com.atlassian.instrumentation.InstrumentRegistry;
import com.atlassian.instrumentation.RegistryConfiguration;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Optional;

import static com.atlassian.plugin.test.Matchers.isPresent;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.startsWith;
import static org.hamcrest.core.Is.is;

public class TestPluginSystemInstrumentationEnabled {

    private final PluginSystemInstrumentation pluginSystemInstrumentation = new PluginSystemInstrumentation();

    @BeforeClass
    public static void beforeClass() throws Exception {
        // enable instrumentation
        System.setProperty(PluginSystemInstrumentation.getEnabledProperty(), "true");
    }

    @AfterClass
    public static void afterClass() throws Exception {
        // reset
        System.setProperty(PluginSystemInstrumentation.getEnabledProperty(), "false");
    }

    @Test
    public void instrumentationCreated() {
        final Optional<InstrumentRegistry> instrumentRegistry = pluginSystemInstrumentation.getInstrumentRegistry();
        assertThat(instrumentRegistry, isPresent());

        final RegistryConfiguration registryConfiguration = instrumentRegistry.get().getRegistryConfiguration();
        assertThat(registryConfiguration, notNullValue());
        assertThat(registryConfiguration.getRegistryName(), is(PluginSystemInstrumentation.REGISTRY_NAME));
        assertThat(registryConfiguration.isCPUCostCollected(), is(true));
        assertThat(registryConfiguration.getRegistryHomeDirectory(), is(PluginSystemInstrumentation.REGISTRY_HOME_DIRECTORY));
    }

    @Test
    public void pullTimerWithName() {
        final Timer timer = pluginSystemInstrumentation.pullTimer("sometimer");
        assertThat(timer, notNullValue());
        assertThat(timer.getOpTimer(), isPresent());
        assertThat(timer.getOpTimer().get().getName(), is("sometimer"));
    }

    @Test
    public void pullSingleTimerWithName() {
        final Timer timer = pluginSystemInstrumentation.pullSingleTimer("somesingletimer");
        assertThat(timer, notNullValue());
        assertThat(timer.getOpTimer(), isPresent());
        assertThat(timer.getOpTimer().get().getName(), startsWith("somesingletimer"));
    }
}
