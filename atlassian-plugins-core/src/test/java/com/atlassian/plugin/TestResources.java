package com.atlassian.plugin;

import com.atlassian.plugin.elements.ResourceDescriptor;
import com.atlassian.plugin.elements.ResourceLocation;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class TestResources {
    private static final String RESOURCE_DOC = "<foo>" + "<resource type=\"velocity\" name=\"view\">the content</resource>" + "<resource type=\"velocity\" name=\"edit\" />"
            + "<resource type=\"image\" name=\"view\" />" + "</foo>";

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testMultipleResources() throws DocumentException, PluginParseException {
        final Resources resources = makeTestResources();

        final List descriptors = resources.getResourceDescriptors();
        assertEquals(3, descriptors.size());

        assertDescriptorMatches((ResourceDescriptor) descriptors.get(0), "velocity", "view");
        assertDescriptorMatches((ResourceDescriptor) descriptors.get(1), "velocity", "edit");
        assertDescriptorMatches((ResourceDescriptor) descriptors.get(2), "image", "view");
    }

    @Test
    public void testGetResourceDescriptor() throws DocumentException, PluginParseException {
        final Resources resources = makeTestResources();

        assertNull(resources.getResourceLocation("image", "edit"));
        assertNull(resources.getResourceLocation("fish", "view"));
        assertNull(resources.getResourceLocation(null, "view"));
        assertNull(resources.getResourceLocation("image", null));

        assertLocationMatches(resources.getResourceLocation("image", "view"), "image", "view");
    }

    @Test
    public void testMultipleResourceWithClashingKeysFail() throws DocumentException {
        expectedException.expect(PluginParseException.class);
        expectedException.expectMessage("Duplicate resource with type 'velocity' and name 'view' found");
        final Document document = DocumentHelper.parseText("<foo>" + "<resource type=\"velocity\" name=\"view\">the content</resource>"
                + "<resource type=\"velocity\" name=\"view\" />" + "</foo>");

        Resources.fromXml(document.getRootElement());
    }

    @Test
    public void testParsingNullElementThrowsException() {
        expectedException.expect(IllegalArgumentException.class);

        Resources.fromXml(null);
    }

    @Test
    public void testEmptyResources() {
        final Resources resources = Resources.EMPTY_RESOURCES;
        assertTrue("Empty resources should be empty", resources.getResourceDescriptors().isEmpty());
        assertNull("Empty resources should return null for any resource", resources.getResourceLocation("i18n", "i18n.properties"));
    }

    private void assertLocationMatches(final ResourceLocation first, final String type, final String name) {
        assertEquals(type, first.getType());
        assertEquals(name, first.getName());
    }

    private void assertDescriptorMatches(final ResourceDescriptor first, final String type, final String name) {
        assertEquals(type, first.getType());
        assertEquals(name, first.getName());
    }

    private Resources makeTestResources() throws DocumentException, PluginParseException {
        final Document document = DocumentHelper.parseText(RESOURCE_DOC);
        return Resources.fromXml(document.getRootElement());
    }
}
