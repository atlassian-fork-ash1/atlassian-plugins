package com.atlassian.plugin.parsers;

import com.google.common.collect.ImmutableMap;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Node;
import org.dom4j.XPath;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;

import static com.atlassian.plugin.parsers.XmlDescriptorParserUtils.removeAllNamespaces;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public final class XmlDescriptorParserUtilsTest {

    private static final String XML =
            "<document xmlns=\"http://www.example.com/schema/main\"" + System.lineSeparator() +
            "          xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"" + System.lineSeparator() +
            "          xmlns:n2=\"http://www.example.com/schema/n2\">" + System.lineSeparator() +
            "    <element1><sub-element1>Text 1</sub-element1></element1>" + System.lineSeparator() +
            "    <n2:element2><n2:sub-element2>Text 2</n2:sub-element2></n2:element2>" + System.lineSeparator() +
            "</document>";

    @Test
    public void xpathWithNoNamespaces_shouldSelectElementWithDocumentNamespace_whenGivenDocumentWithNamespaces() {
        final Document docs = getDocument(XML);

        XPath xPath = DocumentHelper.createXPath("/document/element1/sub-element1");

        assertNull(xPath.selectSingleNode(docs));
    }

    @Test
    public void xpathWithNamespaces_shouldSelectElementWithExternalNamespace_whenGivenDocumentWithNamespaces() {
        final Document docs = getDocument(XML);

        XPath xPath = createXPathWithNamespace("/main:document/n2:element2/n2:sub-element2");
        Node el = xPath.selectSingleNode(docs);

        assertNotNull(el);
        assertEquals("Text 2", el.getText());
    }

    @Test
    public void xpathWithNoNamespaces_shouldSelectElementWithExternalNamespaces_whenGivenDocumentWithNamespaces() {
        final Document docs = getDocument(XML);

        XPath xPath = DocumentHelper.createXPath("/document/element2/sub-element2");
        assertNull(xPath.selectSingleNode(docs));
    }

    @Test
    public void xpathWithNamespaces_shouldSelectElementWithDocumentNamespace_whenGivenDocumentWithNamespaces() {
        final Document docs = getDocument(XML);
        removeAllNamespaces(docs);

        XPath xPath = createXPathWithNamespace("/main:document/main:element1/main:sub-element1");
        assertNull(xPath.selectSingleNode(docs));
    }

    @Test
    public void xpathWithNoNamespaces_shouldSelectElementWithDocumentNamespace_whenGivenDocumentWithNoNamespaces() {
        final Document docs = getDocument(XML);

        removeAllNamespaces(docs);

        XPath xPath = DocumentHelper.createXPath("/document/element1/sub-element1");
        Node el = xPath.selectSingleNode(docs);
        assertNotNull(el);
        assertEquals("Text 1", el.getText());
    }

    @Test
    public void xpathWithNamespaces_shouldSelectElementWithDocumentNamespace_whenGivenDocumentWithNoNamespaces() {
        final Document docs = getDocument(XML);

        removeAllNamespaces(docs);

        XPath xPath = createXPathWithNamespace("/main:document/n2:element2/n2:sub-element2");
        assertNull(xPath.selectSingleNode(docs));
    }

    @Test
    public void xpathWitNoNamespaces_shouldSelectElementWithExternalNamespaces_whenGivenDocumentWithNoNamespaces() {
        final Document docs = getDocument(XML);

        removeAllNamespaces(docs);

        XPath xPath = DocumentHelper.createXPath("/document/element2/sub-element2");
        Node el = xPath.selectSingleNode(docs);
        assertNotNull(el);
        assertEquals("Text 2", el.getText());
    }

    private XPath createXPathWithNamespace(String xpathExpression) {
        final XPath xPath = DocumentHelper.createXPath(xpathExpression);
        xPath.setNamespaceURIs(ImmutableMap.of(
                "main", "http://www.example.com/schema/main",
                "n2", "http://www.example.com/schema/n2"));
        return xPath;
    }

    private Document getDocument(String xml) {
        try {
            return XmlDescriptorParser.createDocument(new ByteArrayInputStream(xml.getBytes("UTF-8")));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
}
