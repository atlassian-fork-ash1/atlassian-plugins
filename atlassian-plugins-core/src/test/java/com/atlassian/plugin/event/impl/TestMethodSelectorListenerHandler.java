package com.atlassian.plugin.event.impl;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.spi.ListenerInvoker;
import com.atlassian.plugin.event.PluginEventListener;
import org.junit.Test;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.*;

public class TestMethodSelectorListenerHandler {

    @Test
    public void getInvokersForEventListenerAnnotation() {
        final MethodSelectorListenerHandler methodSelectorListenerHandler = new MethodSelectorListenerHandler(new AnnotationListenerMethodSelector(EventListener.class));
        final SomeEventListener someEventListener = new SomeEventListener();
        final List<? extends ListenerInvoker> invokers = methodSelectorListenerHandler.getInvokers(someEventListener);
        assertThat(invokers, hasSize(3));
        assertNumberOfCalls(someEventListener, 0, 0, 0, 0);
        for (final ListenerInvoker invoker : invokers) {
            invoker.invoke(new Object());
        }
        assertNumberOfCalls(someEventListener, 1, 1, 0, 1);
        for (final ListenerInvoker invoker : invokers) {
            invoker.invoke(new Object());
        }
        assertNumberOfCalls(someEventListener, 2, 2, 0, 2);
    }

    @Test
    public void getInvokersForPluginEventListenerAnnotation() {
        final MethodSelectorListenerHandler methodSelectorListenerHandler = new MethodSelectorListenerHandler(new AnnotationListenerMethodSelector(PluginEventListener.class));
        final SomeEventListener someEventListener = new SomeEventListener();
        final List<? extends ListenerInvoker> invokers = methodSelectorListenerHandler.getInvokers(someEventListener);
        assertThat(invokers, hasSize(1));
        assertNumberOfCalls(someEventListener, 0, 0, 0, 0);
        for (final ListenerInvoker invoker : invokers) {
            invoker.invoke(new Object());
        }
        assertNumberOfCalls(someEventListener, 0, 0, 1, 0);
        for (final ListenerInvoker invoker : invokers) {
            invoker.invoke(new Object());
        }
        assertNumberOfCalls(someEventListener, 0, 0, 2, 0);
    }

    @Test
    public void orderOnEventListener() {
        final MethodSelectorListenerHandler methodSelectorListenerHandler = new MethodSelectorListenerHandler(new AnnotationListenerMethodSelector(EventListener.class));
        final SomeEventListener someEventListener = new SomeEventListener();
        final List<? extends ListenerInvoker> invokers = methodSelectorListenerHandler.getInvokers(someEventListener);
        assertThat(invokers, hasSize(3));
        assertThat(invokers.stream().map(ListenerInvoker::getOrder).collect(Collectors.toList()), containsInAnyOrder(0, Integer.MIN_VALUE, -10));
    }

    @Test
    public void orderOnPluginEventListener() {
        final MethodSelectorListenerHandler methodSelectorListenerHandler = new MethodSelectorListenerHandler(new AnnotationListenerMethodSelector(PluginEventListener.class));
        final SomeEventListener someEventListener = new SomeEventListener();
        final List<? extends ListenerInvoker> invokers = methodSelectorListenerHandler.getInvokers(someEventListener);
        assertThat(invokers, hasSize(1));
        assertThat(invokers.get(0).getOrder(), equalTo(0));
    }


    private void assertNumberOfCalls(final SomeEventListener someEventListener, final int method1, final int method2, final int method3, final int method4) {
        assertThat(someEventListener.eventListener1Called.get(), equalTo(method1));
        assertThat(someEventListener.eventListener2Called.get(), equalTo(method2));
        assertThat(someEventListener.eventListener3Called.get(), equalTo(method3));
        assertThat(someEventListener.eventListener4Called.get(), equalTo(method4));
    }

    class SomeEventListener {
        final AtomicInteger eventListener1Called = new AtomicInteger();
        final AtomicInteger eventListener2Called = new AtomicInteger();
        final AtomicInteger eventListener3Called = new AtomicInteger();
        final AtomicInteger eventListener4Called = new AtomicInteger();

        @EventListener(order = Integer.MIN_VALUE)
        public void method1(final Object event) {
            eventListener1Called.incrementAndGet();
        }

        @EventListener
        public void method2(final Object event) {
            eventListener2Called.incrementAndGet();
        }

        @PluginEventListener
        public void method3(final Object event) {
            eventListener3Called.incrementAndGet();
        }

        @EventListener(order = -10)
        public void method4(final Object event) {
            eventListener4Called.incrementAndGet();
        }
    }
}