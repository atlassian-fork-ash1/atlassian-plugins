package com.atlassian.plugin.event.impl;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.event.PluginEventListener;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class TestDefaultPluginEventManager {
    private final Object someObject = new Object();
    private final String someString = "string";

    @Mock
    private MethodTestListener methodTestListener;

    private DefaultPluginEventManager defaultPluginEventManager;

    @Before
    public void createDefaultPluginEventManager() {
        defaultPluginEventManager = new DefaultPluginEventManager();
        assertThat(defaultPluginEventManager.getEventPublisher(), notNullValue(EventPublisher.class));
    }

    @After
    public void discardDefaultPluginEventManager() {
        defaultPluginEventManager = null;
    }

    @Test
    public void broadcastCallsListenerChannelForType() {
        defaultPluginEventManager.register(methodTestListener);
        defaultPluginEventManager.broadcast(someObject);
        verify(methodTestListener).channel(someObject);
        verify(methodTestListener, never()).channel(anyString());
    }

    @Test
    public void broadcastCallsListenerChannelForSupertype() {
        final Integer someInteger = 0;
        defaultPluginEventManager.register(methodTestListener);
        defaultPluginEventManager.broadcast(someInteger);
        verify(methodTestListener).channel(someInteger);
        verify(methodTestListener, never()).channel(anyString());
    }

    @Test
    public void broadcastCallsListenerChannelForTypeAndSupertype() {
        defaultPluginEventManager.register(methodTestListener);
        defaultPluginEventManager.broadcast(someString);
        // The cast ensures we verify both overloads are called
        verify(methodTestListener).channel((Object) someString);
        verify(methodTestListener).channel(someString);
    }

    @Test
    public void listenerCalledOnceEvenIfRegisteredTwice() {
        defaultPluginEventManager.register(methodTestListener);
        defaultPluginEventManager.register(methodTestListener);
        defaultPluginEventManager.broadcast(someObject);
        verify(methodTestListener).channel(someObject);
        verify(methodTestListener, never()).channel(anyString());
    }

    @Test
    public void registerUnmatchingListenerThrows() {
        BadListener badListener = mock(BadListener.class);
        try {
            defaultPluginEventManager.register(badListener);
            fail();
        } catch (IllegalArgumentException ex) {
            // test passed
        }
        verify(badListener, never()).someMethod();
    }

    @Test
    public void customSelectorMatchesAndOverridesDefault() {
        ListenerMethodSelector onEventSelector = method -> "onEvent".equals(method.getName());
        defaultPluginEventManager = new DefaultPluginEventManager(onEventSelector);
        defaultPluginEventManager.register(methodTestListener);
        defaultPluginEventManager.broadcast(someString);
        verify(methodTestListener).onEvent(someString);
        verify(methodTestListener, never()).channel(any());
        verify(methodTestListener, never()).channel(anyString());
    }

    @Test
    public void overlappingSelectorsBothMatch() {
        ListenerMethodSelector firstSelector = new MethodNameListenerMethodSelector();
        ListenerMethodSelector secondSelector = new MethodNameListenerMethodSelector();
        defaultPluginEventManager = new DefaultPluginEventManager(firstSelector, secondSelector);
        defaultPluginEventManager.register(methodTestListener);
        defaultPluginEventManager.broadcast(someObject);
        verify(methodTestListener, times(2)).channel(someObject);
    }

    @Test
    public void broadcastCallsAnnotatedMethods() {
        // I don't believe this can be done with mocks, because the stub
        // is not annotated, and i don't see a way of getting it annotated.
        AnnotationTestListener listener = new AnnotationTestListener();
        defaultPluginEventManager.register(listener);
        defaultPluginEventManager.broadcast(someObject);
        assertEquals(1, listener.eventListenerCalled);
        assertEquals(1, listener.pluginEventListenerCalled);
    }

    @Test
    public void unregisterStopsBroadcastSendingEvents() {
        defaultPluginEventManager.register(methodTestListener);
        defaultPluginEventManager.broadcast(someObject);
        verify(methodTestListener).channel(someObject);
        defaultPluginEventManager.unregister(methodTestListener);
        defaultPluginEventManager.broadcast(someObject);
        // This checks it was called once, i.e. first broadcast
        verify(methodTestListener).channel(someObject);
    }

    @Test(expected = IllegalArgumentException.class)
    public void registerNullThrows() {
        defaultPluginEventManager.register(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void unregisterNullThrows() {
        defaultPluginEventManager.unregister(null);
    }

    public static class AnnotationTestListener {
        int pluginEventListenerCalled = 0;
        int eventListenerCalled = 0;

        @PluginEventListener
        public void doEventOld(Object obj) {
            ++pluginEventListenerCalled;
        }

        @EventListener
        public void doEventNew(Object obj) {
            ++eventListenerCalled;
        }
    }

    public static class MethodTestListener {
        public void channel(Object object) {
        }

        public void channel(String string) {
        }

        public void onEvent(String string) {
        }
    }

    public static class BadListener {
        public void someMethod() {
        }
    }
}
