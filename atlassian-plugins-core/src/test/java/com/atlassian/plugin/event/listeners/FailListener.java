package com.atlassian.plugin.event.listeners;

import static org.junit.Assert.fail;

public class FailListener {
    private final Class<?> clazz;

    public FailListener(final Class<?> clazz) {
        this.clazz = clazz;
    }

    public void channel(final Object o) {
        if (clazz.isInstance(o)) {
            fail("Event thrown of type " + clazz.getName());
        }
    }
}
