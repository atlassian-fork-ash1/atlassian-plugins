package it.com.atlassian.plugin.manager;

import com.atlassian.plugin.DefaultModuleDescriptorFactory;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginException;
import com.atlassian.plugin.PluginInternal;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.descriptors.UnrecognisedModuleDescriptor;
import com.atlassian.plugin.event.PluginEventListener;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.PluginModuleDisabledEvent;
import com.atlassian.plugin.event.events.PluginModuleDisablingEvent;
import com.atlassian.plugin.event.events.PluginModuleEnabledEvent;
import com.atlassian.plugin.event.events.PluginModuleEnablingEvent;
import com.atlassian.plugin.event.impl.DefaultPluginEventManager;
import com.atlassian.plugin.factories.LegacyDynamicPluginFactory;
import com.atlassian.plugin.factories.PluginFactory;
import com.atlassian.plugin.hostcontainer.DefaultHostContainer;
import com.atlassian.plugin.loaders.ClassPathPluginLoader;
import com.atlassian.plugin.loaders.DirectoryPluginLoader;
import com.atlassian.plugin.loaders.PluginLoader;
import com.atlassian.plugin.loaders.SinglePluginLoader;
import com.atlassian.plugin.manager.DefaultPluginManager;
import com.atlassian.plugin.manager.store.MemoryPluginPersistentStateStore;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.test.PluginJarBuilder;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import org.dom4j.Element;
import org.dom4j.dom.DOMElement;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.File;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.hamcrest.collection.IsEmptyIterable.emptyIterable;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Tests for addition, enumeration and removal of dynamic modules for a DefaultDynamicPlugin.
 */
@RunWith(Parameterized.class)
public class TestDefaultPluginManagerDynamicModules {
    protected static final String PLUGIN_KEY = "pluginKey";
    protected static final String PLUGIN_NAME = "pluginName";
    protected static final String PLUGIN_VERSION = "3.2.1";

    private static final String MODULE_TYPE = "moduleType";
    private static final String MODULE_KEY = "moduleKey";
    private static final String MODULE_CLASS = "moduleClass";

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    private PluginLoader pluginLoader;

    protected PluginEventManager pluginEventManager;

    private DefaultPluginManager defaultPluginManager;

    private PluginInternal plugin;

    private List<Class> moduleEvents;

    @Parameterized.Parameters(name = "{0}")
    public static Collection<Object[]> data() {
        return ImmutableList.of(
                new Object[]{"ClassPathPluginLoader", new Function<PluginEventManager, PluginLoader>() {
                    @Override
                    public PluginLoader apply(final PluginEventManager pluginEventManager) {
                        return new ClassPathPluginLoader("test-plugin-dynamic-modules.xml");
                    }
                }},
                new Object[]{"SinglePluginLoader", new Function<PluginEventManager, PluginLoader>() {
                    @Override
                    public PluginLoader apply(final PluginEventManager pluginEventManager) {
                        return new SinglePluginLoader("test-plugin-dynamic-modules.xml");
                    }
                }},
                new Object[]{"ScanningPluginLoader (DirectoryPluginLoader) <-> LegacyDynamicPluginFactory", new Function<PluginEventManager, PluginLoader>() {
                    @Override
                    public PluginLoader apply(final PluginEventManager pluginEventManager) {
                        try {
                            // fresh plugin directory for each test
                            final File pluginDir = Files.createTempDirectory(null).toFile();

                            // first create a plugin to load
                            new PluginJarBuilder(PLUGIN_NAME)
                                    .addPluginInformation(PLUGIN_KEY, PLUGIN_NAME, PLUGIN_VERSION, 1)
                                    .build(pluginDir);

                            // now create the plugin loader that will get the above plugin
                            final PluginFactory pluginFactory = new LegacyDynamicPluginFactory(PluginAccessor.Descriptor.FILENAME);
                            return new DirectoryPluginLoader(pluginDir, ImmutableList.of(pluginFactory), pluginEventManager);
                        } catch (Exception e) {
                            throw new RuntimeException(e);
                        }
                    }
                }}
        );
    }

    public TestDefaultPluginManagerDynamicModules(final String bleh, final Function<PluginEventManager, PluginLoader> function) {
        // listen for plugin module events
        pluginEventManager = new DefaultPluginEventManager();
        pluginEventManager.register(this);

        this.pluginLoader = function.apply(pluginEventManager);
    }

    @Before
    public void setUp() throws Exception {
        // dummy module factory
        final DefaultModuleDescriptorFactory moduleDescriptorFactory = new DefaultModuleDescriptorFactory(new DefaultHostContainer());
        moduleDescriptorFactory.addModuleDescriptor(MODULE_TYPE, DummyModuleDescriptor.class);

        // plugin manager that we are testing
        defaultPluginManager = new DefaultPluginManager(new MemoryPluginPersistentStateStore(), ImmutableList.of(pluginLoader), moduleDescriptorFactory, pluginEventManager);
        defaultPluginManager.init();

        // retrieve the loaded plugin
        assertThat(defaultPluginManager.getPlugin(PLUGIN_KEY), instanceOf(PluginInternal.class));
        plugin = (PluginInternal) defaultPluginManager.getPlugin(PLUGIN_KEY);

        // only interested in events from now on
        moduleEvents = Collections.synchronizedList(new ArrayList<Class>());
    }

    @After
    public void tearDown() throws Exception {
        pluginEventManager.unregister(this);
    }

    @Test
    public void removeNonExistent() {
        final ModuleDescriptor moduleDescriptor = mock(ModuleDescriptor.class);
        when(moduleDescriptor.getKey()).thenReturn("dodgeyModuleKey");

        expectedException.expect(PluginException.class);
        expectedException.expectMessage(containsString("dodgeyModuleKey"));
        expectedException.expectMessage(containsString(PLUGIN_KEY));

        defaultPluginManager.removeDynamicModule(plugin, moduleDescriptor);
    }

    @Test
    public void addRemove() throws Exception {
        // no modules, dynamic or otherwise
        assertThat(plugin.getModuleDescriptors(), emptyIterable());
        assertThat(plugin.getDynamicModuleDescriptors(), emptyIterable());
        assertThat(plugin.getModuleDescriptor(MODULE_KEY), nullValue());
        assertThat(moduleEvents, empty());

        final ModuleDescriptor moduleDescriptor = addModule();

        // it's added as dynamic and regular
        assertThat(plugin.getDynamicModuleDescriptors(), Matchers.<ModuleDescriptor<?>>containsInAnyOrder(moduleDescriptor));
        assertThat(plugin.getModuleDescriptors(), Matchers.<ModuleDescriptor<?>>containsInAnyOrder(moduleDescriptor));
        assertThat(plugin.getModuleDescriptor(MODULE_KEY), Matchers.<ModuleDescriptor<?>>is(moduleDescriptor));

        // should be enabled
        assertThat(moduleDescriptor.isEnabled(), is(true));

        // events were published
        assertThat(moduleEvents, Matchers.<Class>contains(PluginModuleEnablingEvent.class, PluginModuleEnabledEvent.class));

        // may be removed
        defaultPluginManager.removeDynamicModule(plugin, moduleDescriptor);

        // no more modules
        assertThat(plugin.getModuleDescriptors(), emptyIterable());
        assertThat(plugin.getDynamicModuleDescriptors(), emptyIterable());
        assertThat(plugin.getModuleDescriptor(MODULE_KEY), nullValue());

        // events were published
        assertThat(moduleEvents, Matchers.<Class>contains(PluginModuleEnablingEvent.class, PluginModuleEnabledEvent.class, PluginModuleDisablingEvent.class, PluginModuleDisabledEvent.class));
    }

    @Test
    public void addTwice() {
        addModule();

        // events were published
        assertThat(moduleEvents, Matchers.<Class>contains(PluginModuleEnablingEvent.class, PluginModuleEnabledEvent.class));

        expectedException.expect(PluginException.class);
        expectedException.expectMessage(containsString(MODULE_KEY));

        addModule();

        // no further events were published
        assertThat(moduleEvents, Matchers.<Class>contains(PluginModuleEnablingEvent.class, PluginModuleEnabledEvent.class));
    }

    @Test
    public void addDisabled() {
        // add a rubbish module that will be unrecognised
        final Element e = new DOMElement("bazza");
        e.addAttribute("key", "mckenzie");
        final ModuleDescriptor moduleDescriptor = defaultPluginManager.addDynamicModule(plugin, e);

        // shouldn't be enabled as it is an UnrecognisedModuleDescriptor
        assertThat(moduleDescriptor.isEnabled(), is(false));
        assertThat(moduleDescriptor, is(instanceOf(UnrecognisedModuleDescriptor.class)));

        // events were not published
        assertThat(moduleEvents, empty());
    }

    private ModuleDescriptor addModule() {
        // add a component which uses a class in the plugin
        final Element e = new DOMElement(MODULE_TYPE);
        e.addAttribute("key", MODULE_KEY);
        e.addAttribute("class", MODULE_CLASS);
        return defaultPluginManager.addDynamicModule(plugin, e);
    }

    @PluginEventListener
    public void onBeforePluginModuleEnabledEvent(PluginModuleEnablingEvent event) {
        if (MODULE_KEY.equals(event.getModule().getKey())) {
            moduleEvents.add(PluginModuleEnablingEvent.class);
        }
    }

    @PluginEventListener
    public void onPluginModuleEnabledEvent(PluginModuleEnabledEvent event) {
        if (MODULE_KEY.equals(event.getModule().getKey())) {
            moduleEvents.add(PluginModuleEnabledEvent.class);
        }
    }

    @PluginEventListener
    public void onBeforePluginModuleDisabledEvent(PluginModuleDisablingEvent event) {
        if (MODULE_KEY.equals(event.getModule().getKey())) {
            moduleEvents.add(PluginModuleDisablingEvent.class);
        }
    }

    @PluginEventListener
    public void onPluginModuleDisableEvent(PluginModuleDisabledEvent event) {
        if (MODULE_KEY.equals(event.getModule().getKey())) {
            moduleEvents.add(PluginModuleDisabledEvent.class);
        }
    }

    public static class DummyModuleDescriptor extends AbstractModuleDescriptor<Object> {
        public DummyModuleDescriptor() {
            super(new DummyModuleFactory());
        }

        @Override
        public Object getModule() {
            return null;
        }
    }

    public static class DummyModuleFactory implements ModuleFactory {
        @Override
        public <T> T createModule(final String name, final ModuleDescriptor<T> moduleDescriptor)
                throws PluginParseException {
            return null;
        }
    }
}
