package it.com.atlassian.plugin.classloader;

import com.atlassian.plugin.classloader.PluginClassLoader;
import com.atlassian.plugin.test.PluginTestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * To make this test pass in your IDE, make sure you run Maven to build the necessary plugins and copy
 * them to the target directory ('mvn package') and when running the tests, set the 'project.version'
 * system property to the current version in the POM. E.g. -Dproject.version=2.1.0-SNAPSHOT
 */
public class TestPluginClassLoader {

    @Rule
    public final TemporaryFolder temporaryFolder = new TemporaryFolder();

    private PluginClassLoader pluginClassLoader;

    @Before
    public void setUp() throws Exception {
        final URL url = getClass().getClassLoader().getResource(PluginTestUtils.SIMPLE_TEST_JAR);
        assertNotNull("Can't find test resource '" + PluginTestUtils.SIMPLE_TEST_JAR + "' -- see class Javadoc, and be sure to set the 'project.version' system property!", url);
        pluginClassLoader = new PluginClassLoader(new File(url.toURI()), getClass().getClassLoader(), temporaryFolder.getRoot());
    }

    @Test
    public void testPluginClassLoaderFindsInnerJars() {
        List innerJars = pluginClassLoader.getPluginInnerJars();
        assertEquals(2, innerJars.size());
    }

    @Test
    public void testPluginClassLoaderLoadsResourceFromOuterJarFirst() throws Exception {
        URL resourceUrl = pluginClassLoader.getResource("testresource.txt");
        assertNotNull(resourceUrl);
        assertEquals("outerjar", IOUtils.toString(resourceUrl.openStream()));
    }

    @Test
    public void testPluginClassLoaderLoadsClassFromOuterJar() throws Exception {
        Class c = pluginClassLoader.loadClass("com.atlassian.plugin.simpletest.TestClassOne");
        assertEquals("com.atlassian.plugin.simpletest", c.getPackage().getName());  // PLUG-27
        assertEquals("com.atlassian.plugin.simpletest.TestClassOne", c.getName());
    }

    @Test
    public void testPluginClassLoaderHandlesDeletedExtractedInnerJars() throws Exception {
        File tmpDir = temporaryFolder.getRoot();
        InputStream inputStream = pluginClassLoader.getResource("innerresource.txt").openStream();
        assertNotNull(inputStream);
        inputStream.close();
        FileUtils.deleteDirectory(tmpDir);
        assertTrue(tmpDir.mkdirs());
        try {
            assertNotNull(pluginClassLoader.getResource("innerresource.txt").openStream());
            fail("underlying extracted inner jar was deleted and should throw FileNotFoundException");
        } catch (IOException e) {
            // expected exception because we deleted the jar
        }
    }

    @Test
    public void testPluginClassLoaderExtractsInnerJarsToSpecifiedDirectory() {
        final List<File> files = pluginClassLoader.getPluginInnerJars();
        for (File file : files) {
            assertEquals(temporaryFolder.getRoot(), file.getParentFile());
        }
    }

    @Test
    public void testPluginClassLoaderDetectsMissingTempDirectory() {
        try {
            new PluginClassLoader(null, getClass().getClassLoader(), new File("/doesnotexisthopefully"));
            fail("should throw IllegalArgumentException when temp directory does not exist");
        } catch (IllegalStateException e) {
            // expected
        }
    }

    @Test
    public void testPluginClassLoaderLoadsResourceFromInnerJarIfNotInOuterJar() throws Exception {
        final URL resourceUrl = pluginClassLoader.getResource("innerresource.txt");
        assertNotNull(resourceUrl);
        InputStream inputStream = resourceUrl.openStream();
        assertEquals("innerresource", IOUtils.toString(inputStream));
        inputStream.close();
    }

    @Test
    public void testPluginClassLoaderDoesNotSwallowClassesFromADifferentClassLoader() throws Exception {
        final Class c = Class.forName(getClass().getName(), true, pluginClassLoader);
        assertEquals(getClass().getClassLoader(), c.getClassLoader());
    }

    @Test
    public void testPluginClassLoaderOverridesContainerClassesWithInnerJarClasses() throws Exception {
        Class mockVersionedClass =
                Class.forName("com.atlassian.plugin.mock.MockVersionedClass", true, pluginClassLoader);
        Object instance = mockVersionedClass.getConstructor().newInstance();
        Method getVersion = instance.getClass().getMethod("getVersion");
        Object version = getVersion.invoke(instance);

        assertEquals("PluginClassLoader is searching the parent classloader for classes before inner JARs",
                2, version);
    }

    @Test
    public void testPluginClassLoaderDoesNotLockTheJarsPermanently() throws Exception {
        //N.B This will probably never fail on a non Windows machine
        File original = new File(getClass().getClassLoader().getResource(PluginTestUtils.SIMPLE_TEST_JAR).toURI());
        File tmpFile = new File(original.getAbsolutePath() + ".tmp");
        FileUtils.copyFile(original, tmpFile);

        PluginClassLoader pluginClassLoaderThatHasNotLockedFileYet = new
                PluginClassLoader(tmpFile, getClass().getClassLoader());
        Class mockVersionedClass =
                Class.forName("com.atlassian.plugin.mock.MockVersionedClass", true, pluginClassLoader);
        assertTrue(tmpFile.delete());
    }
}
