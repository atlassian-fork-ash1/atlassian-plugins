package com.atlassian.plugin;

import com.atlassian.annotations.Internal;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.InputStream;
import java.net.URL;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

public interface Plugin extends ScopeAware, Resourced, Comparable<Plugin> {
    /**
     * This is the historical version of plugins. Which is mostly static plugins loaded from the same classpath to the
     * application.
     */
    int VERSION_1 = 1;

    /**
     * This is the version of plugins which introduced dynamic plugins for all. Based on OSGi and Spring DM. Those plugins
     * undergo some transformations to make the plugin artifact compatible with the OSGi + Spring DM container.
     */
    int VERSION_2 = 2;

    /**
     * This is the versions of plugins that adds remotes plugins (developed outside of the plugin framework itself).
     * Plugins version 3 don't undergo any transformation so it is up to the plugin developer to write their own Spring
     * configuration files if this is their chosen framework, but other frameworks can be introduced.
     */
    int VERSION_3 = 3;

    /**
     * Gets the version of the plugins system to handle this plugin
     *
     * @return The plugins version. If undefined, assumed to be 1.
     */
    int getPluginsVersion();

    /**
     * Sets the version of the plugins system
     *
     * @param version The version
     */
    void setPluginsVersion(int version);

    /**
     * Returns the non-localised name of this plugin if defined.
     *
     * <p> This corresponds to the value of the {@code name} field in the plugin's XML configuration file.
     *
     * <p> You would expect a plugin developer to fill in one of either {@code name}, or {@code i18n-name-key},
     * but the framework does no validation and makes no guarantees that this is the case.
     *
     * @return the non-localised name of this plugin if defined, or null.
     * @see #getI18nNameKey()
     */
    String getName();

    /**
     * Sets the non-localised name of this plugin.
     *
     * @param name the name.
     * @see #getName()
     */
    void setName(String name);

    /**
     * Returns the i18nKey used to get an internationalised name for this plugin.
     *
     * <p> This corresponds to the value of the {@code i18n-name-key} field in the plugin's XML configuration file.
     *
     * <p> You would expect a plugin developer to fill in one of either {@code name}, or {@code i18n-name-key},
     * but the framework does no validation and makes no guarantees that this is the case.
     *
     * @return the i18n Name Key for this plugin if defined, or null.
     * @see #getName()
     */
    String getI18nNameKey();

    /**
     * Sets the i18nKey used to get an internationalised name for this plugin.
     *
     * @param i18nNameKey the i18n Name Key.
     * @see #getI18nNameKey()
     */
    void setI18nNameKey(String i18nNameKey);

    String getKey();

    void setKey(String aPackage);

    void addModuleDescriptor(ModuleDescriptor<?> moduleDescriptor);

    /**
     * Get the {@link Collection} of {@link ModuleDescriptor descriptors}.
     *
     * <p> The iteration order of the collection is
     * the order that the modules will be enabled, and should be the same order that the modules appear in the
     * plugin descriptor.
     *
     * @return the modules contained by this plugin in the order they are to be enabled
     */
    Collection<ModuleDescriptor<?>> getModuleDescriptors();

    /**
     * Get the {@link ModuleDescriptor} for a particular key. Returns <tt>null</tt> if the plugin does not exist.
     * <p>
     * Note: The {@link ModuleDescriptor#getModule()} may throw {@link ClassCastException} if the expected type is incorrect.
     *
     * @param key the {@link String} complete key of the module, in the form "org.example.plugin:module-key".
     * @return the {@link ModuleDescriptor} of the expected type.
     */
    ModuleDescriptor<?> getModuleDescriptor(String key);

    /**
     * Get the {@link ModuleDescriptor descriptors} whose module class implements or is assignable from the supplied {@link Class}.
     * <p>
     * Note: The {@link ModuleDescriptor#getModule()} may throw {@link ClassCastException} if the expected type is incorrect.
     * Normally this method would not be supplied with anything other than {@link Object} or &lt;?&gt;, unless you are
     * confident in the super type of the module classes this {@link Plugin} provides.
     *
     * @param <M>         The expected module type of the returned {@link ModuleDescriptor descriptors}.
     * @param moduleClass the {@link Class super class} the {@link ModuleDescriptor descriptors} return.
     * @return the {@link List} of {@link ModuleDescriptor descriptors} of the expected type.
     */
    <M> List<ModuleDescriptor<M>> getModuleDescriptorsByModuleClass(Class<M> moduleClass);

    /**
     * Gets the installation mode
     *
     * @return the plugin's installation mode, local or remote.
     * @since 3.0
     */
    InstallationMode getInstallationMode();

    boolean isEnabledByDefault();

    void setEnabledByDefault(boolean enabledByDefault);

    PluginInformation getPluginInformation();

    void setPluginInformation(PluginInformation pluginInformation);

    void setResources(Resourced resources);

    /**
     * Returns this plugin's current state.
     *
     * @return the current state of the plugin.
     * @since 2.2.0
     */
    PluginState getPluginState();

    /**
     * Whether the plugin is a "system" plugin that shouldn't be made visible to the user.
     *
     * @return {@code true} if this plugin is a "system" plugin.
     * @deprecated since 2.6.0 use {@link com.atlassian.plugin.metadata.PluginMetadataManager#isSystemProvided(Plugin)}}
     * instead.
     */
    boolean isSystemPlugin();

    /**
     * @param system whether the plugin is a "system" plugin that shouldn't be made visible to the user.
     * @deprecated since 2.6.0 provide {@link com.atlassian.plugin.metadata.PluginMetadataManager} with information about the
     * plugin instead. There is no way to programatically set this value now.
     */
    void setSystemPlugin(boolean system);

    boolean containsSystemModule();

    /**
     * Whether the plugin is a "bundled" plugin that can't be removed.
     *
     * @return {@code true} if this plugin is a "bundled" plugin.
     */
    boolean isBundledPlugin();

    /**
     * The date this plugin was loaded into the system.
     *
     * @return The date this plugin was loaded into the system.
     */
    Date getDateLoaded();

    /**
     * The date this plugin was installed into the system, is the same as the loaded date for non artifact backed plugins
     *
     * @return The date this plugin was installed into the system
     * @since 3.0.0
     */
    Date getDateInstalled();

    /**
     * Whether or not this plugin can be 'uninstalled'.
     *
     * @return {@code true} if this plugin can be 'uninstalled'.
     */
    boolean isUninstallable();

    /**
     * Should the plugin file be deleted on uninstall?
     *
     * @return {@code true} if this plugin file should be deleted on uninstall.
     */
    boolean isDeleteable();

    /**
     * Whether or not this plugin is loaded dynamically at runtime.
     *
     * @return {@code true} if this plugin is loaded dynamically at runtime.
     */
    boolean isDynamicallyLoaded();

    /**
     * Get the plugin to load a specific class.
     *
     * @param clazz        The name of the class to be loaded
     * @param callingClass The class calling the loading (used to help find a classloader)
     * @return The loaded class.
     * @throws ClassNotFoundException if the class cannot be located.
     */
    <T> Class<T> loadClass(String clazz, Class<?> callingClass) throws ClassNotFoundException;

    /**
     * Get the classloader for the plugin.
     *
     * @return The classloader used to load classes for this plugin
     */
    ClassLoader getClassLoader();

    /**
     * Retrieve the URL of the resource from the plugin.
     *
     * @param path the name of the resource to be loaded
     * @return The URL to the resource, or null if the resource is not found
     */
    URL getResource(String path);

    /**
     * Load a given resource from the plugin. Plugins that are loaded dynamically will need
     * to implement this in a way that loads the resource from the same context as the plugin.
     * Static plugins can just pull them from their own classloader.
     *
     * @param name The name of the resource to be loaded.
     * @return An InputStream for the resource, or null if the resource is not found.
     */
    InputStream getResourceAsStream(String name);

    /**
     * Installs the plugin into any internal, managing container. This method will be called on every startup. Unless
     * an exception is thrown, the plugin should be in the {@link PluginState#INSTALLED} state. If the plugin is already
     * in the {@link PluginState#INSTALLED} state, nothing will happen.
     *
     * @throws PluginException If the plugin could not be installed
     * @since 2.2.0
     */
    void install() throws PluginException;

    /**
     * Uninstalls the plugin from any internal container. This method will be called on every shutdown. Unless an
     * exception is thrown, the plugin should be in the {@link PluginState#UNINSTALLED} state. If the plugin is already
     * in the {@link PluginState#UNINSTALLED} state, nothing will happen.
     *
     * @throws PluginException If the plugin could not be uninstalled
     * @since 2.2.0
     */
    void uninstall() throws PluginException;

    /**
     * Enables the plugin. Unless an exception is thrown, the plugin should then be in either the
     * {@link PluginState#ENABLING} or {@link PluginState#ENABLED} state. If the plugin is already in the
     * {@link PluginState#ENABLING} or {@link PluginState#ENABLED} state, nothing will happen.
     *
     * @throws PluginException If the plugin could not be enabled
     * @since 2.2.0
     */
    void enable() throws PluginException;

    /**
     * Disables the plugin. Unless an exception is thrown, the plugin should be in the {@link PluginState#DISABLED}
     * state. If the plugin is already in the {@link PluginState#DISABLED} state, nothing will happen.
     *
     * @throws PluginException If the plugin could not be disabled
     * @since 2.2.0 If the plugin could not be disabled
     */
    void disable() throws PluginException;

    /**
     * Determines which plugin keys are dependencies, categorising them as mandatory, optional or dynamic.
     *
     * @return not null, possibly empty
     * @since 4.0
     */
    @Nonnull
    PluginDependencies getDependencies();

    /**
     * @return the list of permissions currently valid for the plugin
     * @since 3.0
     */
    Set<String> getActivePermissions();

    /**
     * @return {@code true} if the plugin has all the permissions
     * @since 3.0
     */
    boolean hasAllPermissions();

    /**
     * Perform any required resolution.
     *
     * This is a hook to allow the plugin system to perform a resolution pass over a group of modules
     * before the enable pass.
     *
     * @since 4.0.0
     */
    void resolve();

    /**
     * Obtain the date that the plugin system most recently commenced enabling this plugin.
     *
     * This may return null if the plugin has never commenced enabling, or if the value is
     * unavailable for other reasons, such as wrappers around legacy implementations.
     *
     * @return the date that the plugin most recently entered {@link PluginState#ENABLING}.
     * @since 4.0.0
     */
    @Nullable
    Date getDateEnabling();

    /**
     * Obtain the date that the plugin system most recently completed enabling of this plugin.
     *
     * This will return null if the plugin has never been enabled, has not completed enabling
     * since it most recently started enabling, or if the value is unavailable for any other
     * reason, such as wrappers around legacy implementations.
     *
     * @return the date that the plugin most recently entered {@link PluginState#ENABLED},
     * if this was not before the most recent {@link PluginState#ENABLING}, otherwise null.
     * @since 4.0.0
     */
    @Nullable
    Date getDateEnabled();

    /**
     * Retrieve the original, unprocessed or transformed {@link PluginArtifact} used to create this plugin instance.
     * <p>
     * Note that this method may be removed without notice; it is for use only by the host application.
     * <p>
     * This method was originally part of the internal <code>PluginArtifactBackedPlugin</code> interface that is no
     * longer present.
     *
     * @return null if this plugin has no artifact
     * @since 4.0.0
     */
    @Internal
    PluginArtifact getPluginArtifact();
}
