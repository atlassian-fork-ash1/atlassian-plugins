package com.atlassian.plugin;

import org.dom4j.Element;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Set;

/**
 * Interface to control the state of the plugin system
 */
public interface PluginController {
    /**
     * Enable a set of plugins by key. This will implicitly and recursively enable all dependent plugins
     *
     * @param keys The plugin keys. Must not be null.
     * @since 2.5.0
     */
    void enablePlugins(String... keys);

    /**
     * Disables the plugin with the given key.
     * <p>
     * Calling this method will persist the disabled state so that the plugin will also be disabled on next startup.
     * This would normally be used when a user manually disables a plugin.
     * <p>
     * If you extend DefaultPluginManager and override this method, you will also need to override {@link #disablePluginWithoutPersisting(String)}.
     * <p>
     * Mandatory dependent plugins will also be disabled. Optionally dependent plugins will be disabled then enabled.
     * Dynamically dependent plugins will not be disabled.
     *
     * @param key The plugin key.
     * @see #disablePluginWithoutPersisting(String)
     * @see Plugin#getDependencies()
     */
    void disablePlugin(String key);

    /**
     * Disables the plugin with the given key without persisting the disabled state.
     * <p>
     * Calling this method will NOT persist the disabled state so that the framework will try to enable the plugin on next startup.
     * This is used when a plugin has errors on startup.
     * <p>
     * If you extend DefaultPluginManager and override {@link #disablePlugin(String)}, you will also need to override this method.
     * <p>
     * Mandatory dependent plugins will also be disabled. Optionally dependent plugins will be disabled then enabled.
     * Dynamically dependent plugins will not be disabled.
     *
     * @param key The plugin key.
     * @see #disablePlugin(String)
     * @see Plugin#getDependencies()
     * @since 2.3.0
     */
    void disablePluginWithoutPersisting(String key);

    /**
     * Enable a plugin module by key.
     *
     * @param completeKey The "complete key" of the plugin module.
     */
    void enablePluginModule(String completeKey);

    /**
     * Disable a plugin module by key.
     *
     * @param completeKey The "complete key" of the plugin module.
     */
    void disablePluginModule(String completeKey);

    /**
     * Installs multiple plugins and returns the list of plugin keys. All plugin artifacts must be for valid plugins
     * or none will be installed.
     *
     * @param pluginArtifacts The list of plugin artifacts to install
     * @return A list of plugin keys
     * @throws com.atlassian.plugin.PluginParseException if any plugin is not a valid plugin
     * @since 2.3.0
     */
    Set<String> installPlugins(PluginArtifact... pluginArtifacts) throws PluginParseException;

    /**
     * Uninstall the plugin, disabling it first.
     *
     * @param plugin The plugin.
     * @throws PluginException if there was some problem uninstalling the plugin.
     */
    void uninstall(Plugin plugin) throws PluginException;

    /**
     * Uninstall multiple plugin, disabling it first.
     *
     * @param plugins The plugins to uninstall.
     * @throws PluginException if there was some problem uninstalling a plugin.
     */
    default void uninstallPlugins(Collection<Plugin> plugins) throws PluginException {
        LoggerFactory.getLogger(PluginController.class).warn("Naive uninstallPlugins implementation. Please upgrade plugin framework.");
        plugins.forEach(this::uninstall);
    }

    /**
     * Restores the state of any plugin requiring a restart that had been removed, upgraded, or installed. If marked
     * as removed, the mark will be deleted. If marked as upgrade, an attempt to restore the original plugin artifact
     * will be made. If marked as install, the artifact will be deleted.
     *
     * @param pluginKey The plugin key
     * @throws PluginException          if there was some problem reverting the plugin state.
     * @throws IllegalArgumentException if the plugin key is null or cannot be resolved to a plugin
     * @since 2.5.0
     */
    void revertRestartRequiredChange(String pluginKey) throws PluginException;

    /**
     * Search all loaders and add any new plugins you find.
     *
     * @return The number of new plugins found.
     */
    int scanForNewPlugins() throws PluginParseException;

    /**
     * Add a new module described by <code>element</code> to the plugin specified.
     * <p>
     * Module will be enabled if the following conditions are met:
     * <ul>
     * <li>{@link ModuleDescriptor#isEnabledByDefault()} returns <code>true</code></li>
     * <li>This module has not been marked as disabled elsewhere, e.g. by user</li>
     * </ul>
     *
     * @param plugin to add the module to
     * @param module to add
     * @return added module
     * @see com.atlassian.plugin.PluginController#removeDynamicModule(Plugin, ModuleDescriptor)
     * @see com.atlassian.plugin.PluginAccessor#getDynamicModules(Plugin)
     * @since 4.0.0
     */
    ModuleDescriptor<?> addDynamicModule(Plugin plugin, Element module);

    /**
     * Remove a module that was dynamically added to plugin..
     *
     * @param plugin to remove the module from
     * @param module to remove
     * @see com.atlassian.plugin.PluginController#addDynamicModule(Plugin, org.dom4j.Element)
     * @see com.atlassian.plugin.PluginAccessor#getDynamicModules(Plugin)
     */
    void removeDynamicModule(Plugin plugin, ModuleDescriptor<?> module);
}
