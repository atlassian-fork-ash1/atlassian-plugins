package com.atlassian.plugin;

import java.io.File;
import java.io.InputStream;
import java.util.Set;

/**
 * Allows the retrieval of files and/or an input stream of a plugin artifact. Implementations
 * must allow multiple calls to {@link #getInputStream()}.
 *
 * @see PluginController
 * @since 2.0.0
 */
public interface PluginArtifact {
    /**
     * @return true if the resource exists in this artifact, otherwise false
     * @since 2.2.0
     */
    boolean doesResourceExist(String name);

    /**
     * @return an input stream of the resource specified inside the artifact. Null if the resource cannot be found.
     * @throws PluginParseException if the there was an exception retrieving the resource from the artifact
     */
    InputStream getResourceAsStream(String name) throws PluginParseException;

    /**
     * @return the original name of the plugin artifact file. Typically used
     * for persisting it to disk with a meaningful name.
     */
    String getName();

    /**
     * @return an InputStream for the entire plugin artifact. Calling this
     * multiple times will return a fresh input stream each time.
     */
    InputStream getInputStream();

    /**
     * @return the artifact as a file, or its underlying file if it is already one
     * @since 2.2.0
     */
    File toFile();

    /**
     * @return {@code true} if the plugin contains or references java executable code.
     * @since 3.0
     */
    boolean containsJavaExecutableCode();

    /**
     * @return {@code true} if the plugin contains Spring context files or instructions
     * @since 4.1
     */
    boolean containsSpringContext();

    /**
     * @return the ReferenceMode specifying whether or not this PluginArtifact may be reference installed.
     * @since 4.0.0
     */
    ReferenceMode getReferenceMode();

    interface HasExtraModuleDescriptors {
        /**
         * @return An Iterable containing the paths to any additional xml files found in scan folders
         * @since 3.2.16
         */
        Set<String> extraModuleDescriptorFiles(String rootFolder);
    }
}
