package com.atlassian.plugin.event.events;

import com.atlassian.annotations.PublicApi;
import com.atlassian.plugin.Plugin;

/**
 * Event fired when a plugin is explicitly uninstalled (as opposed to as part of an upgrade).
 *
 * @see com.atlassian.plugin.event.events
 * @since 2.5.0
 */
@PublicApi
public class PluginUninstalledEvent extends PluginEvent {
    public PluginUninstalledEvent(final Plugin plugin) {
        super(plugin);
    }
}
