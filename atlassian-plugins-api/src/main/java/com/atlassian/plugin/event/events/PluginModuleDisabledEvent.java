package com.atlassian.plugin.event.events;

import com.atlassian.annotations.PublicApi;
import com.atlassian.plugin.ModuleDescriptor;

/**
 * Event fired when a plugin module is disabled, which can also happen when its
 * plugin is disabled or uninstalled.
 *
 * @see com.atlassian.plugin.event.events
 * @since 2.2.0
 */
@PublicApi
public class PluginModuleDisabledEvent extends PluginModulePersistentEvent {
    public PluginModuleDisabledEvent(final ModuleDescriptor<?> module, final boolean persistent) {
        super(module, persistent);
    }
}
