package com.atlassian.plugin.event.events;

import com.atlassian.annotations.PublicApi;
import com.atlassian.plugin.Plugin;

/**
 * Event fired before a plugin is upgraded at runtime.
 * <p>
 * The plugin of this event is the old plugin, and it is fired before the old plugin is removed from the system.
 *
 * @see com.atlassian.plugin.event.events
 * @since 4.0.0
 */
@PublicApi
public class PluginUpgradingEvent extends PluginEvent {
    public PluginUpgradingEvent(final Plugin plugin) {
        super(plugin);
    }
}
