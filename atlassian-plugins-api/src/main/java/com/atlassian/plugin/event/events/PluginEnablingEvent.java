package com.atlassian.plugin.event.events;

import com.atlassian.annotations.PublicApi;
import com.atlassian.plugin.Plugin;

/**
 * Event fired before a Plugin is enabled.
 *
 * @see com.atlassian.plugin.event.events
 * @since 4.0.0
 */
@PublicApi
public class PluginEnablingEvent extends PluginEvent {
    public PluginEnablingEvent(final Plugin plugin) {
        super(plugin);
    }
}
