package com.atlassian.plugin.event.events;

import com.atlassian.annotations.PublicApi;
import com.atlassian.plugin.Plugin;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Base class for events with Plugin context.
 *
 * @see com.atlassian.plugin.event.events
 * @since 4.0.0
 */
@PublicApi
public class PluginEvent {
    private final Plugin plugin;

    public PluginEvent(final Plugin plugin) {
        this.plugin = checkNotNull(plugin);
    }

    public Plugin getPlugin() {
        return plugin;
    }

    @Override
    public String toString() {
        return getClass().getName() + " for " + plugin;
    }
}
