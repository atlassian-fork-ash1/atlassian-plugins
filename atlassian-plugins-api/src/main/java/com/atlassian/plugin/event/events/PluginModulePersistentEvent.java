package com.atlassian.plugin.event.events;

import com.atlassian.annotations.PublicApi;
import com.atlassian.plugin.ModuleDescriptor;

/**
 * Base class for events with persistent flag in addition to ModuleDescriptor context.
 *
 * @see com.atlassian.plugin.event.events
 * @since 4.0.0
 */
@PublicApi
public class PluginModulePersistentEvent extends PluginModuleEvent {
    private final boolean persistent;

    public PluginModulePersistentEvent(final ModuleDescriptor<?> module, final boolean persistent) {
        super(module);
        this.persistent = persistent;
    }

    /**
     * @return <code>true</code> if this disabling will be persistent, i.e. it is not a transient, such as for an
     * upgrade.
     * @since 2.8.0 (originally in PluginModuleDisabledEvent); moved here in 4.0.0
     */
    public boolean isPersistent() {
        return persistent;
    }
}
