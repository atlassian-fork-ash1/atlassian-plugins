package com.atlassian.plugin.event.events;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Event for when the container of a plugin is been shutdown, usually as a result of the OSGi bundle being stopped.
 *
 * @see com.atlassian.plugin.event.events
 * @since 2.5.0
 */
public class PluginContainerUnavailableEvent {
    private final String key;

    public PluginContainerUnavailableEvent(final String key) {
        this.key = checkNotNull(key, "The plugin key must be available");
    }

    public String getPluginKey() {
        return key;
    }
}
