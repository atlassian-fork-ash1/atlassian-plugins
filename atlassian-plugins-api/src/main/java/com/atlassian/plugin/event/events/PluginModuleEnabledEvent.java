package com.atlassian.plugin.event.events;

import com.atlassian.annotations.PublicApi;
import com.atlassian.plugin.ModuleDescriptor;

/**
 * Event fired when a plugin module is enabled, which can also happen when its
 * plugin is enabled or installed.
 *
 * @see com.atlassian.plugin.event.events
 * @since 2.2.0
 */
@PublicApi
public class PluginModuleEnabledEvent extends PluginModuleEvent {
    public PluginModuleEnabledEvent(final ModuleDescriptor<?> module) {
        super(module);
    }
}
