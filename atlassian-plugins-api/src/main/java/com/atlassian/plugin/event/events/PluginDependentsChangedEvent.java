package com.atlassian.plugin.event.events;

import com.atlassian.annotations.PublicApi;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginState;

import javax.annotation.Nonnull;
import java.util.List;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Event fired after dependent plugins have changed their state in response to a change in this plugin's state i.e.
 * install, enable, uninstall, disable.
 *
 * @see com.atlassian.plugin.event.events
 * @since 4.0.0
 */
@PublicApi
public class PluginDependentsChangedEvent extends PluginEvent {
    final PluginState state;
    final List<Plugin> disabled;
    final List<Plugin> cycled;

    public PluginDependentsChangedEvent(final Plugin plugin, @Nonnull final PluginState state, @Nonnull final List<Plugin> disabled, @Nonnull final List<Plugin> cycled) {
        super(plugin);
        this.state = checkNotNull(state);
        checkArgument(state == PluginState.INSTALLED || state == PluginState.ENABLED || state == PluginState.UNINSTALLED || state == PluginState.DISABLED, "state must be one of INSTALLED, ENABLED, UNINSTALLED, DISABLED");
        this.disabled = checkNotNull(disabled);
        this.cycled = checkNotNull(cycled);
    }

    /**
     * End state of plugin that caused this event.
     *
     * @return one of {@link PluginState#INSTALLED}, {@link PluginState#ENABLED}, {@link PluginState#UNINSTALLED}, {@link PluginState#DISABLED}
     */
    public PluginState getState() {
        return state;
    }

    /**
     * Plugins which had their state changed from enabled to disabled
     *
     * @return possibly empty list
     */
    public List<Plugin> getDisabled() {
        return disabled;
    }

    /**
     * Plugins which had their state changed from enabled to disabled to enabled
     *
     * @return possibly empty list
     */
    public List<Plugin> getCycled() {
        return cycled;
    }

    @Override
    public String toString() {
        return super.toString() + ", disabled=" + disabled + ", cycled=" + cycled;
    }
}
