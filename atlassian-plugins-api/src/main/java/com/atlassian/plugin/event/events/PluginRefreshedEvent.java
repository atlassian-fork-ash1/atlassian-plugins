package com.atlassian.plugin.event.events;

import com.atlassian.annotations.PublicApi;
import com.atlassian.plugin.Plugin;

/**
 * Event fired when the plugin has been refreshed with no user interaction.
 *
 * @see com.atlassian.plugin.event.events
 * @since 2.2.0
 */
@PublicApi
public class PluginRefreshedEvent extends PluginEvent {
    public PluginRefreshedEvent(final Plugin plugin) {
        super(plugin);
    }
}
