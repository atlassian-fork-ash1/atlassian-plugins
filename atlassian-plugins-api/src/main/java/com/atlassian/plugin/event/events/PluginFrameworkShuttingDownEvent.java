package com.atlassian.plugin.event.events;

import com.atlassian.annotations.PublicApi;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginController;

/**
 * Event that signifies the plugin framework is about to be shutdown.
 *
 * @see com.atlassian.plugin.event.events
 * @since 3.2.11
 */
@PublicApi
public class PluginFrameworkShuttingDownEvent extends PluginFrameworkEvent {
    public PluginFrameworkShuttingDownEvent(final PluginController pluginController, final PluginAccessor pluginAccessor) {
        super(pluginController, pluginAccessor);
    }
}
