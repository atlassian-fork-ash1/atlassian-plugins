package com.atlassian.plugin;

import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Multimap;

import java.util.Set;

import static com.atlassian.plugin.PluginDependencies.Type.DYNAMIC;
import static com.atlassian.plugin.PluginDependencies.Type.MANDATORY;
import static com.atlassian.plugin.PluginDependencies.Type.OPTIONAL;

/**
 * All plugin keys which are dependents of another plugin, divided into OSGi style import resolutions: mandatory
 * (default), optional and dynamic.
 *
 * @see Plugin#getDependencies()
 * @since 4.0
 */
public class PluginDependencies {
    // dependency types in the order of significance
    public enum Type {
        MANDATORY,
        OPTIONAL,
        DYNAMIC;

        public boolean lessSignificant(final Type other) {
            return ordinal() > other.ordinal();
        }
    }

    // Plugin keys of mandatory, optional and dynamic dependencies
    // keys can appear in multiple sets
    private final Set<String> mandatory;
    private final Set<String> optional;
    private final Set<String> dynamic;
    /**
     * Plugin keys of all dependencies
     */
    private final Set<String> all;
    /**
     * A list of dependency types by plugin key
     */
    private final Multimap<String, Type> byPluginKey;

    public PluginDependencies() {
        this(null, null, null);
    }

    public PluginDependencies(final Set<String> mandatory, final Set<String> optional, final Set<String> dynamic) {
        this.mandatory = mandatory == null ? ImmutableSet.of() : ImmutableSet.copyOf(mandatory);
        this.optional = optional == null ? ImmutableSet.of() : ImmutableSet.copyOf(optional);
        this.dynamic = dynamic == null ? ImmutableSet.of() : ImmutableSet.copyOf(dynamic);

        this.all = ImmutableSet.<String>builder()
                .addAll(this.mandatory)
                .addAll(this.optional)
                .addAll(this.dynamic)
                .build();

        final ImmutableListMultimap.Builder<String, Type> byPluginKey = ImmutableListMultimap.builder();

        for (final String key : this.mandatory) {
            byPluginKey.put(key, MANDATORY);
        }
        for (final String key : this.optional) {
            byPluginKey.put(key, OPTIONAL);
        }
        for (final String key : this.dynamic) {
            byPluginKey.put(key, DYNAMIC);
        }

        this.byPluginKey = byPluginKey.build();
    }

    public Set<String> getMandatory() {
        return mandatory;
    }

    public Set<String> getOptional() {
        return optional;
    }

    public Set<String> getDynamic() {
        return dynamic;
    }

    public Set<String> getAll() {
        return all;
    }

    public Multimap<String, Type> getByPluginKey() {
        return byPluginKey;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        final ImmutableSet.Builder<String> mandatory = new ImmutableSet.Builder<>();
        final ImmutableSet.Builder<String> optional = new ImmutableSet.Builder<>();
        final ImmutableSet.Builder<String> dynamic = new ImmutableSet.Builder<>();

        private Builder() {
        }

        public Builder withMandatory(String... pluginKey) {
            mandatory.add(pluginKey);
            return this;
        }

        public Builder withOptional(String... pluginKey) {
            optional.add(pluginKey);
            return this;
        }

        public Builder withDynamic(String... pluginKey) {
            dynamic.add(pluginKey);
            return this;
        }

        public PluginDependencies build() {
            return new PluginDependencies(mandatory.build(), optional.build(), dynamic.build());
        }
    }

}
