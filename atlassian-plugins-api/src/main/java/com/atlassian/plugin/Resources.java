package com.atlassian.plugin;

import com.atlassian.plugin.elements.ResourceDescriptor;
import com.atlassian.plugin.elements.ResourceLocation;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.dom4j.Element;

import javax.annotation.Nullable;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.atlassian.plugin.util.Assertions.notNull;
import static com.google.common.collect.Iterables.filter;

/**
 * An aggregate of all resource descriptors within the given plugin module or
 * plugin.
 *
 * @see com.atlassian.plugin.impl.AbstractPlugin#resources
 * @see com.atlassian.plugin.descriptors.AbstractModuleDescriptor#resources
 */
public class Resources implements Resourced {
    public static final Resources EMPTY_RESOURCES = new Resources((Element) null);

    private final List<ResourceDescriptor> resourceDescriptors;

    /**
     * Parses the resource descriptors from the provided plugin XML element and
     * creates a Resources object containing them.
     * <p>
     * If the module or plugin contains no resource elements, an empty Resources
     * object will be returned. This method will not return null.
     *
     * @param element the plugin or plugin module XML fragment which should not
     *                be null
     * @return a Resources object representing the resources in the plugin or
     * plugin module
     * @throws PluginParseException     if there are two resources with the same
     *                                  name and type in this element, or another parse error occurs
     * @throws IllegalArgumentException if the provided element is null
     */
    public static Resources fromXml(final Element element) throws PluginParseException, IllegalArgumentException {
        if (element == null) {
            throw new IllegalArgumentException("Cannot parse resources from null XML element");
        }

        @SuppressWarnings("unchecked")
        final List<Element> elements = element.elements("resource");

        final Set<ResourceDescriptor> templates = new HashSet<>();

        for (final Element e : elements) {
            final ResourceDescriptor resourceDescriptor = new ResourceDescriptor(e);

            if (templates.contains(resourceDescriptor)) {
                throw new PluginParseException("Duplicate resource with type '" + resourceDescriptor.getType() + "' and name '" + resourceDescriptor.getName() + "' found");
            }

            templates.add(resourceDescriptor);
        }

        return new Resources(element);
    }

    /**
     * Private constructor to create a Resources object from XML. Entry via fromXml.
     *
     * @param element
     */
    private Resources(final Element element) {
        if (element != null) {
            this.resourceDescriptors = Lists.newArrayList(Iterables.transform(element.elements("resource"), new Function<Element, ResourceDescriptor>() {

                @Override
                public ResourceDescriptor apply(@Nullable final Element e) {
                    return new ResourceDescriptor(e);
                }
            }));
        } else {
            this.resourceDescriptors = Collections.emptyList();
        }
    }

    public List<ResourceDescriptor> getResourceDescriptors() {
        return resourceDescriptors;
    }

    public ResourceLocation getResourceLocation(final String type, final String name) {
        for (final ResourceDescriptor resourceDescriptor : getResourceDescriptors()) {
            if (resourceDescriptor.doesTypeAndNameMatch(type, name)) {
                return resourceDescriptor.getResourceLocationForName(name);
            }
        }
        return null;
    }

    public ResourceDescriptor getResourceDescriptor(final String type, final String name) {
        for (final ResourceDescriptor resourceDescriptor : getResourceDescriptors()) {
            if (resourceDescriptor.getType().equalsIgnoreCase(type) && resourceDescriptor.getName().equalsIgnoreCase(name)) {
                return resourceDescriptor;
            }
        }
        return null;
    }

    public static class TypeFilter implements Predicate<ResourceDescriptor> {
        private final String type;

        public TypeFilter(final String type) {
            this.type = notNull("type", type);
        }

        public boolean apply(final ResourceDescriptor input) {
            // TODO Auto-generated method stub
            return type.equals(input.getType());
        }
    }
}
