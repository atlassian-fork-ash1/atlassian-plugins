package com.atlassian.plugin;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestModuleDescriptor {

    private static final String MODULE_KEY = "theKey";
    private static final String MODULE_NAME = "theName";

    private ModuleDescriptor moduleDescriptor;

    @Before
    public void setUp() throws Exception {
        moduleDescriptor = mock(ForwardDefaultMethodsModuleDescriptor.class);
        doCallRealMethod().when(moduleDescriptor).getDisplayName();
        doCallRealMethod().when(moduleDescriptor).setBroken();
        doCallRealMethod().when(moduleDescriptor).isBroken();
    }

    @Test
    public void notBrokenByDefault() {
        assertThat(moduleDescriptor.isBroken(), is(false));
    }

    @Test
    public void setBrokenIsNoop() {
        moduleDescriptor.setBroken();
        assertThat(moduleDescriptor.isBroken(), is(false));
    }

    @Test
    public void getDisplayName_whenNameIsSet_shouldReturnThatName() {
        // Set up
        when(moduleDescriptor.getName()).thenReturn(MODULE_NAME);

        // Invoke
        final String displayName = moduleDescriptor.getDisplayName();

        // Check
        assertThat(displayName, is(MODULE_NAME));
    }

    @Test
    public void getDisplayName_whenNameIsNotSet_shouldReturnTheKey() {
        // Set up
        when(moduleDescriptor.getName()).thenReturn(null);
        when(moduleDescriptor.getKey()).thenReturn(MODULE_KEY);

        // Invoke
        final String displayName = moduleDescriptor.getDisplayName();

        // Check
        assertThat(displayName, is(MODULE_KEY));
    }

    static abstract class ForwardDefaultMethodsModuleDescriptor implements ModuleDescriptor {
        @Override
        public void setBroken() {
            ModuleDescriptor.super.setBroken();
        }

        @Override
        public boolean isBroken() {
            return ModuleDescriptor.super.isBroken();
        }
    }
}
