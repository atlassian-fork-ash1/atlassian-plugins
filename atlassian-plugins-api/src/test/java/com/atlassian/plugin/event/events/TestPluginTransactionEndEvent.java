package com.atlassian.plugin.event.events;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.elements.ResourceDescriptor;
import com.atlassian.plugin.elements.ResourceLocation;
import com.google.common.collect.ImmutableList;
import org.dom4j.Element;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Map;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

public class TestPluginTransactionEndEvent {

    private PluginTransactionEndEvent pluginTransactionStopEvent;
    private Plugin plugin1Mock;
    private Plugin plugin2Mock;
    private ModuleDescriptor moduleDescriptorMock1;

    @Before
    public void setUp() throws Exception {
        plugin1Mock = mock(Plugin.class);
        plugin2Mock = mock(Plugin.class);
        moduleDescriptorMock1 = mock(FooModuleDescriptor.class);
        pluginTransactionStopEvent = new PluginTransactionEndEvent(ImmutableList.of(
                new PluginDisablingEvent(plugin1Mock),
                new PluginModuleDisablingEvent(moduleDescriptorMock1, false),
                new PluginModuleDisabledEvent(moduleDescriptorMock1, false),
                new PluginDisabledEvent(plugin1Mock)));
    }

    @Test
    public void numberOfEvents() {
        assertThat(pluginTransactionStopEvent.numberOfEvents(), equalTo(4));
    }

    @Test
    public void hasAnyEventOfTypeMatching() {
        assertThat(pluginTransactionStopEvent.hasAnyEventOfTypeMatching(PluginDisablingEvent.class, pluginDisablingEvent -> pluginDisablingEvent.getPlugin().equals(plugin1Mock)),
                equalTo(true));
        assertThat(pluginTransactionStopEvent.hasAnyEventOfTypeMatching(PluginDisablingEvent.class, pluginDisablingEvent -> pluginDisablingEvent.getPlugin().equals(plugin2Mock)),
                equalTo(false));
        assertThat(pluginTransactionStopEvent.hasAnyEventOfTypeMatching(PluginEnablingEvent.class, pluginDisablingEvent -> pluginDisablingEvent.getPlugin().equals(plugin1Mock)),
                equalTo(false));
    }

    @Test
    public void hasAnyEventWithModuleDescriptorMatching() {
        assertThat(pluginTransactionStopEvent.hasAnyEventWithModuleDescriptorMatching(FooModuleDescriptor.class::isInstance),
                equalTo(true));
        assertThat(pluginTransactionStopEvent.hasAnyEventWithModuleDescriptorMatching(SomeModuleDescriptor.class::isInstance),
                equalTo(true));
        assertThat(pluginTransactionStopEvent.hasAnyEventWithModuleDescriptorMatching(BarModuleDescriptor.class::isInstance),
                equalTo(false));
    }

    private static class SomeModuleDescriptor implements ModuleDescriptor {

        @Override
        public String getCompleteKey() {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public String getPluginKey() {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public String getKey() {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public String getName() {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public String getDescription() {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public Class getModuleClass() {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public Object getModule() {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public void init(@Nonnull Plugin plugin, @Nonnull Element element) throws PluginParseException {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public boolean isEnabledByDefault() {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public boolean isSystemModule() {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public void destroy() {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public Float getMinJavaVersion() {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public boolean satisfiesMinJavaVersion() {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public Map<String, String> getParams() {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public String getI18nNameKey() {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public String getDescriptionKey() {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public Plugin getPlugin() {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public boolean isEnabled() {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public List<ResourceDescriptor> getResourceDescriptors() {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public ResourceDescriptor getResourceDescriptor(String type, String name) {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public ResourceLocation getResourceLocation(String type, String name) {
            throw new UnsupportedOperationException("Not implemented");
        }
    }

    private static class FooModuleDescriptor extends SomeModuleDescriptor {
    }

    private static class BarModuleDescriptor extends SomeModuleDescriptor {
    }
}