package com.atlassian.plugin.event.events;

import com.atlassian.plugin.Plugin;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@RunWith(MockitoJUnitRunner.class)
public class TestPluginEvents {
    @Mock
    private Plugin plugin;

    @Test
    public void pluginDisablingEventGetters() {
        final PluginDisablingEvent pluginDisablingEvent = new PluginDisablingEvent(plugin);
        assertThat(pluginDisablingEvent.getPlugin(), is(plugin));
    }

    @Test
    public void pluginDisabledEventGetters() {
        final PluginDisabledEvent pluginDisabledEvent = new PluginDisabledEvent(plugin);
        assertThat(pluginDisabledEvent.getPlugin(), is(plugin));
    }

    @Test
    public void pluginEnablingEventGetters() {
        final PluginEnablingEvent pluginEnablingEvent = new PluginEnablingEvent(plugin);
        assertThat(pluginEnablingEvent.getPlugin(), is(plugin));
    }

    @Test
    public void pluginEnabledEventGetters() {
        final PluginEnabledEvent pluginEnabledEvent = new PluginEnabledEvent(plugin);
        assertThat(pluginEnabledEvent.getPlugin(), is(plugin));
    }

    @Test
    public void pluginInstallingEventGetters() {
        final PluginInstallingEvent pluginInstallingEvent = new PluginInstallingEvent(plugin);
        assertThat(pluginInstallingEvent.getPlugin(), is(plugin));
    }

    @Test
    public void pluginInstalledEventGetters() {
        final PluginInstalledEvent pluginInstalledEvent = new PluginInstalledEvent(plugin);
        assertThat(pluginInstalledEvent.getPlugin(), is(plugin));
    }

    @Test
    public void pluginRefreshedEventGetters() {
        final PluginRefreshedEvent pluginRefreshedEvent = new PluginRefreshedEvent(plugin);
        assertThat(pluginRefreshedEvent.getPlugin(), is(plugin));
    }

    @Test
    public void pluginUninstallingEventGetters() {
        final PluginUninstallingEvent pluginUninstallingEvent = new PluginUninstallingEvent(plugin);
        assertThat(pluginUninstallingEvent.getPlugin(), is(plugin));
    }

    @Test
    public void pluginUninstalledEventGetters() {
        final PluginUninstalledEvent pluginUninstalledEvent = new PluginUninstalledEvent(plugin);
        assertThat(pluginUninstalledEvent.getPlugin(), is(plugin));
    }

    @Test
    public void pluginUpgradingEventGetters() {
        final PluginUpgradingEvent pluginUpgradingEvent = new PluginUpgradingEvent(plugin);
        assertThat(pluginUpgradingEvent.getPlugin(), is(plugin));
    }

    @Test
    public void pluginUpgradedEventGetters() {
        final PluginUpgradedEvent pluginUpgradedEvent = new PluginUpgradedEvent(plugin);
        assertThat(pluginUpgradedEvent.getPlugin(), is(plugin));
    }
}
