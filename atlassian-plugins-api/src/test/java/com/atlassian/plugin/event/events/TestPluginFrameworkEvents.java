package com.atlassian.plugin.event.events;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@RunWith(MockitoJUnitRunner.class)
public class TestPluginFrameworkEvents {
    @Mock
    private PluginController pluginController;
    @Mock
    private PluginAccessor pluginAccessor;

    @Test
    public void pluginFrameworkDelayedEventGetters() {
        final PluginFrameworkDelayedEvent pluginFrameworkDelayedEvent =
                new PluginFrameworkDelayedEvent(pluginController, pluginAccessor);
        assertThat(pluginFrameworkDelayedEvent.getPluginAccessor(), is(pluginAccessor));
        assertThat(pluginFrameworkDelayedEvent.getPluginController(), is(pluginController));
    }

    @Test
    public void pluginFrameworkResumingEventGetters() {
        final PluginFrameworkResumingEvent pluginFrameworkResumingEvent =
                new PluginFrameworkResumingEvent(pluginController, pluginAccessor);
        assertThat(pluginFrameworkResumingEvent.getPluginAccessor(), is(pluginAccessor));
        assertThat(pluginFrameworkResumingEvent.getPluginController(), is(pluginController));
    }

    @Test
    public void pluginFrameworkShutdownEventGetters() {
        final PluginFrameworkShutdownEvent pluginFrameworkShutdownEvent =
                new PluginFrameworkShutdownEvent(pluginController, pluginAccessor);
        assertThat(pluginFrameworkShutdownEvent.getPluginAccessor(), is(pluginAccessor));
        assertThat(pluginFrameworkShutdownEvent.getPluginController(), is(pluginController));
    }

    @Test
    public void pluginFrameworkShuttingDownEventGetters() {
        final PluginFrameworkShuttingDownEvent pluginFrameworkShuttingDownEvent =
                new PluginFrameworkShuttingDownEvent(pluginController, pluginAccessor);
        assertThat(pluginFrameworkShuttingDownEvent.getPluginAccessor(), is(pluginAccessor));
        assertThat(pluginFrameworkShuttingDownEvent.getPluginController(), is(pluginController));
    }

    @Test
    public void pluginFrameworkStartedEventGetters() {
        final PluginFrameworkStartedEvent pluginFrameworkStartedEvent =
                new PluginFrameworkStartedEvent(pluginController, pluginAccessor);
        assertThat(pluginFrameworkStartedEvent.getPluginAccessor(), is(pluginAccessor));
        assertThat(pluginFrameworkStartedEvent.getPluginController(), is(pluginController));
    }

    @Test
    public void pluginFrameworkStartingEventGetters() {
        final PluginFrameworkStartingEvent pluginFrameworkStartingEvent =
                new PluginFrameworkStartingEvent(pluginController, pluginAccessor);
        assertThat(pluginFrameworkStartingEvent.getPluginAccessor(), is(pluginAccessor));
        assertThat(pluginFrameworkStartingEvent.getPluginController(), is(pluginController));
    }

    @Test
    public void pluginFrameworkWarmRestartedEventGetters() {
        final PluginFrameworkWarmRestartedEvent pluginFrameworkWarmRestartedEvent =
                new PluginFrameworkWarmRestartedEvent(pluginController, pluginAccessor);
        assertThat(pluginFrameworkWarmRestartedEvent.getPluginAccessor(), is(pluginAccessor));
        assertThat(pluginFrameworkWarmRestartedEvent.getPluginController(), is(pluginController));
    }

    @Test
    public void pluginFrameworkWarmRestartingEventGetters() {
        final PluginFrameworkWarmRestartingEvent pluginFrameworkWarmRestartingEvent =
                new PluginFrameworkWarmRestartingEvent(pluginController, pluginAccessor);
        assertThat(pluginFrameworkWarmRestartingEvent.getPluginAccessor(), is(pluginAccessor));
        assertThat(pluginFrameworkWarmRestartingEvent.getPluginController(), is(pluginController));
    }
}
