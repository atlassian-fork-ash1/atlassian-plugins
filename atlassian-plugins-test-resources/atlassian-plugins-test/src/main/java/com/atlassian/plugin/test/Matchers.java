package com.atlassian.plugin.test;

import org.dom4j.Element;
import org.dom4j.util.NodeComparator;
import org.hamcrest.Description;
import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import java.io.File;
import java.net.URI;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.Arrays.spliterator;
import static java.util.stream.StreamSupport.stream;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.core.Is.is;

public class Matchers {
    /**
     * Obtain a matcher for {@link File} instances with given name.
     *
     * @param name the expected name of the file.
     * @return a matcher which matches files whose {@link File#getName} is {@link org.hamcrest.Matchers#equalTo} name.
     */
    public static Matcher<File> fileNamed(final String name) {
        return new TypeSafeMatcher<File>() {
            private final Matcher<String> nameMatcher = equalTo(name);

            @Override
            protected boolean matchesSafely(final File item) {
                return nameMatcher.matches(item.getName());
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("File named ");
                nameMatcher.describeTo(description);
            }
        };
    }

    /**
     * Obtain a matcher for {@link URI} instances with given path.
     *
     * @param path the expected path of the URI.
     * @return a matcher which matches URIs whose {@link URI#getPath} is {@link org.hamcrest.Matchers#equalTo} path.
     */
    public static Matcher<URI> uriWithPath(final String path) {
        return new TypeSafeMatcher<URI>() {
            private final Matcher<String> pathMatcher = equalTo(path);

            @Override
            protected boolean matchesSafely(final URI item) {
                File f = new File(item);
                return pathMatcher.matches(f.getPath());
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("URI with path ");
                pathMatcher.describeTo(description);
            }
        };
    }

    @SuppressWarnings("unchecked")
    static Matcher<String> containsAllStrings(final String... substrings) {
        Matcher<String>[] matchers = stream(spliterator(substrings), false)
                .map(org.hamcrest.Matchers::containsString)
                .toArray(Matcher[]::new);
        return allOf(matchers);
    }

    /**
     * Obtain an equality matcher for {@link org.dom4j.Element}.
     * <p>
     * Uses {@link org.dom4j.util.NodeComparator} as Element doesn't provide equals or compareTo.
     */
    public static Matcher<Element> isElement(final Element expected) {
        return new TypeSafeMatcher<Element>() {
            @Override
            protected boolean matchesSafely(final Element item) {
                return new NodeComparator().compare(expected, item) == 0;
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("is ").appendText(expected.toString());
            }
        };
    }

    /**
     * Matcher for an {@link Optional}, matching on present.
     */
    public static <T> Matcher<Optional<T>> isPresent() {
        return new FeatureMatcher<Optional<T>, Boolean>(is(true), "an Optional whose isPresent", "isPresent") {
            @Override
            protected Boolean featureValueOf(final Optional<T> optional) {
                return optional.isPresent();
            }
        };
    }

    /**
     * Matcher for an {@link Optional}, matching on not present.
     */
    public static <T> Matcher<Optional<T>> notPresent() {
        return new FeatureMatcher<Optional<T>, Boolean>(is(false), "an Optional whose isPresent", "isPresent") {
            @Override
            protected Boolean featureValueOf(final Optional<T> optional) {
                return optional.isPresent();
            }
        };
    }

    /**
     * Matcher for an {@link Optional}, matching when present and the value matching the input.
     */
    public static <T> Matcher<? super Optional<T>> presentWithValue(final Matcher<T> valueMatcher) {
        return new TypeSafeMatcher<Optional<T>>() {
            @Override
            protected boolean matchesSafely(final Optional<T> optional) {
                return optional.isPresent() && valueMatcher.matches(optional.get());
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("a present Optional whose value ");
                description.appendDescriptionOf(valueMatcher);
            }
        };
    }
}
