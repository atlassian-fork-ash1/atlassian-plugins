package com.atlassian.plugin.osgi.spring.extender;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Properties;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@RunWith(SpringRunner.class)
@ContextConfiguration("classpath:/META-INF/spring/extender/extender-configuration.xml")
public class TestSpringExtenderContext {

    @Autowired
    @Qualifier("extenderProperties")
    private Properties extenderProperties;

    @Test
    public void testDefaultShutdownWaitTime() {
        assertThat(extenderProperties.getProperty("shutdown.wait.time"), equalTo("500"));
    }
}
