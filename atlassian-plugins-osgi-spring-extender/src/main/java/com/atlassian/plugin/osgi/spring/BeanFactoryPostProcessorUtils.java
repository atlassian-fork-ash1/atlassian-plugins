package com.atlassian.plugin.osgi.spring;

import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.RootBeanDefinition;

import java.util.function.Supplier;

final class BeanFactoryPostProcessorUtils {
    private BeanFactoryPostProcessorUtils() {
    }

    static <T extends BeanPostProcessor> void registerPostProcessor(
            final ConfigurableListableBeanFactory beanFactory,
            final String beanName,
            final Class<T> beanClass,
            final Supplier<T> postProcessorSupplier) {
        
        if (beanFactory instanceof BeanDefinitionRegistry) {
            final BeanDefinitionRegistry registry = (BeanDefinitionRegistry) beanFactory;
            // register the annotation processor using the name AnnotationConfigUtils uses internally. This ensures that
            // only one instance of this bean is registered
            if (!registry.containsBeanDefinition(beanName)) {
                final RootBeanDefinition def = new RootBeanDefinition(beanClass);
                def.setRole(BeanDefinition.ROLE_INFRASTRUCTURE);
                registry.registerBeanDefinition(beanName, def);
            }
        } else {
            final BeanPostProcessor postProcessor = postProcessorSupplier.get();
            if (postProcessor instanceof BeanFactoryAware) {
                ((BeanFactoryAware)postProcessor).setBeanFactory(beanFactory);
            }
            beanFactory.addBeanPostProcessor(postProcessor);
        }
    }
}
