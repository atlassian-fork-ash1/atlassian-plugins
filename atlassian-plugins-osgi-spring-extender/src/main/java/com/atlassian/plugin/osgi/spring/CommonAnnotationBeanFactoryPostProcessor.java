package com.atlassian.plugin.osgi.spring;

import org.eclipse.gemini.blueprint.extender.OsgiBeanFactoryPostProcessor;
import org.osgi.framework.BundleContext;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.context.annotation.CommonAnnotationBeanPostProcessor;

import java.util.function.Supplier;

import static com.atlassian.plugin.osgi.spring.BeanFactoryPostProcessorUtils.registerPostProcessor;
import static org.springframework.context.annotation.AnnotationConfigUtils.COMMON_ANNOTATION_PROCESSOR_BEAN_NAME;

public class CommonAnnotationBeanFactoryPostProcessor implements OsgiBeanFactoryPostProcessor {
    @Override
    public void postProcessBeanFactory(final BundleContext bundleContext, final ConfigurableListableBeanFactory beanFactory) throws BeansException {
        registerPostProcessor(beanFactory,
                              COMMON_ANNOTATION_PROCESSOR_BEAN_NAME,
                              CommonAnnotationBeanPostProcessor.class,
                              CommonAnnotationBeanPostProcessor::new);
    }
}
