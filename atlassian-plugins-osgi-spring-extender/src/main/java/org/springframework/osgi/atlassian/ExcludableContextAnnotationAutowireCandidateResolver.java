package org.springframework.osgi.atlassian;

import org.springframework.beans.factory.config.BeanDefinitionHolder;
import org.springframework.beans.factory.config.DependencyDescriptor;
import org.springframework.context.annotation.ContextAnnotationAutowireCandidateResolver;

import java.util.HashSet;
import java.util.Set;

/**
 * An autowire candidate resolver that excludes unwanted beans to be autowired.
 * In Spring 4.3, some internal beans (e.g. importRegistry which is of type Collection)
 * may be autowired when there are matched property types.
 *
 * @since 4.6.0
 */
public class ExcludableContextAnnotationAutowireCandidateResolver extends ContextAnnotationAutowireCandidateResolver {

    private Set<String> excludedBeanNames = new HashSet<>();

    public ExcludableContextAnnotationAutowireCandidateResolver() {
        //importRegistry bean is created by ConfigurationClassPostProcessor to support injection for ImportAware classes.
        //It will be autowired to actionMessages and actionErrors properties of xwork actions that implement ValidationAware interface
        //as they share the same Collection interface. It is likely to affect many plugins.
        excludedBeanNames.add("org.springframework.context.annotation.ConfigurationClassPostProcessor.importRegistry");
    }

    public boolean isAutowireCandidate(BeanDefinitionHolder bdHolder, DependencyDescriptor descriptor) {
        if (excludedBeanNames.contains(bdHolder.getBeanName())) {
            return false;
        }

        return super.isAutowireCandidate(bdHolder, descriptor);
    }
}
