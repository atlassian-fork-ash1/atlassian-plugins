package com.atlassian.plugin.main;

import com.atlassian.plugin.osgi.container.PackageScannerConfiguration;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import static com.atlassian.plugin.main.PackageScannerConfigurationBuilder.packageScannerConfiguration;
import static com.atlassian.plugin.main.PluginsConfigurationBuilder.pluginsConfiguration;

/**
 * Simple standalone class for starting the plugin framework. Creates a directory called "plugins" in the current directory
 * and scans it every 5 seconds for new plugins.
 * <p>
 * For embedded use, use the {@link AtlassianPlugins} facade directly
 */
public class Main {
    public static void main(final String[] args) {
        initialiseLogger();
        final File pluginDir = new File("plugins");

        if (pluginDir.mkdir()) {
            System.out.println("Created plugins directory " + pluginDir.getAbsolutePath());
        } else if (pluginDir.isDirectory()) {
            System.out.println("Using existing plugins directory " + pluginDir.getAbsolutePath());
        } else {
            System.out.println("Cannot create plugins directory " + pluginDir.getAbsolutePath());
        }

        final PackageScannerConfiguration packageScannerConfiguration = packageScannerConfiguration()
                .packagesToInclude("org.apache.*", "com.atlassian.*", "org.dom4j*")
                .packagesVersions(Collections.singletonMap("org.apache.log4j", "1.2.16"))
                .build();
        final PluginsConfiguration config = pluginsConfiguration()
                .pluginDirectory(pluginDir)
                .packageScannerConfiguration(packageScannerConfiguration)
                .hotDeployPollingFrequency(5, TimeUnit.SECONDS)
                .build();
        final AtlassianPlugins plugins = new AtlassianPlugins(config);

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            System.out.println("Cleaning up...");
            plugins.getPluginSystemLifecycle().shutdown();
            plugins.destroy();
        }));

        plugins.afterPropertiesSet();
        plugins.getPluginSystemLifecycle().init();

        // Keep the vm alive - kill it with a SIGHUP to test shutdown
        try {
            final Object waitable = new Object();
            // We're synchronizing just so we can wait() to block the thread, so the fact it's a local is moot.
            //noinspection SynchronizationOnLocalVariableOrMethodParameter
            synchronized (waitable) {
                waitable.wait();
            }
        } catch (final InterruptedException e) {
            // ignore, but now the wait is over
        }
    }

    private static void initialiseLogger() {
        final Properties logProperties = new Properties();

        try (InputStream in = Main.class.getResourceAsStream("/log4j-standalone.properties")) {
            logProperties.load(in);
            PropertyConfigurator.configure(logProperties);
            Logger.getLogger(Main.class).info("Logging initialized.");
        } catch (final IOException e) {
            throw new RuntimeException("Unable to load logging");
        }
    }
}
