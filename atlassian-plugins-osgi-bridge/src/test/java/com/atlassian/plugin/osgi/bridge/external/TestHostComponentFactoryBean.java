package com.atlassian.plugin.osgi.bridge.external;

import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceListener;
import org.osgi.framework.ServiceReference;

import java.util.concurrent.Callable;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestHostComponentFactoryBean {

    @SuppressWarnings("unchecked")
    @Test
    public void testGetService() throws Exception {
        BundleContext ctx = mock(BundleContext.class);
        ServiceReference<Callable<String>> ref = mock(ServiceReference.class);
        when(ctx.getServiceReferences((String) null, "(foo=bar)")).thenReturn(new ServiceReference<?>[]{ref});
        when(ctx.getService(ref)).thenReturn(() -> "foo");

        ArgumentCaptor<ServiceListener> serviceListener = ArgumentCaptor.forClass(ServiceListener.class);

        HostComponentFactoryBean bean = new HostComponentFactoryBean();
        bean.setBundleContext(ctx);
        bean.setFilter("(foo=bar)");
        bean.setInterfaces(new Class[]{Callable.class});
        bean.afterPropertiesSet();

        assertEquals("foo", ((Callable) bean.getObject()).call());

        verify(ctx).addServiceListener(serviceListener.capture(), anyString());
        ServiceReference<Callable<String>> updatedRef = mock(ServiceReference.class);
        when(ctx.getService(updatedRef)).thenReturn(() -> "boo");
        serviceListener.getValue().serviceChanged(new ServiceEvent(ServiceEvent.REGISTERED, updatedRef));

        assertEquals("boo", ((Callable) bean.getObject()).call());
    }
}
