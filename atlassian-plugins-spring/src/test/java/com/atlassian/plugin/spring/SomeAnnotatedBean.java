package com.atlassian.plugin.spring;

import java.io.Serializable;
import java.util.HashMap;

@AvailableToPlugins(interfaces = {Fooable.class, Serializable.class})
public class SomeAnnotatedBean extends HashMap implements Fooable, Serializable {
    public void sayHi() {
        System.out.println("hi");
    }
}
