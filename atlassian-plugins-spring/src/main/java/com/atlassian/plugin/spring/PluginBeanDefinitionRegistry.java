package com.atlassian.plugin.spring;

import com.atlassian.plugin.osgi.hostcomponents.ContextClassLoaderStrategy;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.atlassian.plugin.util.Assertions.notNull;

public class PluginBeanDefinitionRegistry {
    public static final String HOST_COMPONENT_PROVIDER = "hostComponentProvider";

    private static final String BEAN_NAMES = "beanNames";
    private static final String BEAN_INTERFACES = "beanInterfaces";
    private static final String BEAN_CONTEXT_CLASS_LOADER_STRATEGIES = "beanContextClassLoaderStrategies";
    private static final String BUNDLE_TRACKING_BEANS = "bundleTrackingBeans";

    private final BeanDefinitionRegistry registry;

    public PluginBeanDefinitionRegistry(BeanDefinitionRegistry registry) {
        this.registry = notNull("registry", registry);
    }

    public BeanDefinition getBeanDefinition() {
        if (!registry.containsBeanDefinition(HOST_COMPONENT_PROVIDER)) {
            BeanDefinitionBuilder builder = BeanDefinitionBuilder.rootBeanDefinition(SpringHostComponentProviderFactoryBean.class);
            BeanDefinition beanDef = builder.getBeanDefinition();
            primeHostComponentBeanDefinition(beanDef);

            registry.registerBeanDefinition(HOST_COMPONENT_PROVIDER, beanDef);
        }

        final BeanDefinition beanDef = registry.getBeanDefinition(HOST_COMPONENT_PROVIDER);
        primeHostComponentBeanDefinition(beanDef);

        if (beanDef == null) {
            throw new IllegalStateException("Host component provider not found nor created. This should never happen.");
        }
        return beanDef;
    }

    private void primeHostComponentBeanDefinition(BeanDefinition beanDef) {
        ensurePropertyNotNull(beanDef, BEAN_NAMES, new ArrayList<String>());
        ensurePropertyNotNull(beanDef, BEAN_INTERFACES, new HashMap<String, List<String>>());
        ensurePropertyNotNull(beanDef, BEAN_CONTEXT_CLASS_LOADER_STRATEGIES, new HashMap<String, ContextClassLoaderStrategy>());
        ensurePropertyNotNull(beanDef, BUNDLE_TRACKING_BEANS, new HashSet<String>());
    }

    private void ensurePropertyNotNull(BeanDefinition beanDef, String propertyName, Object defaultValue) {
        if (!beanDef.getPropertyValues().contains(propertyName)) {
            beanDef.getPropertyValues().addPropertyValue(propertyName, defaultValue);
        }
    }

    public void addBeanName(String beanName) {
        getBeanNames().add(beanName);
    }

    public void addBeanInterface(String beanName, String ifce) {
        addBeanInterfaces(beanName, Collections.singleton(ifce));
    }

    public void addBeanInterfaces(String beanName, Collection<String> ifces) {
        final Map<String, List<String>> beanInterfaces = getBeanInterfaces();

        List<String> interfaces = beanInterfaces.get(beanName);
        if (interfaces == null) {
            interfaces = new ArrayList<>();
            beanInterfaces.put(beanName, interfaces);
        }
        interfaces.addAll(ifces);
    }

    public void addContextClassLoaderStrategy(String beanName, ContextClassLoaderStrategy strategy) {
        getBeanContextClassLoaderStrategies().put(beanName, strategy);
    }

    public void addBundleTrackingBean(String beanName) {
        getBundleTrackingBeans().add(beanName);
    }

    @SuppressWarnings("unchecked")
    private Map<String, ContextClassLoaderStrategy> getBeanContextClassLoaderStrategies() {
        return (Map<String, ContextClassLoaderStrategy>) getPropertyValue(BEAN_CONTEXT_CLASS_LOADER_STRATEGIES);
    }

    @SuppressWarnings("unchecked")
    private Map<String, List<String>> getBeanInterfaces() {
        return (Map<String, List<String>>) getPropertyValue(BEAN_INTERFACES);
    }

    @SuppressWarnings("unchecked")
    private List<String> getBeanNames() {
        return (List<String>) getPropertyValue(BEAN_NAMES);
    }

    @SuppressWarnings("unchecked")
    private Set<String> getBundleTrackingBeans() {
        return (Set<String>) getPropertyValue(BUNDLE_TRACKING_BEANS);
    }

    private Object getPropertyValue(String propertyName) {
        return getBeanDefinition().getPropertyValues().getPropertyValue(propertyName).getValue();
    }
}
