package com.atlassian.plugin.spring;

import com.atlassian.plugin.osgi.hostcomponents.ContextClassLoaderStrategy;
import com.google.common.annotations.VisibleForTesting;
import org.slf4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.AnnotatedBeanDefinition;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.core.type.MethodMetadata;

import javax.annotation.Nonnull;
import java.util.Map;
import java.util.function.Function;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * A Spring {@link BeanDefinitionRegistryPostProcessor} which scans for {@link AnnotatedBeanDefinition}s in the
 * registry (i.e. beans defined by {@link org.springframework.context.annotation.Bean}-annotated methods) which are
 * also annotated with {@link AvailableToPlugins}. These bean definitions are then used to configure the spring
 * host component provider.
 * <p>
 * Note that {@link AvailableToPlugins} can also be used to annotate the bean class itself, in which case this is
 * handled by {@link SpringHostComponentProviderFactoryBean} itself.
 * <p>
 * Usage: simply declare a bean of this class in your application context, and Spring will invoke it appropriately.
 *
 * @see PluginBeanDefinitionRegistry
 * @see SpringHostComponentProviderFactoryBean
 * @since 4.2
 */
public class AvailableToPluginsBeanDefinitionRegistryProcessor implements BeanDefinitionRegistryPostProcessor {
    private static final Logger log = getLogger(AvailableToPluginsBeanDefinitionRegistryProcessor.class);

    private final Function<BeanDefinitionRegistry, PluginBeanDefinitionRegistry> registryFactory;

    public AvailableToPluginsBeanDefinitionRegistryProcessor() {
        this(PluginBeanDefinitionRegistry::new);
    }

    @VisibleForTesting
    AvailableToPluginsBeanDefinitionRegistryProcessor(Function<BeanDefinitionRegistry, PluginBeanDefinitionRegistry> registryFactory) {
        this.registryFactory = registryFactory;
    }

    @Override
    public void postProcessBeanDefinitionRegistry(@Nonnull BeanDefinitionRegistry registry) throws BeansException {
        log.debug("Scanning al;l bean definitions for plugin-available @Bean methods");
        final PluginBeanDefinitionRegistry pluginBeanDefinitionRegistry = registryFactory.apply(registry);
        for (String beanName : registry.getBeanDefinitionNames()) {
            final BeanDefinition beanDefinition = registry.getBeanDefinition(beanName);
            if (beanDefinition instanceof AnnotatedBeanDefinition) {
                final MethodMetadata factoryMethodMetadata = ((AnnotatedBeanDefinition) beanDefinition).getFactoryMethodMetadata();
                if (factoryMethodMetadata != null) {
                    final boolean isPluginAvailable = factoryMethodMetadata.isAnnotated(AvailableToPlugins.class.getName());
                    if (isPluginAvailable) {
                        if (beanDefinition.isSingleton()) {
                            log.debug("Registering bean '{}' as plugin-available", beanName);
                            registerPluginAvailableBean(beanName, factoryMethodMetadata, pluginBeanDefinitionRegistry);
                        } else {
                            log.warn("Bean '{}' is not singleton-scoped, and cannot be made available to plugins", beanName);
                        }
                    }
                }
            }
        }
    }

    /**
     * The logic in this method is very similar to the logic in {@link SpringHostComponentProviderFactoryBean}, but
     * where that class works off the AvailableToPlugins annotation object directly, this class has to work off the
     * lower-level MethodMetadata interface. If the logic in here is altered, then it should most likely be altered
     * there also.
     */
    private void registerPluginAvailableBean(String beanName, MethodMetadata methodMetadata, PluginBeanDefinitionRegistry registry) {
        registry.addBeanName(beanName);
        final Map<String, Object> annotationAttributes = methodMetadata.getAnnotationAttributes(AvailableToPlugins.class.getName(), true);
        final String valueAttribute = (String) annotationAttributes.get("value");
        if (!valueAttribute.equals(Void.class.getName())) {
            registry.addBeanInterface(beanName, valueAttribute);
        }
        final String[] interfacesAttribute = (String[]) annotationAttributes.get("interfaces");
        for (String interfaceName : interfacesAttribute) {
            registry.addBeanInterface(beanName, interfaceName);
        }

        registry.addContextClassLoaderStrategy(beanName, (ContextClassLoaderStrategy) annotationAttributes.get("contextClassLoaderStrategy"));
        final boolean trackBundle = (Boolean) annotationAttributes.get("trackBundle");
        if (trackBundle)
            registry.addBundleTrackingBean(beanName);
    }

    @Override
    public void postProcessBeanFactory(@Nonnull ConfigurableListableBeanFactory beanFactory) throws BeansException {
        // nothing doing
    }
}
