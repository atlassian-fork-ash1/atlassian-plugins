#!/bin/bash

TMPFILE=/tmp/blablabla
rm -f $TMPFILE
for var in "$@"
do
    if [[ $var == *.jmod ]]
    then
	unzip -Z -1 "$var" 2> /dev/null | sed -e '/^classes/!d' -e 's#^classes/##g' -e 's#/*[^/]*\..*##' -e 's#/#.#g' \
					      -e '/^java\./d' -e '/^sun/d' -e '/^com.sun/d' -e '/^com.oracle/d' -e '/^jdk/d' -e '/^META-INF/d' -e '/^org.graalvm/d' \
 					      -e '/^toolbarButtonGraphics./d' -e '/^images.toolbarButtonGraphics./d' -e '/^netscape.javascript/d' \
					      -e '/^org.jcp.xml.dsig.internal/d' -e '/^\s*$/d' >> $TMPFILE
    else
	unzip -Z -1 "$var" 2> /dev/null | sed -e 's#/*[^/]*\..*##' -e 's#/#.#g' -e '/^java\./d' -e '/^sun/d' -e '/^com.sun/d' -e '/^com.oracle/d' -e '/^jdk/d' -e '/^META-INF/d' \
					      -e '/^source_tips/d' -e '/^org.jcp.xml.dsig.internal/d' >> $TMPFILE 
    fi
done
cat $TMPFILE | sort | uniq 
