package com.atlassian.plugin.osgi.container.felix;

import com.atlassian.plugin.ReferenceMode;
import com.atlassian.plugin.event.PluginEventListener;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.PluginFrameworkShutdownEvent;
import com.atlassian.plugin.event.events.PluginFrameworkStartingEvent;
import com.atlassian.plugin.event.events.PluginFrameworkWarmRestartingEvent;
import com.atlassian.plugin.event.events.PluginUninstalledEvent;
import com.atlassian.plugin.event.events.PluginUpgradedEvent;
import com.atlassian.plugin.instrumentation.PluginSystemInstrumentation;
import com.atlassian.plugin.osgi.container.OsgiContainerException;
import com.atlassian.plugin.osgi.container.OsgiContainerManager;
import com.atlassian.plugin.osgi.container.OsgiContainerStartedEvent;
import com.atlassian.plugin.osgi.container.OsgiContainerStoppedEvent;
import com.atlassian.plugin.osgi.container.OsgiPersistentCache;
import com.atlassian.plugin.osgi.container.PackageScannerConfiguration;
import com.atlassian.plugin.osgi.hostcomponents.HostComponentProvider;
import com.atlassian.plugin.osgi.hostcomponents.HostComponentRegistration;
import com.atlassian.plugin.osgi.hostcomponents.impl.DefaultComponentRegistrar;
import com.atlassian.plugin.osgi.util.OsgiHeaderUtil;
import com.atlassian.plugin.util.ContextClassLoaderSwitchingUtil;
import com.atlassian.plugin.util.PluginUtils;
import com.google.common.annotations.VisibleForTesting;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.framework.Felix;
import org.apache.felix.framework.Logger;
import org.apache.felix.framework.cache.BundleArchive;
import org.apache.felix.framework.cache.BundleCache;
import org.apache.felix.framework.util.FelixConstants;
import org.apache.felix.framework.util.StringMap;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleEvent;
import org.osgi.framework.BundleException;
import org.osgi.framework.BundleListener;
import org.osgi.framework.Constants;
import org.osgi.framework.FrameworkEvent;
import org.osgi.framework.FrameworkListener;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.packageadmin.PackageAdmin;
import org.osgi.util.tracker.ServiceTracker;
import org.osgi.util.tracker.ServiceTrackerCustomizer;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import static com.atlassian.plugin.util.FileUtils.conditionallyExtractZipFile;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Felix implementation of the OSGi container manager
 */
public class FelixOsgiContainerManager implements OsgiContainerManager {
    public static final int REFRESH_TIMEOUT = 10;

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(FelixOsgiContainerManager.class);
    public static final String ATLASSIAN_BOOTDELEGATION = "atlassian.org.osgi.framework.bootdelegation";
    public static final String ATLASSIAN_BOOTDELEGATION_EXTRA = "atlassian.org.osgi.framework.bootdelegation.extra";
    public static final String ATLASSIAN_DISABLE_REFERENCE_PROTOCOL = "atlassian.felix.disable.reference.protocol";

    private final OsgiPersistentCache persistentCache;
    private Function<DefaultComponentRegistrar, BundleRegistration> bundleRegistrationFactory;
    private final PackageScannerConfiguration packageScannerConfig;
    private final HostComponentProvider hostComponentProvider;
    private final List<ServiceTracker> trackers;
    private final ExportsBuilder exportsBuilder;

    private final ThreadFactory threadFactory = runnable -> {
        final Thread thread = new Thread(runnable, "Felix:Startup");
        thread.setDaemon(true);
        return thread;
    };
    private BundleRegistration registration = null;
    private Felix felix = null;
    private boolean felixRunning = false;
    private boolean disableMultipleBundleVersions = true;
    private Logger felixLogger;
    private final PluginEventManager pluginEventManager;

    /**
     * Constructs the container manager.
     *
     * @param frameworkBundlesZip  The location of the zip file containing framework bundles
     * @param persistentCache      The persistent cache to use for the framework and framework bundles
     * @param packageScannerConfig The configuration for package scanning
     * @param provider             The host component provider. May be null.
     * @param eventManager         The plugin event manager to register for init and shutdown events
     * @throws OsgiContainerException If the host version isn't supplied and the cache directory cannot be cleaned.
     * @since 2.2.0
     */
    public FelixOsgiContainerManager(
            final URL frameworkBundlesZip,
            final OsgiPersistentCache persistentCache,
            final PackageScannerConfiguration packageScannerConfig,
            final HostComponentProvider provider,
            final PluginEventManager eventManager)
            throws OsgiContainerException {
        this(registrar -> new BundleRegistration(frameworkBundlesZip, persistentCache.getFrameworkBundleCache(), registrar),
                persistentCache, packageScannerConfig, provider, eventManager);
    }

    /**
     * Constructs the container manager.
     * <p>
     * This constructor is used when frameworkBundlesDirectory has already been populated with the framework bundles. It
     * ignores {@link OsgiPersistentCache#getFrameworkBundleCache}.
     *
     * @param frameworkBundlesDirectory The directory containing framework bundles
     * @param persistentCache           The persistent cache to use for the framework
     * @param packageScannerConfig      The configuration for package scanning
     * @param provider                  The host component provider. May be null.
     * @param eventManager              The plugin event manager to register for init and shutdown events
     * @throws OsgiContainerException If the host version isn't supplied and the cache directory cannot be cleaned.
     * @since 3.0.21
     */
    public FelixOsgiContainerManager(
            final File frameworkBundlesDirectory,
            final OsgiPersistentCache persistentCache,
            final PackageScannerConfiguration packageScannerConfig,
            final HostComponentProvider provider,
            final PluginEventManager eventManager)
            throws OsgiContainerException {
        this(registrar -> new BundleRegistration(frameworkBundlesDirectory, registrar), persistentCache,
                packageScannerConfig, provider, eventManager);
    }

    /**
     * Constructs the container manager.
     * <p>
     * This constructor is private because i would like to move to using a builder for classes like this with many
     * constructors and lots of defaults. In any case, it uses internal classes (BundleRegistration) to abstract constructor
     * differences, so it's not useful to clients in general.
     *
     * @param bundleRegistrationFactory The factory for the BundleRegistration helper
     * @param persistentCache           The persistent cache to use for the framework
     * @param packageScannerConfig      The configuration for package scanning
     * @param provider                  The host component provider. May be null.
     * @param eventManager              The plugin event manager to register for init and shutdown events
     * @throws OsgiContainerException If the host version isn't supplied and the cache directory cannot be cleaned.
     * @since 3.0.21
     */
    private FelixOsgiContainerManager(
            final Function<DefaultComponentRegistrar, BundleRegistration> bundleRegistrationFactory,
            final OsgiPersistentCache persistentCache,
            final PackageScannerConfiguration packageScannerConfig,
            final HostComponentProvider provider,
            final PluginEventManager eventManager)
            throws OsgiContainerException {
        checkNotNull(bundleRegistrationFactory, "The bundle registration factory must not be null");
        checkNotNull(persistentCache, "The framework bundles directory must not be null");
        checkNotNull(packageScannerConfig, "The package scanner configuration must not be null");
        checkNotNull(eventManager, "The plugin event manager must not be null");

        this.bundleRegistrationFactory = bundleRegistrationFactory;
        this.packageScannerConfig = packageScannerConfig;
        this.persistentCache = persistentCache;
        hostComponentProvider = provider;
        trackers = Collections.synchronizedList(new ArrayList<>());
        this.pluginEventManager = eventManager;
        eventManager.register(this);
        felixLogger = new FelixLoggerBridge(log);
        exportsBuilder = new ExportsBuilder();
    }

    public void setFelixLogger(final Logger logger) {
        felixLogger = logger;
    }

    public void setDisableMultipleBundleVersions(final boolean val) {
        disableMultipleBundleVersions = val;
    }

    /**
     * Clears export cache.
     *
     * @since 2.9.0
     */
    public void clearExportCache() {
        exportsBuilder.clearExportCache();
    }

    @SuppressWarnings({"UnusedDeclaration"})
    @PluginEventListener
    public void onStart(final PluginFrameworkStartingEvent event) {
        start();
    }

    @SuppressWarnings({"UnusedDeclaration"})
    @PluginEventListener
    public void onShutdown(final PluginFrameworkShutdownEvent event) {
        stop();
    }

    @SuppressWarnings({"UnusedDeclaration"})
    @PluginEventListener
    public void onPluginUpgrade(final PluginUpgradedEvent event) {
        registration.refreshPackages();
    }

    @SuppressWarnings({"UnusedDeclaration"})
    @PluginEventListener
    public void onPluginUninstallation(final PluginUninstalledEvent event) {
        registration.refreshPackages();
    }

    @SuppressWarnings({"UnusedDeclaration"})
    @PluginEventListener
    public void onPluginFrameworkWarmRestarting(final PluginFrameworkWarmRestartingEvent event) {
        registration.loadHostComponents(collectHostComponents(hostComponentProvider));
    }

    public void start() throws OsgiContainerException {
        if (isRunning()) {
            return;
        }

        final DefaultComponentRegistrar registrar = collectHostComponents(hostComponentProvider);
        // Create a case-insensitive configuration property map.
        final StringMap configMap = new StringMap();

        // Add the bundle provided service interface package and the core OSGi
        // packages to be exported from the class path via the system bundle.
        configMap.put(Constants.FRAMEWORK_SYSTEMPACKAGES_EXTRA, exportsBuilder.getExports(registrar.getRegistry(), packageScannerConfig));

        // Explicitly specify the directory to use for caching bundles.
        configMap.put(BundleCache.CACHE_ROOTDIR_PROP, persistentCache.getOsgiBundleCache().getAbsolutePath());

        configMap.put(FelixConstants.LOG_LEVEL_PROP, String.valueOf(felixLogger.getLogLevel()));
        configMap.put(FelixConstants.LOG_LOGGER_PROP, felixLogger);
        String bootDelegation = System.getProperty(ATLASSIAN_BOOTDELEGATION);
        if ((bootDelegation == null) || (bootDelegation.trim().length() == 0)) {
            // These exist to work around JAXP problems. Specifically, bundles that use static factories to create JAXP
            // instances will execute FactoryFinder with the CCL set to the bundle. These delegations ensure the appropriate
            // implementation is found and loaded.
            bootDelegation = "weblogic,weblogic.*," +
                    "META-INF.services," +
                    "jdk.*," +
                    "com.yourkit,com.yourkit.*," +
                    "com.chronon,com.chronon.*," +
                    "org.jboss.byteman,org.jboss.byteman.*," +
                    "com.jprofiler,com.jprofiler.*," +
                    "org.apache.xerces,org.apache.xerces.*," +
                    "org.apache.xalan,org.apache.xalan.*," +
                    "org.apache.xml.serializer," +
                    "sun.*," +
                    "com.sun.xml.bind.v2," +
                    "com.sun.xml.internal.bind.v2," +
                    "com.icl.saxon," +
                    "com_cenqua_clover," +
                    "com.cenqua.clover,com.cenqua.clover.*," +
                    "com.atlassian.clover,com.atlassian.clover.*";
        }
        final String extraBootDelegation = System.getProperty(ATLASSIAN_BOOTDELEGATION_EXTRA, "").trim();
        if (0 != extraBootDelegation.length()) {
            bootDelegation = bootDelegation + "," + extraBootDelegation;
        }

        configMap.put(FelixConstants.FRAMEWORK_BOOTDELEGATION, bootDelegation);
        configMap.put(FelixConstants.IMPLICIT_BOOT_DELEGATION_PROP, "false");

        configMap.put(FelixConstants.FRAMEWORK_BUNDLE_PARENT, FelixConstants.FRAMEWORK_BUNDLE_PARENT_FRAMEWORK);
        if (log.isDebugEnabled()) {
            log.debug("Felix configuration: " + configMap);
        }

        validateConfiguration(configMap);

        try {
            // Create host activator;
            registration = bundleRegistrationFactory.apply(registrar);
            final List<BundleActivator> list = new ArrayList<>();
            list.add(registration);
            configMap.put(FelixConstants.SYSTEMBUNDLE_ACTIVATORS_PROP, list);

            // Now create an instance of the framework with
            // our configuration properties and activator.
            felix = new Felix(configMap);

            // Now start Felix instance. Starting in a different thread to explicitly set daemon status
            final Runnable start = () -> {
                try {
                    Thread.currentThread().setContextClassLoader(null);
                    felix.start();
                    felixRunning = true;
                } catch (final BundleException e) {
                    throw new OsgiContainerException("Unable to start felix", e);
                }
            };
            final Thread t = threadFactory.newThread(start);
            t.start();

            // Give it 10 seconds
            t.join(10 * 60 * 1000);

        } catch (final Exception ex) {
            throw new OsgiContainerException("Unable to start OSGi container", ex);
        }
        pluginEventManager.broadcast(new OsgiContainerStartedEvent(this));
    }

    /**
     * @param configMap The Felix configuration
     * @throws OsgiContainerException If any validation fails
     */
    private void validateConfiguration(final StringMap configMap) throws OsgiContainerException {
        final String systemExports = (String) configMap.get(Constants.FRAMEWORK_SYSTEMPACKAGES_EXTRA);
        // Change in JVM should trigger the cache cleardown, regardless of export changes.
        final String cacheKeySource = StringUtils.join(new Object[]{this.getRuntimeEnvironment(), systemExports}, ',');

        validateCaches(cacheKeySource);

        detectIncorrectOsgiVersion();
        detectXercesOverride(systemExports);
    }

    /**
     * Detect when xerces has no version, most likely due to an installation of Tomcat where an old version of xerces is
     * installed into common/lib/endorsed in order to support Java 1.4.
     *
     * @param systemExports The system exports
     * @throws OsgiContainerException If xerces has no version
     */
    void detectXercesOverride(final String systemExports) throws OsgiContainerException {
        int pos = systemExports.indexOf("org.apache.xerces.util");
        if (pos > -1) {
            if (pos == 0 || systemExports.charAt(pos - 1) == ',') {
                pos += "org.apache.xerces.util".length();

                // only fail if no xerces found and xerces has no version
                if (pos >= systemExports.length() || ';' != systemExports.charAt(pos)) {
                    throw new OsgiContainerException(
                            "Detected an incompatible version of Apache Xerces on the classpath. If using Tomcat, you may have " +
                                    "an old version of Xerces in $TOMCAT_HOME/common/lib/endorsed that will need to be removed."
                    );
                }
            }
        }
    }

    /**
     * Validate caches based on the list of packages and the runtime environment exported from the application. If the
     * settings have changed, the cache directories should be cleared.
     *
     * @param cacheKeySource string of information that generates the cache key.
     */
    private void validateCaches(final String cacheKeySource) {
        log.info("Using Felix bundle cacheKey source: {}", cacheKeySource);
        persistentCache.validate(cacheKeySource);
        log.debug("Using Felix bundle cache directory: {}", persistentCache.getOsgiBundleCache().getAbsolutePath());
    }

    /**
     * Detects incorrect configuration of WebSphere 6.1 that leaks OSGi 4.0 jars into the application
     */
    private void detectIncorrectOsgiVersion() {
        try {
            Bundle.class.getMethod("getBundleContext");
        } catch (final NoSuchMethodException e) {
            throw new OsgiContainerException("Detected older version (4.0 or earlier) of OSGi. If using WebSphere 6.1, "
                    + "please enable application-first (parent-last) classloading and the 'Single classloader for "
                    + "application' WAR classloader policy.");
        }
    }

    public void stop() throws OsgiContainerException {
        if (felixRunning) {
            Collection<ServiceTracker> trackersToStop;
            synchronized (trackers) {
                trackersToStop = new ArrayList<>(trackers);
            }
            for (final ServiceTracker tracker : trackersToStop) {
                tracker.close();
            }
            final FrameworkListener listener = event -> {
                if (event.getType() == FrameworkEvent.WAIT_TIMEDOUT) {
                    log.error("Timeout waiting for OSGi to shutdown");
                    threadDump();
                } else if (event.getType() == FrameworkEvent.STOPPED) {
                    log.info("OSGi shutdown successful");
                }
            };
            try {
                felix.getBundleContext().addFrameworkListener(listener);
                felix.stop();
                felix.waitForStop(TimeUnit.SECONDS.toMillis(60));
            } catch (final InterruptedException e) {
                log.warn("Interrupting Felix shutdown", e);
            } catch (final BundleException ex) {
                log.error("An error occurred while stopping the Felix OSGi Container. ", ex);
            }
        }

        felixRunning = false;
        felix = null;
        pluginEventManager.broadcast(new OsgiContainerStoppedEvent(this));
    }

    private void threadDump() {
        final StringBuilder sb = new StringBuilder();
        final String nl = System.getProperty("line.separator");
        for (final Map.Entry<Thread, StackTraceElement[]> entry : Thread.getAllStackTraces().entrySet()) {
            final Thread key = entry.getKey();
            final StackTraceElement[] trace = entry.getValue();
            sb.append(key).append(nl);
            for (final StackTraceElement aTrace : trace) {
                sb.append(" ").append(aTrace).append(nl);
            }
        }
        log.debug("Thread dump: " + nl + sb.toString());
    }

    public Bundle[] getBundles() {
        if (isRunning()) {
            return registration.getBundles();
        } else {
            throw new IllegalStateException("Cannot retrieve the bundles if the Felix container isn't running. Check "
                    + "earlier in the logs for the possible cause as to why Felix didn't start correctly.");
        }
    }

    public ServiceReference[] getRegisteredServices() {
        return felix.getRegisteredServices();
    }

    public ServiceTracker getServiceTracker(final String interfaceClassName) {
        return getServiceTracker(interfaceClassName, null);
    }

    @Override
    public ServiceTracker getServiceTracker(
            final String interfaceClassName,
            final ServiceTrackerCustomizer serviceTrackerCustomizer) {
        if (!isRunning()) {
            throw new IllegalStateException("Unable to create a tracker when osgi is not running");
        }

        final ServiceTracker tracker = registration.getServiceTracker(interfaceClassName, trackers, serviceTrackerCustomizer);
        tracker.open();
        trackers.add(tracker);
        return tracker;
    }

    @Override
    public void addBundleListener(BundleListener listener) {
        felix.getBundleContext().addBundleListener(listener);
    }

    @Override
    public void removeBundleListener(BundleListener listener) {
        Felix felix = this.felix;
        if (felix != null) {
            BundleContext context = felix.getBundleContext();
            if (context != null) {
                context.removeBundleListener(listener);
            }
        }
    }

    @Override
    public Bundle installBundle(final File file, final ReferenceMode referenceMode) throws OsgiContainerException {
        try {
            return registration.install(file, disableMultipleBundleVersions, referenceMode.allowsReference());
        } catch (final BundleException e) {
            throw new OsgiContainerException("Unable to install bundle", e);
        }
    }

    DefaultComponentRegistrar collectHostComponents(final HostComponentProvider provider) {
        final DefaultComponentRegistrar registrar = new DefaultComponentRegistrar();
        if (provider != null) {
            provider.provide(registrar);
        }
        return registrar;
    }

    public boolean isRunning() {
        return felixRunning;
    }

    public List<HostComponentRegistration> getHostComponentRegistrations() {
        return registration.getHostComponentRegistrations();
    }

    /**
     * Get a string containing the significant runtime environment details that may require a rebuild of the OSGi cache.
     *
     * @return a string containing significant runtime details.
     */
    @VisibleForTesting
    String getRuntimeEnvironment() {
        //Two things currently:
        // - JDK version
        // - Startup timeout which may be cached in the transformed plugins.
        return String.format("java.version=%s,plugin.enable.timeout=%d", System.getProperty("java.version"),
                PluginUtils.getDefaultEnablingWaitPeriod());
    }

    /**
     * Manages framework-level framework bundles and host components registration, and individual plugin bundle installation
     * and removal.
     */
    static class BundleRegistration implements BundleActivator, BundleListener, FrameworkListener {

        private final URL frameworkBundlesUrl;
        private final File frameworkBundlesDir;
        private DefaultComponentRegistrar registrar;
        private ClassLoader initializedClassLoader;

        private BundleContext bundleContext;
        private PackageAdmin packageAdmin;
        private List<ServiceRegistration> hostServicesReferences;
        private List<HostComponentRegistration> hostComponentRegistrations;
        private Optional<ServiceRegistration> instrumentationServiceReference = Optional.empty();

        public BundleRegistration(final File frameworkBundlesDir, final DefaultComponentRegistrar registrar) {
            this(null, frameworkBundlesDir, registrar);
        }

        public BundleRegistration(final URL frameworkBundlesUrl, final File frameworkBundlesDir, final DefaultComponentRegistrar registrar) {
            this.frameworkBundlesUrl = frameworkBundlesUrl;
            this.frameworkBundlesDir = frameworkBundlesDir;
            this.registrar = registrar;
            this.initializedClassLoader = Thread.currentThread().getContextClassLoader();
        }

        public void start(final BundleContext context) throws Exception {
            bundleContext = context;
            final ServiceReference ref = context.getServiceReference(PackageAdmin.class.getName());
            packageAdmin = (PackageAdmin) context.getService(ref);

            context.addBundleListener(this);
            context.addFrameworkListener(this);

            loadHostComponents(registrar);
            if (null != frameworkBundlesUrl) {
                conditionallyExtractZipFile(frameworkBundlesUrl, frameworkBundlesDir);
            }
            installFrameworkBundles();

            // unregister the optional instrumentation service
            instrumentationServiceReference.ifPresent(ServiceRegistration::unregister);
            instrumentationServiceReference = Optional.empty();

            // register the instrumentation service if it's present, keeping aware that InstrumentRegistry might not be in the class loader
            final Optional instrumentRegistry = PluginSystemInstrumentation.instance().getInstrumentRegistry();
            if (instrumentRegistry.isPresent()) {
                instrumentationServiceReference = Optional.of(context.registerService(PluginSystemInstrumentation.INSTRUMENT_REGISTRY_CLASS, instrumentRegistry.get(), null));
            } else {
                instrumentationServiceReference = Optional.empty();
            }
        }

        public void stop(final BundleContext ctx) {
            ctx.removeBundleListener(this);
            ctx.removeFrameworkListener(this);
            if (hostServicesReferences != null) {
                for (final ServiceRegistration ref : hostServicesReferences) {
                    ref.unregister();
                }
            }

            // unregister the optional instrumentation service
            instrumentationServiceReference.ifPresent(ServiceRegistration::unregister);
            instrumentationServiceReference = Optional.empty();

            bundleContext = null;
            packageAdmin = null;
            hostServicesReferences = null;
            hostComponentRegistrations = null;
            registrar = null;
            initializedClassLoader = null;
        }

        public void bundleChanged(final BundleEvent evt) {
            switch (evt.getType()) {
                case BundleEvent.INSTALLED:
                    log.info("Installed bundle " + evt.getBundle().getSymbolicName() + " (" + evt.getBundle().getBundleId() + ")");
                    break;
                case BundleEvent.RESOLVED:
                    log.info("Resolved bundle " + evt.getBundle().getSymbolicName() + " (" + evt.getBundle().getBundleId() + ")");
                    break;
                case BundleEvent.UNRESOLVED:
                    log.info("Unresolved bundle " + evt.getBundle().getSymbolicName() + " (" + evt.getBundle().getBundleId() + ")");
                    break;
                case BundleEvent.STARTED:
                    log.info("Started bundle " + evt.getBundle().getSymbolicName() + " (" + evt.getBundle().getBundleId() + ")");
                    break;
                case BundleEvent.STOPPED:
                    log.info("Stopped bundle " + evt.getBundle().getSymbolicName() + " (" + evt.getBundle().getBundleId() + ")");
                    break;
                case BundleEvent.UNINSTALLED:
                    log.info("Uninstalled bundle " + evt.getBundle().getSymbolicName() + " (" + evt.getBundle().getBundleId() + ")");
                    break;
            }
        }

        public Bundle install(final File path, final boolean uninstallOtherVersions)
                throws BundleException {
            return install(path, uninstallOtherVersions, false);
        }

        public Bundle install(final File path, final boolean uninstallOtherVersions, final boolean allowReference)
                throws BundleException {
            boolean bundleUninstalled = false;
            if (uninstallOtherVersions) {
                final String pluginKey = OsgiHeaderUtil.getPluginKey(path);
                if (null == pluginKey) {
                    // If the jar we're installing doesn't have a manifest, felix will choke on it in short order,
                    // so we just short circuit that to avoid NPE's if we have a malformed jar.
                    throw new BundleException("No plugin key in (possibly malformed) bundle jar '" + path + "'");
                }

                for (final Bundle oldBundle : bundleContext.getBundles()) {
                    if (pluginKey.equals(OsgiHeaderUtil.getPluginKey(oldBundle))) {
                        log.info("Uninstalling existing version " + oldBundle.getHeaders().get(Constants.BUNDLE_VERSION));
                        oldBundle.uninstall();
                        bundleUninstalled = true;
                    }
                }
            }
            String location = path.toURI().toString();
            if (allowReference &&
                    (!Boolean.getBoolean(ATLASSIAN_DISABLE_REFERENCE_PROTOCOL)) &&
                    location.startsWith(BundleArchive.FILE_PROTOCOL)) {
                // This will avoid copying of the bundle jar artifact to the felix-cache
                // location, saving I/O and disk space.
                location = BundleArchive.REFERENCE_PROTOCOL + location;
            }
            final Bundle bundle = bundleContext.installBundle(location);
            if (bundleUninstalled) {
                refreshPackages();
            }
            return bundle;
        }

        public Bundle[] getBundles() {
            return bundleContext.getBundles();
        }

        public ServiceTracker getServiceTracker(final String clazz, final Collection<ServiceTracker> trackedTrackers) {
            return getServiceTracker(clazz, trackedTrackers, null);
        }

        public ServiceTracker getServiceTracker(final String clazz,
                                                final Collection<ServiceTracker> trackedTrackers,
                                                final ServiceTrackerCustomizer customizer) {
            return new ServiceTracker(bundleContext, clazz, customizer) {
                @Override
                public void close() {
                    super.close();
                    trackedTrackers.remove(this);
                }
            };
        }

        public List<HostComponentRegistration> getHostComponentRegistrations() {
            return hostComponentRegistrations;
        }

        void loadHostComponents(final DefaultComponentRegistrar registrar) {
            // Unregister any existing host components
            if (hostServicesReferences != null) {
                for (final ServiceRegistration reg : hostServicesReferences) {
                    reg.unregister();
                }
            }

            ContextClassLoaderSwitchingUtil.runInContext(initializedClassLoader, () -> {
                hostServicesReferences = registrar.writeRegistry(bundleContext);
                hostComponentRegistrations = registrar.getRegistry();
            });
        }

        private void installFrameworkBundles() throws BundleException {
            final File[] bundleFiles = frameworkBundlesDir == null
                    ? null : frameworkBundlesDir.listFiles((file, s) -> s.endsWith(".jar"));
            if (bundleFiles == null) {
                throw new BundleException("Directory with framework bundle jars could not be read: " + frameworkBundlesDir);
            }

            final List<Bundle> bundles = new ArrayList<>();
            for (final File bundleFile : bundleFiles) {
                bundles.add(install(bundleFile, false, false));
            }

            packageAdmin.resolveBundles(null);

            for (final Bundle bundle : bundles) {
                if (bundle.getHeaders().get(Constants.FRAGMENT_HOST) == null) {
                    bundle.start();
                }
            }
        }

        public void refreshPackages() {
            final CountDownLatch latch = new CountDownLatch(1);
            final FrameworkListener refreshListener = event -> {
                if (event.getType() == FrameworkEvent.PACKAGES_REFRESHED) {
                    log.info("Packages refreshed");
                    latch.countDown();
                }
            };

            bundleContext.addFrameworkListener(refreshListener);
            try {
                packageAdmin.refreshPackages(null);
                boolean refreshed = false;
                try {
                    refreshed = latch.await(REFRESH_TIMEOUT, TimeUnit.SECONDS);
                } catch (final InterruptedException e) {
                    // ignore
                }
                if (!refreshed) {
                    log.warn("Timeout exceeded waiting for package refresh");
                }
            } finally {
                bundleContext.removeFrameworkListener(refreshListener);
            }
        }

        @SuppressWarnings({"ThrowableResultOfMethodCallIgnored"})
        public void frameworkEvent(final FrameworkEvent event) {
            String bundleBits = "";
            if (event.getBundle() != null) {
                bundleBits = " in bundle " + event.getBundle().getSymbolicName();
            }
            switch (event.getType()) {
                case FrameworkEvent.ERROR:
                    log.error("Framework error" + bundleBits, event.getThrowable());
                    break;
                case FrameworkEvent.WARNING:
                    log.warn("Framework warning" + bundleBits, event.getThrowable());
                    break;
                case FrameworkEvent.INFO:
                    log.info("Framework info" + bundleBits, event.getThrowable());
                    break;
            }
        }
    }

}
