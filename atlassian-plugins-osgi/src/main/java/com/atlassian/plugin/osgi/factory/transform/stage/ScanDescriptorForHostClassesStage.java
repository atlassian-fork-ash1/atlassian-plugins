package com.atlassian.plugin.osgi.factory.transform.stage;

import com.atlassian.plugin.osgi.factory.transform.PluginTransformationException;
import com.atlassian.plugin.osgi.factory.transform.TransformContext;
import com.atlassian.plugin.osgi.factory.transform.TransformStage;
import com.atlassian.plugin.osgi.hostcomponents.HostComponentRegistration;
import com.atlassian.plugin.util.ClassLoaderUtils;
import org.dom4j.Attribute;
import org.dom4j.DocumentHelper;
import org.dom4j.XPath;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.atlassian.plugin.util.PluginUtils.isAtlassianDevMode;

/**
 * Scans the plugin descriptor for any "class" attribute, and ensures that it will be imported, if appropriate.
 *
 * @since 2.2.0
 */
public class ScanDescriptorForHostClassesStage implements TransformStage {
    private static final Logger log = LoggerFactory.getLogger(ScanDescriptorForHostClassesStage.class);

    @SuppressWarnings("unchecked")
    public void execute(TransformContext context) throws PluginTransformationException {
        XPath xpath = DocumentHelper.createXPath("//@class");
        List<Attribute> attributes = xpath.selectNodes(context.getDescriptorDocument());

        Map<HostComponentRegistration, RequiredComponentRegistration> requiredRegistrationMap = new HashMap<>();

        for (Attribute attr : attributes) {
            String className = attr.getValue();

            scanForHostComponents(context, className, requiredRegistrationMap);

            int dotpos = className.lastIndexOf(".");
            if (dotpos > -1) {
                String pkg = className.substring(0, dotpos);
                String pkgPath = pkg.replace('.', '/') + '/';

                // Only add an import if the system exports it and the plugin isn't using the package
                if (context.getSystemExports().isExported(pkg)) {
                    if (context.getPluginArtifact().doesResourceExist(pkgPath)) {
                        if (isAtlassianDevMode())
                            log.warn("The plugin '{}' uses a package '{}' that is also exported by the application. "
                                    + "It is highly recommended that the plugin use its own packages.",
                                    context.getPluginArtifact(), pkg);
                    } else {
                        context.getExtraImports().add(pkg);
                    }
                }
            }
        }

        requiredRegistrationMap.values().forEach(context::addRequiredHostComponent);
    }

    /**
     * Declares that a given {@link HostComponentRegistration} is a required host component, and will be accessed via
     * the specified {@code requiredInterface}.
     * <p>
     * The ultimate purpose here is to add {@link HostComponentRegistration} instances as "required host components"
     * to the {@link TransformContext}. But adding the raw {@link HostComponentRegistration} means that later transform
     * stages have no idea which interface will be used to access the component and thus they will have no option but
     * to assume all of the interfaces the host component implements may be used. This leads to unnecessary imports.
     * <p>
     * To avoid the above, the registered host components are wrapped in a {@link RequiredComponentRegistration}
     * such that the "main interfaces" can be restricted to the required subset (which still may be multiple
     * interfaces because multiple classes may depend on a given host component via different interfaces).
     */
    private void declareRequiredHostComponent(
            HostComponentRegistration hostComponent,
            Class<?> requiredInterface,
            Map<HostComponentRegistration,RequiredComponentRegistration> requiredRegistrationMap) {
        RequiredComponentRegistration registration =
                requiredRegistrationMap.computeIfAbsent(hostComponent, RequiredComponentRegistration::new);
        registration.addInterface(requiredInterface);
    }

    private void scanForHostComponents(
            TransformContext context,
            String className,
            Map<HostComponentRegistration, RequiredComponentRegistration> requiredRegistrationMap) {
        // Class name can be prefixed with 'bean:' to reference a spring bean, in this case don't attempt to load it. 
        if (className != null && className.contains(":")) {
            return;
        }

        Map<Class<?>, HostComponentRegistration> hostComponentInterfaces = new LinkedHashMap<>();
        for (HostComponentRegistration registration : context.getHostComponentRegistrations()) {
            for (Class<?> cls : registration.getMainInterfaceClasses()) {
                hostComponentInterfaces.put(cls, registration);
            }
        }

        Class<?> cls;
        try {
            cls = ClassLoaderUtils.loadClass(className, getClass());
        } catch (ClassNotFoundException e) {
            // not a host class, ignore
            return;
        }

        // Check constructor arguments for host component interfaces
        for (Constructor<?> ctor : cls.getConstructors()) {
            for (Class<?> ctorParam : ctor.getParameterTypes()) {
                if (hostComponentInterfaces.containsKey(ctorParam)) {
                    declareRequiredHostComponent(hostComponentInterfaces.get(ctorParam),
                            ctorParam, requiredRegistrationMap);
                }
            }
        }

        // Check setters for host component interface arguments
        for (Method method : cls.getMethods()) {
            if (method.getName().startsWith("set") && method.getParameterTypes().length == 1) {
                if (hostComponentInterfaces.containsKey(method.getParameterTypes()[0])) {
                    Class<?> parameterType = method.getParameterTypes()[0];
                    declareRequiredHostComponent(hostComponentInterfaces.get(parameterType),
                            parameterType, requiredRegistrationMap);
                }
            }
        }
    }

    /**
     * Wraps a {@link HostComponentRegistration}, providing a view of the component with potentially a subset of the
     * actual implemented interfaces.
     */
    private static class RequiredComponentRegistration implements HostComponentRegistration {

        private final HostComponentRegistration delegate;
        private final Set<Class<?>> interfaces;

        RequiredComponentRegistration(HostComponentRegistration delegate) {
            this.delegate = delegate;
            interfaces = new HashSet<>();
        }

        void addInterface(Class<?> implementedInterface) {
            if (!Arrays.asList(delegate.getMainInterfaceClasses()).contains(implementedInterface)) {
                throw new IllegalArgumentException(implementedInterface.getName() + " is not an interface of the host component "
                + delegate.getInstance().getClass().getName());
            }
            interfaces.add(implementedInterface);
        }

        @Override
        public Dictionary<String, String> getProperties() {
            return delegate.getProperties();
        }

        @Override
        public String[] getMainInterfaces() {
            return interfaces.stream()
                    .map(Class::getName)
                    .toArray(String[]::new);
        }

        @Override
        public Object getInstance() {
            return delegate.getInstance();
        }

        @Override
        public Class<?>[] getMainInterfaceClasses() {
            return interfaces.toArray(new Class<?>[0]);
        }
    }
}
