package com.atlassian.plugin.osgi.util;

import aQute.bnd.osgi.AbstractResource;
import aQute.bnd.osgi.ClassDataCollector;
import aQute.bnd.osgi.Clazz;
import aQute.bnd.osgi.Descriptors;
import com.google.common.collect.Collections2;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import org.apache.commons.io.IOUtils;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.util.Set;

/**
 * Contains all the utility methods and classes for scanning class file binary.
 *
 * @since 2.10
 */
public class ClassBinaryScanner {
    /**
     * Scans class binary to extract 1) referred classes 2) imported packages 3) the class's superclass.
     *
     * @param clazz the input class binary
     * @return result, never null
     * @throws java.io.IOException if the scan dies along the way
     * @since 2.10
     */
    public static ScanResult scanClassBinary(Clazz clazz) throws IOException {
        final ImmutableSet.Builder<String> allReferredClasses = new ImmutableSet.Builder<>();
        final String[] superClassName = new String[]{null};
        try {
            // TODO: Perhaps, we should start scanning for annotations as well. This ClassDataCollector in bndlib 1.4.3 is quite powerful.
            clazz.parseClassFileWithCollector(new ClassDataCollector() {

                @Override
                public void extendsClass(Descriptors.TypeRef ref) {
                    superClassName[0] = ref.getBinary();
                    allReferredClasses.add(ref.getBinary());
                }

                @Override
                public void implementsInterfaces(Descriptors.TypeRef[] refs) {
                    for (Descriptors.TypeRef ref : refs) {
                        allReferredClasses.add(ref.getBinary());
                    }
                }

                @Override
                public void addReference(Descriptors.TypeRef ref) {
                    allReferredClasses.add(ref.getBinary());
                }
            });
        } catch (Exception e) {
            throw new IOException("Error parsing class file", e);
        }

        Set<String> referredPackages = Sets.newHashSet(Collections2.transform(clazz.getReferred(),
                packageRef -> {
                    if (packageRef != null) {
                        return packageRef.getFQN();
                    } else {
                        return null;
                    }
                }));
        return new ScanResult(allReferredClasses.build(), referredPackages, superClassName[0]);
    }

    /**
     * Contains the result of class binary scanning.
     *
     * @since 2.10
     */
    public static class ScanResult {
        // this classes are referred to as strings since we don't want to load them all at this stage.
        private Set<String> referredClasses;
        private Set<String> referredPackages;
        private String superClass;

        public ScanResult(Set<String> referredClasses, Set<String> referredPackages, String superClass) {
            this.referredClasses = referredClasses;
            this.referredPackages = referredPackages;
            this.superClass = superClass;
        }

        public Set<String> getReferredClasses() {
            return referredClasses;
        }

        public Set<String> getReferredPackages() {
            return referredPackages;
        }

        public String getSuperClass() {
            return superClass;
        }
    }

    /**
     * InputStream-based resource for class scanning purpose (in the format required by bndlib).
     *
     * @since 2.10
     */
    public static class InputStreamResource extends AbstractResource implements Closeable {
        private InputStream inputStream;

        public InputStreamResource(InputStream inputStream) {
            super(-1);
            this.inputStream = inputStream;
        }

        @Override
        protected byte[] getBytes() throws Exception {
            return IOUtils.toByteArray(inputStream);
        }

        @Override
        public void close() throws IOException {
            inputStream.close();
        }

    }
}
