package com.atlassian.plugin.osgi.factory;

import com.atlassian.plugin.IllegalPluginStateException;
import com.atlassian.plugin.PluginDependencies;
import com.atlassian.plugin.module.ContainerAccessor;
import com.atlassian.plugin.osgi.container.OsgiContainerException;
import org.osgi.framework.Bundle;
import org.osgi.util.tracker.ServiceTracker;

import javax.annotation.Nonnull;
import java.io.InputStream;
import java.net.URL;
import java.util.Set;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Helper class that implements common behaviour to {@link OsgiPluginHelper} instances whether
 * before installation or after deinstallation.
 *
 * @since 3.0.17
 */
abstract class OsgiPluginNotInstalledHelperBase implements OsgiPluginHelper {
    private final String key;

    OsgiPluginNotInstalledHelperBase(final String key) {
        this.key = checkNotNull(key);
    }

    public Bundle getBundle() {
        throw new IllegalPluginStateException(getNotInstalledMessage());
    }

    public URL getResource(final String name) {
        throw new IllegalPluginStateException("Cannot getResource(" + name + "): " + getNotInstalledMessage());
    }

    public InputStream getResourceAsStream(final String name) {
        throw new IllegalPluginStateException("Cannot getResourceAsStream(" + name + "): " + getNotInstalledMessage());
    }

    public ClassLoader getClassLoader() {
        throw new IllegalPluginStateException(getNotInstalledMessage());
    }

    public void onEnable(final ServiceTracker... serviceTrackers) throws OsgiContainerException {
        throw new IllegalPluginStateException(getNotInstalledMessage());
    }

    public void onDisable() throws OsgiContainerException {
        throw new IllegalPluginStateException(getNotInstalledMessage());
    }

    public void onUninstall() throws OsgiContainerException {
        throw new IllegalPluginStateException(getNotInstalledMessage());
    }

    @Nonnull
    @Override
    public PluginDependencies getDependencies() {
        throw new IllegalPluginStateException(getNotInstalledMessage());
    }

    public void setPluginContainer(final Object container) {
        throw new IllegalPluginStateException(getNotInstalledMessage());
    }

    public ContainerAccessor getContainerAccessor() {
        throw new IllegalPluginStateException(getNotInstalledMessage());
    }

    public ContainerAccessor getRequiredContainerAccessor() {
        throw new IllegalPluginStateException(getNotInstalledMessage());
    }

    protected String getKey() {
        return key;
    }

    protected abstract String getNotInstalledMessage();
}
