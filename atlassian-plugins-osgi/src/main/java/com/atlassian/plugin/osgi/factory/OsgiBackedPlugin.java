package com.atlassian.plugin.osgi.factory;

import com.atlassian.plugin.IllegalPluginStateException;
import com.atlassian.plugin.Plugin;
import org.osgi.framework.Bundle;

/**
 * Interface for plugins backed by OSGi {@link Bundle}
 * <p>
 * There are a few implementations of {@link Plugin} interface that are backed by OSGi bundles,
 * to identify a such implementations all of them should implement current interface
 */
public interface OsgiBackedPlugin extends Plugin {
    /**
     * Returns <code>OSGi Bundle</code> that corresponds to the current plugin
     * <p>
     * Note that <code>Bundle</code> could be not available on some of {@code Plugin} lifecycle
     * stages. In that case method call results in {@code IllegalPluginStateException}
     *
     * @return <code>OSGi</code> representation of the current plugin
     * @throws IllegalPluginStateException if plugin has no <code>OSGi</code> representation yet
     */
    public Bundle getBundle() throws IllegalPluginStateException;
}
