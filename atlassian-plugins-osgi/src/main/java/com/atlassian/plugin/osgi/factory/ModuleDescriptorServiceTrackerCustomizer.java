package com.atlassian.plugin.osgi.factory;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.PluginModuleAvailableEvent;
import com.atlassian.plugin.event.events.PluginModuleUnavailableEvent;
import org.osgi.framework.Bundle;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTrackerCustomizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Tracks module descriptors registered as services, then updates the descriptors map accordingly
 *
 * @since 2.2.0
 */
class ModuleDescriptorServiceTrackerCustomizer implements ServiceTrackerCustomizer {
    private static final Logger log = LoggerFactory.getLogger(ModuleDescriptorServiceTrackerCustomizer.class);

    private final Bundle bundle;
    private final OsgiPlugin plugin;
    private final PluginEventManager pluginEventManager;

    public ModuleDescriptorServiceTrackerCustomizer(OsgiPlugin plugin, PluginEventManager pluginEventManager) {
        this.plugin = checkNotNull(plugin);
        this.bundle = checkNotNull(plugin.getBundle());
        this.pluginEventManager = checkNotNull(pluginEventManager);
    }

    public Object addingService(final ServiceReference serviceReference) {
        ModuleDescriptor<?> descriptor = null;
        if (serviceReference.getBundle() == bundle) {
            descriptor = (ModuleDescriptor<?>) bundle.getBundleContext().getService(serviceReference);
            plugin.addModuleDescriptor(descriptor);
            if (log.isInfoEnabled()) {
                log.info("Dynamically registered new module descriptor: " + descriptor.getCompleteKey());
            }
            pluginEventManager.broadcast(new PluginModuleAvailableEvent(descriptor));
        }
        return descriptor;
    }

    public void modifiedService(final ServiceReference serviceReference, final Object o) {
        // Don't bother doing anything as it only represents a change in properties
    }

    public void removedService(final ServiceReference serviceReference, final Object o) {
        if (serviceReference.getBundle() == bundle) {
            final ModuleDescriptor<?> descriptor = (ModuleDescriptor<?>) o;
            pluginEventManager.broadcast(new PluginModuleUnavailableEvent(descriptor));
            plugin.clearModuleDescriptor(descriptor.getKey());
            if (log.isInfoEnabled()) {
                log.info("Dynamically removed module descriptor: " + descriptor.getCompleteKey());
            }
        }
    }
}
