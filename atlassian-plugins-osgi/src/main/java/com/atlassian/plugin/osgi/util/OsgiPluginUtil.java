package com.atlassian.plugin.osgi.util;

import com.atlassian.plugin.PluginDependencies;
import com.atlassian.plugin.module.ContainerAccessor;
import org.osgi.framework.Bundle;
import org.osgi.framework.namespace.PackageNamespace;
import org.osgi.framework.wiring.BundleRevision;
import org.osgi.framework.wiring.BundleRevisions;
import org.osgi.framework.wiring.BundleWire;

import java.util.Collection;

/**
 * Collection of util operation on different OSGi bits that results in Plugins
 * items
 */
public final class OsgiPluginUtil {
    // PluginDependencies is immutable...
    private static final PluginDependencies EMPTY_DEPS = new PluginDependencies();

    /**
     * Creates null implementation of ContainerAccessor interface
     * <p>
     * That implementation used when plugin doesn't export access to any internal IoC. In that case a try
     * to access container means some logical bug in the design, for example reference some instance ID in
     * module type definition, so it makes sense to fail fast with reasonable explanation rather then return
     * some empty state which will masks real problem.
     *
     * @param pluginKey plugin identifier to be written in container exceptions
     * @return Null implementation of ContainerAccessor interface which is throwing exceptions on each
     * call
     */
    public static ContainerAccessor createNonExistingPluginContainer(final String pluginKey) {
        return new ContainerAccessor() {

            @Override
            public <T> T createBean(Class<T> clazz) {
                throw new UnsupportedOperationException(String.format("Plugin '%s' has no container", pluginKey));
            }

            @Override
            public <T> T injectBean(T bean) {
                throw new UnsupportedOperationException(String.format("Plugin '%s' has no container", pluginKey));
            }

            @Override
            public <T> T getBean(String id) {
                throw new UnsupportedOperationException(String.format("Plugin '%s' has no container", pluginKey));
            }

            @Override
            public <T> Collection<T> getBeansOfType(Class<T> interfaceClass) {
                throw new UnsupportedOperationException(String.format("Plugin '%s' has no container", pluginKey));
            }
        };
    }

    /**
     * Determines which plugin keys are dependencies based on tracing the wires, categorising them as mandatory,
     * optional or dynamic.
     * <p>
     * Bundle must be in RESOLVED state at least. If Bundle is in INSTALLED or UNINSTALLED states wires are
     * not existing and method returns empty dependencies set
     *
     * {@link PackageNamespace#RESOLUTION_OPTIONAL} and {@link PackageNamespace#RESOLUTION_DYNAMIC} are mapped to
     * optional and dynamic, respectively. Any others are mapped to mandatory.
     *
     * @return not null, possibly empty
     * @since 4.0
     */
    public static PluginDependencies getDependencies(final Bundle bundle) {
        int state = bundle.getState();
        if (state == Bundle.INSTALLED || state == Bundle.UNINSTALLED) {
            return EMPTY_DEPS;
        }

        // TODO: replace with bundle.adapt(BundleWiring.class) as that is the way recommended by 
        // OSGi itself
        final PluginDependencies.Builder depsBuilder = PluginDependencies.builder();
        if (bundle instanceof BundleRevisions) {
            for (final BundleRevision bundleRevision : ((BundleRevisions) bundle).getRevisions()) {
                for (final BundleWire requiredWire : bundleRevision.getWiring().getRequiredWires(null)) {
                    final String pluginKey = OsgiHeaderUtil.getPluginKey(requiredWire.getProviderWiring().getBundle());
                    String resolutionDirective = requiredWire.getRequirement().getDirectives().get(PackageNamespace.REQUIREMENT_RESOLUTION_DIRECTIVE);

                    if (resolutionDirective == null) {
                        // mandatory by default
                        resolutionDirective = PackageNamespace.RESOLUTION_MANDATORY;
                    }

                    switch (resolutionDirective) {
                        case PackageNamespace.RESOLUTION_OPTIONAL:
                            depsBuilder.withOptional(pluginKey);
                            break;
                        case PackageNamespace.RESOLUTION_DYNAMIC:
                            depsBuilder.withDynamic(pluginKey);
                            break;
                        default:
                            // mandatory if not specified
                            depsBuilder.withMandatory(pluginKey);
                            break;
                    }
                }
            }
        }

        return depsBuilder.build();
    }
}
