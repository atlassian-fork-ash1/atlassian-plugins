package com.atlassian.plugin.osgi.factory;

import com.atlassian.plugin.Application;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginArtifact;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.factories.AbstractPluginFactory;
import com.atlassian.plugin.impl.UnloadablePlugin;
import com.atlassian.plugin.osgi.container.OsgiContainerManager;
import com.atlassian.plugin.osgi.factory.transform.PluginTransformationException;
import com.atlassian.plugin.parsers.DescriptorParser;
import org.apache.commons.io.IOUtils;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.InputStream;
import java.util.Set;
import java.util.function.Predicate;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Plugin loader that starts an OSGi container and loads plugins into it, wrapped as OSGi bundles. Supports
 * <ul>
 * <li>Dynamic loading of module descriptors via OSGi services</li>
 * <li>Delayed enabling until the plugin container is active</li>
 * <li>XML or Jar manifest configuration</li>
 * </ul>
 *
 * @since 3.0
 */
public final class RemotablePluginFactory extends AbstractPluginFactory {

    private static final Logger log = LoggerFactory.getLogger(RemotablePluginFactory.class);

    private static final Predicate<Integer> IS_PLUGINS_3 = input -> input == Plugin.VERSION_3;

    private final OsgiContainerManager osgi;
    private final String pluginDescriptorFileName;
    private final PluginEventManager pluginEventManager;

    private final OsgiChainedModuleDescriptorFactoryCreator osgiChainedModuleDescriptorFactoryCreator;

    /**
     * Constructor for implementations that want to override the DefaultPluginTransformer with a custom implementation
     */
    public RemotablePluginFactory(String pluginDescriptorFileName, Set<Application> applications, final OsgiContainerManager osgi, PluginEventManager pluginEventManager) {
        super(new OsgiPluginXmlDescriptorParserFactory(), applications);
        this.pluginDescriptorFileName = checkNotNull(pluginDescriptorFileName, "Plugin descriptor is required");
        this.osgi = checkNotNull(osgi, "The OSGi container is required");
        this.pluginEventManager = checkNotNull(pluginEventManager, "The plugin event manager is required");
        this.osgiChainedModuleDescriptorFactoryCreator = new OsgiChainedModuleDescriptorFactoryCreator(osgi::getServiceTracker);
    }

    @Override
    protected InputStream getDescriptorInputStream(PluginArtifact pluginArtifact) {
        return pluginArtifact.getResourceAsStream(pluginDescriptorFileName);
    }

    @Override
    protected Predicate<Integer> isValidPluginsVersion() {
        return IS_PLUGINS_3;
    }

    /**
     * Creates the plugin
     *
     * @param pluginArtifact          the plugin artifact to deploy
     * @param moduleDescriptorFactory The factory for plugin modules
     * @return The instantiated and populated plugin
     * @throws com.atlassian.plugin.PluginParseException If the descriptor cannot be parsed
     * @throws IllegalArgumentException                  If the plugin descriptor isn't found, and the plugin key and bundle version aren't
     *                                                   specified in the manifest
     */
    public Plugin create(PluginArtifact pluginArtifact, ModuleDescriptorFactory moduleDescriptorFactory) throws PluginParseException {
        checkNotNull(pluginArtifact, "The plugin deployment unit is required");
        checkNotNull(moduleDescriptorFactory, "The module descriptor factory is required");

        Plugin plugin;
        InputStream pluginDescriptor = null;
        try {
            pluginDescriptor = pluginArtifact.getResourceAsStream(pluginDescriptorFileName);
            if (pluginDescriptor != null) {
                final ModuleDescriptorFactory combinedFactory = getChainedModuleDescriptorFactory(moduleDescriptorFactory, pluginArtifact);
                final DescriptorParser parser = descriptorParserFactory.getInstance(pluginDescriptor, applications);

                final String pluginKey = parser.getKey();
                final Plugin osgiPlugin = new OsgiPlugin(pluginKey, osgi, pluginArtifact, pluginArtifact, pluginEventManager);

                // Temporarily configure plugin until it can be properly installed
                plugin = parser.configurePlugin(combinedFactory, osgiPlugin);
            } else {
                throw new PluginParseException("Attempt to create Remotable plugin without a plugin descriptor!");
            }
        } catch (PluginTransformationException ex) {
            return reportUnloadablePlugin(pluginArtifact.toFile(), ex);
        } finally {
            IOUtils.closeQuietly(pluginDescriptor);
        }
        return plugin;
    }

    @Override
    public ModuleDescriptor<?> createModule(final Plugin plugin, final Element module, final ModuleDescriptorFactory moduleDescriptorFactory) {
        // see PLUGDEV-179 - this class should be merged into OsgiPluginFactory
        // for now, OsgiPluginFactory will create modules for plugins created by this factory, probably not cool
        return null;
    }

    /**
     * Get a chained module descriptor factory that includes any dynamically available descriptor factories
     *
     * @param originalFactory The factory provided by the host application
     * @param pluginArtifact
     * @return The composite factory
     */
    private ModuleDescriptorFactory getChainedModuleDescriptorFactory(ModuleDescriptorFactory originalFactory, final PluginArtifact pluginArtifact) {
        return osgiChainedModuleDescriptorFactoryCreator.create(pluginArtifact::doesResourceExist, originalFactory);
    }

    private Plugin reportUnloadablePlugin(File file, Exception e) {
        log.error("Unable to load plugin: " + file, e);

        UnloadablePlugin plugin = new UnloadablePlugin();
        plugin.setErrorText("Unable to load plugin: " + e.getMessage());
        return plugin;
    }
}
