package com.atlassian.plugin.osgi.hostcomponents;

import com.atlassian.plugin.osgi.hostcomponents.impl.CallingBundleStore;
import org.osgi.framework.Bundle;

/**
 * An accessor to the bundle currently invoking a host component
 *
 * @since 3.1.0
 */
public class CallingBundleAccessor {

    /**
     * @return the bundle currently invoking a host component, or {@code null} if the method call did not come from
     * an OSGi bundle or tracking is not enabled.
     */
    public static Bundle getCallingBundle() {
        return CallingBundleStore.get();
    }
}
