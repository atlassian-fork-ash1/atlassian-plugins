package my;

import com.atlassian.plugin.osgi.StaticHolder;
import org.springframework.beans.CachedIntrospectionResults;

import java.lang.reflect.Field;
import java.util.Set;

public class AcceptedClassLoadersRetriever {
    public AcceptedClassLoadersRetriever() {
        try {
            Field field = CachedIntrospectionResults.class.getDeclaredField("acceptedClassLoaders");
            field.setAccessible(true);
            Set acceptedClassLoaders = (Set) field.get(null);

            StaticHolder.set(acceptedClassLoaders);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
