package com.atlassian.plugin.osgi.module;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.impl.AbstractPlugin;
import com.atlassian.plugin.module.ContainerAccessor;
import com.atlassian.plugin.module.ContainerManagedPlugin;
import com.atlassian.plugin.module.ModuleFactory;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.InputStream;
import java.net.URL;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestBeanPrefixModuleFactory {

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    private ModuleFactory moduleCreator;

    @Before
    public void setUp() {
        moduleCreator = new BeanPrefixModuleFactory();
    }

    @Test
    public void testCreateBeanFailedUsingHostContainer() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Failed to resolve 'someBean'. You cannot use 'bean' prefix with non spring plugins");

        ModuleDescriptor moduleDescriptor = mock(ModuleDescriptor.class);
        final Plugin plugin = mock(Plugin.class);
        when(moduleDescriptor.getPlugin()).thenReturn(plugin);

        moduleCreator.createModule("someBean", moduleDescriptor);
    }

    @Test
    public void testCreateBeanUsingPluginContainer() {
        ModuleDescriptor moduleDescriptor = mock(ModuleDescriptor.class);
        final ContainerAccessor pluginContainerAccessor = mock(ContainerAccessor.class);
        final Plugin plugin = new MockContainerManagedPlugin(pluginContainerAccessor);
        when(moduleDescriptor.getPlugin()).thenReturn(plugin);
        final Object pluginBean = new Object();
        when(pluginContainerAccessor.getBean("someBean")).thenReturn(pluginBean);
        final Object obj = moduleCreator.createModule("someBean", moduleDescriptor);
        verify(pluginContainerAccessor).getBean("someBean");
        assertEquals(obj, pluginBean);
    }

    private class MockContainerManagedPlugin extends AbstractPlugin implements ContainerManagedPlugin {
        private ContainerAccessor containerAccessor;

        MockContainerManagedPlugin(ContainerAccessor containerAccessor) {
            super(null);
            this.containerAccessor = containerAccessor;
        }

        public ContainerAccessor getContainerAccessor() {
            return containerAccessor;
        }

        public boolean isUninstallable() {
            return false;
        }

        public boolean isDeleteable() {
            return false;
        }

        public boolean isDynamicallyLoaded() {
            return false;
        }

        public <T> Class<T> loadClass(final String clazz, final Class<?> callingClass) throws ClassNotFoundException {
            return (Class<T>) Class.forName(clazz);
        }

        public ClassLoader getClassLoader() {
            return null;
        }

        public URL getResource(final String path) {
            return null;
        }

        public InputStream getResourceAsStream(final String name) {
            return null;
        }
    }

}
