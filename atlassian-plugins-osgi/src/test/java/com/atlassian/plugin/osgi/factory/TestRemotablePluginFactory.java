package com.atlassian.plugin.osgi.factory;

import com.atlassian.plugin.JarPluginArtifact;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.XmlPluginArtifact;
import com.atlassian.plugin.event.impl.DefaultPluginEventManager;
import com.atlassian.plugin.osgi.container.OsgiContainerManager;
import com.atlassian.plugin.test.PluginJarBuilder;
import com.atlassian.plugin.test.PluginTestUtils;
import com.google.common.collect.ImmutableMap;
import com.mockobjects.dynamic.C;
import com.mockobjects.dynamic.Mock;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.service.packageadmin.PackageAdmin;
import org.osgi.util.tracker.ServiceTracker;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Dictionary;
import java.util.Hashtable;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestRemotablePluginFactory {
    private RemotablePluginFactory factory;

    private File tmpDir;
    private File jar;
    private OsgiContainerManager osgiContainerManager;
    private Mock mockBundle;
    private Mock mockSystemBundle;

    @Before
    public void setUp() throws IOException {
        tmpDir = PluginTestUtils.createTempDirectory(TestRemotablePluginFactory.class);
        osgiContainerManager = mock(OsgiContainerManager.class);
        ServiceTracker tracker = mock(ServiceTracker.class);
        when(tracker.getServices()).thenReturn(new Object[0]);
        when(osgiContainerManager.getServiceTracker(ModuleDescriptorFactory.class.getName())).thenReturn(tracker);

        factory = new RemotablePluginFactory(PluginAccessor.Descriptor.FILENAME, Collections.emptySet(), osgiContainerManager, new DefaultPluginEventManager());
        jar = new PluginJarBuilder("someplugin").addPluginInformation("plugin.key", "My Plugin", "1.0", 3).build();

        mockBundle = new Mock(Bundle.class);
        final Dictionary<String, String> dict = new Hashtable<>();
        dict.put(Constants.BUNDLE_DESCRIPTION, "desc");
        dict.put(Constants.BUNDLE_VERSION, "1.0");
        mockBundle.matchAndReturn("getHeaders", dict);

        mockSystemBundle = new Mock(Bundle.class);
        final Dictionary<String, String> sysDict = new Hashtable<>();
        sysDict.put(Constants.BUNDLE_DESCRIPTION, "desc");
        sysDict.put(Constants.BUNDLE_VERSION, "1.0");
        mockSystemBundle.matchAndReturn("getHeaders", sysDict);
        mockSystemBundle.matchAndReturn("getLastModified", System.currentTimeMillis());
        mockSystemBundle.matchAndReturn("getSymbolicName", "system.bundle");

        Mock mockSysContext = new Mock(BundleContext.class);
        mockSystemBundle.matchAndReturn("getBundleContext", mockSysContext.proxy());

        mockSysContext.matchAndReturn("getServiceReference", C.ANY_ARGS, null);
        mockSysContext.matchAndReturn("getService", C.ANY_ARGS, new Mock(PackageAdmin.class).proxy());
    }

    @After
    public void tearDown() throws IOException {
        factory = null;
        FileUtils.cleanDirectory(tmpDir);
        jar.delete();
    }

    @Test
    public void testCreateOsgiPlugin() throws PluginParseException {
        String key = "plugin.key";
        mockBundle.expectAndReturn("getSymbolicName", key);
        when(osgiContainerManager.getHostComponentRegistrations()).thenReturn(new ArrayList<>());
        when(osgiContainerManager.getBundles()).thenReturn(new Bundle[]{(Bundle) mockSystemBundle.proxy()});
        final Plugin plugin = factory.create(new JarPluginArtifact(jar), (ModuleDescriptorFactory) new Mock(ModuleDescriptorFactory.class).proxy());
        assertNotNull(plugin);
        assertTrue(plugin instanceof OsgiPlugin);
        assertEquals(plugin.getKey(), key);
    }

    @Test
    public void testCreateOsgiPluginWithBadVersion() throws PluginParseException, IOException {
        jar = new PluginJarBuilder("someplugin").addPluginInformation("plugin.key", "My Plugin", "3.2-rc1").build();
        mockBundle.expectAndReturn("getSymbolicName", "plugin.key");
        when(osgiContainerManager.getHostComponentRegistrations()).thenReturn(new ArrayList<>());
        when(osgiContainerManager.getBundles()).thenReturn(new Bundle[]{(Bundle) mockSystemBundle.proxy()});
        Plugin plugin = factory.create(new JarPluginArtifact(jar), (ModuleDescriptorFactory) new Mock(ModuleDescriptorFactory.class).proxy());
        assertTrue(plugin instanceof OsgiPlugin);
    }

    @Test
    public void testCanLoadWithXmlVersion2() throws PluginParseException, IOException {
        final File plugin = new PluginJarBuilder("loadwithxml").addPluginInformation("foo.bar", "", "1.0", 2).build();
        assertNull(factory.canCreate(new JarPluginArtifact(plugin)));
    }

    @Test
    public void testCanLoadWithXmlVersion3() throws PluginParseException, IOException {
        final File plugin = new PluginJarBuilder("loadwithxml").addPluginInformation("foo.bar", "", "1.0", 3).build();
        assertEquals("foo.bar", factory.canCreate(new JarPluginArtifact(plugin)));
    }

    @Test
    public void testCanLoadWithXmlArtifact() throws PluginParseException, IOException {
        File xmlFile = new File(tmpDir, "plugin.xml");
        FileUtils.writeStringToFile(xmlFile, "<somexml />");
        final String key = factory.canCreate(new XmlPluginArtifact(xmlFile));
        assertNull(key);
    }

    @Test
    public void testCanLoadNoXml() throws PluginParseException, IOException {
        final File plugin = new PluginJarBuilder("loadwithxml").build();
        final String key = factory.canCreate(new JarPluginArtifact(plugin));
        assertNull(key);
    }

    @Test
    public void testCanLoadNoXmlButWithManifestEntry() throws PluginParseException, IOException {
        final File plugin = new PluginJarBuilder("loadwithxml")
                .manifest(new ImmutableMap.Builder<String, String>()
                        .put(OsgiPlugin.ATLASSIAN_PLUGIN_KEY, "somekey")
                        .put(Constants.BUNDLE_VERSION, "1.0")
                        .build())
                .build();
        final String key = factory.canCreate(new JarPluginArtifact(plugin));
        assertNull(key);
    }
}
