package com.atlassian.plugin.osgi.factory;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.PluginState;
import com.atlassian.plugin.descriptors.UnrecognisedModuleDescriptor;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.PluginModuleAvailableEvent;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.osgi.StubServletModuleDescriptor;
import com.atlassian.plugin.osgi.external.ListableModuleDescriptorFactory;
import com.atlassian.plugin.servlet.ServletModuleManager;
import com.atlassian.plugin.servlet.descriptors.ServletModuleDescriptor;
import com.google.common.collect.ImmutableSet;
import org.dom4j.Element;
import org.dom4j.dom.DOMElement;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class TestUnrecognizedModuleDescriptorServiceTrackerCustomizer {
    @Mock
    private PluginEventManager pluginEventManager;
    @Mock
    private OsgiPlugin plugin;
    private UnrecognizedModuleDescriptorServiceTrackerCustomizer instance;
    @Mock
    private Bundle bundle;
    @Mock
    private BundleContext bundleContext;
    @Mock
    private ModuleDescriptor<String> aModuleDescriptor;

    @Before
    public void setUp() throws Exception {
        when(plugin.getBundle()).thenReturn(bundle);
        when(bundle.getBundleContext()).thenReturn(bundleContext);
        when(aModuleDescriptor.getKey()).thenReturn("unique-key");
        instance = new UnrecognizedModuleDescriptorServiceTrackerCustomizer(plugin, pluginEventManager);
    }

    @Test
    public void testGetModuleDescriptorsByDescriptorClass() {
        ModuleFactory moduleFactory = mock(ModuleFactory.class);
        ServletModuleManager servletModuleManager = mock(ServletModuleManager.class);
        StubServletModuleDescriptor stubServletModuleDescriptor =
                new StubServletModuleDescriptor(moduleFactory, servletModuleManager);
        when(plugin.getModuleDescriptors()).thenReturn(
                Collections.<ModuleDescriptor<?>>singleton(stubServletModuleDescriptor));

        List<ServletModuleDescriptor> result = instance.getModuleDescriptorsByDescriptorClass(
                ServletModuleDescriptor.class);
        assertEquals(Collections.<ServletModuleDescriptor>singletonList(stubServletModuleDescriptor), result);
    }

    @Test
    public void testGetModuleDescriptorsByDescriptorClassWithSubclass() {
        ModuleFactory moduleFactory = mock(ModuleFactory.class);
        ServletModuleManager servletModuleManager = mock(ServletModuleManager.class);
        ServletModuleDescriptor servletModuleDescriptor =
                new ServletModuleDescriptor(moduleFactory, servletModuleManager);
        when(plugin.getModuleDescriptors()).thenReturn(
                Collections.<ModuleDescriptor<?>>singleton(servletModuleDescriptor));

        List<StubServletModuleDescriptor> result = instance.getModuleDescriptorsByDescriptorClass(
                StubServletModuleDescriptor.class);
        assertTrue(result.isEmpty());
    }

    @Test
    public void unrecognisedModuleDescriptorIsRecognisedOnceAvailable() throws Exception {
        UnrecognisedModuleDescriptor unrecognised = new UnrecognisedModuleDescriptor();
        unrecognised.setKey("unique-key");
        unrecognised.setPlugin(plugin);
        Element elem = new DOMElement("not-initially-known");

        Map<String, Element> moduleElements = Collections.singletonMap("unique-key", elem);
        when(plugin.getModuleElements()).thenReturn(moduleElements);

        when(plugin.getModuleDescriptors()).thenReturn(
                Collections.<ModuleDescriptor<?>>singleton(unrecognised));

        ListableModuleDescriptorFactory moduleDescriptorFactory = mock(ListableModuleDescriptorFactory.class);
        when(moduleDescriptorFactory.hasModuleDescriptor("not-initially-known")).thenReturn(true);
        ModuleDescriptor md = mock(ModuleDescriptor.class);
        when(moduleDescriptorFactory.getModuleDescriptor("not-initially-known")).thenReturn(md);

        ServiceReference serviceReference = mock(ServiceReference.class);
        when(bundleContext.getService(serviceReference)).thenReturn(moduleDescriptorFactory);
        instance.addingService(serviceReference);

        verify(md).init(plugin, elem);
        verify(plugin).addModuleDescriptor(md);
    }

    @Test
    public void recognisedModuleDescriptorNotRevertedOnShuttingDown() throws Exception {
        Element elem = new DOMElement("not-initially-known");
        Map<String, Element> moduleElements = Collections.singletonMap("unique-key", elem);
        when(plugin.getModuleElements()).thenReturn(moduleElements);

        when(plugin.getModuleDescriptors()).thenReturn(Collections.<ModuleDescriptor<?>>singleton(aModuleDescriptor));
        when(plugin.isFrameworkShuttingDown()).thenReturn(true);
        when(plugin.getPluginState()).thenReturn(PluginState.ENABLED);
        ServiceReference serviceReference = mock(ServiceReference.class);
        ListableModuleDescriptorFactory moduleDescriptorFactory = mock(ListableModuleDescriptorFactory.class);
        when(moduleDescriptorFactory.getModuleDescriptorClasses()).thenReturn(ImmutableSet.of(ModuleDescriptor.class));
        instance.removedService(serviceReference, moduleDescriptorFactory);

        verify(plugin).clearModuleDescriptor(aModuleDescriptor.getKey());
        verify(plugin, never()).addModuleDescriptor(any(ModuleDescriptor.class));
        verify(pluginEventManager, never()).broadcast(instanceOf(PluginModuleAvailableEvent.class));
    }
}
