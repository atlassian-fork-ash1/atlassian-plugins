package com.atlassian.plugin.osgi.util;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.osgi.framework.Bundle;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Enumeration;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestBundleClassLoaderAccessor {

    @Test
    public void testGetResource() {
        Bundle bundle = mock(Bundle.class);
        when(bundle.getResource("foo.txt")).thenReturn(getClass().getClassLoader().getResource("foo.txt"));

        URL url = BundleClassLoaderAccessor.getClassLoader(bundle, null).getResource("foo.txt");
        assertNotNull(url);
    }

    @Test
    public void testGetResourceAsStream() throws IOException {
        Bundle bundle = mock(Bundle.class);
        when(bundle.getResource("foo.txt")).thenReturn(getClass().getClassLoader().getResource("foo.txt"));

        InputStream in = BundleClassLoaderAccessor.getClassLoader(bundle, null).getResourceAsStream("foo.txt");
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        IOUtils.copy(in, out);
        assertTrue(out.toByteArray().length > 0);
    }

    @Test
    public void testGetResources() throws IOException {
        Bundle bundle = mock(Bundle.class);
        when(bundle.getResources("foo.txt")).thenReturn(getClass().getClassLoader().getResources("foo.txt"));

        Enumeration<URL> e = BundleClassLoaderAccessor.getClassLoader(bundle, null).getResources("foo.txt");
        assertNotNull(e);
        assertTrue(e.hasMoreElements());
    }

    @Test
    public void testGetResourcesIfNull() throws IOException {
        Bundle bundle = mock(Bundle.class);
        when(bundle.getResources("foo.txt")).thenReturn(null);

        Enumeration<URL> e = BundleClassLoaderAccessor.getClassLoader(bundle, null).getResources("foo.txt");
        assertNotNull(e);
        assertFalse(e.hasMoreElements());
    }

    @Test
    public void testToStringWithSymbolicName() {
        final Bundle bundle = mock(Bundle.class);
        when(bundle.getBundleId()).thenReturn(42L);
        when(bundle.getSymbolicName()).thenReturn("howdy!");

        final ClassLoader classLoader = BundleClassLoaderAccessor.getClassLoader(bundle, null);
        final String result = classLoader.toString();

        assertThat("has class name", result, containsString("BundleClassLoader"));
        assertThat("has symbolic name", result, containsString("howdy!"));
        assertThat("has bundle ID", result, containsString("[42]"));
    }

    @Test
    public void testToStringWithoutSymbolicName() {
        final Bundle bundle = mock(Bundle.class);
        when(bundle.getBundleId()).thenReturn(42L);

        final ClassLoader classLoader = BundleClassLoaderAccessor.getClassLoader(bundle, null);
        final String result = classLoader.toString();

        assertThat("has class name", result, containsString("BundleClassLoader"));
        assertThat("doesn't use 'null' symbolic name", result, not(containsString("null")));
        assertThat("has bundle ID", result, containsString("[42]"));
    }
}
