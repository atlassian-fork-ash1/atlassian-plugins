package com.atlassian.plugin.osgi.factory.transform.stage;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import com.atlassian.plugin.JarPluginArtifact;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.osgi.AnotherInterface;
import com.atlassian.plugin.osgi.SomeInterface;
import com.atlassian.plugin.osgi.container.OsgiContainerManager;
import com.atlassian.plugin.osgi.factory.transform.TransformContext;
import com.atlassian.plugin.osgi.factory.transform.model.SystemExports;
import com.atlassian.plugin.osgi.hostcomponents.HostComponentRegistration;
import com.atlassian.plugin.test.PluginJarBuilder;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.osgi.framework.ServiceReference;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class TestScanDescriptorForHostClassesStage  {
    @Mock
    private OsgiContainerManager osgiContainerManager;

    @Mock
    private HostComponentRegistration registration;

    @Before
    public void setUp() {
        when(osgiContainerManager.getRegisteredServices()).thenReturn(new ServiceReference[0]);
    }

    @Test
    public void testTransform() throws Exception {
        final File plugin = new PluginJarBuilder("plugin")
                .addFormattedResource("atlassian-plugin.xml",
                        "<atlassian-plugin name='Test Bundle instruction plugin 2' key='test.plugin'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "    </plugin-info>",
                        "    <foo key='bar' class='com.atlassian.plugin.osgi.Foo' />",
                        "</atlassian-plugin>")
                .build();

        ScanDescriptorForHostClassesStage stage = new ScanDescriptorForHostClassesStage();
        SystemExports exports = new SystemExports("com.atlassian.plugin.osgi");
        final TransformContext context = new TransformContext(Collections.emptyList(), exports, new JarPluginArtifact(plugin),
                null, PluginAccessor.Descriptor.FILENAME, osgiContainerManager);
        stage.execute(context);
        assertThat(context.getExtraImports(), hasItem("com.atlassian.plugin.osgi"));
    }

    @Test
    public void testTransformAddsOnlyRequiredInterfaces() throws Exception {
        when(registration.getMainInterfaceClasses()).thenReturn(new Class<?>[]{SomeInterface.class, AnotherInterface.class});
        List<HostComponentRegistration> registrations = Collections.singletonList(registration);

        final File plugin = new PluginJarBuilder("testUpgradeOfBundledPlugin")
                .addFormattedResource("atlassian-plugin.xml",
                        "<atlassian-plugin name='Test' key='hostClass' pluginsVersion='2'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "    </plugin-info>",
                        "    <object key='hostClass' class='com.atlassian.plugin.osgi.HostClassUsingHostComponentConstructor'/>",
                        "</atlassian-plugin>")
                .build();

        ScanDescriptorForHostClassesStage stage = new ScanDescriptorForHostClassesStage();
        SystemExports exports = new SystemExports("com.atlassian.plugin.osgi");
        final TransformContext context = new TransformContext(registrations, exports, new JarPluginArtifact(plugin),
                null, PluginAccessor.Descriptor.FILENAME, osgiContainerManager);
        stage.execute(context);

        List<Class<?>> interfaces = extractRequiredComponentInterfaces(context);
        assertThat(interfaces, contains(SomeInterface.class));
        assertThat(interfaces, not(contains(AnotherInterface.class)));
    }

    @Test
    public void testTransformButPackageInPlugin() throws Exception {
        final File plugin = new PluginJarBuilder("plugin")
                .addFormattedResource("atlassian-plugin.xml",
                        "<atlassian-plugin name='Test Bundle instruction plugin 2' key='test.plugin'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "    </plugin-info>",
                        "    <foo key='bar' class='com.atlassian.plugin.osgi.Foo' />",
                        "</atlassian-plugin>")
                .addResource("com/atlassian/plugin/osgi/", "")
                .addResource("com/atlassian/plugin/osgi/Foo.class", "asd")
                .build();

        ScanDescriptorForHostClassesStage stage = new ScanDescriptorForHostClassesStage();
        SystemExports exports = new SystemExports("com.atlassian.plugin.osgi");
        final TransformContext context = new TransformContext(Collections.emptyList(), exports, new JarPluginArtifact(plugin),
                null, PluginAccessor.Descriptor.FILENAME, osgiContainerManager);
        stage.execute(context);
        assertThat(context.getExtraImports(), not(hasItem("com.atlassian.plugin.osgi")));
    }

    @Test
    public void testTransformIgnoreUnknown() throws Exception {
        final File plugin = new PluginJarBuilder("plugin")
                .addFormattedResource("atlassian-plugin.xml",
                        "<atlassian-plugin name='Test Bundle instruction plugin 2' key='test.plugin'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "    </plugin-info>",
                        "    <foo key='bar' class='blat.Foo' />",
                        "</atlassian-plugin>")
                .build();

        ScanDescriptorForHostClassesStage stage = new ScanDescriptorForHostClassesStage();
        SystemExports exports = new SystemExports("com.atlassian.plugin.osgi");
        final TransformContext context = new TransformContext(Collections.emptyList(), exports, new JarPluginArtifact(plugin),
                null, PluginAccessor.Descriptor.FILENAME, osgiContainerManager);
        stage.execute(context);
        assertThat(context.getExtraImports(), not(hasItem("blat")));
    }

    @Test
    public void testTransformWithHostComponentConstructorReferences() throws Exception {
        when(registration.getMainInterfaceClasses()).thenReturn(new Class<?>[]{SomeInterface.class});
        List<HostComponentRegistration> registrations = new ArrayList<>(1);
        registrations.add(registration);

        final File plugin = new PluginJarBuilder("testUpgradeOfBundledPlugin")
                .addFormattedResource("atlassian-plugin.xml",
                        "<atlassian-plugin name='Test' key='hostClass' pluginsVersion='2'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "    </plugin-info>",
                        "    <object key='hostClass' class='com.atlassian.plugin.osgi.HostClassUsingHostComponentConstructor'/>",
                        "</atlassian-plugin>")
                .build();

        ScanDescriptorForHostClassesStage stage = new ScanDescriptorForHostClassesStage();
        SystemExports exports = new SystemExports("com.atlassian.plugin.osgi");
        final TransformContext context = new TransformContext(registrations, exports, new JarPluginArtifact(plugin),
                null, PluginAccessor.Descriptor.FILENAME, osgiContainerManager);
        stage.execute(context);

        List<Class<?>> interfaces = extractRequiredComponentInterfaces(context);
        assertThat(interfaces, contains(SomeInterface.class));
    }

    @Test
    public void testTransformWithHostComponentSetterReferences() throws Exception {
        when(registration.getMainInterfaceClasses()).thenReturn(new Class<?>[]{SomeInterface.class});
        List<HostComponentRegistration> registrations = new ArrayList<>(1);
        registrations.add(registration);

        final File plugin = new PluginJarBuilder("testUpgradeOfBundledPlugin")
                .addFormattedResource("atlassian-plugin.xml",
                        "<atlassian-plugin name='Test' key='hostClass' pluginsVersion='2'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "    </plugin-info>",
                        "    <object key='hostClass' class='com.atlassian.plugin.osgi.HostClassUsingHostComponentSetter'/>",
                        "</atlassian-plugin>")
                .build();

        ScanDescriptorForHostClassesStage stage = new ScanDescriptorForHostClassesStage();
        SystemExports exports = new SystemExports("com.atlassian.plugin.osgi");
        final TransformContext context = new TransformContext(registrations, exports, new JarPluginArtifact(plugin),
                null, PluginAccessor.Descriptor.FILENAME, osgiContainerManager);
        stage.execute(context);

        List<Class<?>> interfaces = extractRequiredComponentInterfaces(context);
        assertThat(interfaces, contains(SomeInterface.class));
    }

    private List<Class<?>> extractRequiredComponentInterfaces(TransformContext context) {
        return context.getRequiredHostComponents().stream()
                .map(HostComponentRegistration::getMainInterfaceClasses)
                .map(Arrays::asList)
                .flatMap(List::stream)
                .collect(Collectors.toList());
    }
}