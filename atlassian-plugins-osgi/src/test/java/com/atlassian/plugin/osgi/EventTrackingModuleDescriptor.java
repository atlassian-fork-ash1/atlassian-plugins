package com.atlassian.plugin.osgi;

import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;

/**
 *
 */
public class EventTrackingModuleDescriptor extends AbstractModuleDescriptor<Void> {
    private volatile int enabledCount = 0;
    private volatile int disabledCount = 0;

    public EventTrackingModuleDescriptor() {
        super(ModuleFactory.LEGACY_MODULE_FACTORY);
    }

    @Override
    public Void getModule() {
        return null;
    }

    @Override
    public void enabled() {
        super.enabled();
        enabledCount++;
    }

    @Override
    public void disabled() {
        super.disabled();
        disabledCount++;
    }

    public int getEnabledCount() {
        return enabledCount;
    }

    public int getDisabledCount() {
        return disabledCount;
    }
}
