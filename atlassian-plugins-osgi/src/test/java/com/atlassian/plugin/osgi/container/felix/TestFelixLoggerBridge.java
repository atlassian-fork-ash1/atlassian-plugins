package com.atlassian.plugin.osgi.container.felix;

import org.apache.felix.framework.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.osgi.framework.BundleException;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestFelixLoggerBridge {
    @Mock
    private org.slf4j.Logger log;

    @Test
    public void testFrameworkLogInfo() {
        when(log.isInfoEnabled()).thenReturn(true);
        FelixLoggerBridge bridge = new FelixLoggerBridge(log);
        bridge.doLog(null, Logger.LOG_INFO, "foo", null);
        verify(log).info("foo");
    }

    @Test
    public void testClassNotFound() {
        when(log.isInfoEnabled()).thenReturn(true);
        FelixLoggerBridge bridge = new FelixLoggerBridge(log);
        bridge.doLog(null, Logger.LOG_WARNING, "foo", new ClassNotFoundException("foo"));
        verify(log).debug("Class not found in bundle: foo");
    }

    @Test
    public void testClassNotFoundOnDebug() {
        when(log.isInfoEnabled()).thenReturn(true);
        FelixLoggerBridge bridge = new FelixLoggerBridge(log);
        bridge.doLog(null, Logger.LOG_WARNING, "*** foo", new ClassNotFoundException("*** foo", new ClassNotFoundException("bar")));
        verify(log).debug("Class not found in bundle: *** foo");
    }

    @Test
    public void testLameClassNotFound() {
        when(log.isInfoEnabled()).thenReturn(true);
        FelixLoggerBridge bridge = new FelixLoggerBridge(log);
        verify(log).isDebugEnabled();
        verify(log).isInfoEnabled();
        bridge.doLog(null, Logger.LOG_WARNING, "org.springframework.foo", new ClassNotFoundException("org.springframework.foo"));
        verifyNoMoreInteractions(log);
    }

    @Test
    public void testLameClassNotFoundInDebug() {
        when(log.isInfoEnabled()).thenReturn(true);
        FelixLoggerBridge bridge = new FelixLoggerBridge(log);
        verify(log).isDebugEnabled();
        verify(log).isInfoEnabled();
        bridge.doLog(null, Logger.LOG_WARNING, "*** org.springframework.foo",
                new ClassNotFoundException("*** org.springframework.foo", new ClassNotFoundException("org.springframework.foo")));
        verifyNoMoreInteractions(log);
    }

    @Test
    public void testBundleExceptionsAreLogged() {
        when(log.isWarnEnabled()).thenReturn(true);
        FelixLoggerBridge bridge = new FelixLoggerBridge(log);
        bridge.doLog(null, Logger.LOG_WARNING, "message", new BundleException("exception"));
        verify(log).warn("message: exception");
    }
}
