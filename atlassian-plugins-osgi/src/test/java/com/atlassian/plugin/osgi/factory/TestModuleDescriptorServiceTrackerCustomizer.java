package com.atlassian.plugin.osgi.factory;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.PluginModuleAvailableEvent;
import com.atlassian.plugin.event.events.PluginModuleUnavailableEvent;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestModuleDescriptorServiceTrackerCustomizer {

    @Mock
    private PluginEventManager pluginEventManager;
    @Mock
    private OsgiPlugin plugin;
    @Mock
    private ModuleDescriptorServiceTrackerCustomizer instance;
    @Mock
    private Bundle bundle;
    @Mock
    private BundleContext bundleContext;
    @Mock
    private ModuleDescriptor moduleDescriptor;
    @Mock
    private ServiceReference serviceReference;

    private final String moduleKey = "MODULE_KEY";

    @Before
    public void setUp() {
        when(serviceReference.getBundle()).thenReturn(bundle);
        when(plugin.getBundle()).thenReturn(bundle);
        when(bundle.getBundleContext()).thenReturn(bundleContext);
        when(bundleContext.getService(serviceReference)).thenReturn(moduleDescriptor);
        when(moduleDescriptor.getKey()).thenReturn(moduleKey);
        instance = new ModuleDescriptorServiceTrackerCustomizer(plugin, pluginEventManager);
    }

    @Test
    public void moduleAddedBeforeEnabling() {
        instance.addingService(serviceReference);
        final InOrder inOrder = inOrder(plugin, pluginEventManager);
        inOrder.verify(plugin).addModuleDescriptor(moduleDescriptor);
        inOrder.verify(pluginEventManager).broadcast(any(PluginModuleAvailableEvent.class));
    }

    @Test
    public void moduleRemovedAfterDisabling() {
        instance.removedService(serviceReference, moduleDescriptor);
        final InOrder inOrder = inOrder(plugin, pluginEventManager);
        inOrder.verify(pluginEventManager).broadcast(any(PluginModuleUnavailableEvent.class));
        inOrder.verify(plugin).clearModuleDescriptor(moduleKey);
    }
}