package com.atlassian.plugin.osgi;

public class StaticHolder {
    private static volatile Object value;

    public static <V> void set(V value) {
        StaticHolder.value = value;
    }

    public static <V> V get() {
        return (V) value;
    }
}
