package com.atlassian.plugin.osgi.factory;

import com.atlassian.plugin.PluginState;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.descriptors.RequiresRestart;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.PluginContainerFailedEvent;
import com.atlassian.plugin.event.events.PluginContainerRefreshedEvent;
import com.atlassian.plugin.event.events.PluginRefreshedEvent;
import com.atlassian.plugin.event.events.PluginTransactionEndEvent;
import com.atlassian.plugin.event.events.PluginTransactionStartEvent;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.osgi.container.OsgiContainerException;
import com.atlassian.plugin.osgi.util.OsgiSystemBundleUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.Constants;
import org.osgi.service.packageadmin.PackageAdmin;

import java.util.Dictionary;
import java.util.Hashtable;

import static org.hamcrest.Matchers.arrayContaining;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.hamcrest.MockitoHamcrest.argThat;

@RunWith(MockitoJUnitRunner.Silent.class)
public class TestOsgiPlugin {
    private static final String PLUGIN_KEY = "plugin-key";

    @Mock
    private Bundle bundle;
    @Mock
    private BundleContext bundleContext;
    @Mock
    private Bundle systemBundle;
    @Mock
    private OsgiPluginHelper helper;
    @Mock
    private PluginEventManager pluginEventManager;
    @Mock
    private PackageAdmin packageAdmin;

    private OsgiPlugin plugin;

    @Before
    public void setUp() {
        final Dictionary<String, String> bundleHeaders = new Hashtable<>();
        bundleHeaders.put(Constants.BUNDLE_DESCRIPTION, "desc");
        bundleHeaders.put(Constants.BUNDLE_VERSION, "1.0");
        when(bundle.getHeaders()).thenReturn(bundleHeaders);
        when(bundle.getBundleContext()).thenReturn(bundleContext);

        when(bundleContext.getBundle(OsgiSystemBundleUtil.SYSTEM_BUNDLE_ID)).thenReturn(systemBundle);

        final BundleContext systemBundleContext = mock(BundleContext.class);
        when(systemBundle.getBundleContext()).thenReturn(systemBundleContext);

        when(helper.getBundle()).thenReturn(bundle);

        plugin = new OsgiPlugin(PLUGIN_KEY, pluginEventManager, helper, packageAdmin);
    }

    @After
    public void tearDown() {
        bundle = null;
        plugin = null;
        bundleContext = null;
    }

    @Test
    public void testEnabled() throws BundleException {
        _enablePlugin();
        verify(bundle).start();
    }

    private void _enablePlugin() {
        when(bundle.getState()).thenReturn(Bundle.RESOLVED);
        plugin.enable();
    }

    @Test
    public void testDisabled() throws BundleException {
        when(bundle.getState()).thenReturn(Bundle.ACTIVE);
        plugin.disable();
        verify(bundle).stop();
    }

    @Test
    public void testDisabledOnNonDynamicPlugin() throws BundleException {
        plugin.addModuleDescriptor(new StaticModuleDescriptor());
        plugin.onPluginFrameworkStartedEvent(null);
        when(bundle.getState()).thenReturn(Bundle.ACTIVE);
        plugin.disable();
        verify(bundle, never()).stop();
    }

    @Test
    public void testUninstall() throws BundleException {
        _enablePlugin();
        when(bundle.getState()).thenReturn(Bundle.ACTIVE);
        _assertPluginUninstalled();
    }

    @Test
    public void testUninstallingUninstalledBundle() throws BundleException {
        _enablePlugin();
        when(bundle.getState()).thenReturn(Bundle.ACTIVE).thenReturn(Bundle.UNINSTALLED);
        _assertPluginUninstalled();
        _assertPluginUninstalled();
    }

    private void _assertPluginUninstalled() {
        plugin.uninstallInternal();
        assertEquals(plugin.getPluginState(), PluginState.UNINSTALLED);
    }

    @Test
    public void testOnPluginContainerRefresh() {
        _enablePlugin();
        assertEquals(PluginState.ENABLING, plugin.getPluginState());
        final PluginContainerRefreshedEvent event = new PluginContainerRefreshedEvent(new Object(), PLUGIN_KEY);
        plugin.onPluginContainerRefresh(event);
        verify(pluginEventManager, times(0)).broadcast(ArgumentMatchers.any());
        assertEquals(PluginState.ENABLED, plugin.getPluginState());
        //again
        plugin.onPluginContainerRefresh(event);
        final ArgumentCaptor<Object> argumentCaptor = ArgumentCaptor.forClass(Object.class);
        verify(pluginEventManager, times(3)).broadcast(argumentCaptor.capture());
        assertThat(argumentCaptor.getAllValues().get(0), is(instanceOf(PluginTransactionStartEvent.class)));
        assertThat(argumentCaptor.getAllValues().get(1), is(instanceOf(PluginRefreshedEvent.class)));
        assertThat(argumentCaptor.getAllValues().get(2), is(instanceOf(PluginTransactionEndEvent.class)));
        final PluginTransactionEndEvent pluginTransactionEndEvent = (PluginTransactionEndEvent)argumentCaptor.getAllValues().get(2);
        assertThat(pluginTransactionEndEvent.getEvents(), hasSize(1));
        assertTrue(pluginTransactionEndEvent.hasAnyEventOfTypeMatching(PluginRefreshedEvent.class, pluginRefreshedEvent -> true));
    }

    @Test
    public void testQuickOnPluginContainerRefresh() throws BundleException, InterruptedException {
        when(bundle.getState()).thenReturn(Bundle.RESOLVED);

        final ConcurrentStateEngine states = new ConcurrentStateEngine(
                "bundle-starting", "container-created", "bundle-started", "mid-start", "end");
        when(bundle.getBundleContext()).thenAnswer(new Answer<Object>() {
            public Object answer(final InvocationOnMock invocation) throws Throwable {
                states.tryNextState("bundle-started", "mid-start");
                final BundleContext context = mock(BundleContext.class);
                when(context.getBundle(OsgiSystemBundleUtil.SYSTEM_BUNDLE_ID)).thenReturn(systemBundle);

                return context;
            }
        });

        doAnswer(new Answer<Object>() {
            public Object answer(final InvocationOnMock invocation) throws Throwable {
                states.state("bundle-starting");
                final Thread t = new Thread() {
                    public void run() {
                        final PluginContainerRefreshedEvent event = new PluginContainerRefreshedEvent(
                                new Object(), PLUGIN_KEY);
                        states.tryNextState("bundle-starting", "container-created");
                        plugin.onPluginContainerRefresh(event);
                    }
                };
                t.start();
                states.tryNextState("container-created", "bundle-started");
                return null;
            }
        }).when(bundle).start();

        plugin.enable();


        states.tryNextState("mid-start", "end");

        assertEquals(PluginState.ENABLED, plugin.getPluginState());
    }

    @Test
    public void testOnPluginContainerRefreshNotEnabling() {
        final PluginContainerRefreshedEvent event = new PluginContainerRefreshedEvent(new Object(), PLUGIN_KEY);
        when(bundle.getState()).thenReturn(Bundle.ACTIVE);
        plugin.disable();
        plugin.onPluginContainerRefresh(event);
        assertEquals(PluginState.DISABLED, plugin.getPluginState());
    }

    @Test
    public void testUninstallRetryLogic() throws BundleException {
        try {
            when(bundle.getState()).thenReturn(Bundle.ACTIVE);
            plugin.enable();
            when(bundle.getSymbolicName()).thenReturn("Mock Bundle");
            doThrow(new BundleException("Mock Bundle Exception")).when(bundle).uninstall();
            plugin.uninstallInternal();
        } catch (final Exception e) {
            assertTrue("Should throw an OsgiContainerException", e instanceof OsgiContainerException);
        } finally {
            assertEquals(PluginState.ENABLED, plugin.getPluginState());
            verify(bundle, times(3)).uninstall();
        }
    }

    @Test
    public void recoverFromPartialUninstall() throws Exception {
        // This test checks a code path that is unreachable if (a) only plugin code uninstalls bundles,
        // and (b) felix satisfies the OSGi postconditions for {@link Bundle#uninstall}. However,
        // since we're adding code to try to diagnose mysterious behaviour, an off the map test is justified.

        _enablePlugin();
        when(bundle.getState()).thenReturn(Bundle.UNINSTALLED);
        _assertPluginUninstalled();
        verify(bundle, never()).uninstall();
        verify(helper).onUninstall();
        verify(pluginEventManager).unregister(plugin);
    }

    @Test
    public void resolveInvokesBundleResolution() {
        plugin.resolve();
        verify(packageAdmin).resolveBundles(argThat(arrayContaining(bundle)));
    }

    @Test
    public void disableOnSpringTimeout() {
        final PluginContainerFailedEvent pluginContainerFailedEvent = mock(PluginContainerFailedEvent.class);
        when(pluginContainerFailedEvent.getPluginKey()).thenReturn(PLUGIN_KEY);

        _enablePlugin();
        plugin.onPluginContainerFailed(pluginContainerFailedEvent);

        assertThat(plugin.getPluginState(), is(PluginState.DISABLED));
        assertThat(plugin.getBundle().getState(), is(Bundle.RESOLVED));
    }

    @RequiresRestart
    public static class StaticModuleDescriptor extends AbstractModuleDescriptor<Object> {
        public StaticModuleDescriptor() {
            super(ModuleFactory.LEGACY_MODULE_FACTORY);
        }

        public Object getModule() {
            return null;
        }
    }
}
