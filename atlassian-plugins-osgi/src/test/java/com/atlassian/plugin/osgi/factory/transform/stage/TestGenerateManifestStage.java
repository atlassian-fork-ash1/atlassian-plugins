package com.atlassian.plugin.osgi.factory.transform.stage;

import com.atlassian.plugin.JarPluginArtifact;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.osgi.container.OsgiContainerManager;
import com.atlassian.plugin.osgi.factory.OsgiPlugin;
import com.atlassian.plugin.osgi.factory.transform.TransformContext;
import com.atlassian.plugin.osgi.factory.transform.model.SystemExports;
import com.atlassian.plugin.test.CapturedLogging;
import com.atlassian.plugin.test.PluginJarBuilder;
import com.google.common.collect.ImmutableMap;
import org.apache.log4j.spi.Filter;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.RestoreSystemProperties;
import org.junit.rules.ExpectedException;
import org.osgi.framework.Constants;
import org.osgi.framework.ServiceReference;

import javax.print.attribute.AttributeSet;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import static com.atlassian.plugin.test.CapturedLogging.didLogWarn;
import static com.atlassian.plugin.util.PluginUtils.ATLASSIAN_DEV_MODE;
import static com.atlassian.plugin.util.PluginUtils.ATLASSIAN_PLUGINS_ENABLE_WAIT;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestGenerateManifestStage {
    @Rule
    public final RestoreSystemProperties restoreSystemProperties = new RestoreSystemProperties();
    @Rule
    public final ExpectedException expectedException = ExpectedException.none();
    @Rule
    public final CapturedLogging capturedLogging = new CapturedLogging(GenerateManifestStage.class);

    private GenerateManifestStage stage;
    private OsgiContainerManager osgiContainerManager;

    @Before
    public void setUp() {
        stage = new GenerateManifestStage();
        osgiContainerManager = mock(OsgiContainerManager.class);
        when(osgiContainerManager.getRegisteredServices()).thenReturn(new ServiceReference[0]);
    }

    @Test
    public void testGenerateManifest() throws Exception {
        final File file = new PluginJarBuilder()
                .addFormattedResource(
                        "atlassian-plugin.xml",
                        "<atlassian-plugin key='com.atlassian.plugins.example' name='Example Plugin'>",
                        "  <plugin-info>",
                        "    <description>",
                        "      A sample plugin for demonstrating the file format.",
                        "    </description>",
                        "    <version>1.1</version>",
                        "    <vendor name='Atlassian Software Systems Pty Ltd' url='http://www.atlassian.com'/>",
                        "  </plugin-info>",
                        "</atlassian-plugin>")
                .addFormattedJava(
                        "com.mycompany.myapp.Foo",
                        "package com.mycompany.myapp; public class Foo {}")
                .build();

        final TransformContext context = getTransformContext(file, SystemExports.NONE);
        context.setShouldRequireSpring(true);

        final Attributes attrs = executeStage(context);

        assertThat(attrs.getValue(Constants.BUNDLE_VERSION), is("1.1"));
        assertThat(attrs.getValue(Constants.BUNDLE_SYMBOLICNAME), is("com.atlassian.plugins.example"));
        assertThat(attrs.getValue(Constants.BUNDLE_DESCRIPTION), is("A sample plugin for demonstrating the file format."));
        assertThat(attrs.getValue(Constants.BUNDLE_VENDOR), is("Atlassian Software Systems Pty Ltd"));
        assertThat(attrs.getValue(Constants.BUNDLE_DOCURL), is("http://www.atlassian.com"));
        assertThat(attrs.getValue(Constants.EXPORT_PACKAGE), nullValue());
        assertThat(attrs.getValue(Constants.BUNDLE_CLASSPATH), nullValue());
        assertThat(attrs.getValue(Constants.REQUIRE_CAPABILITY), nullValue());
        assertThat(attrs.getValue(OsgiPlugin.ATLASSIAN_PLUGIN_KEY), is("com.atlassian.plugins.example"));
        assertThat(attrs.getValue("Spring-Context"), is("*;timeout:=300"));
        assertThat(attrs.getValue(Constants.IMPORT_PACKAGE), nullValue());
        assertThat(attrs.containsKey("Bnd-LastModified"), is(false));
    }

    @Test
    public void testGenerateManifestWithProperInferredImports() throws Exception {

        final File file = new PluginJarBuilder().addPluginInformation("someKey", "someName", "1.0").build();
        final TransformContext context = getTransformContext(file, SystemExports.NONE);
        context.getExtraImports().add(AttributeSet.class.getPackage().getName());
        final Attributes attrs = executeStage(context);

        assertThat(attrs.getValue(Constants.IMPORT_PACKAGE), containsString(AttributeSet.class.getPackage().getName()));

    }

    @Test
    public void testGenerateManifestWithCustomTimeout() throws Exception {
        System.setProperty(ATLASSIAN_PLUGINS_ENABLE_WAIT, "333");
        stage = new GenerateManifestStage();
        final File file = new PluginJarBuilder().addPluginInformation("someKey", "someName", "1.0").build();
        final TransformContext context = getTransformContext(file, SystemExports.NONE);
        context.setShouldRequireSpring(true);
        final Attributes attrs = executeStage(context);

        assertThat(attrs.getValue("Spring-Context"), is("*;timeout:=333"));
    }

    @Test
    public void testGenerateManifestWithExistingSpringContextTimeout() throws Exception {
        System.setProperty(ATLASSIAN_PLUGINS_ENABLE_WAIT, "333");
        stage = new GenerateManifestStage();
        final File file = new PluginJarBuilder().addPluginInformation("someKey", "someName", "1.0")
                .addFormattedResource(
                        "META-INF/MANIFEST.MF",
                        "Manifest-Version: 1.0",
                        "Spring-Context: *;timeout:=60",
                        "Bundle-Version: 4.2.0.jira40",
                        "Bundle-SymbolicName: my.foo.symbolicName")
                .build();
        final TransformContext context = getTransformContext(file, SystemExports.NONE);
        context.setShouldRequireSpring(true);
        final Attributes attrs = executeStage(context);

        assertThat(attrs.getValue("Spring-Context"), is("*;timeout:=333"));
    }

    @Test
    public void testGenerateManifestWithExistingSpringContextTimeoutNoSystemProperty() throws Exception {
        stage = new GenerateManifestStage();
        final File file = new PluginJarBuilder().addPluginInformation("someKey", "someName", "1.0")
                .addFormattedResource("META-INF/MANIFEST.MF",
                        "Manifest-Version: 1.0",
                        "Spring-Context: *;timeout:=60",
                        "Bundle-Version: 4.2.0.jira40",
                        "Bundle-SymbolicName: my.foo.symbolicName")
                .build();
        final TransformContext context = getTransformContext(file, SystemExports.NONE);
        context.setShouldRequireSpring(true);
        final Attributes attrs = executeStage(context);

        assertThat(attrs.getValue("Spring-Context"), is("*;timeout:=60"));
    }

    @Test
    public void testGenerateManifestSpringContextTimeoutNoTimeoutInHeader() throws Exception {
        System.setProperty(ATLASSIAN_PLUGINS_ENABLE_WAIT, "789");
        stage = new GenerateManifestStage();
        final File file = new PluginJarBuilder().addPluginInformation("someKey", "someName", "1.0")
                .addFormattedResource("META-INF/MANIFEST.MF",
                        "Manifest-Version: 1.0",
                        "Spring-Context: *;create-asynchronously:=false",
                        "Bundle-Version: 4.2.0.jira40",
                        "Bundle-SymbolicName: my.foo.symbolicName")
                .build();
        final TransformContext context = getTransformContext(file, SystemExports.NONE);
        context.setShouldRequireSpring(true);
        final Attributes attrs = executeStage(context);
        assertThat(attrs.getValue("Spring-Context"), is("*;create-asynchronously:=false;timeout:=789"));
    }

    @Test
    public void testGenerateManifestSpringContextTimeoutTimeoutAtTheBeginning() throws Exception {
        System.setProperty(ATLASSIAN_PLUGINS_ENABLE_WAIT, "789");
        stage = new GenerateManifestStage();
        final File file = new PluginJarBuilder().addPluginInformation("someKey", "someName", "1.0")
                .addFormattedResource("META-INF/MANIFEST.MF",
                        "Manifest-Version: 1.0",
                        "Spring-Context: timeout:=123;config/account-data-context.xml;create-asynchrously:=false",
                        "Bundle-Version: 4.2.0.jira40",
                        "Bundle-SymbolicName: my.foo.symbolicName")
                .build();
        final TransformContext context = getTransformContext(file, SystemExports.NONE);
        context.setShouldRequireSpring(true);
        final Attributes attrs = executeStage(context);
        assertThat(attrs.getValue("Spring-Context"), is("timeout:=789;config/account-data-context.xml;create-asynchrously:=false"));
    }

    @Test
    public void testGenerateManifestSpringContextTimeoutTimeoutInTheMiddle() throws Exception {
        System.setProperty(ATLASSIAN_PLUGINS_ENABLE_WAIT, "789");
        stage = new GenerateManifestStage();
        final File file = new PluginJarBuilder().addPluginInformation("someKey", "someName", "1.0")
                .addFormattedResource("META-INF/MANIFEST.MF",
                        "Manifest-Version: 1.0",
                        "Spring-Context: config/account-data-context.xml;timeout:=123;create-asynchrously:=false",
                        "Bundle-Version: 4.2.0.jira40",
                        "Bundle-SymbolicName: my.foo.symbolicName")
                .build();
        final TransformContext context = getTransformContext(file, SystemExports.NONE);
        context.setShouldRequireSpring(true);
        final Attributes attrs = executeStage(context);
        assertThat(attrs.getValue("Spring-Context"), is("config/account-data-context.xml;timeout:=789;create-asynchrously:=false"));
    }


    @Test
    public void testGenerateManifestMergeHostComponentImportsWithExisting() throws Exception {
        final File plugin = new PluginJarBuilder("plugin")
                .addFormattedResource("META-INF/MANIFEST.MF",
                        "Manifest-Version: 1.0",
                        "Import-Package: javax.swing, javax.swing",
                        "Bundle-SymbolicName: my.foo.symbolicName",
                        "Bundle-Version: 1.0",
                        "Bundle-ClassPath: .,foo")
                .addResource("foo/bar.txt", "Something")
                .addPluginInformation("innerjarcp", "Some name", "1.0")
                .build();

        final TransformContext context = getTransformContext(plugin, SystemExports.NONE);
        context.getExtraImports().add(AttributeSet.class.getPackage().getName());
        final Attributes attrs = executeStage(context);
        assertThat(attrs.getValue(Constants.BUNDLE_SYMBOLICNAME), is("my.foo.symbolicName"));
        assertThat(attrs.getValue(Constants.BUNDLE_CLASSPATH), is(".,foo"));
        assertThat(attrs.getValue(OsgiPlugin.ATLASSIAN_PLUGIN_KEY), is("innerjarcp"));
        final String importPackage = attrs.getValue(Constants.IMPORT_PACKAGE);
        assertThat(importPackage, containsString(AttributeSet.class.getPackage().getName()));
        assertThat(importPackage, containsString("javax.swing"));
        assertThat(importPackage, not(containsString("~")));
    }

    @Test
    public void testGenerateManifestInvalidVersionWithExisting() throws Exception {
        final File plugin = new PluginJarBuilder("plugin")
                .addFormattedResource("META-INF/MANIFEST.MF",
                        "Manifest-Version: 1.0",
                        "Bundle-SymbolicName: my.foo.symbolicName",
                        "Bundle-Version: beta1",
                        "Bundle-ClassPath: .,foo\n")
                .addResource("foo/bar.txt", "Something")
                .addPluginInformation("innerjarcp", "Some name", "1.0")
                .build();

        final TransformContext context = getTransformContext(plugin, SystemExports.NONE);
        expectedException.expect(PluginParseException.class);
        executeStage(context);
    }

    @Test
    public void testGenerateManifestInvalidVersion() throws Exception {
        final File plugin = new PluginJarBuilder("plugin")
                .addPluginInformation("innerjarcp", "Some name", "beta1.0")
                .build();

        final TransformContext context = getTransformContext(plugin, SystemExports.NONE);
        expectedException.expect(PluginParseException.class);
        executeStage(context);
    }

    @Test
    public void testGenerateManifestWithExistingManifestNoSpringButDescriptor() throws Exception {
        final File plugin = new PluginJarBuilder("plugin")
                .addFormattedResource("META-INF/MANIFEST.MF",
                        "Manifest-Version: 1.0",
                        "Bundle-SymbolicName: my.foo.symbolicName",
                        "Bundle-Version: 1.0",
                        "Bundle-ClassPath: .,foo")
                .addResource("foo/bar.txt", "Something")
                .addPluginInformation("innerjarcp", "Some name", "1.0")
                .build();

        final TransformContext context = getTransformContext(plugin, SystemExports.NONE);
        final Attributes attrs = executeStage(context);
        assertThat(attrs.getValue("Atlassian-Plugin-Key"), is("innerjarcp"));
        assertThat(attrs.getValue("Spring-Context"), notNullValue());
    }

    @Test
    public void testThatGeneratingManifestWithExistingManifestWithSimilarSpringAndAtlassianPluginKeyDoesNotRecreateTheManifest()
            throws Exception {
        final File file = new PluginJarBuilder().addPluginInformation("someKey", "someName", "1.0")
                .addFormattedResource("META-INF/MANIFEST.MF",
                        "Manifest-Version: 1.0",
                        "Spring-Context: *;timeout:=60",
                        "Atlassian-Plugin-Key: someKey",
                        "Bundle-Version: 4.2.0.jira40",
                        "Bundle-SymbolicName: my.foo.symbolicName")
                .build();

        final TransformContext context = getTransformContext(file, SystemExports.NONE);
        final Attributes attrs = executeStage(context);
        assertThat(attrs.getValue("Atlassian-Plugin-Key"), is("someKey"));
        assertThat(attrs.getValue("Spring-Context"), is("*;timeout:=60"));
        assertThat(context.getFileOverrides().get("META-INF/MANIFEST.MF"), nullValue());
    }

    @Test
    public void testThatGeneratingManifestWithExistingManifestWithDifferentSpringTimeoutRecreatesTheManifest() throws Exception {
        System.setProperty(ATLASSIAN_PLUGINS_ENABLE_WAIT, "333");
        stage = new GenerateManifestStage();
        final File file = new PluginJarBuilder().addPluginInformation("someKey", "someName", "1.0")
                .addFormattedResource("META-INF/MANIFEST.MF",
                        "Manifest-Version: 1.0",
                        "Spring-Context: *;timeout:=60",
                        "Atlassian-Plugin-Key: someKey",
                        "Bundle-Version: 4.2.0.jira40",
                        "Bundle-SymbolicName: my.foo.symbolicName")
                .build();

        final TransformContext context = getTransformContext(file, SystemExports.NONE);
        final Attributes attrs = executeStage(context);
        assertThat(attrs.getValue("Atlassian-Plugin-Key"), is("someKey"));
        assertThat(attrs.getValue("Spring-Context"), is("*;timeout:=333"));
        assertThat(context.getFileOverrides().get("META-INF/MANIFEST.MF"), notNullValue());
    }

    @Test
    public void testThatGeneratingManifestWithExistingManifestWithDifferentAtlassianPluginKeyRecreatesTheManifest() throws Exception {
        final File file = new PluginJarBuilder().addPluginInformation("someKey", "someName", "1.0")
                .addFormattedResource("META-INF/MANIFEST.MF",
                        "Manifest-Version: 1.0",
                        "Spring-Context: *;timeout:=60",
                        "Atlassian-Plugin-Key: anotherKey",
                        "Bundle-Version: 4.2.0.jira40",
                        "Bundle-SymbolicName: my.foo.symbolicName")
                .build();

        final TransformContext context = getTransformContext(file, SystemExports.NONE);
        final Attributes attrs = executeStage(context);
        assertThat(attrs.getValue("Atlassian-Plugin-Key"), is("someKey"));
        assertThat(attrs.getValue("Spring-Context"), is("*;timeout:=60"));
        assertThat(context.getFileOverrides().get("META-INF/MANIFEST.MF"), notNullValue());
    }

    @Test
    public void testGenerateManifestWithExistingManifestWithSpringWithDescriptor() throws Exception {
        final File plugin = new PluginJarBuilder("plugin")
                .addFormattedResource("META-INF/MANIFEST.MF",
                        "Manifest-Version: 1.0",
                        "Bundle-SymbolicName: my.foo.symbolicName",
                        "Bundle-Version: 1.0",
                        "Spring-Context: *",
                        "Bundle-ClassPath: .,foo")
                .addResource("foo/bar.txt", "Something")
                .addPluginInformation("innerjarcp", "Some name", "1.0")
                .build();

        final TransformContext context = getTransformContext(plugin, SystemExports.NONE);
        final Attributes attrs = executeStage(context);
        assertThat(attrs.getValue("Atlassian-Plugin-Key"), is("innerjarcp"));
        assertThat(attrs.getValue("Spring-Context"), is("*"));
    }

    @Test
    public void testGenerateManifestNoExistingManifestButDescriptor() throws Exception {
        final File plugin = new PluginJarBuilder("plugin")
                .addResource("foo/bar.txt", "Something")
                .addPluginInformation("innerjarcp", "Some name", "1.0")
                .build();

        final TransformContext context = getTransformContext(plugin, SystemExports.NONE);
        final Attributes attrs = executeStage(context);
        assertThat(attrs.getValue("Atlassian-Plugin-Key"), is("innerjarcp"));
        assertThat(attrs.getValue("Spring-Context"), notNullValue());
    }

    @Test
    public void testGenerateManifestWarnIfTimeoutSpecified() throws Exception {
        System.setProperty(ATLASSIAN_DEV_MODE, "true");
        final File plugin = new PluginJarBuilder("plugin")
                .addFormattedResource("META-INF/MANIFEST.MF",
                        "Manifest-Version: 1.0",
                        "Import-Package: javax.swing",
                        "Bundle-SymbolicName: my.foo.symbolicName",
                        "Bundle-Version: 1.0",
                        "Spring-Context: *;timeout:=60",
                        "Bundle-ClassPath: .,foo")
                .addResource("foo/bar.txt", "Something")
                .addPluginInformation("innerjarcp", "Some name", "1.0")
                .build();

        final TransformContext context = getTransformContext(plugin, SystemExports.NONE);
        context.setShouldRequireSpring(true);
        context.getExtraImports().add(AttributeSet.class.getPackage().getName());
        executeStage(context);
        final String artifactName = context.getPluginArtifact().getName();
        assertThat(capturedLogging, didLogWarn("Use the header 'Spring-Context: *'", "*;timeout:=60", artifactName));
    }

    @Test
    public void testGenerateManifest_innerjarsInImports() throws Exception {
        final File innerJar = new PluginJarBuilder("innerjar")
                .addFormattedJava("my.Foo",
                        "package my;",
                        "import org.apache.log4j.Logger;",
                        "public class Foo{",
                        "   Logger log;",
                        "}")
                .build();
        assertThat(innerJar, notNullValue());
        final File plugin = new PluginJarBuilder("plugin")
                .addJava("my.Bar", "package my;import org.apache.log4j.spi.Filter; public class Bar{Filter log;}")
                .addFile("META-INF/lib/innerjar.jar", innerJar)
                .addPluginInformation("innerjarcp", "Some name", "1.0")
                .build();

        final TransformContext context = getTransformContext(plugin, SystemExports.NONE);
        context.addBundleClasspathJar("META-INF/lib/innerjar.jar");
        final Attributes attrs = executeStage(context);

        assertThat(attrs.getValue(Constants.BUNDLE_VERSION), is("1.0"));
        assertThat(attrs.getValue(Constants.BUNDLE_SYMBOLICNAME), is("innerjarcp"));

        final Collection<String> classpathEntries = Arrays.asList(attrs.getValue(Constants.BUNDLE_CLASSPATH).split(","));
        assertThat(classpathEntries, containsInAnyOrder(".", "META-INF/lib/innerjar.jar"));

        final Collection<String> imports = Arrays.asList(attrs.getValue("Import-Package").split(","));
        assertThat(imports, containsInAnyOrder(
                org.apache.log4j.Logger.class.getPackage().getName() + ";resolution:=\"optional\"",
                Filter.class.getPackage().getName() + ";resolution:=\"optional\""));
    }

    @Test
    public void testGenerateManifestWithBundleInstructions() throws Exception {
        final File plugin = new PluginJarBuilder("plugin")
                .addPluginInformation("test.plugin", "test.plugin", "1.0.0")
                .addJava("foo.MyClass", "package foo; public class MyClass{}")
                .addJava("foo.internal.MyPrivateClass", "package foo.internal; public class MyPrivateClass{}")
                .build();

        final TransformContext context = getTransformContext(plugin, SystemExports.NONE);
        context.getBndInstructions().put("Export-Package", "!*.internal.*,*");
        final Attributes attrs = executeStage(context);
        assertThat(attrs.getValue(Constants.BUNDLE_SYMBOLICNAME), is("test.plugin"));
        assertThat(attrs.getValue(Constants.EXPORT_PACKAGE), is("foo;version=\"1.0.0\""));
    }

    @Test
    public void testGenerateManifestWithHostAndExternalImports() throws Exception {
        final File plugin = new PluginJarBuilder("plugin")
                .addPluginInformation("test.plugin", "test.plugin", "1.0")
                .build();

        final SystemExports exports = new SystemExports("foo.bar,foo.baz;version=\"1.0\"");
        final TransformContext context = getTransformContext(plugin, exports);
        context.getBndInstructions().put("Import-Package", "foo.bar,foo.baz,foo.baz;version=\"99.99\"");
        final Attributes attrs = executeStage(context);
        assertThat(attrs.getValue(Constants.BUNDLE_SYMBOLICNAME), is("test.plugin"));

        final String imports = attrs.getValue(Constants.IMPORT_PACKAGE);
        assertThat(imports, containsString("foo.baz;version=\"[1.0,1.0]\""));
        assertThat(imports, containsString("foo.bar"));
        assertThat(imports, not(containsString("foo.baz;version=\"99.99\"")));
        assertThat(imports, not(containsString("~")));
    }

    @Test
    public void manifestAttributesArePropagatedForNonOsgiBundlePlugins() throws Exception {
        final String atlassianBuildDateKey = "Atlassian-Build-Date";
        final String atlassianBuildDateValue = "2014-05-19T12:48:46-0700";
        final String nonstandardHeaderKey = "Some-Nonstandard-Header";
        final String nonstandardHeaderValue = "Value of the nonstandard header";
        final String pluginKey = "plugin-key";
        final File file = new PluginJarBuilder()
                .addPluginInformation(pluginKey, "plugin name", "1.2.3")
                .manifest(ImmutableMap.<String, String>builder()
                        // This is the actual header of interest in the bug report
                        .put(atlassianBuildDateKey, atlassianBuildDateValue)
                                // And this is just something a bit random to check it's not just Atlassian-* or known
                        .put(nonstandardHeaderKey, nonstandardHeaderValue)
                                // And this is a value we can't override
                        .put(OsgiPlugin.ATLASSIAN_PLUGIN_KEY, "not-" + pluginKey)
                        .build())
                .build();

        final TransformContext context = getTransformContext(file, SystemExports.NONE);

        final Attributes attributes = executeStage(context);

        assertThat(attributes.getValue(atlassianBuildDateKey), is(atlassianBuildDateValue));
        assertThat(attributes.getValue(nonstandardHeaderKey), is(nonstandardHeaderValue));
        assertThat(attributes.getValue(OsgiPlugin.ATLASSIAN_PLUGIN_KEY), is(pluginKey));
    }

    @Test
    public void testGenerateManifestWithScanningEnabled() throws Exception {
        final File file = new PluginJarBuilder()
                .addFormattedResource(
                        "atlassian-plugin.xml",
                        "<atlassian-plugin key='com.atlassian.plugins.example' name='Example Plugin'>",
                        "  <plugin-info>",
                        "    <description>",
                        "      A sample plugin for demonstrating the file format.",
                        "    </description>",
                        "    <version>1.1</version>",
                        "    <vendor name='Atlassian Software Systems Pty Ltd' url='http://www.atlassian.com'/>",
                        "    <scan-modules/>",
                        "  </plugin-info>",
                        "</atlassian-plugin>")
                .build();

        final TransformContext context = getTransformContext(file, SystemExports.NONE);
        final Attributes attrs = executeStage(context);
        assertThat(attrs.getValue("Atlassian-Scan-Folders"), is("META-INF/atlassian"));
    }

    @Test
    public void testGenerateManifestWithScanningEnabledAndFoldersSpecified() throws Exception {
        final File file = new PluginJarBuilder()
                .addFormattedResource(
                        "atlassian-plugin.xml",
                        "<atlassian-plugin key='com.atlassian.plugins.example' name='Example Plugin'>",
                        "  <plugin-info>",
                        "    <description>",
                        "      A sample plugin for demonstrating the file format.",
                        "    </description>",
                        "    <version>1.1</version>",
                        "    <vendor name='Atlassian Software Systems Pty Ltd' url='http://www.atlassian.com'/>",
                        "    <scan-modules>",
                        "       <folder>foo</folder>",
                        "       <folder>bar</folder>",
                        "    </scan-modules>",
                        "  </plugin-info>",
                        "</atlassian-plugin>")
                .build();

        final TransformContext context = getTransformContext(file, SystemExports.NONE);
        final Attributes attrs = executeStage(context);
        assertThat(attrs.getValue("Atlassian-Scan-Folders"), containsString("foo"));
        assertThat(attrs.getValue("Atlassian-Scan-Folders"), containsString("bar"));
        assertThat(attrs.getValue("Atlassian-Scan-Folders"), containsString(","));
    }

    private TransformContext getTransformContext(final File plugin, final SystemExports exports) {
        final JarPluginArtifact pluginArtifact = new JarPluginArtifact(plugin);
        return new TransformContext(null, exports, pluginArtifact, null, PluginAccessor.Descriptor.FILENAME, osgiContainerManager);
    }

    private Attributes executeStage(final TransformContext context) throws IOException {
        stage.execute(context);
        final Manifest mf;
        if (context.getFileOverrides().get("META-INF/MANIFEST.MF") != null) {
            mf = new Manifest(new ByteArrayInputStream(context.getFileOverrides().get("META-INF/MANIFEST.MF")));
        } else {
            mf = context.getManifest();
        }
        return mf.getMainAttributes();
    }
}
