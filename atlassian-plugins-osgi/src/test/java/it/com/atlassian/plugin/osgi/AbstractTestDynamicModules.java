package it.com.atlassian.plugin.osgi;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.PluginException;
import com.atlassian.plugin.PluginInternal;
import com.atlassian.plugin.descriptors.UnrecognisedModuleDescriptor;
import com.atlassian.plugin.event.PluginEventListener;
import com.atlassian.plugin.event.events.PluginModuleDisabledEvent;
import com.atlassian.plugin.event.events.PluginModuleDisablingEvent;
import com.atlassian.plugin.event.events.PluginModuleEnabledEvent;
import com.atlassian.plugin.event.events.PluginModuleEnablingEvent;
import com.atlassian.plugin.osgi.PluginInContainerTestBase;
import org.dom4j.Element;
import org.dom4j.dom.DOMElement;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.hamcrest.collection.IsEmptyIterable.emptyIterable;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public abstract class AbstractTestDynamicModules extends PluginInContainerTestBase {
    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    PluginInternal plugin;

    List<Class> moduleEvents;

    abstract String getPluginKey();

    abstract String getModuleKey();

    abstract ModuleDescriptor addModule();

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();

        pluginEventManager.register(this);

        moduleEvents = Collections.synchronizedList(new ArrayList<Class>());
    }

    @Override
    @After
    public void tearDown() throws Exception {
        pluginEventManager.unregister(this);

        super.tearDown();
    }

    @Test
    public void removeNonExistent() {
        final ModuleDescriptor moduleDescriptor = mock(ModuleDescriptor.class);
        when(moduleDescriptor.getKey()).thenReturn("dodgeyModuleKey");

        expectedException.expect(PluginException.class);
        expectedException.expectMessage(containsString("dodgeyModuleKey"));
        expectedException.expectMessage(containsString(getPluginKey()));

        pluginController.removeDynamicModule(plugin, moduleDescriptor);
    }

    @Test
    public void addRemove() throws Exception {
        // no modules, dynamic or otherwise
        assertThat(plugin.getModuleDescriptors(), emptyIterable());
        assertThat(plugin.getDynamicModuleDescriptors(), emptyIterable());
        assertThat(plugin.getModuleDescriptor(getModuleKey()), nullValue());
        assertThat(moduleEvents, empty());

        final ModuleDescriptor moduleDescriptor = addModule();

        // it's added as dynamic and regular
        assertThat(plugin.getDynamicModuleDescriptors(), Matchers.<ModuleDescriptor<?>>containsInAnyOrder(moduleDescriptor));
        assertThat(plugin.getModuleDescriptors(), Matchers.<ModuleDescriptor<?>>containsInAnyOrder(moduleDescriptor));
        assertThat(plugin.getModuleDescriptor(getModuleKey()), Matchers.<ModuleDescriptor<?>>is(moduleDescriptor));

        // should be enabled
        assertThat(moduleDescriptor.isEnabled(), is(true));

        // events were published
        assertThat(moduleEvents, Matchers.<Class>contains(PluginModuleEnablingEvent.class, PluginModuleEnabledEvent.class));

        // may be removed
        pluginController.removeDynamicModule(plugin, moduleDescriptor);

        // no more modules
        assertThat(plugin.getModuleDescriptors(), emptyIterable());
        assertThat(plugin.getDynamicModuleDescriptors(), emptyIterable());
        assertThat(plugin.getModuleDescriptor(getModuleKey()), nullValue());

        // events were published
        assertThat(moduleEvents, Matchers.<Class>contains(PluginModuleEnablingEvent.class, PluginModuleEnabledEvent.class, PluginModuleDisablingEvent.class, PluginModuleDisabledEvent.class));
    }

    @Test
    public void addTwice() {
        addModule();

        // events were published
        assertThat(moduleEvents, Matchers.<Class>contains(PluginModuleEnablingEvent.class, PluginModuleEnabledEvent.class));

        expectedException.expect(PluginException.class);
        expectedException.expectMessage(containsString(getModuleKey()));

        addModule();

        // no further events were published
        assertThat(moduleEvents, Matchers.<Class>contains(PluginModuleEnablingEvent.class, PluginModuleEnabledEvent.class));
    }

    @Test
    public void addDisabled() {
        // add a rubbish module that will be unrecognised
        final Element e = new DOMElement("bazza");
        e.addAttribute("key", "mckenzie");
        final ModuleDescriptor moduleDescriptor = pluginController.addDynamicModule(plugin, e);

        // shouldn't be enabled as it is an UnrecognisedModuleDescriptor
        assertThat(moduleDescriptor.isEnabled(), is(false));
        assertThat(moduleDescriptor, is(instanceOf(UnrecognisedModuleDescriptor.class)));

        // events were not published
        assertThat(moduleEvents, empty());
    }

    @PluginEventListener
    public void onBeforePluginModuleEnabledEvent(PluginModuleEnablingEvent event) {
        if (getModuleKey().equals(event.getModule().getKey())) {
            moduleEvents.add(PluginModuleEnablingEvent.class);
        }
    }

    @PluginEventListener
    public void onPluginModuleEnabledEvent(PluginModuleEnabledEvent event) {
        if (getModuleKey().equals(event.getModule().getKey())) {
            moduleEvents.add(PluginModuleEnabledEvent.class);
        }
    }

    @PluginEventListener
    public void onBeforePluginModuleDisabledEvent(PluginModuleDisablingEvent event) {
        if (getModuleKey().equals(event.getModule().getKey())) {
            moduleEvents.add(PluginModuleDisablingEvent.class);
        }
    }

    @PluginEventListener
    public void onPluginModuleDisableEvent(PluginModuleDisabledEvent event) {
        if (getModuleKey().equals(event.getModule().getKey())) {
            moduleEvents.add(PluginModuleDisabledEvent.class);
        }
    }
}
