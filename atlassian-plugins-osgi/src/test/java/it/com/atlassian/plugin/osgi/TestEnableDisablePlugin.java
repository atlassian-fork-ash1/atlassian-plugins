package it.com.atlassian.plugin.osgi;

import com.atlassian.plugin.DefaultModuleDescriptorFactory;
import com.atlassian.plugin.JarPluginArtifact;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginRestartState;
import com.atlassian.plugin.PluginState;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.descriptors.RequiresRestart;
import com.atlassian.plugin.hostcontainer.DefaultHostContainer;
import com.atlassian.plugin.module.ContainerManagedPlugin;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.osgi.Callable2;
import com.atlassian.plugin.osgi.Callable3;
import com.atlassian.plugin.osgi.PluginInContainerTestBase;
import com.atlassian.plugin.osgi.hostcomponents.ComponentRegistrar;
import com.atlassian.plugin.osgi.hostcomponents.HostComponentProvider;
import com.atlassian.plugin.test.PluginJarBuilder;
import org.junit.Test;
import org.osgi.framework.Bundle;
import org.osgi.util.tracker.ServiceTracker;

import java.io.File;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class TestEnableDisablePlugin extends PluginInContainerTestBase {
    @Test
    public void testEnableDisableEnable() throws Exception {
        File pluginJar = new PluginJarBuilder("enabledisabletest")
                .addPluginInformation("enabledisable", "foo", "1.0")
                .addJava("my.Foo", "package my;" +
                        "public class Foo {}")
                .build();
        initPluginManager(null);
        pluginController.installPlugins(new JarPluginArtifact(pluginJar));
        Plugin plugin = pluginAccessor.getPlugin("enabledisable");
        assertNotNull(((ContainerManagedPlugin) plugin).getContainerAccessor().createBean(plugin.loadClass("my.Foo", this.getClass())));
        pluginController.disablePlugin("enabledisable");
        pluginController.enablePlugins("enabledisable");

        plugin = pluginAccessor.getPlugin("enabledisable");

        assertNotNull(((ContainerManagedPlugin) plugin).getContainerAccessor().createBean(plugin.loadClass("my.Foo", this.getClass())));
    }

    @Test
    public void testEnableDisableEnableWithPublicComponent() throws Exception {
        File pluginJar = new PluginJarBuilder("enabledisabletest")
                .addFormattedResource("atlassian-plugin.xml",
                        "<atlassian-plugin name='Test 2' key='enabledisablewithcomponent' pluginsVersion='2'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "    </plugin-info>",
                        "    <component key='foo' class='my.Foo' public='true' interface='my.Fooable'/>",
                        "</atlassian-plugin>")
                .addJava("my.Fooable", "package my;" +
                        "public interface Fooable {}")
                .addFormattedJava("my.Foo", "package my;",
                        "public class Foo implements Fooable, org.springframework.beans.factory.DisposableBean {",
                        "  public void destroy() throws Exception { Thread.sleep(500); }",
                        "}")
                .build();
        initPluginManager(null);
        pluginController.installPlugins(new JarPluginArtifact(pluginJar));
        Plugin plugin = pluginAccessor.getPlugin("enabledisablewithcomponent");
        assertEquals(PluginState.ENABLED, plugin.getPluginState());
        assertNotNull(((ContainerManagedPlugin) plugin).getContainerAccessor().createBean(plugin.loadClass("my.Foo", this.getClass())));
        pluginController.disablePlugin("enabledisablewithcomponent");
        pluginController.enablePlugins("enabledisablewithcomponent");

        plugin = pluginAccessor.getPlugin("enabledisablewithcomponent");
        assertEquals(PluginState.ENABLED, plugin.getPluginState());

        assertNotNull(((ContainerManagedPlugin) plugin).getContainerAccessor().createBean(plugin.loadClass("my.Foo", this.getClass())));
    }

    @Test
    public void testDisableEnableOfPluginThatRequiresRestart() throws Exception {
        final DefaultModuleDescriptorFactory factory = new DefaultModuleDescriptorFactory(new DefaultHostContainer());
        factory.addModuleDescriptor("requiresRestart", RequiresRestartModuleDescriptor.class);
        new PluginJarBuilder()
                .addFormattedResource("atlassian-plugin.xml",
                        "<atlassian-plugin name='Test 2' key='test.restartrequired' pluginsVersion='2'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "    </plugin-info>",
                        "    <requiresRestart key='foo' />",
                        "</atlassian-plugin>")
                .build(pluginsDir);

        initPluginManager(null, factory);

        assertEquals(1, pluginAccessor.getPlugins().size());
        assertNotNull(pluginAccessor.getPlugin("test.restartrequired"));
        assertTrue(pluginAccessor.isPluginEnabled("test.restartrequired"));
        assertEquals(1, pluginAccessor.getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class).size());
        assertEquals(PluginRestartState.NONE, pluginAccessor.getPluginRestartState("test.restartrequired"));

        pluginController.disablePlugin("test.restartrequired");
        assertFalse(pluginAccessor.isPluginEnabled("test.restartrequired"));
        pluginController.enablePlugins("test.restartrequired");

        assertEquals(1, pluginAccessor.getPlugins().size());
        assertNotNull(pluginAccessor.getPlugin("test.restartrequired"));
        assertTrue(pluginAccessor.isPluginEnabled("test.restartrequired"));
        assertEquals(PluginRestartState.NONE, pluginAccessor.getPluginRestartState("test.restartrequired"));
        assertEquals(1, pluginAccessor.getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class).size());
    }

    @Test
    public void testEnableEnablesDependentPlugins() throws Exception {
        installDependentPlugins();

        Plugin provider = pluginAccessor.getPlugin("provider");
        Plugin consumer = pluginAccessor.getPlugin("consumer");
        assertEquals(PluginState.ENABLED, provider.getPluginState());
        assertEquals(PluginState.ENABLED, consumer.getPluginState());

        pluginController.disablePlugin("provider");
        pluginController.disablePlugin("consumer");

        assertEquals(PluginState.DISABLED, provider.getPluginState());
        assertEquals(PluginState.DISABLED, consumer.getPluginState());

        pluginController.enablePlugins("consumer");
        assertEquals(PluginState.ENABLED, consumer.getPluginState());
        assertEquals(PluginState.ENABLED, provider.getPluginState());
    }

    @Test
    public void testDisablePersistsDisablesOfDependentPlugins() throws Exception {
        installDependentPlugins();

        Plugin provider = pluginAccessor.getPlugin("provider");
        Plugin consumer = pluginAccessor.getPlugin("consumer");
        assertEquals(PluginState.ENABLED, provider.getPluginState());
        assertEquals(PluginState.ENABLED, consumer.getPluginState());

        pluginController.disablePlugin("provider");

        assertFalse("The provider should be disabled as we've disabled it explicitly", store.load().isEnabled(provider));
        assertFalse("The consumer should be disabled as it depends on the provider", store.load().isEnabled(consumer));

    }

    @Test
    public void testStoppedOsgiBundleDetected() throws Exception {
        new PluginJarBuilder("osgi")
                .addFormattedResource("META-INF/MANIFEST.MF",
                        "Manifest-Version: 1.0",
                        "Bundle-SymbolicName: my",
                        "Bundle-Version: 1.0",
                        "")
                .build(pluginsDir);
        initPluginManager();
        Plugin plugin = pluginAccessor.getPlugin("my-1.0");
        assertTrue("Enabled in accessor", pluginAccessor.isPluginEnabled("my-1.0"));
        assertTrue("Enabled in plugin", plugin.getPluginState() == PluginState.ENABLED);

        for (Bundle bundle : osgiContainerManager.getBundles()) {
            if (bundle.getSymbolicName().equals("my")) {
                bundle.stop();
            }
        }

        assertFalse("Disabled in accessor", pluginAccessor.isPluginEnabled("my-1.0"));
        assertTrue("Disabled in plugin", plugin.getPluginState() == PluginState.DISABLED);

    }

    @Test
    public void testStoppedOsgiPluginDetected() throws Exception {
        new PluginJarBuilder("osgi")
                .addPluginInformation("my", "foo", "1.0")
                .build(pluginsDir);
        initPluginManager();
        Plugin plugin = pluginAccessor.getPlugin("my");
        assertTrue(pluginAccessor.isPluginEnabled("my"));
        assertTrue(plugin.getPluginState() == PluginState.ENABLED);

        for (Bundle bundle : osgiContainerManager.getBundles()) {
            if (bundle.getSymbolicName().equals("my")) {
                bundle.stop();
            }
        }

        assertFalse(pluginAccessor.isPluginEnabled("my"));
        assertTrue(plugin.getPluginState() == PluginState.DISABLED);

    }

    @Test
    public void testEnableEnablesDependentPluginsWithBundles() throws Exception {
        PluginJarBuilder builderProvider = new PluginJarBuilder("enabledisable-prov")
                .addFormattedResource("META-INF/MANIFEST.MF",
                        "Manifest-Version: 1.0",
                        "Bundle-SymbolicName: my",
                        "Atlassian-Plugin-Key: provider",
                        "Export-Package: my",
                        "")
                .addJava("my.Foo", "package my;" +
                        "public interface Foo {}");


        PluginJarBuilder builderConsumer = new PluginJarBuilder("enabledisable-con", builderProvider.getClassLoader())
                .addFormattedResource("atlassian-plugin.xml",
                        "<atlassian-plugin name='Test' key='consumer' pluginsVersion='2'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "        <bundle-instructions><Import-Package>my</Import-Package></bundle-instructions>",
                        "    </plugin-info>",
                        "</atlassian-plugin>")
                .addJava("my2.Bar", "package my2;" +
                        "public class Bar implements my.Foo {}");

        initPluginManager(null);
        pluginController.installPlugins(new JarPluginArtifact(builderProvider.build()));
        pluginController.installPlugins(new JarPluginArtifact(builderConsumer.build()));

        Plugin provider = pluginAccessor.getPlugin("provider");
        Plugin consumer = pluginAccessor.getPlugin("consumer");
        assertEquals(PluginState.ENABLED, provider.getPluginState());
        assertEquals(PluginState.ENABLED, consumer.getPluginState());

        pluginController.disablePlugin("provider");
        pluginController.disablePlugin("consumer");

        assertEquals(PluginState.DISABLED, provider.getPluginState());
        assertEquals(PluginState.DISABLED, consumer.getPluginState());

        pluginController.enablePlugins("consumer");
        assertEquals(PluginState.ENABLED, consumer.getPluginState());
        assertEquals(PluginState.ENABLED, provider.getPluginState());
    }

    @Test
    public void testDisableDoesNotKillLongRunningOperation() throws Exception {
        File pluginJar = new PluginJarBuilder("longrunning")
                .addFormattedResource("atlassian-plugin.xml",
                        "<atlassian-plugin name='Test' key='longrunning' pluginsVersion='2'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "    </plugin-info>",
                        "    <component key='comp' class='my.Foo' public='true'>",
                        "       <interface>com.atlassian.plugin.osgi.Callable3</interface>",
                        "    </component>",
                        "</atlassian-plugin>")
                .addFormattedJava("my.Foo",
                        "package my;",
                        "import com.atlassian.plugin.osgi.*;",
                        "public class Foo implements Callable3{",
                        "  private Callable2 callable;",
                        "  public Foo(Callable2 callable) {",
                        "    this.callable = callable;",
                        "  }",
                        "  public String call() throws Exception {",
                        "    Thread.sleep(2000);",
                        "    return callable.call();",
                        "  }",
                        "}")
                .build();
        initPluginManager(new HostComponentProvider() {
            public void provide(ComponentRegistrar registrar) {
                registrar.register(Callable2.class).forInstance(new Callable2() {

                    public String call() {
                        return "called";
                    }
                }).withName("foobar");
            }
        });

        pluginController.installPlugins(new JarPluginArtifact(pluginJar));
        assertTrue(pluginAccessor.getPlugin("longrunning").getPluginState() == PluginState.ENABLED);
        final ServiceTracker tracker = osgiContainerManager.getServiceTracker("com.atlassian.plugin.osgi.Callable3");
        final Callable3 service = (Callable3) tracker.getService();
        final StringBuilder sb = new StringBuilder();
        Thread t = new Thread() {
            public void run() {
                try {
                    sb.append(service.call());
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        };
        t.start();
        pluginController.disablePlugin("longrunning");
        t.join();
        assertEquals("called", sb.toString());
    }

    @RequiresRestart
    public static class RequiresRestartModuleDescriptor extends AbstractModuleDescriptor<Void> {
        public RequiresRestartModuleDescriptor() {
            super(ModuleFactory.LEGACY_MODULE_FACTORY);
        }

        @Override
        public Void getModule() {
            throw new UnsupportedOperationException("You should never be getting a module from this descriptor " + this.getClass().getName());
        }
    }

    private void installDependentPlugins() throws Exception
    {
        PluginJarBuilder builderProvider = new PluginJarBuilder("enabledisable-prov")
                .addFormattedResource("atlassian-plugin.xml",
                        "<atlassian-plugin name='Test' key='provider' pluginsVersion='2'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "        <bundle-instructions><Export-Package>my</Export-Package></bundle-instructions>",
                        "    </plugin-info>",
                        "</atlassian-plugin>")
                .addJava("my.Foo", "package my;" +
                        "public interface Foo {}");

        PluginJarBuilder builderConsumer = new PluginJarBuilder("enabledisable-con", builderProvider.getClassLoader())
                .addFormattedResource("atlassian-plugin.xml",
                        "<atlassian-plugin name='Test' key='consumer' pluginsVersion='2'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "        <bundle-instructions><Import-Package>my</Import-Package></bundle-instructions>",
                        "    </plugin-info>",
                        "</atlassian-plugin>")
                .addJava("my2.Bar", "package my2;" +
                        "public class Bar implements my.Foo {}");

        initPluginManager(null);
        pluginController.installPlugins(new JarPluginArtifact(builderProvider.build()));
        pluginController.installPlugins(new JarPluginArtifact(builderConsumer.build()));
    }
}
