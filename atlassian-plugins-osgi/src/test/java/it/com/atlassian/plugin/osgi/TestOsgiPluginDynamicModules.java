package it.com.atlassian.plugin.osgi;

import com.atlassian.plugin.JarPluginArtifact;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.PluginInternal;
import com.atlassian.plugin.test.PluginJarBuilder;
import org.dom4j.Element;
import org.dom4j.dom.DOMElement;
import org.junit.Before;

import java.io.File;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Tests for addition, enumeration and removal of dynamic modules for an OsgiPlugin
 *
 * Tests wiring from DefaultPluginManager <-> ScanningPluginLoader <-> OsgiPluginFactory
 */
public class TestOsgiPluginDynamicModules extends AbstractTestDynamicModules {
    private static final String PLUGIN_KEY = "pluginKey";
    private static final String PLUGIN_NAME = "pluginName";
    private static final String PLUGIN_VERSION = "3.2.1";

    private static final String MODULE_KEY = "moduleKey";
    private static final String MODULE_CLASS = "moduleClass";

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();

        final File pluginJar = new PluginJarBuilder(PLUGIN_NAME)
                .addPluginInformation(PLUGIN_KEY, PLUGIN_NAME, PLUGIN_VERSION)
                .addJava(MODULE_CLASS, "public class " + MODULE_CLASS + " {}")
                .build();
        initPluginManager(null);

        assertThat(pluginController.installPlugins(new JarPluginArtifact(pluginJar)), contains(PLUGIN_KEY));

        plugin = ((PluginInternal) pluginAccessor.getPlugin(PLUGIN_KEY));
        assertThat("could not retrieve the newly created plugin from pluginManager", plugin, notNullValue());
    }

    @Override
    ModuleDescriptor addModule() {
        // add a component which uses a class in the plugin
        final Element e = new DOMElement("component");
        e.addAttribute("key", MODULE_KEY);
        e.addAttribute("class", MODULE_CLASS);
        return pluginController.addDynamicModule(plugin, e);
    }

    @Override
    String getPluginKey() {
        return PLUGIN_KEY;
    }

    @Override
    String getModuleKey() {
        return MODULE_KEY;
    }
}
