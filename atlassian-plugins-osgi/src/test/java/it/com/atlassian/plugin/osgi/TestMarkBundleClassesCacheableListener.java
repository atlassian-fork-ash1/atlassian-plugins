package it.com.atlassian.plugin.osgi;

import com.atlassian.plugin.osgi.AbstractWaitCondition;
import com.atlassian.plugin.osgi.PluginInContainerTestBase;
import com.atlassian.plugin.osgi.StaticHolder;
import com.atlassian.plugin.test.PluginJarBuilder;
import com.atlassian.plugin.util.WaitUntil;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import my.AcceptedClassLoadersRetriever;
import my.StrongClassCacheRetriever;
import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;
import org.junit.Test;
import org.osgi.framework.BundleReference;

import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;

public class TestMarkBundleClassesCacheableListener extends PluginInContainerTestBase {
    @Test
    public void testBundleClassesAreCacheableInSpring() throws Exception {
        withSystemProperty("atlassian.enable.spring.strong.cache.bean.metadata", "true", new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                preparePluginWithClass(AcceptedClassLoadersRetriever.class);

                initPluginManager();
                final Set<ClassLoader> acceptedClassLoaders = StaticHolder.get();

                assertThat(acceptedClassLoaders,
                        hasModuleClassLoadersWithKeys("test.plugin", "com.atlassian.plugin.osgi.bridge"));

                pluginController.disablePlugin("test.plugin");
                assertThatSoon(acceptedClassLoaders,
                        not(hasModuleClassLoadersWithKeys("test.plugin")));


                pluginController.enablePlugins("test.plugin");
                assertThatSoon(acceptedClassLoaders,
                        hasModuleClassLoadersWithKeys("test.plugin"));

                pluginSystemLifecycle.shutdown();
                assertThat(acceptedClassLoaders, empty());

                return null;
            }
        });
    }

    @Test
    public void testBundleClassesAreNotCacheableByDefaultInSpring() throws Exception {
        preparePluginWithClass(AcceptedClassLoadersRetriever.class);

        initPluginManager();
        final Set<ClassLoader> acceptedClassLoaders = StaticHolder.get();

        assertThat(acceptedClassLoaders, not(hasModuleClassLoadersWithKeys("test.plugin")));

        pluginController.disablePlugin("test.plugin");
        pluginController.enablePlugins("test.plugin");
        assertThatSoon(acceptedClassLoaders, not(hasModuleClassLoadersWithKeys("test.plugin")));

        pluginSystemLifecycle.shutdown();
        assertThat(acceptedClassLoaders, empty());
    }

    @Test
    public void testBundleClassesCacheIsFlushed() throws Exception {
        withSystemProperty("atlassian.enable.spring.strong.cache.bean.metadata.flush", "true", new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                preparePluginWithClass(StrongClassCacheRetriever.class);

                initPluginManager();

                final Map<?, ?> classCache = StaticHolder.get();

                assertThat(classCache.entrySet(), empty());

                return null;
            }
        });
    }

    @Test
    public void testBundleClassesCacheIsNotFlushedByDefault() throws Exception {
        preparePluginWithClass(StrongClassCacheRetriever.class);

        initPluginManager();

        final Map<?, ?> classCache = StaticHolder.get();

        assertThat(classCache.entrySet(), not(empty()));
    }

    private void preparePluginWithClass(Class clazz) throws IOException {
        new PluginJarBuilder("testClassLoaderCachedChecker")
                .addFormattedResource("atlassian-plugin.xml",
                        "<atlassian-plugin name='Test' key='test.plugin' pluginsVersion='2'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "    </plugin-info>",
                        "    <component key='obj' class='" + clazz.getName() + "'/>",
                        "</atlassian-plugin>")
                .addClass(clazz)
                .build(pluginsDir);
    }

    private Matcher<Iterable<ClassLoader>> hasModuleClassLoadersWithKeys(String... keys) {
        Matcher<ClassLoader>[] matchers = Lists.transform(Arrays.asList(keys), new Function<String, Matcher<ClassLoader>>() {
            @Override
            public Matcher<ClassLoader> apply(String key) {
                return classLoaderWithBundleName(key);
            }
        }).toArray(new Matcher[keys.length]);
        return hasItems(matchers);
    }

    private Matcher<ClassLoader> classLoaderWithBundleName(final String bundleName) {
        return new FeatureMatcher<ClassLoader, String>(equalTo(bundleName), "a class loader for bundle", "bundle of a class loader") {
            @Override
            protected String featureValueOf(ClassLoader actual) {
                if (actual instanceof BundleReference) {
                    BundleReference moduleClassLoader = (BundleReference) actual;
                    return moduleClassLoader.getBundle().getSymbolicName();
                }
                return null;
            }
        };
    }

    private <T> T withSystemProperty(String key, String value, Callable<T> action) throws Exception {
        String originalValue = System.setProperty(key, value);
        try {
            return action.call();
        } finally {
            if (originalValue != null) {
                System.setProperty(key, originalValue);
            } else {
                System.clearProperty(key);
            }
        }
    }

    private static <T> void assertThatSoon(final T actual, final Matcher<? super T> matcher) {
        // The concurrency of these tests is difficult to be precise about, because the tracking of the accepted class loaders is
        // done via BundleEvent tracking, and there's not an easy way to "happen after" that.
        WaitUntil.invoke(new AbstractWaitCondition() {
            @Override
            public boolean isFinished() {
                // Thread safety here is because the underlying CachedIntrospectionResults uses Collections.synchronizedSet, and
                // since matcher iterates, we need to synchronize explicitly to avoid ConcurrentModificationException.
                synchronized (actual) {
                    return matcher.matches(actual);
                }
            }
        }, 500, MILLISECONDS, 50);
        assertThat(actual, matcher);
    }
}
