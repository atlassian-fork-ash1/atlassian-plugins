package com.atlassian.plugin.schema.impl;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.Permissions;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.RequirePermission;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.descriptors.CannotDisable;
import com.atlassian.plugin.module.ContainerManagedPlugin;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.osgi.external.ListableModuleDescriptorFactory;
import com.atlassian.plugin.osgi.factory.OsgiPlugin;
import com.atlassian.plugin.schema.descriptor.DescribedModuleDescriptorFactory;
import com.atlassian.plugin.schema.spi.DocumentBasedSchema;
import com.atlassian.plugin.schema.spi.Schema;
import com.atlassian.plugin.schema.spi.SchemaFactory;
import com.atlassian.plugin.schema.spi.SchemaTransformer;
import com.atlassian.plugin.util.resource.AlternativeResourceLoader;
import com.google.common.collect.ImmutableSet;
import org.dom4j.Element;
import org.osgi.framework.BundleContext;

import javax.annotation.Nonnull;
import java.io.InputStream;
import java.net.URL;
import java.util.Collections;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static com.google.common.collect.Iterables.transform;
import static java.util.Optional.ofNullable;

/**
 * Descriptor that allows described module descriptor factories to be configured in XML. Main value
 * is the ability to reuse the name and description of the module descriptor configuration.
 */
@CannotDisable
public final class DescribedModuleTypeModuleDescriptor extends AbstractModuleDescriptor<DescribedModuleDescriptorFactory> {
    private static final String[] PUBLIC_INTERFACES = new String[]{
            ModuleDescriptorFactory.class.getName(),
            ListableModuleDescriptorFactory.class.getName(),
            DescribedModuleDescriptorFactory.class.getName()
    };

    private String schemaFactoryClassName;
    private String type;
    private String schemaTransformerClassName;
    private String maxOccurs;
    private Iterable<String> requiredPermissions;
    private Iterable<String> optionalPermissions;

    public DescribedModuleTypeModuleDescriptor(ModuleFactory moduleFactory) {
        super(moduleFactory);
    }

    @Override
    public void init(@Nonnull Plugin plugin, @Nonnull Element element) throws PluginParseException {
        checkState(plugin instanceof OsgiPlugin, "Described module types can only be declared in OSGi Plugins, %s is not such a plugin", plugin.getKey());

        super.init(plugin, element);

        this.type = getOptionalAttribute(element, "type", getKey());
        this.schemaFactoryClassName = getOptionalAttribute(element, "schema-factory-class", null);
        this.schemaTransformerClassName = getOptionalAttribute(element, "schema-transformer-class", null);
        this.maxOccurs = getOptionalAttribute(element, "max-occurs", "unbounded");
        this.requiredPermissions = getPermissions(element.element("required-permissions"));
        this.optionalPermissions = getPermissions(element.element("optional-permissions"));
    }

    private static Iterable<String> getPermissions(Element element) {
        return ofNullable(element)
                .map(e -> transform(getElements(e, "permission"), Element::getTextTrim))
                .orElseGet(Collections::emptyList);
    }

    @SuppressWarnings("unchecked")
    private static List<Element> getElements(Element element, String name) {
        return element.elements(name);
    }

    @Override
    public void enabled() {
        checkState(plugin instanceof OsgiPlugin, "Described module types can only be declared in OSGi Plugins, %s is not such a plugin", plugin.getKey());

        super.enabled();

        final SchemaTransformer schemaTransformer = schemaTransformerClassName != null
                ? create(findClass(schemaTransformerClassName, SchemaTransformer.class))
                : SchemaTransformer.IDENTITY;

        final Class<? extends ModuleDescriptor> moduleClass = findClass(moduleClassName, ModuleDescriptor.class);

        final SchemaFactory schemaFactory = schemaFactoryClassName != null
                ? create(findClass(schemaFactoryClassName, SchemaFactory.class))
                : buildSingleton(DocumentBasedSchema.builder(type)
                        .setResourceLoader(new AlternativePluginResourceLoader(plugin))
                        .setName(getDisplayName())
                        .setDescription(getDescription() != null ? getDescription() : "")
                        .setTransformer(schemaTransformer)
                        .setMaxOccurs(maxOccurs)
                        .setRequiredPermissions(getModuleRequiredPermissions(moduleClass))
                        .setOptionalPermissions(optionalPermissions)
                        .build()
        );

        @SuppressWarnings("unchecked")
        final DescribedModuleDescriptorFactory factory = new DescribedModuleTypeDescribedModuleDescriptorFactory(
                (ContainerManagedPlugin) plugin,
                type,
                moduleClass,
                schemaFactory);

        getBundleContext().registerService(PUBLIC_INTERFACES, factory, null);
    }

    /**
     * Gets the required module description for the new module descriptor being defined.
     * This adds together the permissions defined in the plugin descriptor, see {@link #requiredPermissions} and the ones
     * defined on the module class itself, see {@link RequirePermission}.
     */
    private Iterable<String> getModuleRequiredPermissions(Class<? extends ModuleDescriptor> moduleClass) {
        return ImmutableSet.<String>builder()
                .addAll(requiredPermissions)
                .addAll(Permissions.getRequiredPermissions(moduleClass))
                .build();
    }

    private BundleContext getBundleContext() {
        return ((OsgiPlugin) plugin).getBundle().getBundleContext();
    }

    private <T> T create(Class<? extends T> type) {
        return ((ContainerManagedPlugin) plugin).getContainerAccessor().createBean(type);
    }

    private <T> Class<? extends T> findClass(String className, Class<T> castTo) {
        checkNotNull(className);
        Class<T> clazz;
        try {
            clazz = plugin.loadClass(className, getClass());
        } catch (ClassNotFoundException e) {
            throw new PluginParseException("Unable to find class " + className);
        }
        return clazz.asSubclass(castTo);
    }

    @Override
    public DescribedModuleDescriptorFactory getModule() {
        return moduleFactory.createModule(moduleClassName, this);
    }

    private SchemaFactory buildSingleton(final Schema schema) {
        return () -> schema;
    }

    public static String getOptionalAttribute(Element e, String name, Object defaultValue) {
        String value = e.attributeValue(name);
        return value != null ? value :
                defaultValue != null ? defaultValue.toString() : null;
    }

    private static final class AlternativePluginResourceLoader implements AlternativeResourceLoader {
        private final Plugin plugin;

        public AlternativePluginResourceLoader(final Plugin plugin) {
            this.plugin = checkNotNull(plugin);
        }

        @Override
        public URL getResource(final String path) {
            return plugin.getResource(path);
        }

        @Override
        public InputStream getResourceAsStream(final String name) {
            return plugin.getResourceAsStream(name);
        }
    }
}
