package com.atlassian.plugin.schema.impl;

import com.atlassian.plugin.DefaultModuleDescriptorFactory;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.schema.descriptor.DescribedModuleDescriptorFactory;
import com.atlassian.plugin.schema.spi.DocumentBasedSchema;
import com.atlassian.plugin.schema.spi.Schema;
import com.atlassian.plugin.util.resource.AlternativeClassLoaderResourceLoader;
import com.google.common.collect.ImmutableSet;

import java.util.Set;

import static com.atlassian.plugin.Permissions.getRequiredPermissions;

public class DefaultDescribedModuleDescriptorFactory extends DefaultModuleDescriptorFactory implements DescribedModuleDescriptorFactory {
    public DefaultDescribedModuleDescriptorFactory(final HostContainer hostContainer) {
        super(hostContainer);
        addModuleDescriptor("described-module-type", DescribedModuleTypeModuleDescriptor.class);
    }

    @Override
    public final Iterable<String> getModuleDescriptorKeys() {
        return ImmutableSet.copyOf(getDescriptorClassesMap().keySet());
    }

    @Override
    public final Set<Class<? extends ModuleDescriptor>> getModuleDescriptorClasses() {
        return ImmutableSet.copyOf(getDescriptorClassesMap().values());
    }

    @Override
    public final Schema getSchema(final String type) {
        if (!getDescriptorClassesMap().containsKey(type)) {
            return null;
        }

        DocumentBasedSchema.DynamicSchemaBuilder builder = DocumentBasedSchema
                .builder(type)
                .setResourceLoader(new AlternativeClassLoaderResourceLoader(this.getClass()))
                .setRequiredPermissions(getRequiredPermissions(getModuleDescriptorClass(type)));
        return builder.validate() ? builder.build() : null;
    }
}
