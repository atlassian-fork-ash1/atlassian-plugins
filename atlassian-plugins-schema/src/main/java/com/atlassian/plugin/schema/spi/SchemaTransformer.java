package com.atlassian.plugin.schema.spi;

import org.dom4j.Document;

/**
 * Transforms a loaded schema
 */
public interface SchemaTransformer {
    SchemaTransformer IDENTITY = document -> document;

    Document transform(Document document);
}
