package com.atlassian.plugin.validation;


import com.atlassian.plugin.Application;
import com.atlassian.plugin.InstallationMode;
import com.atlassian.plugin.parsers.ModuleReader;
import com.atlassian.plugin.parsers.PluginDescriptorReader;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import io.atlassian.fugue.Either;

import java.io.InputStream;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static com.atlassian.plugin.validation.Dom4jUtils.readDocument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.ImmutableSet.copyOf;
import static com.google.common.collect.Iterables.transform;

/**
 * A simple validator that given a descriptor and a schema will check that the permissions set in the plugin are valid
 * and all required have been asked.
 *
 * @since 3.0.0
 */
public final class DescriptorValidator {
    private static final String REMOTE_PLUGIN_CONTAINER_MODULE_NAME = "remote-plugin-container";

    private final PluginDescriptorReader descriptorReader;
    private final SchemaReader schemaReader;

    public DescriptorValidator(InputStream descriptor, InputStream schema, Set<Application> applications) {
        descriptorReader = new PluginDescriptorReader(readDocument(descriptor), copyOf(checkNotNull(applications)));
        schemaReader = new SchemaReader(readDocument(schema));
    }

    public Either<ValidationError, ValidationSuccess> validate(InstallationMode installationMode) {
        final Set<String> allowedPermissions = schemaReader.getAllowedPermissions();
        final Set<String> askedPermissions = descriptorReader.getPluginInformationReader().getPermissions(installationMode);

        final Sets.SetView<String> invalidPermissions = Sets.difference(askedPermissions, allowedPermissions);

        final Set<String> requiredPermissions = getRequiredPermissions(installationMode);
        final Sets.SetView<String> notAskedPermissions = Sets.difference(requiredPermissions, askedPermissions);

        if (!invalidPermissions.isEmpty() || (!descriptorReader.getPluginInformationReader().hasAllPermissions() && !notAskedPermissions.isEmpty())) {
            return Either.left(new ValidationError(invalidPermissions, notAskedPermissions));
        } else {
            return Either.right(new ValidationSuccess(isRemotable(installationMode)));
        }
    }

    private boolean isRemotable(InstallationMode installationMode) {
        final ModuleReader remotePluginContainerModuleReader = Iterables.find(descriptorReader.getModuleReaders(installationMode),
                moduleReader -> moduleReader.getType().equals(REMOTE_PLUGIN_CONTAINER_MODULE_NAME), null);

        return remotePluginContainerModuleReader != null;
    }

    @VisibleForTesting
    Set<String> getRequiredPermissions(InstallationMode installationMode) {
        final Set<String> moduleKeys = getModuleKeys(installationMode);
        final Map<String, Set<String>> modulesRequiredPermissions = schemaReader.getModulesRequiredPermissions();

        return Collections.unmodifiableSet(modulesRequiredPermissions.entrySet().stream()
                .filter(entry -> moduleKeys.contains(entry.getKey()))
                .flatMap(entry -> entry.getValue().stream())
                .collect(Collectors.toSet()));
    }

    private Set<String> getModuleKeys(InstallationMode installationMode) {
        return copyOf(transform(descriptorReader.getModuleReaders(installationMode), ModuleReader::getType));
    }

    public static final class ValidationError {
        private final Set<String> nonValidPermissions;
        private final Set<String> notAskedPermissions;

        private ValidationError(Sets.SetView<String> nonValidPermissions, Sets.SetView<String> notAskedPermissions) {
            this.nonValidPermissions = nonValidPermissions.immutableCopy();
            this.notAskedPermissions = notAskedPermissions.immutableCopy();
        }

        public Set<String> getNonValidPermissions() {
            return nonValidPermissions;
        }

        public Set<String> getNotAskedPermissions() {
            return notAskedPermissions;
        }
    }

    public static final class ValidationSuccess {
        private final boolean remotable;

        private ValidationSuccess(boolean remotable) {
            this.remotable = remotable;
        }

        public boolean isRemotable() {
            return remotable;
        }
    }
}
