package com.atlassian.plugin.validation;

import com.atlassian.plugin.Application;
import com.atlassian.plugin.InstallationMode;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import io.atlassian.fugue.Either;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Set;

import static com.atlassian.plugin.validation.DescriptorValidator.ValidationError;
import static com.atlassian.plugin.validation.DescriptorValidator.ValidationSuccess;
import static com.atlassian.plugin.validation.ResourcesLoader.getInput;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public final class DescriptorValidatorTest {
    @Test
    public void testGetRequiredPermissionsIsEmptyForEmptyDescriptor() throws IOException {
        try (InputStream descriptorStream = getInput("/empty-descriptor.xml");
             InputStream schemaStream = getInput("/schema.xsd")) {
            final DescriptorValidator descriptorValidator = new DescriptorValidator(descriptorStream, schemaStream, getApplications());

            final Set<String> requiredPermissions = descriptorValidator.getRequiredPermissions(InstallationMode.LOCAL);
            assertTrue(requiredPermissions.isEmpty());
        }
    }

    @Test
    public void testGetRequiredPermissionsIsEmptyForDescriptorWithModuleRequiringPermission() throws IOException {
        try (InputStream descriptorStream = getInput("/descriptor-with-module-requiring-exec-java.xml");
             InputStream schemaStream = getInput("/schema.xsd")) {
            final DescriptorValidator descriptorValidator = new DescriptorValidator(descriptorStream, schemaStream, getApplications());

            final Set<String> requiredPermissions = descriptorValidator.getRequiredPermissions(InstallationMode.LOCAL);
            assertEquals(1, requiredPermissions.size());
            assertEquals("execute_java", Iterables.getFirst(requiredPermissions, null));
        }
    }

    @Test
    public void testHasNecessaryPermissionsForEmptyDescriptor() throws IOException {
        try (InputStream descriptorStream = getInput("/empty-descriptor.xml");
             InputStream schemaStream = getInput("/schema.xsd")) {
            final DescriptorValidator descriptorValidator = new DescriptorValidator(descriptorStream, schemaStream, getApplications());

            final Either<ValidationError, ValidationSuccess> validation = descriptorValidator.validate(InstallationMode.LOCAL);
            assertTrue(validation.isRight());
            assertFalse(validation.right().get().isRemotable());
        }
    }

    @Test
    public void testHasNecessaryPermissionsForRemotableDescriptor() throws IOException {
        try (InputStream descriptorStream = getInput("/remotable-descriptor.xml");
             InputStream schemaStream = getInput("/schema.xsd")) {
            final DescriptorValidator descriptorValidator = new DescriptorValidator(descriptorStream, schemaStream, getApplications());

            final Either<ValidationError, ValidationSuccess> validation = descriptorValidator.validate(InstallationMode.LOCAL);
            assertTrue(validation.isRight());
            assertTrue(validation.right().get().isRemotable());
        }
    }

    @Test
    public void testHasNecessaryPermissionsForDescriptorWithModuleRequiringPermission() throws IOException {
        try (InputStream descriptorStream = getInput("/descriptor-with-module-requiring-exec-java.xml");
             InputStream schemaStream = getInput("/schema.xsd")) {
            final DescriptorValidator descriptorValidator = new DescriptorValidator(descriptorStream, schemaStream, getApplications());

            final Either<ValidationError, ValidationSuccess> validation = descriptorValidator.validate(InstallationMode.LOCAL);
            assertFalse(validation.isRight());
            final ValidationError validationError = validation.left().get();
            assertEquals(1, validationError.getNotAskedPermissions().size());
            assertEquals("execute_java", Iterables.getFirst(validationError.getNotAskedPermissions(), null));
        }
    }

    @Test
    public void testHasNecessaryPermissionsForDescriptorWithModuleRequiringPermissionButAtlassianPlugins2()
            throws IOException {
        try (InputStream descriptorStream = getInput("/descriptor-with-module-requiring-exec-java-but-version-2.xml");
             InputStream schemaStream = getInput("/schema.xsd")) {
            final DescriptorValidator descriptorValidator = new DescriptorValidator(descriptorStream, schemaStream, getApplications());

            final Either<ValidationError, ValidationSuccess> validation = descriptorValidator.validate(InstallationMode.LOCAL);
            assertTrue(validation.isRight());
        }
    }

    @Test
    public void testHasNecessaryPermissionsForDescriptorWithModuleRequiringPermissionAndDescriptorAskingForIt()
            throws IOException {
        try (InputStream descriptorStream = getInput("/descriptor-asking-exec-java.xml");
             InputStream schemaStream = getInput("/schema.xsd")) {
            final DescriptorValidator descriptorValidator = new DescriptorValidator(descriptorStream, schemaStream, getApplications());

            final Either<ValidationError, ValidationSuccess> validation = descriptorValidator.validate(InstallationMode.LOCAL);
            assertTrue(validation.isRight());
        }
    }

    @Test
    public void testHasNecessaryPermissionsForDescriptorWithModuleRequiringPermissionAndDescriptorAskingForItInLocalMode()
            throws IOException {
        try (InputStream descriptorStream = getInput("/descriptor-asking-exec-java-when-installed-locally.xml");
             InputStream schemaStream = getInput("/schema.xsd")) {
            final DescriptorValidator descriptorValidator = new DescriptorValidator(descriptorStream, schemaStream, getApplications());

            final Either<ValidationError, ValidationSuccess> validation = descriptorValidator.validate(InstallationMode.LOCAL);
            assertTrue(validation.isRight());
        }
    }

    @Test
    public void testHasNecessaryPermissionsForDescriptorWithModuleRequiringPermissionAndDescriptorAskingForItInRemoteMode()
            throws IOException {
        try (InputStream descriptorStream = getInput("/descriptor-asking-exec-java-when-installed-locally.xml");
             InputStream schemaStream = getInput("/schema.xsd")) {
            final DescriptorValidator descriptorValidator = new DescriptorValidator(descriptorStream, schemaStream, getApplications());

            final Either<ValidationError, ValidationSuccess> validation = descriptorValidator.validate(InstallationMode.REMOTE);
            assertTrue(validation.isRight());
        }
    }

    @Test
    public void testHasNecessaryPermissionsForDescriptorWithModuleRequiringPermissionAndDescriptorAskingForItInLocalModeButRequireInRemoteTestingLocal()
            throws IOException {
        try (InputStream descriptorStream = getInput("/descriptor-asking-exec-java-locally-but-require-remotely.xml");
             InputStream schemaStream = getInput("/schema.xsd")) {
            final DescriptorValidator descriptorValidator = new DescriptorValidator(descriptorStream, schemaStream, getApplications());

            final Either<ValidationError, ValidationSuccess> validation = descriptorValidator.validate(InstallationMode.LOCAL);
            assertTrue(validation.isRight());
        }
    }

    @Test
    public void testHasNecessaryPermissionsForDescriptorWithModuleRequiringPermissionAndDescriptorAskingForItInLocalModeButRequireInRemoteTestingRemote()
            throws IOException {
        try (InputStream descriptorStream = getInput("/descriptor-asking-exec-java-locally-but-require-remotely.xml");
             InputStream schemaStream = getInput("/schema.xsd")) {
            final DescriptorValidator descriptorValidator = new DescriptorValidator(descriptorStream, schemaStream, getApplications());

            final Either<ValidationError, ValidationSuccess> validation = descriptorValidator.validate(InstallationMode.REMOTE);
            assertTrue(validation.isLeft());
            final ValidationError validationError = validation.left().get();
            assertEquals(1, validationError.getNotAskedPermissions().size());
            assertEquals("execute_java", Iterables.getFirst(validationError.getNotAskedPermissions(), null));
        }
    }

    @Test
    public void testHasNecessaryPermissionsForDescriptorRequiringNonValidPermission() throws IOException {
        try (InputStream descriptorStream = getInput("/descriptor-requiring-non-valid-permission.xml");
             InputStream schemaStream = getInput("/schema.xsd")) {
            final DescriptorValidator descriptorValidator = new DescriptorValidator(descriptorStream, schemaStream, getApplications());

            final Either<ValidationError, ValidationSuccess> validation = descriptorValidator.validate(InstallationMode.LOCAL);
            assertFalse(validation.isRight());
            final ValidationError validationError = validation.left().get();
            assertEquals(1, validationError.getNonValidPermissions().size());
            assertEquals("funky_permission", Iterables.getFirst(validationError.getNonValidPermissions(), null));
        }
    }

    @Test
    public void testHasNecessaryPermissionsForDescriptorRequiringNonValidPermissionForOtherInstallationMode()
            throws IOException {
        try (InputStream descriptorStream = getInput("/descriptor-requiring-non-valid-permission-local.xml");
             InputStream schemaStream = getInput("/schema.xsd")) {
            final DescriptorValidator descriptorValidator = new DescriptorValidator(descriptorStream, schemaStream, getApplications());

            final Either<ValidationError, ValidationSuccess> validation = descriptorValidator.validate(InstallationMode.REMOTE);
            assertTrue(validation.isRight());
        }
    }

    @Test
    public void testHasNecessaryPermissionsForDescriptorRequiringNonValidPermissionForSameInstallationMode()
            throws IOException {
        try (InputStream descriptorStream = getInput("/descriptor-requiring-non-valid-permission-local.xml");
             InputStream schemaStream = getInput("/schema.xsd")) {
            final DescriptorValidator descriptorValidator = new DescriptorValidator(descriptorStream, schemaStream, getApplications());

            final Either<ValidationError, ValidationSuccess> validation = descriptorValidator.validate(InstallationMode.LOCAL);
            assertFalse(validation.isRight());
            final ValidationError validationError = validation.left().get();
            assertEquals(1, validationError.getNonValidPermissions().size());
            assertEquals("funky_permission", Iterables.getFirst(validationError.getNonValidPermissions(), null));
        }
    }

    private ImmutableSet<Application> getApplications() {
        return ImmutableSet.of(new Application() {
            @Override
            public String getKey() {
                return "test-app";
            }

            @Override
            public String getVersion() {
                return "1.0";
            }

            @Override
            public String getBuildNumber() {
                return "1";
            }
        });
    }
}
