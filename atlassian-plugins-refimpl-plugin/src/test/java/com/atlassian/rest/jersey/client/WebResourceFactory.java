package com.atlassian.rest.jersey.client;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;

import javax.ws.rs.core.UriBuilder;
import java.net.URI;

/**
 * Gets {@link WebResource web resources} for testing.
 */
public final class WebResourceFactory {
    private static final String PORT = System.getProperty("http.port", "5990");
    private static final String CONTEXT = System.getProperty("context.path", "/refapp");

    public static WebResource anonymous(URI uri) {
        return createClient().resource(uri);
    }

    public static UriBuilder getUriBuilder() {
        return UriBuilder.fromUri("http://localhost/").port(Integer.parseInt(PORT)).path(CONTEXT);
    }

    private static Client createClient() {
        return Client.create();
    }
}
