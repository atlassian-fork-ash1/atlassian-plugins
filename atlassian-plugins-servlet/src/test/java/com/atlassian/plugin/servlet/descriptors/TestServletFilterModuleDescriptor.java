package com.atlassian.plugin.servlet.descriptors;

import com.atlassian.plugin.Permissions;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.impl.StaticPlugin;
import com.atlassian.plugin.module.PrefixDelegatingModuleFactory;
import com.atlassian.plugin.servlet.ServletModuleManager;
import com.atlassian.plugin.servlet.filter.FilterLocation;
import com.atlassian.plugin.servlet.filter.FilterTestUtils.FilterAdapter;
import org.dom4j.Element;
import org.dom4j.dom.DOMElement;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.RestoreSystemProperties;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import javax.servlet.DispatcherType;
import java.util.Collections;

import static com.atlassian.plugin.Permissions.addPermission;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;

public class TestServletFilterModuleDescriptor {
    public static final String PLUGIN_KEY = "somekey";
    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();
    @Rule
    public final ExpectedException expectedException = ExpectedException.none();
    @Rule
    public final RestoreSystemProperties restoreSystemProperties = new RestoreSystemProperties();

    @Mock
    private ServletModuleManager servletModuleManager;

    ServletFilterModuleDescriptor descriptor;

    @Before
    public void setUp() {
        ServletFilterModuleDescriptor.FORCE_ASYNC.reset();
        descriptor = new ServletFilterModuleDescriptor
                (new PrefixDelegatingModuleFactory(Collections.emptySet()), servletModuleManager);
    }
    @After
    public void resetForceAsyncFlag() {
        ServletFilterModuleDescriptor.FORCE_ASYNC.reset();
    }

    @Test
    public void testInit() {
        final Plugin plugin = newPlugin(PLUGIN_KEY);
        Element e = getValidConfig();
        descriptor.init(plugin, e);
        assertThat(descriptor.getLocation(), is(FilterLocation.BEFORE_DISPATCH));
        assertThat(descriptor.getWeight(), is(100));
    }

    private Plugin newPlugin(String key) {
        Plugin plugin = new StaticPlugin();
        plugin.setKey(key);
        return addPermission(plugin, Permissions.EXECUTE_JAVA, null);
    }

    private Element getValidConfig() {
        Element e = new DOMElement("servlet-filter");
        e.addAttribute("key", "key2");
        e.addAttribute("class", FilterAdapter.class.getName());
        Element url = new DOMElement("url-pattern");
        url.setText("/foo");
        e.add(url);
        Element dispatcher1 = new DOMElement("dispatcher");
        dispatcher1.setText("REQUEST");
        e.add(dispatcher1);
        Element dispatcher2 = new DOMElement("dispatcher");
        dispatcher2.setText("FORWARD");
        e.add(dispatcher2);
        return e;
    }

    @Test
    public void testInitWithNoUrlPattern() {
        Plugin plugin = newPlugin(PLUGIN_KEY);
        Element e = new DOMElement("servlet-filter");
        e.addAttribute("key", "key2");
        e.addAttribute("class", FilterAdapter.class.getName());
        expectedException.expect(PluginParseException.class);
        descriptor.init(plugin, e);
    }

    @Test
    public void testInitWithDetails() {
        Plugin plugin = newPlugin(PLUGIN_KEY);
        Element e = getValidConfig();
        e.addAttribute("location", "after-encoding");
        e.addAttribute("weight", "122");
        descriptor.init(plugin, e);
        assertThat(descriptor.getLocation(), is(FilterLocation.AFTER_ENCODING));
        assertThat(descriptor.getWeight(), is(122));
        assertThat(descriptor.getDispatcherTypes(), containsInAnyOrder(DispatcherType.REQUEST, DispatcherType.FORWARD));
    }

    @Test
    public void testInitWithBadLocation() {
        Plugin plugin = newPlugin(PLUGIN_KEY);
        Element e = getValidConfig();
        e.addAttribute("location", "t23op");
        expectedException.expect(PluginParseException.class);
        descriptor.init(plugin, e);
    }

    @Test
    public void testInitWithBadWeight() {
        Plugin plugin = newPlugin(PLUGIN_KEY);
        Element e = getValidConfig();
        e.addAttribute("weight", "t23op");
        expectedException.expect(PluginParseException.class);
        descriptor.init(plugin, e);
    }

    @Test
    public void testInitWithBadDispatcher() {
        Plugin plugin = newPlugin(PLUGIN_KEY);
        Element e = getValidConfig();
        Element badDispatcher = new DOMElement("dispatcher");
        badDispatcher.setText("badValue");
        e.add(badDispatcher);
        expectedException.expect(PluginParseException.class);
        descriptor.init(plugin, e);
    }

    @Test
    public void testWithNoDispatcher() {
        Plugin plugin = newPlugin(PLUGIN_KEY);

        Element e = new DOMElement("servlet-filter");
        e.addAttribute("key", "key2");
        e.addAttribute("class", FilterAdapter.class.getName());
        Element url = new DOMElement("url-pattern");
        url.setText("/foo");
        e.add(url);

        descriptor.init(plugin, e);
        assertThat(descriptor.getDispatcherTypes(), containsInAnyOrder(DispatcherType.REQUEST));
    }

    @Test
    public void testWithNoDispatcherForcedAsync() {
        System.setProperty(ServletFilterModuleDescriptor.FORCE_ASYNC_DISPATCHER_SYSPROP, "true");
        Plugin plugin = newPlugin(PLUGIN_KEY);

        Element e = new DOMElement("servlet-filter");
        e.addAttribute("key", "key2");
        e.addAttribute("class", FilterAdapter.class.getName());
        Element url = new DOMElement("url-pattern");
        url.setText("/foo");
        e.add(url);

        descriptor.init(plugin, e);
        assertThat(descriptor.getDispatcherTypes(), containsInAnyOrder(DispatcherType.REQUEST, DispatcherType.ASYNC));
    }

    @Test
    public void testInitWithDetailsForceAsync() {
        System.setProperty(ServletFilterModuleDescriptor.FORCE_ASYNC_DISPATCHER_SYSPROP, "true");
        Plugin plugin = newPlugin(PLUGIN_KEY);
        Element e = getValidConfig();
        e.addAttribute("location", "after-encoding");
        e.addAttribute("weight", "122");
        descriptor.init(plugin, e);
        assertThat(descriptor.getLocation(), is(FilterLocation.AFTER_ENCODING));
        assertThat(descriptor.getWeight(), is(122));
        assertThat(descriptor.getDispatcherTypes(), containsInAnyOrder(DispatcherType.REQUEST, DispatcherType.FORWARD, DispatcherType.ASYNC));
    }
}
