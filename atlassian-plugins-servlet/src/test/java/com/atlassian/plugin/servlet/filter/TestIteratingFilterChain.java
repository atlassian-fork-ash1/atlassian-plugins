package com.atlassian.plugin.servlet.filter;

import com.atlassian.plugin.servlet.filter.FilterTestUtils.FilterAdapter;
import com.atlassian.plugin.servlet.filter.FilterTestUtils.SoundOffFilter;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static com.atlassian.plugin.servlet.filter.FilterTestUtils.singletonFilterChain;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestIteratingFilterChain {
    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testFiltersCalledInProperOrder() throws IOException, ServletException {
        List<Integer> filterCallOrder = new LinkedList<>();
        List<Filter> filters = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            filters.add(new SoundOffFilter(filterCallOrder, i));
        }

        FilterChain chain = new IteratingFilterChain(filters.iterator(), singletonFilterChain(new SoundOffFilter(filterCallOrder, 100)));

        HttpServletRequest mockRequest = mock(HttpServletRequest.class);
        when(mockRequest.getPathInfo()).thenReturn("some/path");
        HttpServletResponse mockResponse = mock(HttpServletResponse.class);

        chain.doFilter(mockRequest, mockResponse);

        // make sure that all filters were called and unrolled in the proper order
        assertThat(filterCallOrder, contains(0, 1, 2, 3, 4, 100, 100, 4, 3, 2, 1, 0));
    }

    @Test
    public void testFilterCanAbortChain() throws IOException, ServletException {
        final List<Integer> filterCallOrder = new LinkedList<>();
        List<Filter> filters = new ArrayList<>();
        for (int i = 0; i < 2; i++) {
            filters.add(new SoundOffFilter(filterCallOrder, i));
        }
        filters.add(new FilterAdapter() {
            @Override
            public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) {
                filterCallOrder.add(50);
            }
        });
        for (int i = 3; i < 5; i++) {
            filters.add(new SoundOffFilter(filterCallOrder, i));
        }

        FilterChain chain = new IteratingFilterChain(filters.iterator(), singletonFilterChain(new SoundOffFilter(filterCallOrder, 100)));

        HttpServletRequest mockRequest = mock(HttpServletRequest.class);
        when(mockRequest.getPathInfo()).thenReturn("some/path");
        HttpServletResponse mockResponse = mock(HttpServletResponse.class);

        chain.doFilter(mockRequest, mockResponse);

        // make sure that all filters were called and unrolled in the proper order
        assertThat(filterCallOrder, contains(0, 1, 50, 1, 0));
    }

    @Test
    public void testExceptionFiltersUpWhenFilterThrowsException() throws IOException {
        final List<Integer> filterCallOrder = new LinkedList<>();
        List<Filter> filters = new ArrayList<>();
        for (int i = 0; i < 2; i++) {
            filters.add(new SoundOffFilter(filterCallOrder, i));
        }
        filters.add(new FilterAdapter() {
            @Override
            public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
                    throws ServletException {
                throw new ServletException();
            }
        });
        for (int i = 3; i < 5; i++) {
            filters.add(new SoundOffFilter(filterCallOrder, i));
        }

        FilterChain chain = new IteratingFilterChain(filters.iterator(), singletonFilterChain(new SoundOffFilter(filterCallOrder, 100)));

        HttpServletRequest mockRequest = mock(HttpServletRequest.class);
        when(mockRequest.getPathInfo()).thenReturn("some/path");
        HttpServletResponse mockResponse = mock(HttpServletResponse.class);

        try {
            chain.doFilter(mockRequest, mockResponse);
            fail("ServletException should filter up");
        } catch (ServletException e) {
            // yay! make sure the filter call order is as we expect
            assertThat(filterCallOrder, contains(0, 1));
        }
    }
}
