package com.atlassian.plugin.servlet.descriptors;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.StateAware;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.servlet.ServletModuleManager;
import com.atlassian.plugin.servlet.filter.FilterDispatcherCondition;
import com.atlassian.plugin.servlet.filter.FilterLocation;
import com.atlassian.plugin.util.validation.ValidationPattern;
import com.google.common.annotations.VisibleForTesting;
import io.atlassian.util.concurrent.ResettableLazyReference;
import org.dom4j.Element;

import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import java.util.Arrays;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static com.atlassian.plugin.util.validation.ValidationPattern.test;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.stream.Collectors.toSet;

/**
 * A module descriptor that allows plugin developers to define servlet filters. Developers can define what urls the
 * filter should be applied to by defining one or more &lt;url-pattern&gt; elements and they can decide where in the
 * filter stack a plugin filter should go by defining the "location" and "weight" attributes.
 * <p>
 * The location attribute can have one of four values:
 *
 * <ul>
 * <li>after-encoding - after the character encoding filter</li>
 * <li>before-login - before the login filter</li>
 * <li>before-decoration - before any global decoration like sitemesh</li>
 * <li>before-dispatch - before any dispatching filters or servlets</li>
 * </ul>
 * The default for the location attribute is "before-dispatch".
 * <p>
 * The weight attribute can have any integer value. Filters with lower values of the weight attribute will come before
 * those with higher values within the same location.
 *
 * @since 2.1.0
 */
public class ServletFilterModuleDescriptor extends BaseServletModuleDescriptor<Filter> implements StateAware {
    /**
     * Set to make all filters apply to the async dispatcher chain.
     * @since 4.6.0
     */
    @VisibleForTesting
    static final String FORCE_ASYNC_DISPATCHER_SYSPROP = "atlassian.plugins.filter.force.async.dispatcher";
    @VisibleForTesting
    static final ResettableLazyReference<Boolean> FORCE_ASYNC = new ResettableLazyReference<Boolean>() {
        @Override
        protected Boolean create() {
            return Boolean.getBoolean(FORCE_ASYNC_DISPATCHER_SYSPROP);
        }
    };

    static final String DEFAULT_LOCATION = FilterLocation.BEFORE_DISPATCH.name();
    static final String DEFAULT_WEIGHT = "100";

    private FilterLocation location;

    private int weight;
    private final ServletModuleManager servletModuleManager;

    private Set<DispatcherType> dispatcherTypes = EnumSet.of(DispatcherType.REQUEST);

    /**
     * Creates a descriptor that uses a module class factory to create instances.
     *
     * @param moduleFactory        The module factory
     * @param servletModuleManager The module manager
     * @since 2.5.0
     */
    public ServletFilterModuleDescriptor(ModuleFactory moduleFactory, ServletModuleManager servletModuleManager) {
        super(moduleFactory);
        this.servletModuleManager = checkNotNull(servletModuleManager);
    }

    public static final Comparator<ServletFilterModuleDescriptor> byWeight =
            Comparator.comparingInt(ServletFilterModuleDescriptor::getWeight);

    @SuppressWarnings("unchecked")
    public void init(Plugin plugin, Element element) throws PluginParseException {
        super.init(plugin, element);
        try {
            location = FilterLocation.parse(element.attributeValue("location", DEFAULT_LOCATION));
            weight = Integer.valueOf(element.attributeValue("weight", DEFAULT_WEIGHT));
        } catch (IllegalArgumentException ex) {
            throw new PluginParseException(ex);
        }

        List<Element> dispatcherElements = element.elements("dispatcher");
        if (!dispatcherElements.isEmpty()) {
            dispatcherTypes.clear();
            for (Element dispatcher : dispatcherElements) {
                // already been validated via the validation rules
                dispatcherTypes.add(DispatcherType.valueOf(dispatcher.getTextTrim()));
            }
        }
        if (FORCE_ASYNC.get()) {
            dispatcherTypes.add(DispatcherType.ASYNC);
        }
    }

    @Override
    protected void provideValidationRules(ValidationPattern pattern) {
        super.provideValidationRules(pattern);
        StringBuilder conditionRule = new StringBuilder();
        conditionRule.append("dispatcher[");
        DispatcherType[] dispatcherTypes = DispatcherType.values();
        for (int x = 0; x < dispatcherTypes.length; x++) {
            conditionRule.append(". != '").append(dispatcherTypes[x]).append("'");
            if (x + 1 < dispatcherTypes.length) {
                conditionRule.append(" and ");
            }
        }
        conditionRule.append("]");
        pattern.rule(conditionRule.toString(),
                test("dispatcher").withError("The dispatcher value must be one of the following only " + Arrays.asList(DispatcherType.values())),
                test("@class").withError("The class is required"));
    }

    public void enabled() {
        super.enabled();
        servletModuleManager.addFilterModule(this);
    }

    public void disabled() {
        servletModuleManager.removeFilterModule(this);
        super.disabled();
    }

    @Override
    public Filter getModule() {
        return moduleFactory.createModule(moduleClassName, this);
    }

    public FilterLocation getLocation() {
        return location;
    }

    public int getWeight() {
        return weight;
    }

    /**
     * Returns a set of dispatcher conditions that have been set for this filter, these conditions
     * will be one of the following: <code>REQUEST, FORWARD, INCLUDE or ERROR</code>.
     *
     * @return A set of dispatcher conditions that have been set for this filter.
     * @since 2.5.0
     * @deprecated since 4.6.0. Use {@link #getDispatcherTypes()} instead.
     */
    public Set<FilterDispatcherCondition> getDispatcherConditions() {
        return dispatcherTypes.stream()
                .map(FilterDispatcherCondition::fromDispatcherType)
                .filter(Objects::nonNull)
                .collect(toSet());
    }

    /**
     * Returns a set of dispatcher types that have been set for this filter. These can be any of the values supported by
     * {@link DispatcherType}.
     *
     * @return A set of dispatcher types that have been set for this filter.
     * @since 4.6.0
     */
    public Set<DispatcherType> getDispatcherTypes() {
        return dispatcherTypes;
    }
}
